import "../model/vendingmachine.ecore"
/**
 * This is a specification for a vending machine. Customers can choose between two beverages (1 and 2)
 * and pay with coins for it. The coins can have values of 1,2,5,10,20,50,100 and 200 cent.
 * We model the user wallet, because future versions of the machine should be able to abort the payment process
 * and to return change. 
 */
specification VendingMachineSpecification {
	
	domain vendingmachine

	controllable {
		VendingMachine
	}
	/**
	 * We assume that users do not enter coins until the vending machine displays the price.
	 */
	non-spontaneous events {
		VendingMachine.enterCoin
	}

	parameter ranges {
		VendingMachine.enterCoin(eIntParameter1 = [ 1, 2, 5, 10, 20, 50, 100, 200 ]), VendingMachine.selectBeverage(beverage = [ 1, 2 ])
	}
	/**
	 * This collaboration between customer and vending machine describes 
	 * how users can interact with the machine in order to obtain beverages.
	 */

	collaboration VendingMachineCollaboration {

		static role Customer customer
		static role VendingMachine machine
/**
		 * When the user selects a beverage, the machine must display it's price.
		 * When the user has entered enough money, the machine must output the
		 * selected beverage.
		 */
		singular guarantee scenario MyScenario {
			var EInt b
			customer -> machine.selectBeverage(bind b)
			var EInt price = 200
			strict urgent machine -> customer.displayPrice(price)
			// let's assume that the price is fixed for now...
			var EInt remaining = price
			var EInt lastCoin = 0
			while [ remaining > 0 ] {
				strict customer -> machine.enterCoin(bind lastCoin)
				remaining = remaining - lastCoin
			} constraints [
				ignore customer -> machine.selectBeverage(*)
			]
			strict urgent machine -> machine.outputBeverage(b)
		}
	/**
		 * We assume that the customer cannot conjure up coins from thin air.
		 */
		assumption scenario CustomerPaysFromChange {
			var EInt toPay
			machine -> customer.displayPrice(bind toPay)
			while [ ! customer.availableChange.isEmpty() && toPay > 0 ] {
				{
					strict eventually customer -> machine.enterCoin(customer.availableChange.first())
				} constraints [
					forbidden customer -> machine.enterCoin(*)
				]
				toPay = toPay - customer.availableChange.first()
				strict eventually customer -> customer.availableChange.remove(customer.availableChange.first())
			}
		}
// The following scenarios describe test cases.
//		existential scenario machineDisplaysPriceAfterSelection {
//			var Beverage b
//			customer -> machine.selectBeverage(bind b)
//			machine -> customer.displayPrice(*)
//		}
//
//		existential scenario CustomerPaysWithTwoEuro {
//			customer -> machine.enterCoin(200)
//			machine -> machine.outputBeverage(*)
//		}
//
//		existential scenario CustomerPaysWithOneEuro {
//			customer -> machine.enterCoin(100)
//			customer -> machine.enterCoin(100)
//			machine -> machine.outputBeverage(*)
//		}

	}

}