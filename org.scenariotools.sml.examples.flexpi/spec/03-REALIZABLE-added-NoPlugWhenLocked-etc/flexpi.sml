import "../../model/flexpi.ecore"

specification FlexPi {
	domain flexpi
	
	controllable {
		Controller
	}
	
	collaboration PlugAndStart {
		
		static role Socket socket
		static role Relays relays
		static role StartButton startButton
		static role Controller controller


		/*
		 * When the plug is plugged into the socket, then the socket must lock the plug.
		 */
		guarantee scenario WhenPluggedThenLock{
			socket->controller.plugIn()
			//strict requested controller->socket.lock()
			strict requested controller->socket.setLocked(true)  // <-- (1) we need to set the state
		}
		
		
		// When locked, unplug cannot happen <-- (2) so that we can formulate this assumption 
		assumption scenario NoUnplugWhenLocked{
			socket->controller.unplug()
			violation [socket.locked]
		}   

		/*
		 * If the start-button is pressed, then the relays 
		 * have to close.
		 */
		guarantee scenario WhenStartPressedThenCloseContact{
			startButton->controller.startPressed()
			interrupt [!socket.locked] // <-- (3) and so that we can formulate condition
			strict requested controller->relays.setClosed(true)
		}
		
		
		/*
		 * When the relays are closed, the plug must not be unplugged.
		 */
		guarantee scenario WhenRelayClosedUnplugForbbidden{
			socket->controller.unplug()
			violation [relays.closed]
		}
		
		/*
		 * If the plug is not plugged or not locked then the relays must not make contact.
		 */
		// TODO
		
		
	}
	
}