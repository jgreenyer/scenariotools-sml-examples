import "../../model/flexpi.ecore"

specification FlexPi {
	domain flexpi
	
	controllable {
		Controller
	}
	
	collaboration PlugAndStart {
		
		static role Socket socket
		static role Relays relays
		static role StartButton startButton
		static role Controller controller


		/*
		 * When the plug is plugged into the socket, then the socket must lock the plug.
		 */
		guarantee scenario WhenPluggedThenLock{
			socket->controller.plugIn()
			strict requested controller->socket.lock()
		}

		/*
		 * If the start-button is pressed, then the relays 
		 * have to close.
		 */
		guarantee scenario WhenStartPressedThenCloseContact{
			startButton->controller.startPressed()
			//strict requested controller->relays.close()
			strict requested controller->relays.setClosed(true) // <-- we need to set the state
		}
		
		
		/*
		 * When the relays are closed, the plug must not be unplugged.
		 */
		guarantee scenario WhenRelayClosedUnplugForbbidden{
			socket->controller.unplug()
			violation [relays.closed] // <-- we refer to the relays' state.
		}
		
		/*
		 * If the plug is not plugged or not locked then the relays must not make contact.
		 */
		// TODO
		
		
	}
	
}