import "../../model/flexpi.ecore"

specification FlexPi {
	domain flexpi
	
	controllable {
		Controller
	}
	
	non-spontaneous events {
		Controller.plugIn
		Controller.unplug
	}
	
	collaboration PlugAndStart {
		
		static role Socket socket
		static role Relays relays
		static role StartButton startButton
		static role User user
		static role Controller controller


		/*
		 * When the plug is plugged into the socket, then the socket must lock the plug.
		 */
		guarantee scenario WhenPluggedThenLock{
			socket->controller.plugIn()
			strict urgent controller->socket.setLocked(true)
		}
		
		
		// When locked, unplug cannot happen 
		assumption scenario NoUnplugWhenLocked{
			socket->controller.unplug()
			violation [socket.locked]
		}   

		/*
		 * If the start-button is pressed, then the relays 
		 * have to close.
		 */
		guarantee scenario WhenStartPressedThenCloseContact{
			startButton->controller.startPressed()
			interrupt [!socket.locked]
			strict urgent controller->relays.setClosed(true)
		}
		
		
		/*
		 * When the relays are closed, the plug must not be unplugged.
		 */
		guarantee scenario WhenRelayClosedUnplugForbbidden{
			socket->controller.unplug()
			violation [relays.closed]
		}
		
	
		/*
		 * If the plug is not plugged or not locked then the relays must not make contact.
		 */
		guarantee scenario WhenNotPluggedAndLockedThenCloseRelayForbidden{
			controller->relays.close()
			violation [!(socket.plugged && socket.locked)] // <-- (1) we refer to the sockets' plugged state.
		}
		

		// --> (2) we assume that setting the plugged state will be caused by the user:
		// we need to remember the plugged state of the socket
		assumption scenario PluggingIn{
			user->socket.setPlugged(true)
			strict eventually socket->controller.plugIn()
		}constraints[
			forbidden socket->controller.unplug()
		]
		// symmetrically for unplugging
		assumption scenario UnPlugging{
			user->socket.setPlugged(false)
			strict eventually socket->controller.unplug()
		}constraints[
			forbidden socket->controller.plugIn()
		]

		
	}
	
}