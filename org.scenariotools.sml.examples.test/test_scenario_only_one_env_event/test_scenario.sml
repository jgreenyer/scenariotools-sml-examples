import "../model.ecore"

specification test_scenario_only_one_env_event {
	domain model
	
	controllable {
		A
		B
	}
	
	collaboration test_scenario{
		
		static role A a 
		static role B b 
		static role Environment env
		
		guarantee scenario requirementScenario1 {
			env -> a.opA1()
		}
	}
}