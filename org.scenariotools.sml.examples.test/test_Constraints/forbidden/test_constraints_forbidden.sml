import "../../model.ecore"

specification test_constraints_forbidden {
	domain model
	
	controllable {
		A
		B
	}
	
	collaboration test_constraints_forbidden{
		
		static role A a 
		static role B b 
		static role Environment env
		
		guarantee scenario requirementScenario1 {
			env -> a.opA1()
			strict urgent a -> b.opB1()			 
		}constraints[forbidden b -> a.opA2()]
		
		guarantee scenario requirementScenario2 {
			env -> a.opA1()
			strict urgent b -> a.opA2()
		}
	}
}