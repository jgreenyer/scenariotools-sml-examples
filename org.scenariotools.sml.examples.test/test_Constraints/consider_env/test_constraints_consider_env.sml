import "../../model.ecore"

specification test_constraints_consider_env {
	domain model
	
	controllable {
		A
		B
	}
	
	collaboration test_constraints_consider_env{
		
		static role A a 
		static role B b 
		static role Environment env
		
		guarantee scenario requirementScenario1 {
			env -> a.opA1()
			strict env -> b.opB1()	
			strict urgent a -> b.opB3()		 
		}constraints[consider env -> a.opA2()]
		
		assumption scenario requirementScenario2 {
			env -> a.opA1()
			strict eventually env -> a.opA2()
		}
	}
}