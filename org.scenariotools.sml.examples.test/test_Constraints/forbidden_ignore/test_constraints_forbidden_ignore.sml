import "../../model.ecore"

specification test_constraints_forbidden_ignore {
	domain model
	
	controllable {
		A
		B
	}
	
	collaboration test_constraints_forbidden_ignore{
		
		static role A a 
		static role B b 
		static role Environment env
		
		guarantee scenario requirementScenario1 {
			env -> a.opA1()
			strict urgent a -> b.opB1()	
			{
				strict urgent b -> a.opA1()
				strict urgent b -> a.opA2()
			}constraints[ignore b -> a.opA2()]		 
		}constraints[forbidden b -> a.opA2()]
		
		guarantee scenario requrirementScenario2 {
			env -> a.opA1()
			a -> b.opB1()
			strict urgent b -> a.opA2()
		}
		
		guarantee scenario requirementScenario3 {
			env -> a.opA1()
			strict urgent b -> a.opA2()
		}
	}
}