import "../../model.ecore"

specification test_FirstMessageWithParameter {
	
	domain model
	controllable {
		A
		B
	}
	
	collaboration test_FirstMessageWithParameter {
		
		static role  A a
		dynamic role B b
		static role B lastB
		static role Environment env
		
		guarantee scenario  StartScenario 
			bindings [ b = a.b]
		{
			env -> a.opA1()
			strict urgent a -> b.opBPInt(22)
		}
		
		guarantee scenario InitParameterScenario {
			env -> b.opBPInt(*)
//			env -> b.opBPInt(22)
			strict urgent b -> a.opA1()
			strict urgent b -> a.opA2()
			strict eventually env -> b.opBPInt(44)
		}
		
		guarantee scenario OneMessage {
			b -> a.opA1()
		}
		
		guarantee scenario EnvStartPatameter {
//			var EInt i = 5
//			i = i + b.intValue
			//env -> b.opBPInt(i)
			env -> b.opBPInt(b.intValue)
			strict eventually env -> b.opB1()
		}
		
	}
	
	
	
}