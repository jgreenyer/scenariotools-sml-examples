import "../model.ecore"

specification ParameterTestWithInstanceModel {
	
	domain model
	controllable {
		A
		B
	}
	
	collaboration Cyclic {
		
		dynamic role A a
		dynamic role B b
		dynamic role B lastB
		dynamic role Environment env
		
		guarantee scenario  UpdateB bindings [b = a.b] {
			env -> a.opA1()
			strict urgent a -> a.setB(b.next)
		}
	}
	
	
	
}