import "../model.ecore"

specification test_scenario {
	domain model
	
	controllable {
		A
		B
	}

	non-spontaneous events {
		A.opA2		
	}
	
	collaboration test_scenario{
		
		static role A a 
		static role B b 
		static role Environment env
		
		guarantee scenario specificationScenario1 {
			env -> a.opA1()
			strict urgent a -> b.opB1()
		}
		
		guarantee scenario specificationScenario2 {
			env -> a.opA2()
			strict urgent a -> b.opB2()
		}
		
		assumption scenario assumptionScenario1{
			a -> b.opB1()
			strict eventually env -> a.opA2()
		} constraints [
			forbidden env -> a.opA1()
		]
		
		guarantee scenario requirementScenario1 {
			env -> a.opA1()
			strict monitored eventually a -> b.opB3()
		} constraints [
			ignore env -> a.opA1()
		]

		
	}
}