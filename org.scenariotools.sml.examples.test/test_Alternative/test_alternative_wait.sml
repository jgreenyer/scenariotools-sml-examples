import "../model.ecore"

specification test_alternative_wait {
	domain model
	
	controllable {
		A
		B
	}
	
	collaboration test_alternative_wait{
		
		static role A a 
		static role B b 
		static role Environment env
		
		guarantee scenario requirementScenario1 {
			env -> a.opA1()
			strict urgent a -> b.opB2()
			alternative [b.intValue == 1]{
				strict urgent b -> a.opA1()
				env -> a.opA3()
			}or [b.intValue == 2]{
				strict urgent b -> a.opA2()
			}			 
		}
		
		assumption scenario IncrementB1 {
			env -> b.setIntValue(1)
		}
		assumption scenario IncrementB2 {
			env -> b.setIntValue(2)
		}
	}
}