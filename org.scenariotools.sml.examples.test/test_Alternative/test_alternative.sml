import "../model.ecore"

specification test_alternative {
	domain model
	
	controllable {
		A
		B
	}
	
	collaboration test_alternative{
		
		static role A a 
		static role B b 
		static role Environment env
		
		guarantee scenario requirementScenario1 {
			env -> a.opA1()
			strict urgent a -> b.opB1()
			alternative {
				strict urgent b -> a.opA1()
			}or{
				strict urgent b -> a.opA2()
			}			 
		}
	}
}