import "../model.ecore"

specification test_alternative_multiple_cases_active {
	domain model
	
	controllable {
		A
		B
	}
	
	collaboration test_alternative_multiple_cases_active{
		
		static role A a 
		static role B b 
		static role Environment env
		
		guarantee scenario requirementScenario1 {
			env -> a.opA1()
			var EInt i = 0
			strict urgent a -> b.opB2()
			alternative {
				strict urgent b -> a.opA1()
//				i = 10
				env -> a.opA3()
			}or {
				strict urgent b -> a.opA1()
				i = 55
				env -> a.opA4()
			}			 
		}
	}
}