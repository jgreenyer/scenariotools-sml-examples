import "../model.ecore"
import "test_scenario_with_included_collaboration.collaboration"

specification test_scenario_with_auxiliary_collaboration {

	domain model
	controllable {
		A
		B
	}

	collaboration TheCollaboration{

		static role Environment env
		static role A a
		static role B b

		guarantee scenario TheScenario {
			env -> a.opA1()
			a -> b.opB2()
		}

	}

}