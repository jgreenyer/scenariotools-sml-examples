import "../model.ecore"
import "test_scenario_with_included_collaboration.collaboration"

specification test_scenario_with_included_collaboration {

	domain model
	controllable {
		A
		B
	}

	include collaboration test_scenario

}