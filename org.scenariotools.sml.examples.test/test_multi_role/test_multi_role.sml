import "../model.ecore"

specification test_multi_role {
	domain model
	
	controllable {
		A
		B
	}
	
	collaboration test1{
		
		static role A a 
		static role Environment env
		dynamic multi role B b 
		
		guarantee scenario SpecificationScenario1 
		bindings [
			b = a.setOfB
		]{
			env -> a.opA1()
			strict urgent a -> b.opB1()
		}

	}

}