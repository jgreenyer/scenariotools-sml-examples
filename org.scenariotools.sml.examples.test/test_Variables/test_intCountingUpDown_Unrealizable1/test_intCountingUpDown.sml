import "../../model.ecore"

specification test_intCountingUpDown_Unrealizable1 {
	domain model
	
	controllable {
		A
		B
	}
	
	collaboration test_variables{
		
		static role A a 
		dynamic role B b 
		static role Environment env
		
		guarantee scenario requirementScenario1 bindings [ b = a.b] {
			env -> a.opA1()
			var EInt c = 0
			while [c >= 0]{
				strict urgent a -> b.opB1()
				alternative{
					env -> b.opB1()
					c = c + 1
				}or{
					env -> a.opA2()					
					c = c - 1
				}
				// should eventually lead to a violation after opA1, opB1, ... 
				violation [c >= 3]
			}
		}
	}
}