import "../../model.ecore"

specification test_intCountingUpDown_Realizable1 {
	domain model
	
	controllable {
		A
		B
	}
	
	collaboration test_variables{
		
		static role A a 
		dynamic role B b 
		static role Environment env
		
		guarantee scenario requirementScenario1 bindings [ b = a.b] {
			env -> a.opA1()
			var EInt c = 0
			// should not enter loop and exit active scenario right away
			while [c >= 1]{
				strict urgent a -> b.opB1()
				strict urgent b -> a.opA1()
			}
		}
		
		guarantee scenario specScenario2 bindings [ b = a.b] {
			env -> a.opA1()
			strict urgent b -> a.opA1()
			strict urgent a -> b.opB1()
		}
		
		
	}
}