import "../../model.ecore"

specification test_variableBindTo {
	domain model
	
	controllable {
		A
		B
	}
	
	collaboration test_variableBindTo {
		
		static role A a 
		static role B b 
		static role Environment env
		
		guarantee scenario scenario1 {
			env -> a.opA1()
			var EBoolean x = false
			strict a -> b.opBP1(bind x)
			strict urgent a -> b.opBP1(x)	
		}
		
		guarantee scenario scenario2 {
			env -> a.opA1()
			strict urgent a -> b.opBP1(true)
			strict urgent a -> b.opBP1(true)
		}
	}
}