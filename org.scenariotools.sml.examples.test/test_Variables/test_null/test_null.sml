import "../../model.ecore"

specification test_null {
	domain model
	
	controllable {
		A
		B
	}
	
	collaboration test_null{
		
		static role A a 
		dynamic role B b 
		static role Environment env
		
		guarantee scenario requirementScenario1 bindings [b = a.b] {
			
			env -> a.opA1()
			var EString x = "test"
			var EString y = x
			var EString z = a.name
			urgent a -> env.opEnv1()
			interrupt [null == b.next]
			interrupt [b.next == null]	
			z = "bla bla"
			urgent a -> env.opEnv1()	
		}
	}
}