import "../../model.ecore"

specification WaitSpec {

	domain model

	controllable {
		A
		B
	}

	collaboration WaitingB {

		static role Environment env
		static role A a
		static role B rightB

		guarantee scenario UpdatingB {
			env -> a.opA1()
			var B b = a.b
			var B n = b.next
			strict urgent rightB -> a.setB(n)
			interrupt [ a.b != rightB ]
			env -> rightB.opB1()
		}

		/* 
		 * the execution progresses to a point where a.b == rightB.
		 * Then, if the environment always only sends env -> rightB.opB1(), 
		 * the scenario WaitingForRightB will be always terminated 
		 * and immediately re-initialized in a state where the urgent
		 * wait condition is enabled. Although this actually means that
		 * the scenarios has progressed, by interrupting it, the state
		 * that will be reached is immediately again one where the scenario
		 * is in a urgent state.
		 * Currently, the synthesis algorithm is not able to see this progress,
		 * and will thus detect an inevitable liveness violation. 
		 */   

		guarantee scenario WaitingForRightB {
			env -> rightB.opB1()
			wait eventually [ a.b != rightB ]
			strict urgent rightB -> a.opA2()
		}
	}

}