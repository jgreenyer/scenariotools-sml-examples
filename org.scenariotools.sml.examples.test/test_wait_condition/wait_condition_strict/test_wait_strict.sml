import "../../model.ecore"

specification WaitSpec {

	domain model

	controllable {
		A
		B
	}

	collaboration WaitingB {

		static role Environment env
		static role A a
		static role B rightB

		guarantee scenario UpdatingB {
			env -> a.opA1()
			var B b = a.b
			var B n = b.next
			strict urgent rightB -> a.setB(n)
			interrupt [ a.b != rightB ]
			env -> rightB.opB1()
		}

		guarantee scenario WaitingForRightB {
			env -> rightB.opB1()
			wait strict [ a.b != rightB ]
			strict urgent rightB -> a.opA2()
		}

	}

}