import "../model.ecore"

specification test_loop_withConditionAndCounter {
	domain model

	controllable {
		A
		B
	}	
	
	collaboration test_loop_withConditionAndCounter{
		
		static role A a 
		static role B b 
		static role Environment env
		
		guarantee scenario requirementScenario1 {
			env -> a.opA1()
			strict urgent a -> b.opB1()
			var EInt i = 0
			while [ i < 3 ] { 
				strict urgent b -> a.opA1()
				i = i + 1
			}
			strict urgent b -> a.opA2()			 
		}
	}
}