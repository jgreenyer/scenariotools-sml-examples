import "../model.ecore"

specification test_loop {
	domain model
	
	controllable {
		A
		B
	}
	
	collaboration test_loop{
		
		static role A a 
		static role B b 
		static role Environment env
		
		guarantee scenario requirementScenario1 {
			env -> a.opA1()
			strict urgent a -> b.opB1()
			while { 
				strict urgent b -> a.opA1()
			}
			strict urgent b -> a.opA2()			 
		}
	}
}