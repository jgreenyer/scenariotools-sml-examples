import "../model.ecore"

specification test_InterruptCondition {
	domain model
	
	controllable {
		A
		B
	}
	
	collaboration InterruptCondition{
		
		static role A a 
		static role B b 
		static role Environment env
		
		guarantee scenario requirementScenario1 {
			env -> a.opA1()
			strict urgent a -> b.opB1()
			interrupt [ 2 == 2 ]
			strict urgent b -> a.opA2()			 
		}
	}
}