import "../model.ecore"

specification test_memory_unrealizable {
	domain model
	
	controllable {
		A
	}
	
	non-spontaneous events {
		A.opA6
		A.opA7
		A.opA8
	}
	
	collaboration test_memory_unrealizable {
		
		static role A a 
		static role Environment env
		
		guarantee scenario buildGameGraph {
			env -> a.opA1()
			alternative {
				urgent a -> a.opA2()
			} or {
				urgent a -> a.opA3()
			}
		}
		
		assumption scenario buildGameGraph_sub {
			alternative {
				a -> a.opA2()
			} or {
				a -> a.opA3()
			}
			alternative {
				env -> a.opA6()
			} or {
				env -> a.opA7()
			} or {
				env -> a.opA8()				
			}
		} constraints [
			forbidden env -> a.opA1()
		]
		
		guarantee scenario buildGameGraph_sub1 {
			a -> a.opA2()
			alternative {
				env -> a.opA6()
			} or {
				env -> a.opA7()
			} or {
				env -> a.opA8()				
			}
			urgent a -> a.opA2()
		}

		guarantee scenario buildGameGraph_sub2 {
			a -> a.opA3()
			alternative {
				env -> a.opA6()
			} or {
				env -> a.opA7()
			} or {
				env -> a.opA8()				
			}
			urgent a -> a.opA3()
		}

		assumption scenario condition_a1 {
			alternative {
				a -> a.opA2()
			} or {
				a -> a.opA3()
			}
			monitored eventually env -> a.opA6()
		}
		
		assumption scenario condition_a2 {
			alternative {
				a -> a.opA2()
			} or {
				a -> a.opA3()
			}
			alternative {
				monitored eventually env -> a.opA7()
			} or {
				monitored eventually env -> a.opA8()			
			}
		}
		
		guarantee scenario condition_g1 {
			env -> a.opA1()
			alternative {				
				a -> a.opA2()
				while [true] {
					alternative {				
						monitored eventually env -> a.opA6()
						a -> a.opA2()
					} or {
						monitored eventually env -> a.opA7()			
						a -> a.opA2()
					} or {
						monitored eventually env -> a.opA8()
						monitored eventually a -> a.opA2()
					}
				}
			} or {
				a -> a.opA3()
				while [true] {
					alternative {				
						monitored eventually env -> a.opA6()
						monitored eventually a -> a.opA3()
					} or {
						monitored eventually env -> a.opA7()			
						monitored eventually a -> a.opA3()
					} or {
						monitored eventually env -> a.opA8()
						a -> a.opA3()
					}
				}
			}
		}

		guarantee scenario condition_g2 {
			env -> a.opA1()
			alternative {				
				a -> a.opA2()
				while [true] {
					alternative {				
						monitored eventually env -> a.opA6()
						monitored eventually a -> a.opA2()
					} or {
						monitored eventually env -> a.opA7()			
						monitored eventually a -> a.opA2()
					} or {
						monitored eventually env -> a.opA8()
						a -> a.opA2()
					}
				}
			} or {
				a -> a.opA3()
				while [true] {
					alternative {				
						monitored eventually env -> a.opA6()
						a -> a.opA3()
					} or {
						monitored eventually env -> a.opA7()			
						a -> a.opA3()
					} or {
						monitored eventually env -> a.opA8()
						monitored eventually a -> a.opA3()
					}
				}
			}
		}
	}
}