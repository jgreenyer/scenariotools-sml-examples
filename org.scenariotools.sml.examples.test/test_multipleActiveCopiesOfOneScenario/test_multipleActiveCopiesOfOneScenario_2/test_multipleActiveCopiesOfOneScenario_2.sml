import "../../model.ecore"

specification test_multipleActiveCopiesOfOneScenario_2 {
	domain model
	
	controllable {
		A
		B
	}
	
	collaboration test_multipleActiveCopiesOfOneScenario_2 {
		
		static role A a 
		dynamic role B b 
		static role Environment env
		
		guarantee scenario requirementScenario1 
			bindings [b = a.b]{
			env -> a.opA1()
			strict urgent a -> b.opB1()
			strict urgent b -> a.opA2()		
			strict urgent b -> a.setB(b.next)	 		 
		}
		
		guarantee scenario allowMutlipleActiveCopiesOfMe {
			a -> b.opB1()
			b -> a.opA2()
			{
				var EInt i = 0
				while [ i < 5 ]{
					a -> b.opB1()
					b -> a.opA2()
					i = i + 1	
				}
			} 
		}
	}
}