import "../../model.ecore"

specification test_multipleActiveCopiesOfOneScenario_DirtyRead {
	domain model
	
	controllable {
		A
		B
	}
	
	collaboration test_multipleActiveCopiesOfOneScenario_DirtyRead{
		
		static role A a 
		dynamic role B b 
		static role Environment env
		
		guarantee scenario requirementScenario1 
			bindings [b = a.b]
		{
			env -> a.opA1()
			strict urgent a -> a.opA1()
			strict urgent b -> a.setB(b.next)
			strict urgent a -> a.opA1()
			// Now we have 'allowMultipleActiveCopiesOfMe' two times in the same cut.
			// Trigger the next message will lead to progress in the first copy 
			// and produce a cold violation in the second.
			strict urgent a -> a.opA2()
		}
		
		guarantee scenario allowMutlipleActiveCopiesOfMe 
			bindings [b = a.b] 
		{
			a -> a.opA1()
			var EInt i = 0
			while [ i < 5 ]{
				{
					a -> a.opA2()
				}
				a -> a.opA3()
				i = i + 1	
			}constraints[
				ignore a -> a.opA1()
			]
		}
	}
}