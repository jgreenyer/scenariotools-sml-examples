import "../model.ecore"

specification test_scenario {
	domain model
	
	controllable {
		A
		B
	}
	
	collaboration test_scenario{
		
		static role A a 
		static role B b 
		static role Environment env
		
		guarantee scenario requirementScenario1 {
			env -> a.opA1()
			strict urgent a -> b.opB1()
			strict urgent b -> a.opA2()			 
		}
	}
}