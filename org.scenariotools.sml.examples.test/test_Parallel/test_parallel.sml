import "../model.ecore"

specification test_parallel {
	domain model
	
	controllable {
		A
		B
	}
	
	collaboration test_parallel{
		
		static role A a 
		static role B b 
		static role Environment env
		
		guarantee scenario requirementScenario1 {
			env -> a.opA1()
			strict urgent a -> b.opB1()
			parallel { 
				strict urgent b -> a.opA1()
			}and {
				strict urgent b -> a.opA2()
			}
			strict urgent b -> a.opA2()			 
		}
	}
}