import "../model.ecore"

specification test_parameter_ranges {
	domain model
	
	controllable {
		A
		B
	}
	
	parameter ranges {
	 	B.opBP3(intPara1 = [1, 25, 35, 45 ], intPara2 = [ 1000 .. 1002 ], intPara3 = [ 222 .. 224 ]),
	 	B.opBPMix(stringParam = [ "Hello", "ScenarioTools" ], intParam = [ 1 .. 3, 6, 8 ], enumParam = [ EnumA:A, EnumA:C ]),
	 	B.setEnumAttr(enumAttr = [ EnumB:A, EnumB:B ])
	}
	
	collaboration test_parameter_ranges{
		
		static role A a 
		static role B b 
		static role Environment env
		
		guarantee scenario requirementScenario1 {
			var EInt x
			env -> b.opBP3(1, bind x , *)
			strict urgent a -> b.opB1()
			env -> b.opBP3(x, *, 4)

			strict urgent b -> a.opA1()
			
			env -> b.opBPMix(*,*,*)
			strict urgent b -> a.opA2()
			env -> b.setEnumAttr(*)
			strict urgent a -> b.setEnumAttr(b.enumAttr)
		}
	}
}