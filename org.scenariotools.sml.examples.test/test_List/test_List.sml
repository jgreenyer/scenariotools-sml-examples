import "../model.ecore"


specification ListTestSpecification {
	
	domain model
	controllable {
		A
		B
	}
	
	collaboration ListTestCollaboration {
		static role Environment env
		static role A a
		static role B b
		static role B lastB
		static role B unknownB
		guarantee scenario  FirstTest{
			env -> a.opA1()
			violation [b!=a.listOfB.first()]
		}
		guarantee scenario LastTest {
			env -> a.opA2() 
			violation [ lastB != a.listOfB.last()]
		}
		guarantee scenario EmptyTest {
			env -> a.opA3()
			violation [a.nonUniqueEnvironments.isEmpty()]
		}
		guarantee scenario ContainsAllTest {
			env -> a.opA4() 
			violation [ a.listOfB.containsAll(a.setOfB)]
			violation [ !b.strings.containsAll(b.moreStrings)]
		}
		guarantee scenario ContainsTest {
			env -> a.opA5()
			violation [ a.listOfB.contains(unknownB)]
			violation [!b.strings.contains("Hello")] 
		}
		guarantee scenario SizeTest {
			env -> a.opA6()
			violation [a.listOfB.size() < 2]
		}
		guarantee scenario GetTest {
			env -> a.opA7()
			var EInt penultimateIndex = a.listOfB.size() - 2 
			violation [ a.listOfB.get(penultimateIndex) == lastB]
		}
	}	
}