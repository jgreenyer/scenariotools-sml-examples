import "../model.ecore"

specification test_channels {

	domain model
	
	controllable {
		A
		B
	}

	channels {
// The following channel definition should result in a violation in the specification when simulating the simple system.  After the scenario 'ViolatingScenario' is activated, the admissible channel for
// opB2 is closed, but opB2 is still urgent to be sent in the active scenario. This causes a violation, since opB2 requires a channel.
		B.opB2 over A.b
//  Commenting out the following channel should result in two enabled initial message event when simulating the simple system.
		A.opA1 over Environment.knownA
	}

	collaboration test_channels {

		dynamic role A a
		dynamic role B b
		dynamic role Environment env
		guarantee scenario InitScenario bindings [
			b = a.b
		] {
			env -> a.opA1()
			strict urgent a -> b.opB1()
			strict urgent b ->a.setB(null)
			strict urgent a -> b.opB2()
		}
		guarantee scenario ViolatingScenario {
			a -> b.opB1()
			strict urgent a -> b.opB2()
		}

	}

}