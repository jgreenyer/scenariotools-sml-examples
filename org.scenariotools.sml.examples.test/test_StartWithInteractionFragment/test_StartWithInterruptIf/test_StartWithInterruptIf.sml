import "../../model.ecore"

specification test_StartWithInterruptIf {
	domain model
	
	controllable {
		A
		B
	}
	
	collaboration test_StartWithInterruptIf{
		
		static role A a 
		static role B b 
		static role Environment env
		
		guarantee scenario requirementScenario1 {
			env -> a.opA1()
			interrupt [ "af" == a.name]
			strict urgent b -> a.opA1()
			strict urgent b -> a.opA2()
		}
	}
}