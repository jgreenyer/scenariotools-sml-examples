import "../../model.ecore"

specification test_StartWithLoop {
	domain model
	
	controllable {
		A
		B
	}
	
	collaboration test_StartWithLoop {
		
		static role A a 
		static role B b 
		static role Environment env
		
		guarantee scenario requirementScenario1 {
			var EInt i
			env-> a.opA1()
			i = 1
			while [i < 4] {
				env-> a.opA1()
				i = i + 1
				
			}	
			strict urgent b -> a.opA1()
			strict urgent b -> a.opA2()
		}
	}
}