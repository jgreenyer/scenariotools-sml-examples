import "../model.ecore"

specification test_parameter_merge {
	domain model
	
	controllable {
		A
		B
	}
	
	parameter ranges {
	 	B.opBP3(intPara1 = [1, 25, 35, 45 ], intPara2 = [ 1000 .. 1002 ], intPara3 = [ 222 .. 224 ]),
	 	B.opBPMix(stringParam = [ "Hello", "ScenarioTools" ], intParam = [ 1 .. 3, 6, 8 ], enumParam = [ EnumA:A, EnumA:C ]),
	 	B.setEnumAttr(enumAttr = [ EnumB:A, EnumB:B ])
	}
	
	collaboration test_parameter_merge{
		
		static role A a 
		static role B b 
		static role Environment env
		
		guarantee scenario requirementScenario1 {
			env -> a.opA1()
			strict urgent a -> b.opBP3(*,1000,*)
			strict urgent a -> b.opB1()
		}constraints[
			ignore a -> b.opBP3(*,*,*)
		]
		
		guarantee scenario requirementScenario2 {
			env -> a.opA1()
			strict urgent a -> b.opBP3(1,*,*)
		}constraints[
			ignore a -> b.opBP3(*,*,*)
		]
		
		guarantee scenario requirementScenario3 {
			env -> a.opA1()
			strict urgent a -> b.opBP3(1, * , 222)
		}constraints[
			ignore a -> b.opBP3(*,*,*)
		]
		
		guarantee scenario requirementScenario4 {
			env -> a.opA1()
			strict urgent a -> b.opBP3(1, * , 223)
		}constraints[
			ignore a -> b.opBP3(*,*,*)
		]
		
		guarantee scenario requirementScenario5 {
			env -> a.opA1()
			strict urgent a -> b.opBPMix("Hello", 2 , EnumA:A)
		}constraints[
			ignore a -> b.opBPMix(*,*,*)
		]
		
		guarantee scenario requirementScenario6 {
			env -> a.opA1()
			strict urgent a -> b.opBPMix(*, * , EnumA:C)
		}constraints[
			ignore a -> b.opBPMix(*,*,*)
		]
	}
}