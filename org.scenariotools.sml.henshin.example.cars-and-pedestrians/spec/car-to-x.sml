import "car-to-x.ecore"

specification CarsAndPedestrians {
	
	domain cartox
	
	controllable {
		Car
		StreetSectionControl
	}
	
	non-spontaneous events {
		Car.tick
		Car.pedestrainDetectedAhead
	}
	
	collaboration TimeTickAssumption {   
		
		dynamic role Environment env 
		static role Car car1 
		static role Car car2 
		
		assumption scenario TimeTick{
			env -> env.tick()
			// "time" passes for both cars
			strict eventually env -> car1.tick() 
			strict eventually env -> car2.tick() 
			// pedestrian can appear only after time tick, reduces size of state graph.
			alternative{
				env->car1.pedestrainDetectedAhead()
			} or {
				env->car2.pedestrainDetectedAhead()
			}
		}
	}

	collaboration PedestrianDetectionAssumption {   
		
		dynamic role Environment env 
		dynamic  role Car car 
		
		assumption scenario PedestrianDetectionAssumption {
			env -> car.tick()
			alternative{
				env->car.pedestrainDetectedAhead()
			} or {
				env -> env.tick()
			}
		}
	}
	
	collaboration CarMoves {   
		
		dynamic role Environment env 
		dynamic role Car car 
		
		guarantee scenario CarMoves{
			env -> car.tick()
			alternative{
				strict urgent car->env.moveToNextArea()
			} or {
				strict urgent car->env.stay()
			}
		}
		
	}

	collaboration Braking {   
		
		dynamic role Environment env 
		dynamic role Car car 
		dynamic role Car followedByCar 
		dynamic role LaneArea currentLaneArea
		
		guarantee scenario BreakForPedestrainDetectedAhead{
			env -> car.pedestrainDetectedAhead()
			strict urgent car->env.break()
		}

		guarantee scenario MustNotBreakWhenFollowedByCarOnSameLaneArea bindings [
			followedByCar = car.followedBy
			currentLaneArea = car.inArea
		]{
			car->env.break()
			violation [followedByCar.inArea == currentLaneArea]
		}
		
	}
	
	
}