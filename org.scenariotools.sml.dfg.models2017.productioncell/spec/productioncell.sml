import "../model/productioncell.ecore"

specification ProductioncellSpecification {

	domain productioncell

	controllable {
		Controller
	}
	
	non-spontaneous events {
		Controller.accelerateAndMove
		Controller.decelerate
		Controller.pressingFinished
		RobotArm.setCarriesItem
		RobotArm.setLocation
		Press.setHasItem
	}

	collaboration FeedBeltBehavior {

		static role Controller controller
		static role ConveyorBelt feedBelt
		static role RobotArm feedArm
		static role Press press

		guarantee scenario BlankArrives {
			feedBelt -> controller.blankArrived()
			wait [feedArm.location == feedBelt && !feedArm.carriesItem]
			urgent controller -> feedArm.pickUp()
		}
		
		guarantee scenario ArmDeliversItemToPress {
			feedArm -> feedArm.setCarriesItem(true)
			urgent controller -> feedArm.moveTo(press)
			feedArm -> feedArm.setLocation(press)
			wait [!press.hasItem]
			urgent controller -> feedArm.releaseItem()
			feedArm -> feedArm.setCarriesItem(false)
			urgent controller -> feedArm.moveTo(feedBelt)
		}
		
		assumption scenario ArmPicksUpItemBeforeNewBlankArrives {
			feedBelt -> controller.blankArrived()
			controller -> feedArm.pickUp()
		} constraints [
			forbidden feedBelt -> controller.blankArrived()
		]

	}
	
	collaboration PressBehavior {
		
		static role Controller controller
		static role RobotArm feedArm
		static role ConveyorBelt depositBelt
		static role RobotArm depositArm
		static role Press press

		guarantee scenario PressStartsPressing {
			feedArm -> feedArm.setCarriesItem(false)
			urgent controller -> press.startPressing()
		}
		
		guarantee scenario PickUpPressedItem {
			press -> controller.pressingFinished()
			wait [depositArm.location == press && !depositArm.carriesItem]
			urgent controller -> depositArm.pickUp()
		}
		
		guarantee scenario DepositArmDeliversPressedItem
		optimize cost {
			depositArm -> depositArm.setCarriesItem(true)
			eventually controller -> depositArm.moveTo(depositBelt)
			depositArm -> depositArm.setLocation(depositBelt)
			urgent controller -> depositArm.releaseItem()
			depositArm -> depositArm.setCarriesItem(false)
			eventually controller -> depositArm.moveTo(press)
		}
		
		assumption scenario PressEventuallyFinishes {
			controller -> press.startPressing()
			eventually press -> controller.pressingFinished()
		}
	}
	
	collaboration RobotArmBehavior {
		
		dynamic role Controller controller
		dynamic role RobotArm arm
		dynamic role Location targetLocation
		static role Press press
		
		assumption scenario ArmMovesToLocation {
			controller -> arm.moveTo(bind targetLocation)
			committed arm -> controller.accelerateAndMove()
			eventually arm -> controller.decelerate()
			eventually arm -> arm.setLocation(targetLocation) // also implies an end of the motion, i.e., arrived at target location
		}
		
		assumption scenario ArmPicksUpItem {
			controller -> arm.pickUp()
			strict committed arm -> arm.setCarriesItem(true) 
			interrupt [arm.location != press]
			strict committed press -> press.setHasItem(false)
		}
		
		assumption scenario ArmReleasesItem {
			controller -> arm.releaseItem()
			strict committed arm -> arm.setCarriesItem(false)
			interrupt [arm.location != press]
			strict committed press -> press.setHasItem(true)
		}
	}

	collaboration Costs {
		
		dynamic role Controller controller
		dynamic role RobotArm arm
		
		assumption scenario AccelerationAndMovementEnergy
		cost [15.0] {
			arm -> controller.accelerateAndMove()
			monitored eventually arm -> controller.decelerate()
		}
		
		assumption scenario BrakingEnergy
		cost [-5.0] {
			arm -> controller.decelerate()
			monitored eventually arm -> arm.setLocation(*)
		}
	}

}
