package vacuumrobotspecification.runtime;

import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import vacuumrobot.VacuumrobotPackage;
import vacuumrobot.VacuumRobotSystem;

public class VacuumrobotSpecificationObjectSystem {

	private static VacuumrobotSpecificationObjectSystem instance;

	public static VacuumrobotSpecificationObjectSystem getInstance() {
		if (instance == null)
			instance = new VacuumrobotSpecificationObjectSystem();
		return instance;
	}

	public VacuumRobotSystem VacuumRobotSystem = (VacuumRobotSystem) loadVacuumRobotSystem(
			"classes/model/vacuumrobotsystem_ThreeRoomHorizontal.xmi");
	
	
	public VacuumRobotSystem loadVacuumRobotSystem(String uri) {
		VacuumrobotPackage.eINSTANCE.eClass();

		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		Map<String, Object> m = reg.getExtensionToFactoryMap();
		m.put("xmi", new XMIResourceFactoryImpl());

		ResourceSet resSet = new ResourceSetImpl();

		Resource resource = resSet.getResource(URI.createURI(uri), true);
		VacuumRobotSystem vacuumRobotSystem = (VacuumRobotSystem) resource.getContents().get(0);
		return vacuumRobotSystem;
	}
}
