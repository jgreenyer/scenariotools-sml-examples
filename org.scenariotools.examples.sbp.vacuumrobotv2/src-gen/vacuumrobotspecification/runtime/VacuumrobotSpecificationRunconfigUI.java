package vacuumrobotspecification.runtime;

import vacuumrobotspecification.specification.VacuumrobotSpecificationSpecification;
import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.settings.Settings;
import vacuumrobotspecification.ui.VacuumrobotSpecificationEnvironmentFrame;

public class VacuumrobotSpecificationRunconfigUI extends SpecificationRunconfig<VacuumrobotSpecificationSpecification> {

	public VacuumrobotSpecificationRunconfigUI() {
		super(new VacuumrobotSpecificationSpecification());
		VacuumrobotSpecificationEnvironmentFrame logFrame = new VacuumrobotSpecificationEnvironmentFrame(
				"VacuumrobotSpecificationSpecification", getAdapter());
		logFrame.setVisible(true);
		Settings.setRuntimeOut(logFrame.getRuntimeLog());
		Settings.setServerOut(logFrame.getServerLog());
		Settings.SPECTATOR__PRINT_CAUGHT_MESSAGE = true;
	}

	@Override
	protected void registerParticipatingObjects() {
		VacuumrobotSpecificationObjectSystem objectSystem = VacuumrobotSpecificationObjectSystem.getInstance();
		registerObject(objectSystem.VacuumRobotSystem, UNCONTROLLABLE); // VacuumRobotSystem
		registerObject(objectSystem.VacuumRobotSystem.getRoom().get(0), UNCONTROLLABLE); // Room01
		registerObject(objectSystem.VacuumRobotSystem.getRoom().get(1), UNCONTROLLABLE); // Room02
		registerObject(objectSystem.VacuumRobotSystem.getRoom().get(2), UNCONTROLLABLE); // Room03
		registerObject(objectSystem.VacuumRobotSystem.getVacuumRobot().get(0), UNCONTROLLABLE); // robot1
		registerObject(objectSystem.VacuumRobotSystem.getVacuumRobot().get(0).getController(), CONTROLLABLE); // r1ctrl
		registerObject(objectSystem.VacuumRobotSystem.getVacuumRobotManager(), CONTROLLABLE); // rManager
	}

	@Override
	protected void registerNetworkAdressesForObjects() {
		VacuumrobotSpecificationObjectSystem objectSystem = VacuumrobotSpecificationObjectSystem.getInstance();
		// registerAddress(objectSystem.VacuumRobotSystem.getRoom().get(0),
		// ROOM01_HOSTNAME, ROOM01_PORT);
		// registerAddress(objectSystem.VacuumRobotSystem.getRoom().get(1),
		// ROOM02_HOSTNAME, ROOM02_PORT);
		// registerAddress(objectSystem.VacuumRobotSystem.getRoom().get(2),
		// ROOM03_HOSTNAME, ROOM03_PORT);
		// registerAddress(objectSystem.VacuumRobotSystem.getVacuumRobot().get(0),
		// ROBOT1_HOSTNAME, ROBOT1_PORT);
		// registerAddress(objectSystem.VacuumRobotSystem.getVacuumRobotManager(),
		// RMANAGER_HOSTNAME, RMANAGER_PORT);
	}

	@Override
	protected void registerObservers() {
		// Add Observers here.
		// addScenarioObserver(scenarioObserver);
	}

	public static void main(String[] args) {
		SpecificationRunconfig.run(VacuumrobotSpecificationRunconfigUI.class);
	}

}
