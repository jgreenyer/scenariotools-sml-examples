package vacuumrobotspecification.ui;

import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import sbp.runtime.adapter.RuntimeAdapter;
import sbp.ui.ConsoleFrame;
import sbp.ui.MessageButton;
import vacuumrobot.DirtyableRoom;
import vacuumrobot.VacuumRobot;
import vacuumrobot.VacuumRobotController;
import vacuumrobot.VacuumRobotManager;
import vacuumrobotspecification.runtime.VacuumrobotSpecificationObjectSystem;

@SuppressWarnings("serial")
public class VacuumrobotSpecificationEnvironmentFrame extends ConsoleFrame {

	private RuntimeAdapter runtimeAdapter;

	private void createButtons() {
		VacuumrobotSpecificationObjectSystem objectSystem = VacuumrobotSpecificationObjectSystem.getInstance();
		JPanel buttonPane = new JPanel();
		JScrollPane scrollPane = new JScrollPane(buttonPane, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.Y_AXIS));
		scrollPane.setPreferredSize(new Dimension(400, 400));

		buttonPane.add(new MessageButton<DirtyableRoom, VacuumRobotManager>(runtimeAdapter,
				(DirtyableRoom) objectSystem.VacuumRobotSystem.getRoom().get(1),
				(VacuumRobotManager) objectSystem.VacuumRobotSystem.getVacuumRobotManager(), "dirtDetected"));
		buttonPane.add(new MessageButton<DirtyableRoom, VacuumRobotManager>(runtimeAdapter,
				(DirtyableRoom) objectSystem.VacuumRobotSystem.getRoom().get(2),
				(VacuumRobotManager) objectSystem.VacuumRobotSystem.getVacuumRobotManager(), "dirtDetected"));

		/*
		 * buttonPane.add(new MessageButton<VacuumRobot,
		 * VacuumRobotController>(runtimeAdapter, (VacuumRobot)
		 * objectSystem.VacuumRobotSystem.getVacuumRobot().get(0),
		 * (VacuumRobotController)
		 * objectSystem.VacuumRobotSystem.getVacuumRobot().get(0).getController(
		 * ), "SETCharge", 1)); buttonPane.add(new MessageButton<VacuumRobot,
		 * VacuumRobotController>(runtimeAdapter, (VacuumRobot)
		 * objectSystem.VacuumRobotSystem.getVacuumRobot().get(0),
		 * (VacuumRobotController)
		 * objectSystem.VacuumRobotSystem.getVacuumRobot().get(0).getController(
		 * ), "SETCharge", 2));
		 * 
		 * buttonPane.add(new MessageButton<VacuumRobot,
		 * VacuumRobotController>(runtimeAdapter, (VacuumRobot)
		 * objectSystem.VacuumRobotSystem.getVacuumRobot().get(0),
		 * (VacuumRobotController)
		 * objectSystem.VacuumRobotSystem.getVacuumRobot().get(0).getController(
		 * ), "arrivedInRoom",
		 * objectSystem.VacuumRobotSystem.getRoom().get(0))); buttonPane.add(new
		 * MessageButton<VacuumRobot, VacuumRobotController>(runtimeAdapter,
		 * (VacuumRobot) objectSystem.VacuumRobotSystem.getVacuumRobot().get(0),
		 * (VacuumRobotController)
		 * objectSystem.VacuumRobotSystem.getVacuumRobot().get(0).getController(
		 * ), "arrivedInRoom",
		 * objectSystem.VacuumRobotSystem.getRoom().get(1))); buttonPane.add(new
		 * MessageButton<VacuumRobot, VacuumRobotController>(runtimeAdapter,
		 * (VacuumRobot) objectSystem.VacuumRobotSystem.getVacuumRobot().get(0),
		 * (VacuumRobotController)
		 * objectSystem.VacuumRobotSystem.getVacuumRobot().get(0).getController(
		 * ), "arrivedInRoom",
		 * objectSystem.VacuumRobotSystem.getRoom().get(2)));
		 * 
		 * buttonPane.add(new MessageButton<VacuumRobot,
		 * VacuumRobotController>(runtimeAdapter, (VacuumRobot)
		 * objectSystem.VacuumRobotSystem.getVacuumRobot().get(0),
		 * (VacuumRobotController)
		 * objectSystem.VacuumRobotSystem.getVacuumRobot().get(0).getController(
		 * ), "arrivedInRoom")); buttonPane.add(new MessageButton<VacuumRobot,
		 * VacuumRobotController>(runtimeAdapter, (VacuumRobot)
		 * objectSystem.VacuumRobotSystem.getVacuumRobot().get(0),
		 * (VacuumRobotController)
		 * objectSystem.VacuumRobotSystem.getVacuumRobot().get(0).getController(
		 * ), "arrivedInRoom")); buttonPane.add(new MessageButton<VacuumRobot,
		 * VacuumRobotController>(runtimeAdapter, (VacuumRobot)
		 * objectSystem.VacuumRobotSystem.getVacuumRobot().get(0),
		 * (VacuumRobotController)
		 * objectSystem.VacuumRobotSystem.getVacuumRobot().get(0).getController(
		 * ), "SETCharge")); buttonPane.add(new MessageButton<VacuumRobot,
		 * VacuumRobotController>(runtimeAdapter, (VacuumRobot)
		 * objectSystem.VacuumRobotSystem.getVacuumRobot().get(0),
		 * (VacuumRobotController)
		 * objectSystem.VacuumRobotSystem.getVacuumRobot().get(0).getController(
		 * ), "SETCharge"));
		 */

		add(scrollPane);
	}

	private void createButtons(String name) {
		VacuumrobotSpecificationObjectSystem objectSystem = VacuumrobotSpecificationObjectSystem.getInstance();
		JPanel buttonPane = new JPanel();
		JScrollPane scrollPane = new JScrollPane(buttonPane, ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		buttonPane.setLayout(new BoxLayout(buttonPane, BoxLayout.Y_AXIS));
		scrollPane.setPreferredSize(new Dimension(400, 400));

		
		// only for robot controller
		// the following buttons will be generated
		// dirDetected
		if (name.equals("RobotCtrl")) {
			buttonPane.add(new MessageButton<DirtyableRoom, VacuumRobotManager>(runtimeAdapter,
					(DirtyableRoom) objectSystem.VacuumRobotSystem.getRoom().get(1),
					(VacuumRobotManager) objectSystem.VacuumRobotSystem.getVacuumRobotManager(), "dirtDetected"));
			buttonPane.add(new MessageButton<DirtyableRoom, VacuumRobotManager>(runtimeAdapter,
					(DirtyableRoom) objectSystem.VacuumRobotSystem.getRoom().get(2),
					(VacuumRobotManager) objectSystem.VacuumRobotSystem.getVacuumRobotManager(), "dirtDetected"));
		}

		// only for robot
		// the following buttons will be generated
		// SETCharge & arrivedInRoom
		if (name.equals("Robot")) {
			buttonPane.add(new MessageButton<VacuumRobot, VacuumRobotController>(runtimeAdapter,
					(VacuumRobot) objectSystem.VacuumRobotSystem.getVacuumRobot().get(0),
					(VacuumRobotController) objectSystem.VacuumRobotSystem.getVacuumRobot().get(0).getController(),
					"SETCharge", 1));
			buttonPane.add(new MessageButton<VacuumRobot, VacuumRobotController>(runtimeAdapter,
					(VacuumRobot) objectSystem.VacuumRobotSystem.getVacuumRobot().get(0),
					(VacuumRobotController) objectSystem.VacuumRobotSystem.getVacuumRobot().get(0).getController(),
					"SETCharge", 2));

			buttonPane.add(new MessageButton<VacuumRobot, VacuumRobotController>(runtimeAdapter,
					(VacuumRobot) objectSystem.VacuumRobotSystem.getVacuumRobot().get(0),
					(VacuumRobotController) objectSystem.VacuumRobotSystem.getVacuumRobot().get(0).getController(),
					"arrivedInRoom", objectSystem.VacuumRobotSystem.getRoom().get(0)));
			buttonPane.add(new MessageButton<VacuumRobot, VacuumRobotController>(runtimeAdapter,
					(VacuumRobot) objectSystem.VacuumRobotSystem.getVacuumRobot().get(0),
					(VacuumRobotController) objectSystem.VacuumRobotSystem.getVacuumRobot().get(0).getController(),
					"arrivedInRoom", objectSystem.VacuumRobotSystem.getRoom().get(1)));
			buttonPane.add(new MessageButton<VacuumRobot, VacuumRobotController>(runtimeAdapter,
					(VacuumRobot) objectSystem.VacuumRobotSystem.getVacuumRobot().get(0),
					(VacuumRobotController) objectSystem.VacuumRobotSystem.getVacuumRobot().get(0).getController(),
					"arrivedInRoom", objectSystem.VacuumRobotSystem.getRoom().get(2)));
		}
		/*
		 * buttonPane.add(new MessageButton<VacuumRobot,
		 * VacuumRobotController>(runtimeAdapter, (VacuumRobot)
		 * objectSystem.VacuumRobotSystem.getVacuumRobot().get(0),
		 * (VacuumRobotController)
		 * objectSystem.VacuumRobotSystem.getVacuumRobot().get(0).getController(
		 * ), "arrivedInRoom")); buttonPane.add(new MessageButton<VacuumRobot,
		 * VacuumRobotController>(runtimeAdapter, (VacuumRobot)
		 * objectSystem.VacuumRobotSystem.getVacuumRobot().get(0),
		 * (VacuumRobotController)
		 * objectSystem.VacuumRobotSystem.getVacuumRobot().get(0).getController(
		 * ), "arrivedInRoom")); buttonPane.add(new MessageButton<VacuumRobot,
		 * VacuumRobotController>(runtimeAdapter, (VacuumRobot)
		 * objectSystem.VacuumRobotSystem.getVacuumRobot().get(0),
		 * (VacuumRobotController)
		 * objectSystem.VacuumRobotSystem.getVacuumRobot().get(0).getController(
		 * ), "SETCharge")); buttonPane.add(new MessageButton<VacuumRobot,
		 * VacuumRobotController>(runtimeAdapter, (VacuumRobot)
		 * objectSystem.VacuumRobotSystem.getVacuumRobot().get(0),
		 * (VacuumRobotController)
		 * objectSystem.VacuumRobotSystem.getVacuumRobot().get(0).getController(
		 * ), "SETCharge"));
		 */

		add(scrollPane);
	}

	public VacuumrobotSpecificationEnvironmentFrame(String name, RuntimeAdapter adapter) {
		super(name);
		this.runtimeAdapter = adapter;
		createButtons(name);
		pack();
	}

}
