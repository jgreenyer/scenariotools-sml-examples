package vacuumrobotspecification.collaborations.vacuumrobotcollaboration1;

import sbp.specification.scenarios.violations.Violation;
import sbp.specification.events.Message;

// Collaboration
import vacuumrobotspecification.collaborations.vacuumrobotcollaboration1.VacuumrobotCollaboration1Collaboration;

// Roles

@SuppressWarnings("serial")
public class VacuumrobotCollaboration1_RobotManagerAddsDirtyRoomToListScenario
		extends VacuumrobotCollaboration1Collaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(dirtyableRoom, rManager, "dirtDetected");
		// TODO : to remove ? and replace by RANDOM : No dirtyableRoom has already binding by init message
		setBlocked(rManager, rManager, "ADDDirtyRooms", dirtyableRoom.getBinding());
		setBlocked(rManager, rManager, "orderRobotToCleanRoom",
				rManager.getBinding().getVacuumRobotControllers().get(0));
	}

	@Override
	protected void initialisation() {
		// []
		addInitializingMessage(new Message(dirtyableRoom, rManager, "dirtDetected"));
	}

	@Override
	protected void registerRoleBindings() {
	}

	@Override
	protected void body() throws Violation {
		// true
		if (rManager.getBinding().getDirtyRooms().contains(dirtyableRoom.getBinding())) {
			throwViolation(INTERRUPT);
		}
		request(STRICT, rManager, rManager, "ADDDirtyRooms", dirtyableRoom.getBinding());
		request(STRICT, rManager, rManager, "orderRobotToCleanRoom",
				rManager.getBinding().getVacuumRobotControllers().get(0));
	}

}
