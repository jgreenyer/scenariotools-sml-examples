package vacuumrobotspecification.collaborations.vacuumrobotcollaboration1;

// Framework
import sbp.specification.role.Role;
import sbp.specification.scenarios.Scenario;

// Roles
import vacuumrobot.VacuumRobotManager;
import vacuumrobot.DirtyableRoom;
import vacuumrobot.Room;
import vacuumrobot.VacuumRobot;
import vacuumrobot.VacuumRobotController;

// ObjectSystem
import vacuumrobotspecification.runtime.VacuumrobotSpecificationObjectSystem;

@SuppressWarnings("serial")
public abstract class VacuumrobotCollaboration1Collaboration extends Scenario {

	// Roles
	protected Role<VacuumRobotManager> rManager = createRole(VacuumRobotManager.class, "rManager");
	protected Role<DirtyableRoom> dirtyableRoom = createRole(DirtyableRoom.class, "dirtyableRoom");
	protected Role<Room> room = createRole(Room.class, "room");
	protected Role<VacuumRobot> robot = createRole(VacuumRobot.class, "robot");
	protected Role<VacuumRobotController> robotCtrl = createRole(VacuumRobotController.class, "robotCtrl");

	// Constructor
	public VacuumrobotCollaboration1Collaboration() {
		super();
		VacuumrobotSpecificationObjectSystem objectSystem = VacuumrobotSpecificationObjectSystem.getInstance();
		// Bind static Role to Object
		bindRoleToObject(rManager, objectSystem.VacuumRobotSystem.getVacuumRobotManager());
	}
}