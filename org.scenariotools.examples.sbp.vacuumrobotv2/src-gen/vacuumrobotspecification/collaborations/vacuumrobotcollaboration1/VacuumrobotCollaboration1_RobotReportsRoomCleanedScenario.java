package vacuumrobotspecification.collaborations.vacuumrobotcollaboration1;

import sbp.specification.scenarios.violations.Violation;
import sbp.specification.events.Message;

// Collaboration
import vacuumrobotspecification.collaborations.vacuumrobotcollaboration1.VacuumrobotCollaboration1Collaboration;

// Roles
import vacuumrobot.DirtyableRoom;

@SuppressWarnings("serial")
public class VacuumrobotCollaboration1_RobotReportsRoomCleanedScenario extends VacuumrobotCollaboration1Collaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(robot, robotCtrl, "arrivedInRoom", RANDOM);
		//TODO inline binding
		//setBlocked(robotCtrl, rManager, "robotCleanedRoom", dirtyableRoom.getBinding());
		setBlocked(robotCtrl, rManager, "robotCleanedRoom", RANDOM);
		//TODO inline binding
		//setBlocked(rManager, rManager, "REMOVEDirtyRooms", dirtyableRoom.getBinding());
		setBlocked(rManager, rManager, "REMOVEDirtyRooms", RANDOM);
		setBlocked(robotCtrl, robot, "moveToAdjacentRoom", RANDOM);
	}

	@Override
	protected void initialisation() {
		// []
		addInitializingMessage(new Message(robot, robotCtrl, "arrivedInRoom", RANDOM));
		// true
		// 1
	}

	@Override
	protected void registerRoleBindings() {
	}

	@Override
	protected void body() throws Violation {
		//TODO: check room class
		if (!(getLastMessage().getParameters().get(0) instanceof DirtyableRoom)) {
			return;
		}
		dirtyableRoom.setBinding((DirtyableRoom) getLastMessage().getParameters().get(0));
		// true
		request(STRICT, robotCtrl, rManager, "robotCleanedRoom", dirtyableRoom.getBinding());
		request(STRICT, rManager, rManager, "REMOVEDirtyRooms", dirtyableRoom.getBinding());
		waitFor(robotCtrl, robot, "moveToAdjacentRoom", RANDOM);
	}

}
