package vacuumrobotspecification.collaborations.vacuumrobotcollaboration1;

import sbp.specification.scenarios.violations.Violation;
import sbp.specification.events.Message;

// Collaboration
import vacuumrobotspecification.collaborations.vacuumrobotcollaboration1.VacuumrobotCollaboration1Collaboration;

// Roles
import vacuumrobot.VacuumRobotController;

@SuppressWarnings("serial")
public class VacuumrobotCollaboration1_RobotManagerOrdersRobotToCleanRoomScenario
		extends VacuumrobotCollaboration1Collaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(rManager, rManager, "orderRobotToCleanRoom", RANDOM);
		setBlocked(rManager, robotCtrl, "startMoving");
	}

	@Override
	protected void initialisation() {
		// []
		addInitializingMessage(new Message(rManager, rManager, "orderRobotToCleanRoom", RANDOM));
		// true
		// 1
	}

	@Override
	protected void registerRoleBindings() {
	}

	@Override
	protected void body() throws Violation {
		robotCtrl.setBinding((VacuumRobotController) getLastMessage().getParameters().get(0));
		// true
		request(STRICT, rManager, robotCtrl, "startMoving");
	}

}
