package vacuumrobotspecification.collaborations.vacuumrobotcollaboration1;

import java.util.ArrayList;
import java.util.List;

import sbp.specification.events.Message;
import sbp.specification.scenarios.violations.Violation;
import vacuumrobot.Room;
// Roles
import vacuumrobot.VacuumRobot;

@SuppressWarnings("serial")
public class VacuumrobotCollaboration1_MoveRobotWhenDirtDetectedScenario
		extends VacuumrobotCollaboration1Collaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(rManager, robotCtrl, "startMoving");
		//TODO: any in parameter
		//setBlocked(robotCtrl, robot, "moveToAdjacentRoom", currentRoom.getBinding().getAdjacentRooms().get(0));
		setBlocked(robotCtrl, robot, "moveToAdjacentRoom", RANDOM);
		setBlocked(robotCtrl, robotCtrl, "SETMoving", true);
	}

	@Override
	protected void initialisation() {
		// []
		addInitializingMessage(new Message(rManager, robotCtrl, "startMoving"));
	}

	@Override
	protected void registerRoleBindings() {
		bindRoleToObject(robot, (VacuumRobot) robotCtrl.getBinding().getRobot());
	}

	@Override
	protected void body() throws Violation {
		//true
		//TODO TypedVariableDeclaration
		//org.scenariotools.sml.expressions.scenarioExpressions.impl.TypedVariableDeclarationImpl@52785d6d (name: currentRoom)
		Room currentRoom = robotCtrl.getBinding().getRoom();
		//TODO TypedVariableDeclaration usage
		//TODO any => alternative
		/*request(STRICT, robotCtrl, robot, "moveToAdjacentRoom", currentRoom
		.getBinding().getAdjacentRooms()
		.get(0)
		);*/
		List<Message> requestedMessages = new ArrayList<Message>();
		List<Message> waitedForMessages = new ArrayList<Message>();
		//for(int index = 0; index < currentRoom.getAdjacentRooms().size(); index++) {
			//requestedMessages.add(new Message(STRICT, robotCtrl, robot, "moveToAdjacentRoom", currentRoom.getAdjacentRooms().get(index)));
		requestedMessages.add(new Message(STRICT, robotCtrl, robot, "moveToAdjacentRoom", RANDOM));
		//}
		doStep(requestedMessages, waitedForMessages);
		request(STRICT, robotCtrl, robotCtrl, "SETMoving", true
		);
	}

}
