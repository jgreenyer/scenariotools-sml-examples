package vacuumrobotspecification.collaborations.vacuumrobotcollaboration1;

import sbp.specification.scenarios.violations.Violation;
import sbp.specification.events.Message;

// Collaboration
import vacuumrobotspecification.collaborations.vacuumrobotcollaboration1.VacuumrobotCollaboration1Collaboration;

// Roles

@SuppressWarnings("serial")
public class VacuumrobotCollaboration1_RobotMustNotRunOutOfChargeScenario
		extends VacuumrobotCollaboration1Collaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(robot, robotCtrl, "SETCharge", RANDOM);
	}

	@Override
	protected void initialisation() {
		// []
		addInitializingMessage(new Message(robot, robotCtrl, "SETCharge", RANDOM));
		// 1
	}

	@Override
	protected void registerRoleBindings() {
	}

	@Override
	protected void body() throws Violation {
		// true
		if ((robotCtrl.getBinding().getCharge() <= 0)) {
			throwViolation(SAFETY);
		}
	}

}
