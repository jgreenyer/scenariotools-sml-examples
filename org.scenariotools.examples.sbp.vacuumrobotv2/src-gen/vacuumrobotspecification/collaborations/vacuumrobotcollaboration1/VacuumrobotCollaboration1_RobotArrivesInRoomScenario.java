package vacuumrobotspecification.collaborations.vacuumrobotcollaboration1;

import sbp.specification.scenarios.violations.Violation;
import sbp.specification.events.Message;

// Collaboration
import vacuumrobotspecification.collaborations.vacuumrobotcollaboration1.VacuumrobotCollaboration1Collaboration;

// Roles
import vacuumrobot.Room;

@SuppressWarnings("serial")
public class VacuumrobotCollaboration1_RobotArrivesInRoomScenario extends VacuumrobotCollaboration1Collaboration {

	@Override
	protected void registerAlphabet() {
		setBlocked(robotCtrl, robot, "moveToAdjacentRoom", RANDOM);
		//TODO : parameter provided by inline binding
		//setBlocked(robot, robotCtrl, "arrivedInRoom", room.getBinding());
		setBlocked(robot, robotCtrl, "arrivedInRoom", RANDOM);
		//TODO : parameter provided by inline binding
		//setBlocked(robotCtrl, robotCtrl, "SETRoom", room.getBinding());
		setBlocked(robotCtrl, robotCtrl, "SETRoom", RANDOM);
	}

	@Override
	protected void initialisation() {
		// []
		addInitializingMessage(new Message(robotCtrl, robot, "moveToAdjacentRoom", RANDOM));
		// true
		// 1
	}

	@Override
	protected void registerRoleBindings() {
	}

	@Override
	protected void body() throws Violation {
		room.setBinding((Room) getLastMessage().getParameters().get(0));
		//true
		//TODO : TypedVariableDeclaration
		//org.scenariotools.sml.expressions.scenarioExpressions.impl.TypedVariableDeclarationImpl@752e6e0 (name: currentRoom)
		Room currentRoom = robotCtrl.getBinding().getRoom();
		//TODO: TypedVariableDeclaration currentRoom usage
		if (! currentRoom
		//.getBinding().getAdjacentRooms()
		.getAdjacentRooms()
		.contains(room.getBinding()
		)
		) {
			throwViolation(INTERRUPT);
		}
		//TODO : request instead of waitFor
		//request(robot, robotCtrl, "arrivedInRoom", room.getBinding());
		waitFor(robot, robotCtrl, "arrivedInRoom", room.getBinding());
		request(STRICT, robotCtrl, robotCtrl, "SETRoom", room.getBinding()
		);
	}

}
