/**
 */
package vacuumrobot;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vacuum Robot</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link vacuumrobot.VacuumRobot#getController <em>Controller</em>}</li>
 *   <li>{@link vacuumrobot.VacuumRobot#getMaxCharge <em>Max Charge</em>}</li>
 * </ul>
 *
 * @see vacuumrobot.VacuumrobotPackage#getVacuumRobot()
 * @model
 * @generated
 */
public interface VacuumRobot extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Controller</b></em>' containment reference.
	 * It is bidirectional and its opposite is '{@link vacuumrobot.VacuumRobotController#getRobot <em>Robot</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Controller</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Controller</em>' containment reference.
	 * @see #setController(VacuumRobotController)
	 * @see vacuumrobot.VacuumrobotPackage#getVacuumRobot_Controller()
	 * @see vacuumrobot.VacuumRobotController#getRobot
	 * @model opposite="robot" containment="true" required="true"
	 * @generated
	 */
	VacuumRobotController getController();

	/**
	 * Sets the value of the '{@link vacuumrobot.VacuumRobot#getController <em>Controller</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Controller</em>' containment reference.
	 * @see #getController()
	 * @generated
	 */
	void setController(VacuumRobotController value);

	/**
	 * Returns the value of the '<em><b>Max Charge</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Charge</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Charge</em>' attribute.
	 * @see #setMaxCharge(int)
	 * @see vacuumrobot.VacuumrobotPackage#getVacuumRobot_MaxCharge()
	 * @model
	 * @generated
	 */
	int getMaxCharge();

	/**
	 * Sets the value of the '{@link vacuumrobot.VacuumRobot#getMaxCharge <em>Max Charge</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Charge</em>' attribute.
	 * @see #getMaxCharge()
	 * @generated
	 */
	void setMaxCharge(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void moveToAdjacentRoom(Room room);

} // VacuumRobot
