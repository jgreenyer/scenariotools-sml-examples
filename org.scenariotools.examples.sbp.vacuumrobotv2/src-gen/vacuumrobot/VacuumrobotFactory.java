/**
 */
package vacuumrobot;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see vacuumrobot.VacuumrobotPackage
 * @generated
 */
public interface VacuumrobotFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	VacuumrobotFactory eINSTANCE = vacuumrobot.impl.VacuumrobotFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Vacuum Robot System</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vacuum Robot System</em>'.
	 * @generated
	 */
	VacuumRobotSystem createVacuumRobotSystem();

	/**
	 * Returns a new object of class '<em>Room</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Room</em>'.
	 * @generated
	 */
	Room createRoom();

	/**
	 * Returns a new object of class '<em>Vacuum Robot</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vacuum Robot</em>'.
	 * @generated
	 */
	VacuumRobot createVacuumRobot();

	/**
	 * Returns a new object of class '<em>Vacuum Robot Controller</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vacuum Robot Controller</em>'.
	 * @generated
	 */
	VacuumRobotController createVacuumRobotController();

	/**
	 * Returns a new object of class '<em>Vacuum Robot Manager</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Vacuum Robot Manager</em>'.
	 * @generated
	 */
	VacuumRobotManager createVacuumRobotManager();

	/**
	 * Returns a new object of class '<em>Dirtyable Room</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dirtyable Room</em>'.
	 * @generated
	 */
	DirtyableRoom createDirtyableRoom();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	VacuumrobotPackage getVacuumrobotPackage();

} //VacuumrobotFactory
