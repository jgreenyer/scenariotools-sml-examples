/**
 */
package vacuumrobot;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Room</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link vacuumrobot.Room#isHasChargingStation <em>Has Charging Station</em>}</li>
 *   <li>{@link vacuumrobot.Room#getAdjacentRooms <em>Adjacent Rooms</em>}</li>
 * </ul>
 *
 * @see vacuumrobot.VacuumrobotPackage#getRoom()
 * @model
 * @generated
 */
public interface Room extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Has Charging Station</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Charging Station</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Charging Station</em>' attribute.
	 * @see #setHasChargingStation(boolean)
	 * @see vacuumrobot.VacuumrobotPackage#getRoom_HasChargingStation()
	 * @model
	 * @generated
	 */
	boolean isHasChargingStation();

	/**
	 * Sets the value of the '{@link vacuumrobot.Room#isHasChargingStation <em>Has Charging Station</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Charging Station</em>' attribute.
	 * @see #isHasChargingStation()
	 * @generated
	 */
	void setHasChargingStation(boolean value);

	/**
	 * Returns the value of the '<em><b>Adjacent Rooms</b></em>' reference list.
	 * The list contents are of type {@link vacuumrobot.Room}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Adjacent Rooms</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Adjacent Rooms</em>' reference list.
	 * @see vacuumrobot.VacuumrobotPackage#getRoom_AdjacentRooms()
	 * @model
	 * @generated
	 */
	EList<Room> getAdjacentRooms();

} // Room
