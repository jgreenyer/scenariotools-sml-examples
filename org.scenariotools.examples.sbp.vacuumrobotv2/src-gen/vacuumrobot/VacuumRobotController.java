/**
 */
package vacuumrobot;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vacuum Robot Controller</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link vacuumrobot.VacuumRobotController#getRobot <em>Robot</em>}</li>
 *   <li>{@link vacuumrobot.VacuumRobotController#isMoving <em>Moving</em>}</li>
 *   <li>{@link vacuumrobot.VacuumRobotController#getRoom <em>Room</em>}</li>
 *   <li>{@link vacuumrobot.VacuumRobotController#getCharge <em>Charge</em>}</li>
 * </ul>
 *
 * @see vacuumrobot.VacuumrobotPackage#getVacuumRobotController()
 * @model
 * @generated
 */
public interface VacuumRobotController extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Robot</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link vacuumrobot.VacuumRobot#getController <em>Controller</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Robot</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Robot</em>' container reference.
	 * @see #setRobot(VacuumRobot)
	 * @see vacuumrobot.VacuumrobotPackage#getVacuumRobotController_Robot()
	 * @see vacuumrobot.VacuumRobot#getController
	 * @model opposite="controller" required="true" transient="false"
	 * @generated
	 */
	VacuumRobot getRobot();

	/**
	 * Sets the value of the '{@link vacuumrobot.VacuumRobotController#getRobot <em>Robot</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Robot</em>' container reference.
	 * @see #getRobot()
	 * @generated
	 */
	void setRobot(VacuumRobot value);

	/**
	 * Returns the value of the '<em><b>Moving</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Moving</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Moving</em>' attribute.
	 * @see #setMoving(boolean)
	 * @see vacuumrobot.VacuumrobotPackage#getVacuumRobotController_Moving()
	 * @model
	 * @generated
	 */
	boolean isMoving();

	/**
	 * Sets the value of the '{@link vacuumrobot.VacuumRobotController#isMoving <em>Moving</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Moving</em>' attribute.
	 * @see #isMoving()
	 * @generated
	 */
	void setMoving(boolean value);

	/**
	 * Returns the value of the '<em><b>Room</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room</em>' reference.
	 * @see #setRoom(Room)
	 * @see vacuumrobot.VacuumrobotPackage#getVacuumRobotController_Room()
	 * @model
	 * @generated
	 */
	Room getRoom();

	/**
	 * Sets the value of the '{@link vacuumrobot.VacuumRobotController#getRoom <em>Room</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room</em>' reference.
	 * @see #getRoom()
	 * @generated
	 */
	void setRoom(Room value);

	/**
	 * Returns the value of the '<em><b>Charge</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Charge</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Charge</em>' attribute.
	 * @see #setCharge(int)
	 * @see vacuumrobot.VacuumrobotPackage#getVacuumRobotController_Charge()
	 * @model
	 * @generated
	 */
	int getCharge();

	/**
	 * Sets the value of the '{@link vacuumrobot.VacuumRobotController#getCharge <em>Charge</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Charge</em>' attribute.
	 * @see #getCharge()
	 * @generated
	 */
	void setCharge(int value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void arrivedInRoom(Room room);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void startMoving();

} // VacuumRobotController
