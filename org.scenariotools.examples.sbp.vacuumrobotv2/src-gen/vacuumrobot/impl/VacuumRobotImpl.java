/**
 */
package vacuumrobot.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import vacuumrobot.Room;
import vacuumrobot.VacuumRobot;
import vacuumrobot.VacuumRobotController;
import vacuumrobot.VacuumrobotPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Vacuum Robot</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link vacuumrobot.impl.VacuumRobotImpl#getController <em>Controller</em>}</li>
 *   <li>{@link vacuumrobot.impl.VacuumRobotImpl#getMaxCharge <em>Max Charge</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VacuumRobotImpl extends NamedElementImpl implements VacuumRobot {
	/**
	 * The cached value of the '{@link #getController() <em>Controller</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getController()
	 * @generated
	 * @ordered
	 */
	protected VacuumRobotController controller;

	/**
	 * The default value of the '{@link #getMaxCharge() <em>Max Charge</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxCharge()
	 * @generated
	 * @ordered
	 */
	protected static final int MAX_CHARGE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMaxCharge() <em>Max Charge</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxCharge()
	 * @generated
	 * @ordered
	 */
	protected int maxCharge = MAX_CHARGE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VacuumRobotImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VacuumrobotPackage.Literals.VACUUM_ROBOT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VacuumRobotController getController() {
		return controller;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetController(VacuumRobotController newController, NotificationChain msgs) {
		VacuumRobotController oldController = controller;
		controller = newController;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VacuumrobotPackage.VACUUM_ROBOT__CONTROLLER, oldController, newController);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setController(VacuumRobotController newController) {
		if (newController != controller) {
			NotificationChain msgs = null;
			if (controller != null)
				msgs = ((InternalEObject)controller).eInverseRemove(this, VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__ROBOT, VacuumRobotController.class, msgs);
			if (newController != null)
				msgs = ((InternalEObject)newController).eInverseAdd(this, VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__ROBOT, VacuumRobotController.class, msgs);
			msgs = basicSetController(newController, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VacuumrobotPackage.VACUUM_ROBOT__CONTROLLER, newController, newController));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMaxCharge() {
		return maxCharge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxCharge(int newMaxCharge) {
		int oldMaxCharge = maxCharge;
		maxCharge = newMaxCharge;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VacuumrobotPackage.VACUUM_ROBOT__MAX_CHARGE, oldMaxCharge, maxCharge));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void moveToAdjacentRoom(Room room) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VacuumrobotPackage.VACUUM_ROBOT__CONTROLLER:
				if (controller != null)
					msgs = ((InternalEObject)controller).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VacuumrobotPackage.VACUUM_ROBOT__CONTROLLER, null, msgs);
				return basicSetController((VacuumRobotController)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VacuumrobotPackage.VACUUM_ROBOT__CONTROLLER:
				return basicSetController(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VacuumrobotPackage.VACUUM_ROBOT__CONTROLLER:
				return getController();
			case VacuumrobotPackage.VACUUM_ROBOT__MAX_CHARGE:
				return getMaxCharge();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VacuumrobotPackage.VACUUM_ROBOT__CONTROLLER:
				setController((VacuumRobotController)newValue);
				return;
			case VacuumrobotPackage.VACUUM_ROBOT__MAX_CHARGE:
				setMaxCharge((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VacuumrobotPackage.VACUUM_ROBOT__CONTROLLER:
				setController((VacuumRobotController)null);
				return;
			case VacuumrobotPackage.VACUUM_ROBOT__MAX_CHARGE:
				setMaxCharge(MAX_CHARGE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VacuumrobotPackage.VACUUM_ROBOT__CONTROLLER:
				return controller != null;
			case VacuumrobotPackage.VACUUM_ROBOT__MAX_CHARGE:
				return maxCharge != MAX_CHARGE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case VacuumrobotPackage.VACUUM_ROBOT___MOVE_TO_ADJACENT_ROOM__ROOM:
				moveToAdjacentRoom((Room)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (maxCharge: ");
		result.append(maxCharge);
		result.append(')');
		return result.toString();
	}

} //VacuumRobotImpl
