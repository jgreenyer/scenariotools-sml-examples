/**
 */
package vacuumrobot.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import vacuumrobot.DirtyableRoom;
import vacuumrobot.NamedElement;
import vacuumrobot.Room;
import vacuumrobot.VacuumRobot;
import vacuumrobot.VacuumRobotController;
import vacuumrobot.VacuumRobotManager;
import vacuumrobot.VacuumRobotSystem;
import vacuumrobot.VacuumrobotFactory;
import vacuumrobot.VacuumrobotPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class VacuumrobotPackageImpl extends EPackageImpl implements VacuumrobotPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vacuumRobotSystemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vacuumRobotEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vacuumRobotControllerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vacuumRobotManagerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dirtyableRoomEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see vacuumrobot.VacuumrobotPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private VacuumrobotPackageImpl() {
		super(eNS_URI, VacuumrobotFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link VacuumrobotPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static VacuumrobotPackage init() {
		if (isInited) return (VacuumrobotPackage)EPackage.Registry.INSTANCE.getEPackage(VacuumrobotPackage.eNS_URI);

		// Obtain or create and register package
		VacuumrobotPackageImpl theVacuumrobotPackage = (VacuumrobotPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof VacuumrobotPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new VacuumrobotPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theVacuumrobotPackage.createPackageContents();

		// Initialize created meta-data
		theVacuumrobotPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theVacuumrobotPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(VacuumrobotPackage.eNS_URI, theVacuumrobotPackage);
		return theVacuumrobotPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamedElement() {
		return namedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedElement_Name() {
		return (EAttribute)namedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVacuumRobotSystem() {
		return vacuumRobotSystemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVacuumRobotSystem_Room() {
		return (EReference)vacuumRobotSystemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVacuumRobotSystem_VacuumRobot() {
		return (EReference)vacuumRobotSystemEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVacuumRobotSystem_VacuumRobotManager() {
		return (EReference)vacuumRobotSystemEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoom() {
		return roomEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoom_HasChargingStation() {
		return (EAttribute)roomEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoom_AdjacentRooms() {
		return (EReference)roomEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVacuumRobot() {
		return vacuumRobotEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVacuumRobot_Controller() {
		return (EReference)vacuumRobotEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVacuumRobot_MaxCharge() {
		return (EAttribute)vacuumRobotEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getVacuumRobot__MoveToAdjacentRoom__Room() {
		return vacuumRobotEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVacuumRobotController() {
		return vacuumRobotControllerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVacuumRobotController_Robot() {
		return (EReference)vacuumRobotControllerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVacuumRobotController_Moving() {
		return (EAttribute)vacuumRobotControllerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVacuumRobotController_Room() {
		return (EReference)vacuumRobotControllerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVacuumRobotController_Charge() {
		return (EAttribute)vacuumRobotControllerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getVacuumRobotController__ArrivedInRoom__Room() {
		return vacuumRobotControllerEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getVacuumRobotController__StartMoving() {
		return vacuumRobotControllerEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVacuumRobotManager() {
		return vacuumRobotManagerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVacuumRobotManager_VacuumRobotControllers() {
		return (EReference)vacuumRobotManagerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVacuumRobotManager_DirtyRooms() {
		return (EReference)vacuumRobotManagerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getVacuumRobotManager__DirtDetected() {
		return vacuumRobotManagerEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getVacuumRobotManager__RobotCleanedRoom__DirtyableRoom() {
		return vacuumRobotManagerEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getVacuumRobotManager__OrderRobotToCleanRoom__VacuumRobotController() {
		return vacuumRobotManagerEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDirtyableRoom() {
		return dirtyableRoomEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VacuumrobotFactory getVacuumrobotFactory() {
		return (VacuumrobotFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		namedElementEClass = createEClass(NAMED_ELEMENT);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__NAME);

		vacuumRobotSystemEClass = createEClass(VACUUM_ROBOT_SYSTEM);
		createEReference(vacuumRobotSystemEClass, VACUUM_ROBOT_SYSTEM__ROOM);
		createEReference(vacuumRobotSystemEClass, VACUUM_ROBOT_SYSTEM__VACUUM_ROBOT);
		createEReference(vacuumRobotSystemEClass, VACUUM_ROBOT_SYSTEM__VACUUM_ROBOT_MANAGER);

		roomEClass = createEClass(ROOM);
		createEAttribute(roomEClass, ROOM__HAS_CHARGING_STATION);
		createEReference(roomEClass, ROOM__ADJACENT_ROOMS);

		vacuumRobotEClass = createEClass(VACUUM_ROBOT);
		createEReference(vacuumRobotEClass, VACUUM_ROBOT__CONTROLLER);
		createEAttribute(vacuumRobotEClass, VACUUM_ROBOT__MAX_CHARGE);
		createEOperation(vacuumRobotEClass, VACUUM_ROBOT___MOVE_TO_ADJACENT_ROOM__ROOM);

		vacuumRobotControllerEClass = createEClass(VACUUM_ROBOT_CONTROLLER);
		createEReference(vacuumRobotControllerEClass, VACUUM_ROBOT_CONTROLLER__ROBOT);
		createEAttribute(vacuumRobotControllerEClass, VACUUM_ROBOT_CONTROLLER__MOVING);
		createEReference(vacuumRobotControllerEClass, VACUUM_ROBOT_CONTROLLER__ROOM);
		createEAttribute(vacuumRobotControllerEClass, VACUUM_ROBOT_CONTROLLER__CHARGE);
		createEOperation(vacuumRobotControllerEClass, VACUUM_ROBOT_CONTROLLER___ARRIVED_IN_ROOM__ROOM);
		createEOperation(vacuumRobotControllerEClass, VACUUM_ROBOT_CONTROLLER___START_MOVING);

		vacuumRobotManagerEClass = createEClass(VACUUM_ROBOT_MANAGER);
		createEReference(vacuumRobotManagerEClass, VACUUM_ROBOT_MANAGER__VACUUM_ROBOT_CONTROLLERS);
		createEReference(vacuumRobotManagerEClass, VACUUM_ROBOT_MANAGER__DIRTY_ROOMS);
		createEOperation(vacuumRobotManagerEClass, VACUUM_ROBOT_MANAGER___DIRT_DETECTED);
		createEOperation(vacuumRobotManagerEClass, VACUUM_ROBOT_MANAGER___ROBOT_CLEANED_ROOM__DIRTYABLEROOM);
		createEOperation(vacuumRobotManagerEClass, VACUUM_ROBOT_MANAGER___ORDER_ROBOT_TO_CLEAN_ROOM__VACUUMROBOTCONTROLLER);

		dirtyableRoomEClass = createEClass(DIRTYABLE_ROOM);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		vacuumRobotSystemEClass.getESuperTypes().add(this.getNamedElement());
		roomEClass.getESuperTypes().add(this.getNamedElement());
		vacuumRobotEClass.getESuperTypes().add(this.getNamedElement());
		vacuumRobotControllerEClass.getESuperTypes().add(this.getNamedElement());
		vacuumRobotManagerEClass.getESuperTypes().add(this.getNamedElement());
		dirtyableRoomEClass.getESuperTypes().add(this.getRoom());

		// Initialize classes, features, and operations; add parameters
		initEClass(namedElementEClass, NamedElement.class, "NamedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamedElement_Name(), ecorePackage.getEString(), "name", null, 0, 1, NamedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(vacuumRobotSystemEClass, VacuumRobotSystem.class, "VacuumRobotSystem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVacuumRobotSystem_Room(), this.getRoom(), null, "room", null, 1, -1, VacuumRobotSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getVacuumRobotSystem_VacuumRobot(), this.getVacuumRobot(), null, "vacuumRobot", null, 1, -1, VacuumRobotSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getVacuumRobotSystem_VacuumRobotManager(), this.getVacuumRobotManager(), null, "vacuumRobotManager", null, 1, 1, VacuumRobotSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(roomEClass, Room.class, "Room", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRoom_HasChargingStation(), ecorePackage.getEBoolean(), "hasChargingStation", null, 0, 1, Room.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRoom_AdjacentRooms(), this.getRoom(), null, "adjacentRooms", null, 0, -1, Room.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(vacuumRobotEClass, VacuumRobot.class, "VacuumRobot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVacuumRobot_Controller(), this.getVacuumRobotController(), this.getVacuumRobotController_Robot(), "controller", null, 1, 1, VacuumRobot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVacuumRobot_MaxCharge(), ecorePackage.getEInt(), "maxCharge", null, 0, 1, VacuumRobot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getVacuumRobot__MoveToAdjacentRoom__Room(), null, "moveToAdjacentRoom", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getRoom(), "room", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(vacuumRobotControllerEClass, VacuumRobotController.class, "VacuumRobotController", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVacuumRobotController_Robot(), this.getVacuumRobot(), this.getVacuumRobot_Controller(), "robot", null, 1, 1, VacuumRobotController.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVacuumRobotController_Moving(), ecorePackage.getEBoolean(), "moving", null, 0, 1, VacuumRobotController.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getVacuumRobotController_Room(), this.getRoom(), null, "room", null, 0, 1, VacuumRobotController.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVacuumRobotController_Charge(), ecorePackage.getEInt(), "charge", null, 0, 1, VacuumRobotController.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getVacuumRobotController__ArrivedInRoom__Room(), null, "arrivedInRoom", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getRoom(), "room", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEOperation(getVacuumRobotController__StartMoving(), null, "startMoving", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(vacuumRobotManagerEClass, VacuumRobotManager.class, "VacuumRobotManager", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVacuumRobotManager_VacuumRobotControllers(), this.getVacuumRobotController(), null, "vacuumRobotControllers", null, 0, -1, VacuumRobotManager.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getVacuumRobotManager_DirtyRooms(), this.getDirtyableRoom(), null, "dirtyRooms", null, 0, -1, VacuumRobotManager.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEOperation(getVacuumRobotManager__DirtDetected(), null, "dirtDetected", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getVacuumRobotManager__RobotCleanedRoom__DirtyableRoom(), null, "robotCleanedRoom", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getDirtyableRoom(), "dirtyableRoom", 0, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getVacuumRobotManager__OrderRobotToCleanRoom__VacuumRobotController(), null, "orderRobotToCleanRoom", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVacuumRobotController(), "vacuumRobotController", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(dirtyableRoomEClass, DirtyableRoom.class, "DirtyableRoom", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //VacuumrobotPackageImpl
