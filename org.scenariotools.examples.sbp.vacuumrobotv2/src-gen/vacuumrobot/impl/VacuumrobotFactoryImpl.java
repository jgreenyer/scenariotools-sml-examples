/**
 */
package vacuumrobot.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import vacuumrobot.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class VacuumrobotFactoryImpl extends EFactoryImpl implements VacuumrobotFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static VacuumrobotFactory init() {
		try {
			VacuumrobotFactory theVacuumrobotFactory = (VacuumrobotFactory)EPackage.Registry.INSTANCE.getEFactory(VacuumrobotPackage.eNS_URI);
			if (theVacuumrobotFactory != null) {
				return theVacuumrobotFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new VacuumrobotFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VacuumrobotFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case VacuumrobotPackage.VACUUM_ROBOT_SYSTEM: return createVacuumRobotSystem();
			case VacuumrobotPackage.ROOM: return createRoom();
			case VacuumrobotPackage.VACUUM_ROBOT: return createVacuumRobot();
			case VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER: return createVacuumRobotController();
			case VacuumrobotPackage.VACUUM_ROBOT_MANAGER: return createVacuumRobotManager();
			case VacuumrobotPackage.DIRTYABLE_ROOM: return createDirtyableRoom();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VacuumRobotSystem createVacuumRobotSystem() {
		VacuumRobotSystemImpl vacuumRobotSystem = new VacuumRobotSystemImpl();
		return vacuumRobotSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Room createRoom() {
		RoomImpl room = new RoomImpl();
		return room;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VacuumRobot createVacuumRobot() {
		VacuumRobotImpl vacuumRobot = new VacuumRobotImpl();
		return vacuumRobot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VacuumRobotController createVacuumRobotController() {
		VacuumRobotControllerImpl vacuumRobotController = new VacuumRobotControllerImpl();
		return vacuumRobotController;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VacuumRobotManager createVacuumRobotManager() {
		VacuumRobotManagerImpl vacuumRobotManager = new VacuumRobotManagerImpl();
		return vacuumRobotManager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DirtyableRoom createDirtyableRoom() {
		DirtyableRoomImpl dirtyableRoom = new DirtyableRoomImpl();
		return dirtyableRoom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VacuumrobotPackage getVacuumrobotPackage() {
		return (VacuumrobotPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static VacuumrobotPackage getPackage() {
		return VacuumrobotPackage.eINSTANCE;
	}

} //VacuumrobotFactoryImpl
