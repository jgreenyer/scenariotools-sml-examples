/**
 */
package vacuumrobot.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import vacuumrobot.Room;
import vacuumrobot.VacuumRobot;
import vacuumrobot.VacuumRobotController;
import vacuumrobot.VacuumrobotPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Vacuum Robot Controller</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link vacuumrobot.impl.VacuumRobotControllerImpl#getRobot <em>Robot</em>}</li>
 *   <li>{@link vacuumrobot.impl.VacuumRobotControllerImpl#isMoving <em>Moving</em>}</li>
 *   <li>{@link vacuumrobot.impl.VacuumRobotControllerImpl#getRoom <em>Room</em>}</li>
 *   <li>{@link vacuumrobot.impl.VacuumRobotControllerImpl#getCharge <em>Charge</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VacuumRobotControllerImpl extends NamedElementImpl implements VacuumRobotController {
	/**
	 * The default value of the '{@link #isMoving() <em>Moving</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isMoving()
	 * @generated
	 * @ordered
	 */
	protected static final boolean MOVING_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isMoving() <em>Moving</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isMoving()
	 * @generated
	 * @ordered
	 */
	protected boolean moving = MOVING_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRoom() <em>Room</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoom()
	 * @generated
	 * @ordered
	 */
	protected Room room;

	/**
	 * The default value of the '{@link #getCharge() <em>Charge</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCharge()
	 * @generated
	 * @ordered
	 */
	protected static final int CHARGE_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getCharge() <em>Charge</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCharge()
	 * @generated
	 * @ordered
	 */
	protected int charge = CHARGE_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VacuumRobotControllerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VacuumrobotPackage.Literals.VACUUM_ROBOT_CONTROLLER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VacuumRobot getRobot() {
		if (eContainerFeatureID() != VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__ROBOT) return null;
		return (VacuumRobot)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRobot(VacuumRobot newRobot, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newRobot, VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__ROBOT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRobot(VacuumRobot newRobot) {
		if (newRobot != eInternalContainer() || (eContainerFeatureID() != VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__ROBOT && newRobot != null)) {
			if (EcoreUtil.isAncestor(this, newRobot))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newRobot != null)
				msgs = ((InternalEObject)newRobot).eInverseAdd(this, VacuumrobotPackage.VACUUM_ROBOT__CONTROLLER, VacuumRobot.class, msgs);
			msgs = basicSetRobot(newRobot, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__ROBOT, newRobot, newRobot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isMoving() {
		return moving;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMoving(boolean newMoving) {
		boolean oldMoving = moving;
		moving = newMoving;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__MOVING, oldMoving, moving));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Room getRoom() {
		if (room != null && room.eIsProxy()) {
			InternalEObject oldRoom = (InternalEObject)room;
			room = (Room)eResolveProxy(oldRoom);
			if (room != oldRoom) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__ROOM, oldRoom, room));
			}
		}
		return room;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Room basicGetRoom() {
		return room;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoom(Room newRoom) {
		Room oldRoom = room;
		room = newRoom;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__ROOM, oldRoom, room));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getCharge() {
		return charge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCharge(int newCharge) {
		int oldCharge = charge;
		charge = newCharge;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__CHARGE, oldCharge, charge));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void arrivedInRoom(Room room) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void startMoving() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__ROBOT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetRobot((VacuumRobot)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__ROBOT:
				return basicSetRobot(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__ROBOT:
				return eInternalContainer().eInverseRemove(this, VacuumrobotPackage.VACUUM_ROBOT__CONTROLLER, VacuumRobot.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__ROBOT:
				return getRobot();
			case VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__MOVING:
				return isMoving();
			case VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__ROOM:
				if (resolve) return getRoom();
				return basicGetRoom();
			case VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__CHARGE:
				return getCharge();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__ROBOT:
				setRobot((VacuumRobot)newValue);
				return;
			case VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__MOVING:
				setMoving((Boolean)newValue);
				return;
			case VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__ROOM:
				setRoom((Room)newValue);
				return;
			case VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__CHARGE:
				setCharge((Integer)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__ROBOT:
				setRobot((VacuumRobot)null);
				return;
			case VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__MOVING:
				setMoving(MOVING_EDEFAULT);
				return;
			case VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__ROOM:
				setRoom((Room)null);
				return;
			case VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__CHARGE:
				setCharge(CHARGE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__ROBOT:
				return getRobot() != null;
			case VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__MOVING:
				return moving != MOVING_EDEFAULT;
			case VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__ROOM:
				return room != null;
			case VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER__CHARGE:
				return charge != CHARGE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER___ARRIVED_IN_ROOM__ROOM:
				arrivedInRoom((Room)arguments.get(0));
				return null;
			case VacuumrobotPackage.VACUUM_ROBOT_CONTROLLER___START_MOVING:
				startMoving();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (moving: ");
		result.append(moving);
		result.append(", charge: ");
		result.append(charge);
		result.append(')');
		return result.toString();
	}

} //VacuumRobotControllerImpl
