/**
 */
package vacuumrobot;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dirtyable Room</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see vacuumrobot.VacuumrobotPackage#getDirtyableRoom()
 * @model
 * @generated
 */
public interface DirtyableRoom extends Room {
} // DirtyableRoom
