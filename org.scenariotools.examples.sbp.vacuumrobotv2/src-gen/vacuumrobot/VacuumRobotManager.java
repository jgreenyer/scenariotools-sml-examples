/**
 */
package vacuumrobot;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vacuum Robot Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link vacuumrobot.VacuumRobotManager#getVacuumRobotControllers <em>Vacuum Robot Controllers</em>}</li>
 *   <li>{@link vacuumrobot.VacuumRobotManager#getDirtyRooms <em>Dirty Rooms</em>}</li>
 * </ul>
 *
 * @see vacuumrobot.VacuumrobotPackage#getVacuumRobotManager()
 * @model
 * @generated
 */
public interface VacuumRobotManager extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Vacuum Robot Controllers</b></em>' reference list.
	 * The list contents are of type {@link vacuumrobot.VacuumRobotController}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vacuum Robot Controllers</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vacuum Robot Controllers</em>' reference list.
	 * @see vacuumrobot.VacuumrobotPackage#getVacuumRobotManager_VacuumRobotControllers()
	 * @model
	 * @generated
	 */
	EList<VacuumRobotController> getVacuumRobotControllers();

	/**
	 * Returns the value of the '<em><b>Dirty Rooms</b></em>' reference list.
	 * The list contents are of type {@link vacuumrobot.DirtyableRoom}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dirty Rooms</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dirty Rooms</em>' reference list.
	 * @see vacuumrobot.VacuumrobotPackage#getVacuumRobotManager_DirtyRooms()
	 * @model ordered="false"
	 * @generated
	 */
	EList<DirtyableRoom> getDirtyRooms();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void dirtDetected();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void robotCleanedRoom(DirtyableRoom dirtyableRoom);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void orderRobotToCleanRoom(VacuumRobotController vacuumRobotController);

} // VacuumRobotManager
