import "vacuumrobot.ecore"

specification VacuumrobotSpecification {

	domain vacuumrobot
	
	controllable {
		VacuumRobotManager
		VacuumRobotController
	}
	
	non-spontaneous events {
		VacuumRobotController.setCharge
		VacuumRobotController.setRoom
	}
	
	collaboration VacuumrobotCollaboration1 {

		static role VacuumRobotManager rManager // receives dirtDetected sensor events, knows robots in the system, selects robot to clean room and orders that robot to start moving 
		dynamic role DirtyableRoom dirtyableRoom // room that can become dirty, acts the sensor that notifies the rManager when it gets dirty
		dynamic role Room room // room (passive), a robotCtrl maintains a pointer to room in which the robot is in.
		//dynamic role Room previousRoom // previous room in a scenario where the robot moves from the previous room to another (new) room.
		dynamic role VacuumRobot robot // a physical robot, 
				// represents the robot's sensors that
				// 1. detect when the robot arrives in a room: robot->robotCtrl.arrivedInRoom( ... )
				// 2. detect when the robot's battery charge changes: robot->robotCtrl.setCharge( ... )
				// also represents the robot's actuators for
				// 1. ordering the robot to move to an adjacent room: robotCtrl->robot.moveToAdjacentRoom( ... )
		dynamic role VacuumRobotController robotCtrl // the software controller(s) of a robot (/ robots) 

		guarantee scenario RobotManagerAddsDirtyRoomToList{
			dirtyableRoom->rManager.dirtDetected
			interrupt [rManager.dirtyRooms.contains(dirtyableRoom)]
			strict urgent rManager->rManager.dirtyRooms.add(dirtyableRoom)
			strict urgent rManager->rManager.orderRobotToCleanRoom(rManager.vacuumRobotControllers.first())
		}

		guarantee scenario RobotManagerOrdersRobotToCleanRoom{
			rManager->rManager.orderRobotToCleanRoom(bind robotCtrl)
			//interrupt [robotCtrl.moving]
			strict urgent rManager->robotCtrl.startMoving()
		}

		guarantee scenario EventuallyCleanDirtyRoom{
			rManager->rManager.dirtyRooms.add(bind dirtyableRoom)
			wait eventually [!rManager.dirtyRooms.contains(dirtyableRoom)]
		}

		guarantee scenario MoveRobotWhenDirtDetected 
		bindings [
			robot = robotCtrl.robot
		]{
			rManager->robotCtrl.startMoving()
			var Room currentRoom = robotCtrl.room
			strict urgent robotCtrl->robot.moveToAdjacentRoom(currentRoom.adjacentRooms.any())
			strict urgent robotCtrl->robotCtrl.setMoving(true)
		}

		guarantee scenario RobotMustNotRunOutOfCharge{
			robot->robotCtrl.setCharge(*)
			violation [robotCtrl.charge <= 0]
		}

		guarantee scenario RobotMustEventuallyRecharge{
			robot->robotCtrl.setCharge(*)
			wait eventually [robotCtrl.charge == robot.maxCharge]
		}

		guarantee scenario KeepMovingOrStopAtChargingStation {
			robot->robotCtrl.arrivedInRoom(bind room)
			alternative { 
			 	strict urgent robotCtrl->robot.moveToAdjacentRoom(room.adjacentRooms.any())
			} or [room.hasChargingStation]{
				strict urgent robotCtrl->robotCtrl.setMoving(false)
		 	} 
		}
		
		guarantee scenario RobotReportsRoomCleaned{
			robot->robotCtrl.arrivedInRoom(bind dirtyableRoom)
			strict urgent robotCtrl->rManager.robotCleanedRoom(dirtyableRoom)
			strict urgent rManager->rManager.dirtyRooms.remove(dirtyableRoom)
			robotCtrl->robot.moveToAdjacentRoom(*) // before ordering the next move
		}


		guarantee scenario RobotControllerSetsRoom{
			robot->robotCtrl.arrivedInRoom(bind room)
			strict requested robotCtrl->robotCtrl.setRoom(room)
			robotCtrl->robot.moveToAdjacentRoom(*) // before ordering the next move
		}
		 
		assumption scenario RobotArrivesInRoom {
			robotCtrl->robot.moveToAdjacentRoom(bind room)
			var Room currentRoom = robotCtrl.room
			interrupt [!currentRoom.adjacentRooms.contains(room)]
			eventually robot->robotCtrl.arrivedInRoom(room)
			strict committed robotCtrl->robotCtrl.setRoom(room)
		}

		assumption scenario RobotDischargesOrRecharges {
			robot->robotCtrl.arrivedInRoom(bind room)
			alternative [room.hasChargingStation]{
				strict committed robot->robotCtrl.setCharge(robot.maxCharge)
			} or [!room.hasChargingStation]{
				strict committed robot->robotCtrl.setCharge(robotCtrl.charge - 1)
			}
		}

// 		***replaced by guarantee scenario RobotReportsRoomCleaned 
//		assumption scenario RobotCleansDirtyRoom {
//			robot->robotCtrl.arrivedInRoom(bind dirtyableRoom)
//			alternative [dirtyableRoom.dirty]{
//				strict eventually env->dirtyableRoom.setDirty(false)
//			}
//		}

//		assumption scenario RoomCannotGetDirtyWhenRobotIsInIt
//		bindings [
//			robotCtrl = rManager.vacuumRobotControllers
//		]{
//			dirtyableRoom->rManager.dirtDetected
//			violation [robotCtrl.room == dirtyableRoom]
//		}
		 
//		assumption scenario DoNothing {
//			env->env.doNothing
//		}
		
	}

}
