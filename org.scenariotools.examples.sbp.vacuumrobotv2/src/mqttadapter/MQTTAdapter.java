// Platform-specific code BEGIN
package mqttadapter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import sbp.runtime.ObjectRegistry;
import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.adapter.AbstractRuntimeAdapter;
import sbp.runtime.settings.Settings;
import sbp.specification.events.Message;

public class MQTTAdapter extends AbstractRuntimeAdapter {
	private static final String BROKER_URL = "tcp://iot.eclipse.org:1883";
	private static final int QOS_LEVEL = 0; // avoid seting to 1, this implementation can't handle duplicate messages!
	private static final String TOPIC = "de.uni_hannover.se.org.scenariotools.examples.sbp.vacuumrobotv2";
	
	private String clientID;
	private MqttClient mqttClient;
	
	public MQTTAdapter(SpecificationRunconfig<?> specificationRunconfig, String clientID) {
		super(specificationRunconfig);
		this.clientID = TOPIC + "." + clientID + "/" + MqttClient.generateClientId();
	}
	
	@Override
	public void run() {
		try {
			Settings.NETWORK_OUT.println("MQTT broker URL: " + BROKER_URL);
			Settings.NETWORK_OUT.println("MQTT clientID: " + clientID);
			Settings.NETWORK_OUT.println("MQTT QoS level: " + QOS_LEVEL);		
			mqttClient = new MqttClient(BROKER_URL, clientID, new MemoryPersistence());
			mqttClient.setCallback(new MqttCallback() {
				@Override
				public void connectionLost(Throwable cause) {
					Settings.NETWORK_OUT.println("Connection lost: " + cause.getMessage());
					connectClients();
				}

				@Override
				public void deliveryComplete(IMqttDeliveryToken token) {
					Settings.NETWORK_OUT.println("Message delivery acknowledged");
				}

				@Override
				public void messageArrived(String topic, MqttMessage message) throws Exception {
					ByteArrayInputStream bais = new ByteArrayInputStream(message.getPayload());
					ObjectInputStream ois = new ObjectInputStream(bais);
					Message event = ((Message)ois.readObject()).deserializeForNetwork();		
					
					Settings.NETWORK_OUT.println("Received '" + event + "'");
					if(!ObjectRegistry.getInstance().isControllable(event.getSender().getBinding())) {
						Settings.NETWORK_OUT.println("Putting '" + event + "' into local queue");
						getEventQueueScenario().enqueueEvent(event);						
					}
				}
			});
			connectClients();
		} catch (MqttException e) {
			Settings.NETWORK_OUT.println("MQTT Exception during setup: " + e.getMessage());
		}		
	}

	@Override
	public void startServer() {
		// not necessary; that role is fulfilled by the broker
	}

	@Override
	public void connectClients() {
		try {
			Settings.NETWORK_OUT.println("Connecting to broker...");
			mqttClient.connect();
			Settings.NETWORK_OUT.println("Connected to broker");
			mqttClient.subscribe(TOPIC);
			Settings.NETWORK_OUT.println("Subscribed to topic '" + TOPIC + "'");		
		} catch (MqttException e) {
			Settings.NETWORK_OUT.println("MQTT Exception while connecting: " + e.getMessage());
		}
	}

	@Override
	public void publish(Message event) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		try {
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(event.serializeForNetwork());
			oos.flush();
		} catch (IOException e) {
			Settings.NETWORK_OUT.println("Exception during object serialization: " + e.getMessage());
		}
		try {
			Settings.NETWORK_OUT.println("Publishing '" + event + "'");
			mqttClient.publish(TOPIC, baos.toByteArray(), QOS_LEVEL, false);
		} catch (MqttException e) {
			Settings.NETWORK_OUT.println("MQTT Exception during publishing: " + e.getMessage());			
		}
	}

	@Override
	public void receive(Message event) {
//		getEventQueueScenario().enqueueEvent(event);
	}
}
//Platform-specific code END