package vacuumrobotspecification.runtime;

import sbp.runtime.SpecificationRunconfig;
import vacuumrobot.NamedElement;

public class MQTTDistributedRunconfig_RobotCtrl extends MQTTDistributedRunconfig {

	public MQTTDistributedRunconfig_RobotCtrl() {
		super("RobotCtrl");
	}

	@Override
	protected void registerObservers() {
	}

	public static void main(String[] args) {
		SpecificationRunconfig.run(MQTTDistributedRunconfig_RobotCtrl.class);
	}

	@Override
	public int isControllable(NamedElement namedElement) {
		VacuumrobotSpecificationObjectSystem objectSystem = VacuumrobotSpecificationObjectSystem.getInstance();
		if (namedElement.equals(objectSystem.VacuumRobotSystem.getVacuumRobot().get(0).getController())) {
			return CONTROLLABLE;
		}
		if (namedElement.equals(objectSystem.VacuumRobotSystem.getVacuumRobotManager())) {
			return CONTROLLABLE;
		}
		return UNCONTROLLABLE;
	}
}
