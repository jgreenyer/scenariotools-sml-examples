package vacuumrobotspecification.runtime;

import sbp.runtime.SpecificationRunconfig;
import sbp.runtime.settings.Settings;
import vacuumrobot.NamedElement;
import vacuumrobotspecification.specification.VacuumrobotSpecificationSpecification;
import vacuumrobotspecification.ui.VacuumrobotSpecificationEnvironmentFrame;

public abstract class ADistributedRunconfig extends SpecificationRunconfig<VacuumrobotSpecificationSpecification> {

	public ADistributedRunconfig() {
		super(new VacuumrobotSpecificationSpecification());
	}
	
	protected void setUpGUI(String name) {
		VacuumrobotSpecificationEnvironmentFrame logFrame = new VacuumrobotSpecificationEnvironmentFrame(name,
				getAdapter());
		logFrame.setVisible(true);
		Settings.setRuntimeOut(logFrame.getRuntimeLog());
		Settings.setServerOut(logFrame.getServerLog());
		Settings.SPECTATOR__PRINT_CAUGHT_MESSAGE = true;
		Settings.EXECUTOR__PRINT_ON_METHOD_EXECUTION = true;
		Settings.EXECUTOR__PRINT_ON_RULE_EXECUTION = true;
	}
	
	@Override
	protected void registerParticipatingObjects() {
		VacuumrobotSpecificationObjectSystem objectSystem = VacuumrobotSpecificationObjectSystem.getInstance();
		registerObject(objectSystem.VacuumRobotSystem, UNCONTROLLABLE); // VacuumRobotSystem
		registerObject(objectSystem.VacuumRobotSystem.getRoom().get(0), UNCONTROLLABLE); // Room01
		registerObject(objectSystem.VacuumRobotSystem.getRoom().get(1), UNCONTROLLABLE); // Room02
		registerObject(objectSystem.VacuumRobotSystem.getRoom().get(2), UNCONTROLLABLE); // Room03
		registerObject(objectSystem.VacuumRobotSystem.getVacuumRobot().get(0),
				isControllable(objectSystem.VacuumRobotSystem.getVacuumRobot().get(0))); // robot1
		registerObject(objectSystem.VacuumRobotSystem.getVacuumRobot().get(0).getController(),
				isControllable(objectSystem.VacuumRobotSystem.getVacuumRobot().get(0).getController())); // r1ctrl
		registerObject(objectSystem.VacuumRobotSystem.getVacuumRobotManager(),
				isControllable(objectSystem.VacuumRobotSystem.getVacuumRobotManager())); // rManager
	}

	public abstract int isControllable(NamedElement namedElement);
}
