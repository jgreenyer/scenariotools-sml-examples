package vacuumrobotspecification.runtime;

import sbp.runtime.SpecificationRunconfig;
import vacuumrobot.NamedElement;

public class DistributedRunconfig_Robot extends DistributedRunconfig {
	
	public DistributedRunconfig_Robot() {
		super("Robot", ROBOT_PORT);
	}

	@Override
	protected void registerNetworkAdressesForObjects() {
		VacuumrobotSpecificationObjectSystem objectSystem = VacuumrobotSpecificationObjectSystem.getInstance();
		registerAddress(objectSystem.VacuumRobotSystem.getVacuumRobot().get(0).getController(), "localhost", ROBOTCTRL_PORT);
		registerAddress(objectSystem.VacuumRobotSystem.getVacuumRobotManager(), "localhost", ROBOTCTRL_PORT);
	}

	@Override
	protected void registerObservers() {
		//addScenarioObserver(new RobotObserver());
	}

	public static void main(String[] args) {
		SpecificationRunconfig.run(DistributedRunconfig_Robot.class);
	}

	@Override
	public int isControllable(NamedElement namedElement) {
		VacuumrobotSpecificationObjectSystem objectSystem = VacuumrobotSpecificationObjectSystem.getInstance();
		if (namedElement.equals(objectSystem.VacuumRobotSystem.getVacuumRobot().get(0))) {
			return CONTROLLABLE;
		}
		return UNCONTROLLABLE;
	}
}