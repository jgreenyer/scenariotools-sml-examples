package vacuumrobotspecification.runtime;

import sbp.runtime.SpecificationRunconfig;
import vacuumrobot.NamedElement;

public class MQTTDistributedRunconfig_Robot extends MQTTDistributedRunconfig {

	public MQTTDistributedRunconfig_Robot() {
		super("Robot");
	}

	@Override
	protected void registerObservers() {
		//addScenarioObserver(new RobotObserver());
	}

	public static void main(String[] args) {
		SpecificationRunconfig.run(MQTTDistributedRunconfig_Robot.class);
	}

	@Override
	public int isControllable(NamedElement namedElement) {
		VacuumrobotSpecificationObjectSystem objectSystem = VacuumrobotSpecificationObjectSystem.getInstance();
		if (namedElement.equals(objectSystem.VacuumRobotSystem.getVacuumRobot().get(0))) {
			return CONTROLLABLE;
		}
		return UNCONTROLLABLE;
	}
}
