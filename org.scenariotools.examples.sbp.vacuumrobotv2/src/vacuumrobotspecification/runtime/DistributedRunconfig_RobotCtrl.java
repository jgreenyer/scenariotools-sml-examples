package vacuumrobotspecification.runtime;

import sbp.runtime.SpecificationRunconfig;
import vacuumrobot.NamedElement;

public class DistributedRunconfig_RobotCtrl extends DistributedRunconfig {

	public DistributedRunconfig_RobotCtrl() {
		super("RobotCtrl", ROBOTCTRL_PORT);
	}

	@Override
	protected void registerNetworkAdressesForObjects() {
		VacuumrobotSpecificationObjectSystem objectSystem = VacuumrobotSpecificationObjectSystem.getInstance();
		registerAddress(objectSystem.VacuumRobotSystem.getVacuumRobot().get(0), "localhost", ROBOT_PORT);
	}

	@Override
	protected void registerObservers() {
	}

	public static void main(String[] args) {
		SpecificationRunconfig.run(DistributedRunconfig_RobotCtrl.class);
	}

	@Override
	public int isControllable(NamedElement namedElement) {
		VacuumrobotSpecificationObjectSystem objectSystem = VacuumrobotSpecificationObjectSystem.getInstance();
		if (namedElement.equals(objectSystem.VacuumRobotSystem.getVacuumRobot().get(0).getController())) {
			return CONTROLLABLE;
		}
		if (namedElement.equals(objectSystem.VacuumRobotSystem.getVacuumRobotManager())) {
			return CONTROLLABLE;
		}
		return UNCONTROLLABLE;
	}
}
