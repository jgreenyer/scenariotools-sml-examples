package transformationrules;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;

import sbp.runtime.objectsystem.TransformationRule;
import sbp.specification.events.Message;
import sbp.specification.events.ParameterRandom;

public class ERemoveEventTransformationRule extends TransformationRule{

	public static final ParameterRandom RANDOM = ParameterRandom.getInstance();

	public boolean match(Message event) {
		boolean matches = event.getMessage().startsWith("REMOVE");
		return matches;
	}

	@SuppressWarnings({ "rawtypes" })
	public void execute(Message event){
		//System.out.println("#####executing SetEventTransformationRule");
		EObject receiver = (EObject) event.getReceiver().getBinding();
		String featureName = event.getMessage().substring(6);
		featureName = Character.toLowerCase(featureName.charAt(0)) + featureName.substring(1); 
		EStructuralFeature feature = receiver.eClass().getEStructuralFeature(featureName);
		((EList)receiver.eGet(feature)).remove(event.getParameters().get(0));
		System.out.println(receiver);
	}

	@Override
	public Message getTriggerMessage() {
		return null;
	}

}
