import "../../model/highvoltagecoupling.ecore"

specification HighvoltagecouplingSpecification {

	domain highvoltagecoupling

	controllable {
		Controller
	}
	
	collaboration PlugAndStart {
		
		static role Socket socket
		static role Relays relays
		static role StartButton startButton
		static role Controller controller


		/*
		 * When the plug is plugged into the socket, 
		 * then the socket must be locked.
		 */
		guarantee scenario WhenPluggedThenLock{
			socket->controller.plugged()
			strict urgent controller->socket.lock()
		}
		

		/*
		 * When the start-button is pressed, 
		 * then the relays have to be closed.
		 */
		guarantee scenario WhenStartPressedThenCloseContact{
			startButton->controller.startPressed()
			strict urgent controller->relays.close()
		}

		/*
		 * When the plug is not plugged or not 
		 * locked then the relays must not be closed.
		 */
		//TODO model this requirement
		
		/*
		 * When the relays are closed, the plug 
		 * must not be unplugged.
		 */
		//TODO model this requirement
				
	}

}
