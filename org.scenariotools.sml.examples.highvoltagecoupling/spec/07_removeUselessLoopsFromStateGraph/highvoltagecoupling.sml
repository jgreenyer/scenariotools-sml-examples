import "../../model/highvoltagecoupling.ecore"
 
specification HighvoltagecouplingSpecification {
 
	domain highvoltagecoupling
 
	controllable {
		Controller
	}
	
	non-spontaneous events {
		Controller.unplugged
	}
	
	collaboration PlugAndStart {
		
		static role Socket socket
		static role Relays relays
		static role StartButton startButton
		static role StopButton stopButton // added to allow unplugging
		static role Controller controller
 
 
		/*
		 * When the plug is plugged into the socket, 
		 * then the socket must be locked.
		 */
		guarantee scenario WhenPluggedThenLock{
			socket->controller.plugged()
			strict urgent controller->socket.lock()
		}
 
		guarantee scenario SetPlugged{
			socket->controller.plugged()
			strict urgent controller->controller.setPlugged(true)
		}		
 
		guarantee scenario SetLocked{
			controller->socket.lock()
			strict urgent controller->controller.setSocketLocked(true)
		}		
 
		/*
		 * When the start-button is pressed, 
		 * then the relays have to be closed.
		 */
		guarantee scenario WhenStartPressedThenCloseContact{
			startButton->controller.startPressed()
			interrupt [!controller.plugged || !controller.socketLocked || controller.relaysClosed]
			strict urgent controller->relays.close()
		}
 
		/*
		 * When the plug is not plugged or not 
		 * locked then the relays must not be closed.
		 * -> Modeled as an anti-scenario: closing the relays
		 *    leads to a violation if not plugged or not locked
		 */
		guarantee scenario OnlyCloseRelaysWhenPlugPluggedAndLocked {
			controller->relays.close()
			violation [!controller.plugged || !controller.socketLocked]
		}
		
		guarantee scenario SetRelaysClosed{
			controller->relays.close()
			strict urgent controller->controller.setRelaysClosed(true)
		}		
		
		/*
		 * When the relays are closed, the plug 
		 * must not be unplugged.
		 */
		guarantee scenario UnplugForbiddenWhenRelaysClosed {
			socket->controller.unplugged()
			violation [controller.relaysClosed]
		}
 
		assumption scenario NoUnplugBetweenLockAndUnlock{
			controller->socket.lock()
			controller->socket.unlock()
		}constraints[
			forbidden socket->controller.unplugged()
		]
	
	
		/*
		 * Stop the flow of electricity to when pressing the
		 * stop button to allow unplugging
		 */
		guarantee scenario OpenRelaysWhenStopping {
			stopButton->controller.stopPressed()
			interrupt [!controller.relaysClosed]
			strict urgent controller->relays.open()
		}
		
		guarantee scenario ReleaseLockWhenStopping {
			stopButton->controller.stopPressed()
			interrupt [!controller.socketLocked]
			strict urgent controller->socket.unlock()
		}

		guarantee scenario SetRelaysOpen {
			controller->relays.open()
			strict urgent controller->controller.setRelaysClosed(false)
		}
		
		guarantee scenario SetUnlocked {
			controller->socket.unlock()
			strict urgent controller->controller.setSocketLocked(false)			
		}
		
		/*
		 * Perform unplugging
		 */
		guarantee scenario SetUnplugged {
			socket->controller.unplugged()
			strict urgent controller->controller.setPlugged(false)
		}
		
		/*
		 * user has to unplug plug before plugging it back in
		 */
		assumption scenario NoRepeatedPluggingIn{
			socket->controller.plugged()
			socket->controller.unplugged()
		}constraints[
			forbidden socket->controller.plugged()
		]

		/*
		 * user eventually unplugs plug
		 */
		assumption scenario UserUnplugsPlugAfterPressingStop {
			socket->controller.plugged()
			socket->controller.unplugged()
		}		
	}
 
}