import "../../model/highvoltagecoupling.ecore"

specification HighvoltagecouplingSpecification {

	domain highvoltagecoupling

	controllable {
		Controller
	}
	
	collaboration PlugAndStart {
		
		static role Socket socket
		static role Relays relays
		static role StartButton startButton
		static role Controller controller


		/*
		 * When the plug is plugged into the socket, 
		 * then the socket must be locked.
		 */
		guarantee scenario WhenPluggedThenLock{
			socket->controller.plugged()
			strict urgent controller->socket.lock()
		}

		guarantee scenario SetPlugged{
			socket->controller.plugged()
			strict urgent controller->controller.setPlugged(true)
		}		

		guarantee scenario SetLocked{
			controller->socket.lock()
			strict urgent controller->controller.setSocketLocked(true)
		}		

		/*
		 * When the start-button is pressed, 
		 * then the relays have to be closed.
		 */
		guarantee scenario WhenStartPressedThenCloseContact{
			startButton->controller.startPressed()
			strict urgent controller->relays.close()
		}

		/*
		 * When the plug is not plugged or not 
		 * locked then the relays must not be closed.
		 * -> Modeled as an anti-scenario: closing the relays
		 *    leads to a violation if not plugged or not locked
		 */
		guarantee scenario OnlyCloseRelaysWhenPlugPluggedAndLocked {
			controller->relays.close()
			violation [!controller.plugged || !controller.socketLocked]
		}
		
	}

}