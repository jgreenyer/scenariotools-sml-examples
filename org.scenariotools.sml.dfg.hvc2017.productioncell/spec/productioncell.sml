import "../model/productioncell.ecore"

specification ProductioncellSpecification {

	domain productioncell

	controllable {
		Controller
	}
	
	non-spontaneous events {
		Controller.accelerateAndMove
		Controller.decelerate
		Controller.pressingFinished
		RobotArm.setCarriesItem
		RobotArm.setLocation
		Location.setHasItem
	}

	collaboration FeedBeltBehavior {

		static role Controller controller
		static role Location feedBelt
		dynamic role RobotArm arm
		dynamic role Location nextLocation

		guarantee scenario BlankArrives
		bindings [
			arm = feedBelt.arm
		] {
			feedBelt -> controller.blankArrived()
			wait [arm.location == arm.home && !arm.carriesItem]
			urgent controller -> arm.pickUp()
		}
		
		guarantee scenario ArmDeliversItemToNextLocation
		bindings [
			nextLocation = arm.destination
		] {
			arm -> arm.setCarriesItem(true)
			interrupt [arm.home != feedBelt] // only use this scenario for the first robot arm in the cell
			urgent controller -> arm.moveTo(nextLocation)
			arm -> arm.setLocation(nextLocation)
			wait [!nextLocation.hasItem]
			urgent controller -> arm.releaseItem()
			arm -> arm.setCarriesItem(false)
			urgent controller -> arm.moveTo(arm.home)
		}
		
		assumption scenario ArmPicksUpItemBeforeNewBlankArrives
		bindings [
			arm = feedBelt.arm
		] {
			feedBelt -> controller.blankArrived()
			controller -> arm.pickUp()
		} constraints [
			forbidden feedBelt -> controller.blankArrived()
		]

	}
	
	collaboration PressBehavior {
		
		static role Controller controller
		static role Location feedBelt
		dynamic role RobotArm arm
		dynamic role Location location

		guarantee scenario PressStartsPressing {
			location -> location.setHasItem(true)
			urgent controller -> location.startPressing()
		}
		
		guarantee scenario PickUpPressedItem
		bindings [
			arm = location.arm
		] {
			location -> controller.pressingFinished()
			wait [arm.location == location && !arm.carriesItem]
			urgent controller -> arm.pickUp()
		}
		
		guarantee scenario DepositArmDeliversPressedItem
		optimize cost
		bindings [
			location = arm.destination
		] {
			arm -> arm.setCarriesItem(true)
			interrupt [arm.home == feedBelt] // do NOT use this scenario for the first robot arm in the cell
			eventually controller -> arm.moveTo(location)
			arm -> arm.setLocation(location)
			wait [!location.hasItem]
			urgent controller -> arm.releaseItem()
			arm -> arm.setCarriesItem(false)
			eventually controller -> arm.moveTo(arm.home)
		}
		
		assumption scenario PressEventuallyFinishes {
			controller -> location.startPressing()
			eventually location -> controller.pressingFinished()
		}
	}
	
	collaboration RobotArmBehavior {
		
		dynamic role Controller controller
		dynamic role RobotArm arm
		dynamic role Location location
		
		assumption scenario ArmMovesToLocation {
			controller -> arm.moveTo(bind location)
			committed arm -> controller.accelerateAndMove()
			eventually arm -> controller.decelerate()
			eventually arm -> arm.setLocation(location)
		}
		
		assumption scenario ArmPicksUpItem
		bindings [
			location = arm.location
		] {
			controller -> arm.pickUp()
			strict committed arm -> arm.setCarriesItem(true) 
			interrupt [!location.isPress]
			strict committed location -> location.setHasItem(false)			
		}
		
		assumption scenario ArmReleasesItem
		bindings [
			location = arm.location
		] {
			controller -> arm.releaseItem()
			strict committed arm -> arm.setCarriesItem(false)
			interrupt [!location.isPress]
			strict committed location -> location.setHasItem(true)			
		}
	}

	collaboration Costs {
		
		dynamic role Controller controller
		dynamic role RobotArm arm
		
		assumption scenario AccelerationAndMovementEnergy
		cost [15.0] {
			arm -> controller.accelerateAndMove()
			monitored eventually arm -> controller.decelerate()
		}
		
		assumption scenario BrakingEnergy
		cost [-5.0] {
			arm -> controller.decelerate()
			monitored eventually arm -> arm.setLocation(*)
		}
	}

}
