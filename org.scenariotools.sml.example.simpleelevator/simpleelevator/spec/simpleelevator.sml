import "simpleelevator.ecore"

specification SimpleelevatorSpecification {

	domain simpleelevator
	
	controllable {
		ElevatorController
	}
	
	non-spontaneous events {
		ElevatorController.reachedLevel
		ElevatorCabin.setCurrentLevel
	}
	
	/*
	 * Collaborations describe how objects interact in a certain
	 * context to collectively accomplish some desired functionality.
	 */
	collaboration SimpleelevatorCollaboration1 {

		dynamic role Level level
		dynamic role Level reachedLevel
		static role ElevatorCabin eCabin
		static role ElevatorController eCtrl

		/** Initial state: 
		 *  - eCabin.currentLevel = level 1, 
		 *  - !eCtrl.elevatorMoving, 
		 *  - eCtrl.requestedLevel empty
		 */		
		
		guarantee scenario WhenButtonPressedElevatorMustEventuallyStopAtLevel {
			level->eCtrl.buttonPressed()
			wait eventually [eCabin.currentLevel == level && eCabin.movement == Movement:STOP]
		}

		guarantee scenario AddRequstedLevel {
			level->eCtrl.buttonPressed()
			interrupt [eCabin.currentLevel == level && eCabin.movement == Movement:STOP]
			strict committed eCtrl->eCtrl.requestedLevel.add(level)
		}


		guarantee scenario RemoveRequstedLevelWhenElevatorArrives {
			eCtrl->eCabin.setMovement(Movement:STOP)
			interrupt [!eCtrl.requestedLevel.contains(eCabin.currentLevel)]
			strict committed eCtrl->eCtrl.requestedLevel.remove(eCabin.currentLevel)
		}

		guarantee scenario ElevatorMovesUpOrDownOnButtonPress {
			level->eCtrl.buttonPressed()
			wait eventually [eCabin.movement == Movement:STOP]
			interrupt [eCabin.currentLevel == level]
			alternative{
				strict requested eCtrl->eCabin.setMovement(Movement:UP)
			}or{
				strict requested eCtrl->eCabin.setMovement(Movement:DOWN)
			}
		}

//		guarantee scenario ElevatorMovesUpOrDownOnButtonPressWithGoodDecision {
//			message level->eCtrl.buttonPressed()
//			requested wait until [eCabin.movement == Movement:STOP]
//			interrupt [eCabin.currentLevel == level]
//			var Level currentLevel = eCabin.currentLevel
//			alternative [level.number > currentLevel.number]{
//				message strict requested eCtrl->eCabin.setMovement(Movement:UP)
//			}or [level.number < currentLevel.number]{
//				message strict requested eCtrl->eCabin.setMovement(Movement:DOWN)
//			}
//		}

		guarantee scenario ElevatorMustStopAtHighestLevel {
			eCabin->eCtrl.reachedLevel(bind reachedLevel)
			interrupt [reachedLevel.nextLevel != null]
			strict urgent eCtrl->eCabin.setMovement(Movement:STOP)
		}

		guarantee scenario ElevatorMustStopAtLowestLevel {
			eCabin->eCtrl.reachedLevel(bind reachedLevel)
			interrupt [reachedLevel.previousLevel != null]
			strict urgent eCtrl->eCabin.setMovement(Movement:STOP)
		}

		guarantee scenario ElevatorMustNotMoveDownOnLowestLevel {
			eCtrl->eCabin.setMovement(Movement:DOWN)
			var Level currentLevel = eCabin.currentLevel
			violation [currentLevel.previousLevel == null]
		}
		guarantee scenario ElevatorMustNotMoveUpOnHighestLevel {
			eCtrl->eCabin.setMovement(Movement:UP)
			var Level currentLevel = eCabin.currentLevel
			violation [currentLevel.nextLevel == null]
		}

		guarantee scenario ElevatorStopsOnRequestedLevel {
			eCabin->eCtrl.reachedLevel(bind level)
			interrupt [!eCtrl.requestedLevel.contains(level)]
			strict requested eCtrl->eCabin.setMovement(Movement:STOP)
		}

		guarantee scenario ContinueMovingIfRequestedLevelsRemain {
			// possible flaw: renive this first message
			eCtrl->eCabin.setMovement(Movement:STOP)
			eCtrl->eCtrl.requestedLevel.remove(*)
			interrupt [eCtrl.requestedLevel.isEmpty()]
			alternative{
				requested eCtrl->eCabin.setMovement(Movement:UP)
			}or{
				requested eCtrl->eCabin.setMovement(Movement:DOWN)
			}
		}
		
		assumption scenario ElevatorMovesUpAssumption {
			eCtrl->eCabin.setMovement(Movement:UP)
			var Level currentLevel = eCabin.currentLevel
			while[eCabin.movement == Movement:UP]{
				interrupt [currentLevel.nextLevel == null]
				eventually eCabin->eCabin.setCurrentLevel(currentLevel.nextLevel)
				committed eCabin->eCtrl.reachedLevel(currentLevel.nextLevel)
				currentLevel = eCabin.currentLevel
			}
		}

		assumption scenario ElevatorMovesDownAssumption {
			eCtrl->eCabin.setMovement(Movement:DOWN)
			var Level currentLevel = eCabin.currentLevel
			while[eCabin.movement == Movement:DOWN]{
				interrupt [currentLevel.previousLevel == null]
				eventually eCabin->eCabin.setCurrentLevel(currentLevel.previousLevel)
				committed eCabin->eCtrl.reachedLevel(currentLevel.previousLevel)
				currentLevel = eCabin.currentLevel
			}
		}
		
	}

}
