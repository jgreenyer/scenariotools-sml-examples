import "productioncell_ProductionCellIntegrated.ecore"

specification ProductionCellSpecification_ArmAAndPress {

	domain ProductionCellIntegrated

	controllable {
		Controller
	}

	non-spontaneous events {
		Controller.arrivedAtDepositBelt
		Controller.arrivedAtPress
		Controller.arrivedAtTable
		Controller.pressingFinished
	}
	
	
	/*
	 * 
	 * S ArmPickUpBlank
	 * A NoBlankArrivesBeforeArmAPicksUpBlank
	 * S SetAtTableFalse
	 * A ArmAMoveFromTableToPressAssumption
	 * S ArmATransportBlankToPress
	 * S ItemInPress
	 * S PressPlateAfterArmAReleaseBlankPlate
	 * S ArmAMoveToTableAfterReleaseBlank
	 * A ArmAMoveFromPressToTableAssumption
	 * A PressingFinishedHappens
	 * S SetPressingFlagToTrue
	 * -> 11 from previously 21 scenarios
	 */
	

	collaboration ProductionCellIntegrated {

		static role TableSensor ts
		static role Controller c
		static role ArmA a
		static role ArmB b
		static role Press p

		 
		/*
		 * Arm A must pick up a blank after it arrived.
		 * Blanks may arrive before or after the arm has returned to the table
		 */
		guarantee scenario ArmAPickUpBlank {
			ts -> c.blankArrived()
			var EInt x = 0
			wait strict [c.armAAtTable]
			strict urgent c -> a.pickUp()
			strict urgent c -> a.moveToPress()	
		}constraints[
			forbidden c -> a.moveToPress()
			consider c -> a.moveToPress()
			ignore c -> a.moveToPress()
		]
		
//		guarantee scenario ArmANoPickUpWhenNotAtTable {
//			c -> a.pickUp()
//			violation if [!c.armAAtTable]
//		}
		

		guarantee scenario SetAtTableFalse {
			c -> a.moveToPress()
			strict urgent c -> c.setArmAAtTable(false)
		}
		
// feels strange (obviously wrong) to remove this one...		
//		guarantee scenario SetAtTableTrue {
//			a -> c.arrivedAtTable()
//			strict eventually c -> c.setArmAAtTable(true)
//		}
		

		/*
		 * After arriving at the press, arm A must release the blank
		 */
		guarantee scenario ArmAReleaseBlank {
			a -> c.arrivedAtPress()
			wait strict [!c.pressing && !c.itemInPress]
			strict urgent c -> a.releaseBlank()
		}





		guarantee scenario ItemInPress{
			c -> a.releaseBlank()
			strict urgent c -> c.setItemInPress(true)
		}
		
// feels strange (obviously wrong) to remove this one...		
//		guarantee scenario ItemTakenFromInPress{
//			c -> b.pickUp()
//			strict eventually c -> c.setItemInPress(false)
//		}

		
		/*
		 * The press must press the blank after it is released by arm A
		 */
		guarantee scenario PressPlateAfterArmAReleasesBlankPlate {
			c -> a.releaseBlank()
			strict urgent c -> p.press()
		}
		
		/*
		 * After releasing the blank, arm A must return to the table
		 */
		guarantee scenario ArmAMoveToTableAfterReleaseBlank {
			c -> a.releaseBlank()
			strict urgent c -> a.moveToTable()
		}




		
		/*
		 * We assume that not two blanks arrive before arm A has picked up one.
		 */
		assumption scenario NoBlankArrivesBeforeArmAPicksUpBlank {
			ts -> c.blankArrived()
			c -> a.pickUp()
		} constraints [
			forbidden ts -> c.blankArrived()
		]
		
		/*
		 * Arm A movement from table to press: 
		 * When the controller tells the arm to move to the press, it will eventually arrive there.
		 */
		assumption scenario ArmAMoveFromTableToPressAssumption {
			c -> a.moveToPress()
			eventually a -> c.arrivedAtPress()
		} constraints [
	 		// while the arm is moving to the press, it will not arrive at the table
			forbidden a -> c.arrivedAtTable()
	 		// unless meanwhile the controller tells the arm to move to the table
			interrupt c -> a.moveToTable()
		]

		/*
		 * Arm A movement from press to table: 
		 * When the controller tells the arm to move to the table, it will eventually arrive there.
		 */
		assumption scenario ArmAMoveFromPressToTableAssumption {
			c -> a.moveToTable()
			eventually a -> c.arrivedAtTable()
		} constraints [
	 		// while the arm is moving to the table, it will not arrive at the press
			forbidden a -> c.arrivedAtPress()
	 		// unless meanwhile the controller tells the arm to move to the press
			interrupt c -> a.moveToPress()
		]

		
		
//		guarantee scenario ArmBPicksUpPressedPlate{
//			p -> c.pressingFinished()
//			strict wait until [c.armBAtPress]
//			strict urgent c -> b.pickUp()
//			strict urgent c -> b.moveToDepositBelt()	
//		} 
//		
//		guarantee scenario SetArmBArPressToFalse{
//			c -> b.moveToDepositBelt()
//			strict urgent c -> c.setArmBAtPress(false)
//		}		
//		guarantee scenario SetArmBArPressToTrue{
//			b -> c.arrivedAtPress()
//			strict urgent c -> c.setArmBAtPress(true)
//		}		
//
//
//
//		guarantee scenario ArmBReleasedPlateOnDepositBelt{
//			b -> c.arrivedAtDepositBelt()
//			strict urgent c -> b.releasePlate()
//			strict urgent c -> b.moveToPress()
//		}
//		
//		guarantee scenario ArmBMustNotPickUpPlateBeforeBackAtPress{
//			c -> b.pickUp()
//			strict b -> c.arrivedAtDepositBelt()
//			strict c -> b.releasePlate()
//			strict b -> c.arrivedAtPress()
//		}
		
		
		assumption scenario PressingFinishedHappens{
			c -> p.press()
			eventually p -> c.pressingFinished()
		}
		
		guarantee scenario SetPressingFlagToTrue{
			c -> p.press()
			strict urgent c -> c.setPressing(true)
		}
		
// feels strange (obviously wrong) to remove this one...		
//		guarantee scenario SetPressingFlagToFalse{
//			p -> c.pressingFinished()
//			strict urgent c -> c.setPressing(false)
//		}
		
//		/*
//		 * Arm A movement from table to press: 
//		 * When the controller tells the arm to move to the press, it will eventually arrive there.
//		 */
//		assumption scenario ArmBMoveFromPressToDepositBeltAssumption {
//			c -> b.moveToDepositBelt()
//			eventually b -> c.arrivedAtDepositBelt()
//		} constraints [
//	 		// while the arm is moving to the press, it will not arrive at the table
//			forbidden b -> c.arrivedAtPress()
//	 		// unless meanwhile the controller tells the arm to move to the table
//			interrupt c -> b.moveToPress()
//		]
//
//		/*
//		 * Arm A movement from table to press: 
//		 * When the controller tells the arm to move to the press, it will eventually arrive there.
//		 */
//		assumption scenario ArmBMoveFromDepositBeltToPressAssumption {
//			c -> b.moveToPress()
//			eventually b -> c.arrivedAtPress()
//		} constraints [
//	 		// while the arm is moving to the press, it will not arrive at the table
//			forbidden b -> c.arrivedAtDepositBelt()
//	 		// unless meanwhile the controller tells the arm to move to the table
//			interrupt c -> b.moveToDepositBelt()
//		]
//		
		
		
		
		
	}
}