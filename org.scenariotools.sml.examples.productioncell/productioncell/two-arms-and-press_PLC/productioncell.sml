import "productioncell_ProductionCellIntegrated.ecore"

specification ProductionCellSpecification_ArmAAndPress {

	domain ProductionCellIntegrated

	controllable {
		Controller
	}

	non-spontaneous events {
		Controller.arrivedAtTable
		Controller.pickedUpBlank
		Controller.pressingFinished
	}
	
	collaboration Requirements {
		static role TableSensor ts
		static role Controller c
		static role ArmA a
		static role ArmB b
		static role Press p

		guarantee scenario ArmAPicksUpBlank {
			ts -> c.blankArrived()
			strict eventually c -> a.moveToTable()
			strict eventually c -> a.pickUp()
		}
		
		assumption scenario ArmAMovesBlankToPress {
			ts -> c.blankArrived()
			c -> a.pickUp()
		} constraints [
			forbidden ts -> c.blankArrived()
		]
		
		guarantee scenario ArmBPicksUpItem {
			p -> c.pressingFinished()
			strict eventually c -> b.moveToPress()
			strict eventually c -> b.pickUp()
		} 
	}
	
	collaboration PLCDetails { // Name Convention
		static role TableSensor ts
		static role Controller c
		static role ArmA a
		static role ArmB b
		static role Press p
		
		guarantee scenario ArmAMovesToTable {
			c -> a.moveToTable()
			strict eventually a -> c.arrivedAtTable()
			c -> a.pickUp()
			strict committed a -> c.pickedUpBlank()
		}
		
		assumption scenario ArmBMovesToPress {
			c-> b.moveToPress()
			strict eventually b -> c.arrivedAtPress()
		}
	}
}