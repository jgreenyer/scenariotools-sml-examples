import "productioncell.ecore"

specification ProductioncellSpecification {

	/*
	 * Refer to a package in the imported ecore model.
	 * Hint: Use ctrl+alt+R to rename packages and classes.
	 */
	domain productioncell
	
	/* 
	 * Define classes of objects that are controllable
	 * or uncontrollable.
	 */
	controllable {
		Controller
	}
	
	non-spontaneous events {
		Controller.arrivedAt
		Controller.pressingFinished
	}
	
	/*
	 * Collaborations describe how objects interact in a certain
	 * context to collectively accomplish some desired functionality.
	 */
	collaboration RobotA {

		static role Controller controller
		static role Robot robotA
		static role Press press
		static role FeedLocation table
		static role ArrivalSensor tableSeneor
		
		
		guarantee scenario RobotAPickUpBlankAndMoveToPress {
			tableSeneor -> controller.blankArrived()
			wait strict [robotA.currentLocation == table &&
				!controller.carriesItem.contains(robotA)]
			strict urgent controller -> robotA.pickUp()
			strict urgent controller -> robotA.moveTo(press)
		}constraints[
			// this was tricky: why was moveTo(table) not possible...?
			ignore controller -> robotA.moveTo(*)
		]

		guarantee scenario RobotATransportBlankToPress {
			robotA -> controller.arrivedAt(press)
			wait strict [!controller.pressing && !controller.itemInPress]
			strict urgent controller -> robotA.releaseItem()
		}
	
		/*
		 * After releasing the blank item, RobotA must return to the table
		 */
		guarantee scenario RobotAMoveToTableAfterReleaseBlank {
			controller -> robotA.releaseItem()
			strict urgent controller -> robotA.moveTo(table)
		}
		
		/*
		 * We assume that not two blank items arrive before arm A has picked up one.
		 */
		assumption scenario NoBlankArrivesBeforeArmAPicksUpBlank {
			tableSeneor -> controller.blankArrived()
			controller -> robotA.pickUp()
		} constraints [
			forbidden tableSeneor -> controller.blankArrived()
		]

	}
	
	collaboration Press {
		static role Controller controller
		static role Robot robotA
		static role Press press

		/*
		 * The press must press the blank item after it is released by RobotA
		 */
		guarantee scenario PressPlateAfterArmAReleasesBlankPlate {
			controller -> robotA.releaseItem()
			strict urgent controller -> controller.setItemInPress(true)
			strict urgent controller -> press.press()
		}

		assumption scenario PressingFinishedHappens{
			controller -> press.press()
			eventually press -> controller.pressingFinished()
		}
		
	
	}
	
	collaboration RobotB {
		
		static role Controller controller
		static role Robot robotB
		static role Press press
		static role DepositLocation depositBelt
		
		guarantee scenario RobotBPicksUpPressedPlate{
			press -> controller.pressingFinished()
			wait strict [robotB.currentLocation == press &&
				// tricky: robotB may still be at the press after picking up the previous item!
				// so we need this extra condition to not order it to pick-up another item!
				!controller.carriesItem.contains(robotB)
			]
			strict urgent controller -> robotB.pickUp()
			strict urgent controller -> controller.setItemInPress(false)
			strict urgent controller -> robotB.moveTo(depositBelt)	
		}constraints[
			ignore controller -> robotB.moveTo(*)	
		]

		guarantee scenario ArmBReleasedPlateOnDepositBelt{
			robotB -> controller.arrivedAt(depositBelt)
			strict urgent controller -> robotB.releaseItem()
			strict urgent controller -> robotB.moveTo(press)
		}
		
	}
	
	collaboration RobotArmMovementAssumptions{
		
		dynamic role Robot robot
		dynamic role Controller controller
		dynamic role Location targetLocation
		
		assumption scenario RobotArmArrivesAtTarget{
			controller -> robot.moveTo(bind targetLocation)
			strict eventually robot -> controller.departedFrom(robot.currentLocation)
			strict eventually robot -> controller.arrivedAt(targetLocation)
		}
		
	}


}