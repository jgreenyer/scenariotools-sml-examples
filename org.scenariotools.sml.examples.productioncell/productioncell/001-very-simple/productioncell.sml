import "productioncell_ProductionCellIntegrated.ecore"

specification ProductionCellSpecification_001 {

	domain ProductionCellIntegrated

	controllable {
		Controller
	}

	non-spontaneous events {
		Controller.arrivedAtDepositBelt
		Controller.arrivedAtPress
		Controller.arrivedAtTable
		Controller.pressingFinished
	}

	collaboration ProductionCell {

		static role TableSensor ts
		static role Controller c
		static role ArmA a
		static role ArmB b
		static role Press p

		guarantee scenario PickUpOnBlankArrived {
			ts -> c.blankArrived()
			strict urgent c -> a.pickUp()
		}

		guarantee scenario MoveToPressAfterPickUp {
			ts -> c.blankArrived()
			requested c -> a.pickUp()
			strict urgent c -> a.moveToPress()
		}

	}

}

