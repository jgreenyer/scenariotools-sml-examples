import "010-productioncell_ProductionCellIntegrated.ecore"

specification ProductionCellSpecification010 {

	domain ProductionCellIntegrated

	controllable {
		Controller
	}

	non-spontaneous events {
		Controller.arrivedAtDepositBelt
		Controller.arrivedAtPress
		Controller.arrivedAtTable
		Controller.pressingFinished
	}

	collaboration ProductionCellIntegrated010 {

		static role TableSensor ts
		static role Controller c
		static role ArmA a
		static role ArmB b
		static role Press p
		
		singular guarantee scenario HighLevelMakePlate {
			ts -> c.blankArrived()
			var EInt numPlatesInSystem = 1
			while [ (numPlatesInSystem >= 1) ] {
				alternative {
					ts -> c.blankArrived()
					numPlatesInSystem = numPlatesInSystem + 1
				} or {
					monitored eventually c -> b.releasePlate()
					numPlatesInSystem = numPlatesInSystem - 1
				}
			}	
		} constraints [
			ignore ts -> c.blankArrived()
			ignore c -> a.pickUp()
			ignore c -> b.releasePlate()
		]

		assumption scenario ArmBMoveFromDepositBeltToPressAssumption {
			c -> b.moveToPress()
			strict eventually b -> c.arrivedAtPress()
		} constraints [
			interrupt c -> b.moveToPress()
			interrupt c -> b.moveToDepositBelt()
			forbidden b -> c.arrivedAtDepositBelt()
		]

		assumption scenario ArmAMoveFromTableToPressAssumption {
			c -> a.moveToPress()
			strict eventually a -> c.arrivedAtPress()
		} constraints [
			forbidden a -> c.arrivedAtTable()
			interrupt c -> a.moveToTable()
			interrupt c -> a.moveToPress()
		]

		assumption scenario ArmAMoveFromPressToTableAssumption {
			c -> a.moveToTable()
			strict eventually a -> c.arrivedAtTable()
		} constraints [
			forbidden a -> c.arrivedAtPress()
			interrupt c -> a.moveToPress()
			interrupt c -> a.moveToTable()
		]
 
		assumption scenario NoRepeatedPressingFinished {
			c -> p.press()
			p -> c.pressingFinished()
			strict c -> p.press()
		}

		assumption scenario ArmBMoveFromPressToDepositBeltAssumption {
			c -> b.moveToDepositBelt()
			strict eventually b -> c.arrivedAtDepositBelt()
		} constraints [
			interrupt c -> b.moveToDepositBelt()
			interrupt c -> b.moveToPress()
			forbidden b -> c.arrivedAtPress()
		]

		guarantee scenario ArmATransportBlankToPress {
			ts -> c.blankArrived()
			strict urgent c -> a.pickUp()
			strict urgent c -> a.moveToPress()
			strict a -> c.arrivedAtPress()
			strict urgent c -> a.releaseBlank()
			strict urgent c -> a.moveToTable()
			strict a -> c.arrivedAtTable()
		}

		assumption scenario NoBlankArrivesBeforeArmAReturnedToTable {
			ts -> c.blankArrived()
			c -> a.moveToPress()
			a -> c.arrivedAtTable()
		} constraints [
			forbidden ts -> c.blankArrived()
		]

		assumption scenario PressPlateAssumption {
			c -> a.releaseBlank()
			c -> p.press()
			strict eventually p -> c.pressingFinished()
		}

		guarantee scenario ArmBTransportToDepositBeltAfterPickUpFromPress {
			c -> b.pickUp()
			strict urgent c -> b.moveToDepositBelt()
			strict b -> c.arrivedAtDepositBelt()
			strict urgent c -> b.releasePlate()
			strict urgent c -> b.moveToPress()
			strict b -> c.arrivedAtPress()
		}

		assumption scenario BackAtPressBeforeNextReleaseBlank {
			 c -> a.releaseBlank()
			strict c -> b.pickUp()
			strict b -> c.arrivedAtPress()
		}

		guarantee scenario PressPlateAfterArmAReleasesBlankPlate {
			c -> a.releaseBlank()
			strict urgent c -> p.press()
			strict p -> c.pressingFinished()
			strict urgent c -> b.pickUp()
		}
		assumption scenario PressingFinishedBeforeArmAReturnsToPress {
			c->p.press()
			p->c.pressingFinished()
		}constraints [
			forbidden a->c.arrivedAtPress()
		]
	}

}

