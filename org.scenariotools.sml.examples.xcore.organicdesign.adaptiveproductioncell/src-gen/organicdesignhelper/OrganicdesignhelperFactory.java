/**
 */
package organicdesignhelper;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see organicdesignhelper.OrganicdesignhelperPackage
 * @generated
 */
public interface OrganicdesignhelperFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OrganicdesignhelperFactory eINSTANCE = organicdesignhelper.impl.OrganicdesignhelperFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Perform Task Context</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Perform Task Context</em>'.
	 * @generated
	 */
	PerformTaskContext createPerformTaskContext();

	/**
	 * Returns a new object of class '<em>Next Agent For Resource Context</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Next Agent For Resource Context</em>'.
	 * @generated
	 */
	NextAgentForResourceContext createNextAgentForResourceContext();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	OrganicdesignhelperPackage getOrganicdesignhelperPackage();

} //OrganicdesignhelperFactory
