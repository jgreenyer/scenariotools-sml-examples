/**
 */
package organicdesignhelper;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import organicdesign.Resource;
import organicdesign.Task;
import organicdesign.TransformationAgent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Agent And Resource Context</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see organicdesignhelper.OrganicdesignhelperPackage#getAgentAndResourceContext()
 * @model abstract="true"
 * @generated
 */
public interface AgentAndResourceContext extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" transformationAgentUnique="false" resourceUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='<%organicdesign.Recipe%> _recipe = resource.getRecipe();\n<%org.eclipse.emf.common.util.EList%><<%organicdesign.Task%>> _requiredTasks = _recipe.getRequiredTasks();\nfinal <%org.eclipse.xtext.xbase.lib.Functions.Function1%><<%organicdesign.Task%>, <%java.lang.Boolean%>> _function = new <%org.eclipse.xtext.xbase.lib.Functions.Function1%><<%organicdesign.Task%>, <%java.lang.Boolean%>>()\n{\n\tpublic <%java.lang.Boolean%> apply(final <%organicdesign.Task%> t)\n\t{\n\t\t<%org.eclipse.emf.common.util.EList%><<%organicdesign.Task%>> _fulfilledTask = resource.getFulfilledTask();\n\t\tboolean _contains = _fulfilledTask.contains(t);\n\t\treturn <%java.lang.Boolean%>.valueOf((!_contains));\n\t}\n};\n<%java.lang.Iterable%><<%organicdesign.Task%>> _filter = <%org.eclipse.xtext.xbase.lib.IterableExtensions%>.<<%organicdesign.Task%>>filter(_requiredTasks, _function);\nfinal <%org.eclipse.xtext.xbase.lib.Functions.Function1%><<%organicdesign.Task%>, <%java.lang.Boolean%>> _function_1 = new <%org.eclipse.xtext.xbase.lib.Functions.Function1%><<%organicdesign.Task%>, <%java.lang.Boolean%>>()\n{\n\tpublic <%java.lang.Boolean%> apply(final <%organicdesign.Task%> t)\n\t{\n\t\t<%organicdesign.Recipe%> _recipe = resource.getRecipe();\n\t\t<%org.eclipse.emf.common.util.EList%><<%organicdesign.TaskDependency%>> _taskdependency = _recipe.getTaskdependency();\n\t\tfinal <%org.eclipse.xtext.xbase.lib.Functions.Function1%><<%organicdesign.TaskDependency%>, <%java.lang.Boolean%>> _function = new <%org.eclipse.xtext.xbase.lib.Functions.Function1%><<%organicdesign.TaskDependency%>, <%java.lang.Boolean%>>()\n\t\t{\n\t\t\tpublic <%java.lang.Boolean%> apply(final <%organicdesign.TaskDependency%> d)\n\t\t\t{\n\t\t\t\treturn <%java.lang.Boolean%>.valueOf((<%com.google.common.base.Objects%>.equal(d.getTask(), t) && (!resource.getFulfilledTask().contains(d.getPrecondition()))));\n\t\t\t}\n\t\t};\n\t\tboolean _exists = <%org.eclipse.xtext.xbase.lib.IterableExtensions%>.<<%organicdesign.TaskDependency%>>exists(_taskdependency, _function);\n\t\treturn <%java.lang.Boolean%>.valueOf((!_exists));\n\t}\n};\n<%java.lang.Iterable%><<%organicdesign.Task%>> _filter_1 = <%org.eclipse.xtext.xbase.lib.IterableExtensions%>.<<%organicdesign.Task%>>filter(_filter, _function_1);\nfinal <%org.eclipse.xtext.xbase.lib.Functions.Function1%><<%organicdesign.Task%>, <%java.lang.Boolean%>> _function_2 = new <%org.eclipse.xtext.xbase.lib.Functions.Function1%><<%organicdesign.Task%>, <%java.lang.Boolean%>>()\n{\n\tpublic <%java.lang.Boolean%> apply(final <%organicdesign.Task%> t)\n\t{\n\t\treturn <%java.lang.Boolean%>.valueOf(((!<%com.google.common.base.Objects%>.equal(transformationAgent.getCurrentRole(), null)) && transformationAgent.getCurrentRole().getProvidedCapabilities().contains(t.getRequiredCapability())));\n\t}\n};\n<%java.lang.Iterable%><<%organicdesign.Task%>> _filter_2 = <%org.eclipse.xtext.xbase.lib.IterableExtensions%>.<<%organicdesign.Task%>>filter(_filter_1, _function_2);\nreturn <%org.eclipse.emf.common.util.ECollections%>.<<%organicdesign.Task%>>toEList(_filter_2);'"
	 * @generated
	 */
	EList<Task> tasksThatAgentCanPerform(TransformationAgent transformationAgent, Resource resource);

} // AgentAndResourceContext
