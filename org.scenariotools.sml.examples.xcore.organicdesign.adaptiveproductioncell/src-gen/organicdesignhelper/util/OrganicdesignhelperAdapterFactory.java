/**
 */
package organicdesignhelper.util;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

import organicdesignhelper.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see organicdesignhelper.OrganicdesignhelperPackage
 * @generated
 */
public class OrganicdesignhelperAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static OrganicdesignhelperPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrganicdesignhelperAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = OrganicdesignhelperPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OrganicdesignhelperSwitch<Adapter> modelSwitch =
		new OrganicdesignhelperSwitch<Adapter>() {
			@Override
			public Adapter caseAgentAndResourceContext(AgentAndResourceContext object) {
				return createAgentAndResourceContextAdapter();
			}
			@Override
			public Adapter casePerformTaskContext(PerformTaskContext object) {
				return createPerformTaskContextAdapter();
			}
			@Override
			public Adapter caseNextAgentForResourceContext(NextAgentForResourceContext object) {
				return createNextAgentForResourceContextAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link organicdesignhelper.AgentAndResourceContext <em>Agent And Resource Context</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see organicdesignhelper.AgentAndResourceContext
	 * @generated
	 */
	public Adapter createAgentAndResourceContextAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link organicdesignhelper.PerformTaskContext <em>Perform Task Context</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see organicdesignhelper.PerformTaskContext
	 * @generated
	 */
	public Adapter createPerformTaskContextAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link organicdesignhelper.NextAgentForResourceContext <em>Next Agent For Resource Context</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see organicdesignhelper.NextAgentForResourceContext
	 * @generated
	 */
	public Adapter createNextAgentForResourceContextAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //OrganicdesignhelperAdapterFactory
