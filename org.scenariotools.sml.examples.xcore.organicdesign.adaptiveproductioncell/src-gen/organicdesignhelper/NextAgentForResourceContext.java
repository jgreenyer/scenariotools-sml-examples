/**
 */
package organicdesignhelper;

import org.eclipse.emf.common.util.EList;

import organicdesign.Cart;
import organicdesign.Resource;
import organicdesign.TransformationAgent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Next Agent For Resource Context</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link organicdesignhelper.NextAgentForResourceContext#getSender <em>Sender</em>}</li>
 *   <li>{@link organicdesignhelper.NextAgentForResourceContext#getReceiver <em>Receiver</em>}</li>
 *   <li>{@link organicdesignhelper.NextAgentForResourceContext#getResource <em>Resource</em>}</li>
 *   <li>{@link organicdesignhelper.NextAgentForResourceContext#getConnectedAgentThatCanPerformTask <em>Connected Agent That Can Perform Task</em>}</li>
 * </ul>
 *
 * @see organicdesignhelper.OrganicdesignhelperPackage#getNextAgentForResourceContext()
 * @model
 * @generated
 */
public interface NextAgentForResourceContext extends AgentAndResourceContext {
	/**
	 * Returns the value of the '<em><b>Sender</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sender</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sender</em>' reference.
	 * @see #setSender(TransformationAgent)
	 * @see organicdesignhelper.OrganicdesignhelperPackage#getNextAgentForResourceContext_Sender()
	 * @model
	 * @generated
	 */
	TransformationAgent getSender();

	/**
	 * Sets the value of the '{@link organicdesignhelper.NextAgentForResourceContext#getSender <em>Sender</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sender</em>' reference.
	 * @see #getSender()
	 * @generated
	 */
	void setSender(TransformationAgent value);

	/**
	 * Returns the value of the '<em><b>Receiver</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Receiver</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Receiver</em>' reference.
	 * @see #setReceiver(Cart)
	 * @see organicdesignhelper.OrganicdesignhelperPackage#getNextAgentForResourceContext_Receiver()
	 * @model
	 * @generated
	 */
	Cart getReceiver();

	/**
	 * Sets the value of the '{@link organicdesignhelper.NextAgentForResourceContext#getReceiver <em>Receiver</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Receiver</em>' reference.
	 * @see #getReceiver()
	 * @generated
	 */
	void setReceiver(Cart value);

	/**
	 * Returns the value of the '<em><b>Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource</em>' reference.
	 * @see #setResource(Resource)
	 * @see organicdesignhelper.OrganicdesignhelperPackage#getNextAgentForResourceContext_Resource()
	 * @model
	 * @generated
	 */
	Resource getResource();

	/**
	 * Sets the value of the '{@link organicdesignhelper.NextAgentForResourceContext#getResource <em>Resource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resource</em>' reference.
	 * @see #getResource()
	 * @generated
	 */
	void setResource(Resource value);

	/**
	 * Returns the value of the '<em><b>Connected Agent That Can Perform Task</b></em>' reference list.
	 * The list contents are of type {@link organicdesign.TransformationAgent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connected Agent That Can Perform Task</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connected Agent That Can Perform Task</em>' reference list.
	 * @see #isSetConnectedAgentThatCanPerformTask()
	 * @see organicdesignhelper.OrganicdesignhelperPackage#getNextAgentForResourceContext_ConnectedAgentThatCanPerformTask()
	 * @model unsettable="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='<%org.eclipse.emf.common.util.EList%><<%organicdesign.TransformationAgent%>> agentList = new <%org.eclipse.emf.common.util.BasicEList%><<%organicdesign.TransformationAgent%>>();\n<%organicdesign.Cart%> _receiver = this.getReceiver();\n<%org.eclipse.emf.common.util.EList%><<%organicdesign.Location%>> _connectedLocations = _receiver.getConnectedLocations();\nfor (final <%organicdesign.Location%> loc : _connectedLocations)\n{\n\tif ((loc instanceof <%organicdesign.TransformationAgent%>))\n\t{\n\t\t<%organicdesign.Resource%> _resource = this.getResource();\n\t\t<%org.eclipse.emf.common.util.EList%><<%organicdesign.Task%>> _tasksThatAgentCanPerform = this.tasksThatAgentCanPerform(((<%organicdesign.TransformationAgent%>)loc), _resource);\n\t\tboolean _isEmpty = _tasksThatAgentCanPerform.isEmpty();\n\t\tboolean _not = (!_isEmpty);\n\t\tif (_not)\n\t\t{\n\t\t\tagentList.add(((<%organicdesign.TransformationAgent%>)loc));\n\t\t}\n\t}\n}\nreturn agentList;'"
	 * @generated
	 */
	EList<TransformationAgent> getConnectedAgentThatCanPerformTask();

	/**
	 * Returns whether the value of the '{@link organicdesignhelper.NextAgentForResourceContext#getConnectedAgentThatCanPerformTask <em>Connected Agent That Can Perform Task</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Connected Agent That Can Perform Task</em>' reference list is set.
	 * @see #getConnectedAgentThatCanPerformTask()
	 * @generated
	 */
	boolean isSetConnectedAgentThatCanPerformTask();

} // NextAgentForResourceContext
