/**
 */
package organicdesignhelper.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import organicdesign.Environment;
import organicdesign.Resource;
import organicdesign.Task;
import organicdesign.TransformationAgent;

import organicdesignhelper.OrganicdesignhelperPackage;
import organicdesignhelper.PerformTaskContext;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Perform Task Context</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link organicdesignhelper.impl.PerformTaskContextImpl#getReceiver <em>Receiver</em>}</li>
 *   <li>{@link organicdesignhelper.impl.PerformTaskContextImpl#getSender <em>Sender</em>}</li>
 *   <li>{@link organicdesignhelper.impl.PerformTaskContextImpl#getResource <em>Resource</em>}</li>
 *   <li>{@link organicdesignhelper.impl.PerformTaskContextImpl#getTasksThatAgentCanPerform <em>Tasks That Agent Can Perform</em>}</li>
 *   <li>{@link organicdesignhelper.impl.PerformTaskContextImpl#isMatches <em>Matches</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PerformTaskContextImpl extends AgentAndResourceContextImpl implements PerformTaskContext {
	/**
	 * The cached value of the '{@link #getReceiver() <em>Receiver</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReceiver()
	 * @generated
	 * @ordered
	 */
	protected TransformationAgent receiver;

	/**
	 * The cached value of the '{@link #getSender() <em>Sender</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSender()
	 * @generated
	 * @ordered
	 */
	protected Environment sender;

	/**
	 * The cached value of the '{@link #getResource() <em>Resource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResource()
	 * @generated
	 * @ordered
	 */
	protected Resource resource;

	/**
	 * The default value of the '{@link #isMatches() <em>Matches</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isMatches()
	 * @generated
	 * @ordered
	 */
	protected static final boolean MATCHES_EDEFAULT = false;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PerformTaskContextImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OrganicdesignhelperPackage.Literals.PERFORM_TASK_CONTEXT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationAgent getReceiver() {
		if (receiver != null && receiver.eIsProxy()) {
			InternalEObject oldReceiver = (InternalEObject)receiver;
			receiver = (TransformationAgent)eResolveProxy(oldReceiver);
			if (receiver != oldReceiver) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OrganicdesignhelperPackage.PERFORM_TASK_CONTEXT__RECEIVER, oldReceiver, receiver));
			}
		}
		return receiver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationAgent basicGetReceiver() {
		return receiver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReceiver(TransformationAgent newReceiver) {
		TransformationAgent oldReceiver = receiver;
		receiver = newReceiver;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OrganicdesignhelperPackage.PERFORM_TASK_CONTEXT__RECEIVER, oldReceiver, receiver));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Environment getSender() {
		if (sender != null && sender.eIsProxy()) {
			InternalEObject oldSender = (InternalEObject)sender;
			sender = (Environment)eResolveProxy(oldSender);
			if (sender != oldSender) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OrganicdesignhelperPackage.PERFORM_TASK_CONTEXT__SENDER, oldSender, sender));
			}
		}
		return sender;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Environment basicGetSender() {
		return sender;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSender(Environment newSender) {
		Environment oldSender = sender;
		sender = newSender;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OrganicdesignhelperPackage.PERFORM_TASK_CONTEXT__SENDER, oldSender, sender));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Resource getResource() {
		if (resource != null && resource.eIsProxy()) {
			InternalEObject oldResource = (InternalEObject)resource;
			resource = (Resource)eResolveProxy(oldResource);
			if (resource != oldResource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OrganicdesignhelperPackage.PERFORM_TASK_CONTEXT__RESOURCE, oldResource, resource));
			}
		}
		return resource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Resource basicGetResource() {
		return resource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResource(Resource newResource) {
		Resource oldResource = resource;
		resource = newResource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OrganicdesignhelperPackage.PERFORM_TASK_CONTEXT__RESOURCE, oldResource, resource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Task> getTasksThatAgentCanPerform() {
		TransformationAgent _receiver = this.getReceiver();
		Resource _resource = this.getResource();
		return this.tasksThatAgentCanPerform(_receiver, _resource);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetTasksThatAgentCanPerform() {
		// TODO: implement this method to return whether the 'Tasks That Agent Can Perform' reference list is set
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isMatches() {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OrganicdesignhelperPackage.PERFORM_TASK_CONTEXT__RECEIVER:
				if (resolve) return getReceiver();
				return basicGetReceiver();
			case OrganicdesignhelperPackage.PERFORM_TASK_CONTEXT__SENDER:
				if (resolve) return getSender();
				return basicGetSender();
			case OrganicdesignhelperPackage.PERFORM_TASK_CONTEXT__RESOURCE:
				if (resolve) return getResource();
				return basicGetResource();
			case OrganicdesignhelperPackage.PERFORM_TASK_CONTEXT__TASKS_THAT_AGENT_CAN_PERFORM:
				return getTasksThatAgentCanPerform();
			case OrganicdesignhelperPackage.PERFORM_TASK_CONTEXT__MATCHES:
				return isMatches();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OrganicdesignhelperPackage.PERFORM_TASK_CONTEXT__RECEIVER:
				setReceiver((TransformationAgent)newValue);
				return;
			case OrganicdesignhelperPackage.PERFORM_TASK_CONTEXT__SENDER:
				setSender((Environment)newValue);
				return;
			case OrganicdesignhelperPackage.PERFORM_TASK_CONTEXT__RESOURCE:
				setResource((Resource)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OrganicdesignhelperPackage.PERFORM_TASK_CONTEXT__RECEIVER:
				setReceiver((TransformationAgent)null);
				return;
			case OrganicdesignhelperPackage.PERFORM_TASK_CONTEXT__SENDER:
				setSender((Environment)null);
				return;
			case OrganicdesignhelperPackage.PERFORM_TASK_CONTEXT__RESOURCE:
				setResource((Resource)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OrganicdesignhelperPackage.PERFORM_TASK_CONTEXT__RECEIVER:
				return receiver != null;
			case OrganicdesignhelperPackage.PERFORM_TASK_CONTEXT__SENDER:
				return sender != null;
			case OrganicdesignhelperPackage.PERFORM_TASK_CONTEXT__RESOURCE:
				return resource != null;
			case OrganicdesignhelperPackage.PERFORM_TASK_CONTEXT__TASKS_THAT_AGENT_CAN_PERFORM:
				return isSetTasksThatAgentCanPerform();
			case OrganicdesignhelperPackage.PERFORM_TASK_CONTEXT__MATCHES:
				return isMatches() != MATCHES_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

} //PerformTaskContextImpl
