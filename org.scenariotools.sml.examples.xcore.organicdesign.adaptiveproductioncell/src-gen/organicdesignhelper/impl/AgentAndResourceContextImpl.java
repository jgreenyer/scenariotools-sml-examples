/**
 */
package organicdesignhelper.impl;

import com.google.common.base.Objects;

import java.lang.Iterable;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.util.ECollections;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.xtext.xbase.lib.Functions.Function1;

import org.eclipse.xtext.xbase.lib.IterableExtensions;

import organicdesign.Recipe;
import organicdesign.Resource;
import organicdesign.Task;
import organicdesign.TaskDependency;
import organicdesign.TransformationAgent;

import organicdesignhelper.AgentAndResourceContext;
import organicdesignhelper.OrganicdesignhelperPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Agent And Resource Context</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class AgentAndResourceContextImpl extends MinimalEObjectImpl.Container implements AgentAndResourceContext {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AgentAndResourceContextImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OrganicdesignhelperPackage.Literals.AGENT_AND_RESOURCE_CONTEXT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Task> tasksThatAgentCanPerform(final TransformationAgent transformationAgent, final Resource resource) {
		Recipe _recipe = resource.getRecipe();
		EList<Task> _requiredTasks = _recipe.getRequiredTasks();
		final Function1<Task, Boolean> _function = new Function1<Task, Boolean>() {
			public Boolean apply(final Task t) {
				EList<Task> _fulfilledTask = resource.getFulfilledTask();
				boolean _contains = _fulfilledTask.contains(t);
				return Boolean.valueOf((!_contains));
			}
		};
		Iterable<Task> _filter = IterableExtensions.<Task>filter(_requiredTasks, _function);
		final Function1<Task, Boolean> _function_1 = new Function1<Task, Boolean>() {
			public Boolean apply(final Task t) {
				Recipe _recipe = resource.getRecipe();
				EList<TaskDependency> _taskdependency = _recipe.getTaskdependency();
				final Function1<TaskDependency, Boolean> _function = new Function1<TaskDependency, Boolean>() {
					public Boolean apply(final TaskDependency d) {
						return Boolean.valueOf((Objects.equal(d.getTask(), t) && (!resource.getFulfilledTask().contains(d.getPrecondition()))));
					}
				};
				boolean _exists = IterableExtensions.<TaskDependency>exists(_taskdependency, _function);
				return Boolean.valueOf((!_exists));
			}
		};
		Iterable<Task> _filter_1 = IterableExtensions.<Task>filter(_filter, _function_1);
		final Function1<Task, Boolean> _function_2 = new Function1<Task, Boolean>() {
			public Boolean apply(final Task t) {
				return Boolean.valueOf(((!Objects.equal(transformationAgent.getCurrentRole(), null)) && transformationAgent.getCurrentRole().getProvidedCapabilities().contains(t.getRequiredCapability())));
			}
		};
		Iterable<Task> _filter_2 = IterableExtensions.<Task>filter(_filter_1, _function_2);
		return ECollections.<Task>toEList(_filter_2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case OrganicdesignhelperPackage.AGENT_AND_RESOURCE_CONTEXT___TASKS_THAT_AGENT_CAN_PERFORM__TRANSFORMATIONAGENT_RESOURCE:
				return tasksThatAgentCanPerform((TransformationAgent)arguments.get(0), (Resource)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

} //AgentAndResourceContextImpl
