/**
 */
package organicdesignhelper.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import organicdesignhelper.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OrganicdesignhelperFactoryImpl extends EFactoryImpl implements OrganicdesignhelperFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OrganicdesignhelperFactory init() {
		try {
			OrganicdesignhelperFactory theOrganicdesignhelperFactory = (OrganicdesignhelperFactory)EPackage.Registry.INSTANCE.getEFactory(OrganicdesignhelperPackage.eNS_URI);
			if (theOrganicdesignhelperFactory != null) {
				return theOrganicdesignhelperFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new OrganicdesignhelperFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrganicdesignhelperFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case OrganicdesignhelperPackage.PERFORM_TASK_CONTEXT: return createPerformTaskContext();
			case OrganicdesignhelperPackage.NEXT_AGENT_FOR_RESOURCE_CONTEXT: return createNextAgentForResourceContext();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PerformTaskContext createPerformTaskContext() {
		PerformTaskContextImpl performTaskContext = new PerformTaskContextImpl();
		return performTaskContext;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NextAgentForResourceContext createNextAgentForResourceContext() {
		NextAgentForResourceContextImpl nextAgentForResourceContext = new NextAgentForResourceContextImpl();
		return nextAgentForResourceContext;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrganicdesignhelperPackage getOrganicdesignhelperPackage() {
		return (OrganicdesignhelperPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static OrganicdesignhelperPackage getPackage() {
		return OrganicdesignhelperPackage.eINSTANCE;
	}

} //OrganicdesignhelperFactoryImpl
