/**
 */
package organicdesignhelper.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import organicdesign.OrganicdesignPackage;

import organicdesignhelper.AgentAndResourceContext;
import organicdesignhelper.NextAgentForResourceContext;
import organicdesignhelper.OrganicdesignhelperFactory;
import organicdesignhelper.OrganicdesignhelperPackage;
import organicdesignhelper.PerformTaskContext;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OrganicdesignhelperPackageImpl extends EPackageImpl implements OrganicdesignhelperPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass agentAndResourceContextEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass performTaskContextEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nextAgentForResourceContextEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see organicdesignhelper.OrganicdesignhelperPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private OrganicdesignhelperPackageImpl() {
		super(eNS_URI, OrganicdesignhelperFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link OrganicdesignhelperPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static OrganicdesignhelperPackage init() {
		if (isInited) return (OrganicdesignhelperPackage)EPackage.Registry.INSTANCE.getEPackage(OrganicdesignhelperPackage.eNS_URI);

		// Obtain or create and register package
		OrganicdesignhelperPackageImpl theOrganicdesignhelperPackage = (OrganicdesignhelperPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof OrganicdesignhelperPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new OrganicdesignhelperPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		OrganicdesignPackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theOrganicdesignhelperPackage.createPackageContents();

		// Initialize created meta-data
		theOrganicdesignhelperPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theOrganicdesignhelperPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(OrganicdesignhelperPackage.eNS_URI, theOrganicdesignhelperPackage);
		return theOrganicdesignhelperPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAgentAndResourceContext() {
		return agentAndResourceContextEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAgentAndResourceContext__TasksThatAgentCanPerform__TransformationAgent_Resource() {
		return agentAndResourceContextEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPerformTaskContext() {
		return performTaskContextEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerformTaskContext_Receiver() {
		return (EReference)performTaskContextEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerformTaskContext_Sender() {
		return (EReference)performTaskContextEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerformTaskContext_Resource() {
		return (EReference)performTaskContextEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPerformTaskContext_TasksThatAgentCanPerform() {
		return (EReference)performTaskContextEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPerformTaskContext_Matches() {
		return (EAttribute)performTaskContextEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNextAgentForResourceContext() {
		return nextAgentForResourceContextEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNextAgentForResourceContext_Sender() {
		return (EReference)nextAgentForResourceContextEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNextAgentForResourceContext_Receiver() {
		return (EReference)nextAgentForResourceContextEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNextAgentForResourceContext_Resource() {
		return (EReference)nextAgentForResourceContextEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNextAgentForResourceContext_ConnectedAgentThatCanPerformTask() {
		return (EReference)nextAgentForResourceContextEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrganicdesignhelperFactory getOrganicdesignhelperFactory() {
		return (OrganicdesignhelperFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		agentAndResourceContextEClass = createEClass(AGENT_AND_RESOURCE_CONTEXT);
		createEOperation(agentAndResourceContextEClass, AGENT_AND_RESOURCE_CONTEXT___TASKS_THAT_AGENT_CAN_PERFORM__TRANSFORMATIONAGENT_RESOURCE);

		performTaskContextEClass = createEClass(PERFORM_TASK_CONTEXT);
		createEReference(performTaskContextEClass, PERFORM_TASK_CONTEXT__RECEIVER);
		createEReference(performTaskContextEClass, PERFORM_TASK_CONTEXT__SENDER);
		createEReference(performTaskContextEClass, PERFORM_TASK_CONTEXT__RESOURCE);
		createEReference(performTaskContextEClass, PERFORM_TASK_CONTEXT__TASKS_THAT_AGENT_CAN_PERFORM);
		createEAttribute(performTaskContextEClass, PERFORM_TASK_CONTEXT__MATCHES);

		nextAgentForResourceContextEClass = createEClass(NEXT_AGENT_FOR_RESOURCE_CONTEXT);
		createEReference(nextAgentForResourceContextEClass, NEXT_AGENT_FOR_RESOURCE_CONTEXT__SENDER);
		createEReference(nextAgentForResourceContextEClass, NEXT_AGENT_FOR_RESOURCE_CONTEXT__RECEIVER);
		createEReference(nextAgentForResourceContextEClass, NEXT_AGENT_FOR_RESOURCE_CONTEXT__RESOURCE);
		createEReference(nextAgentForResourceContextEClass, NEXT_AGENT_FOR_RESOURCE_CONTEXT__CONNECTED_AGENT_THAT_CAN_PERFORM_TASK);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		OrganicdesignPackage theOrganicdesignPackage = (OrganicdesignPackage)EPackage.Registry.INSTANCE.getEPackage(OrganicdesignPackage.eNS_URI);
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		performTaskContextEClass.getESuperTypes().add(this.getAgentAndResourceContext());
		nextAgentForResourceContextEClass.getESuperTypes().add(this.getAgentAndResourceContext());

		// Initialize classes, features, and operations; add parameters
		initEClass(agentAndResourceContextEClass, AgentAndResourceContext.class, "AgentAndResourceContext", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getAgentAndResourceContext__TasksThatAgentCanPerform__TransformationAgent_Resource(), theOrganicdesignPackage.getTask(), "tasksThatAgentCanPerform", 0, -1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theOrganicdesignPackage.getTransformationAgent(), "transformationAgent", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, theOrganicdesignPackage.getResource(), "resource", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(performTaskContextEClass, PerformTaskContext.class, "PerformTaskContext", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPerformTaskContext_Receiver(), theOrganicdesignPackage.getTransformationAgent(), null, "receiver", null, 0, 1, PerformTaskContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPerformTaskContext_Sender(), theOrganicdesignPackage.getEnvironment(), null, "sender", null, 0, 1, PerformTaskContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPerformTaskContext_Resource(), theOrganicdesignPackage.getResource(), null, "resource", null, 0, 1, PerformTaskContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPerformTaskContext_TasksThatAgentCanPerform(), theOrganicdesignPackage.getTask(), null, "tasksThatAgentCanPerform", null, 0, -1, PerformTaskContext.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);
		initEAttribute(getPerformTaskContext_Matches(), theEcorePackage.getEBoolean(), "matches", null, 0, 1, PerformTaskContext.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(nextAgentForResourceContextEClass, NextAgentForResourceContext.class, "NextAgentForResourceContext", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNextAgentForResourceContext_Sender(), theOrganicdesignPackage.getTransformationAgent(), null, "sender", null, 0, 1, NextAgentForResourceContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNextAgentForResourceContext_Receiver(), theOrganicdesignPackage.getCart(), null, "receiver", null, 0, 1, NextAgentForResourceContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNextAgentForResourceContext_Resource(), theOrganicdesignPackage.getResource(), null, "resource", null, 0, 1, NextAgentForResourceContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getNextAgentForResourceContext_ConnectedAgentThatCanPerformTask(), theOrganicdesignPackage.getTransformationAgent(), null, "connectedAgentThatCanPerformTask", null, 0, -1, NextAgentForResourceContext.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //OrganicdesignhelperPackageImpl
