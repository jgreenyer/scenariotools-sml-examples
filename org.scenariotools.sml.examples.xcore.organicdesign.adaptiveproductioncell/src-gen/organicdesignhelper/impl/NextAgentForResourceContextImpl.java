/**
 */
package organicdesignhelper.impl;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import organicdesign.Cart;
import organicdesign.Location;
import organicdesign.Resource;
import organicdesign.Task;
import organicdesign.TransformationAgent;

import organicdesignhelper.NextAgentForResourceContext;
import organicdesignhelper.OrganicdesignhelperPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Next Agent For Resource Context</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link organicdesignhelper.impl.NextAgentForResourceContextImpl#getSender <em>Sender</em>}</li>
 *   <li>{@link organicdesignhelper.impl.NextAgentForResourceContextImpl#getReceiver <em>Receiver</em>}</li>
 *   <li>{@link organicdesignhelper.impl.NextAgentForResourceContextImpl#getResource <em>Resource</em>}</li>
 *   <li>{@link organicdesignhelper.impl.NextAgentForResourceContextImpl#getConnectedAgentThatCanPerformTask <em>Connected Agent That Can Perform Task</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NextAgentForResourceContextImpl extends AgentAndResourceContextImpl implements NextAgentForResourceContext {
	/**
	 * The cached value of the '{@link #getSender() <em>Sender</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSender()
	 * @generated
	 * @ordered
	 */
	protected TransformationAgent sender;

	/**
	 * The cached value of the '{@link #getReceiver() <em>Receiver</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReceiver()
	 * @generated
	 * @ordered
	 */
	protected Cart receiver;

	/**
	 * The cached value of the '{@link #getResource() <em>Resource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResource()
	 * @generated
	 * @ordered
	 */
	protected Resource resource;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NextAgentForResourceContextImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OrganicdesignhelperPackage.Literals.NEXT_AGENT_FOR_RESOURCE_CONTEXT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationAgent getSender() {
		if (sender != null && sender.eIsProxy()) {
			InternalEObject oldSender = (InternalEObject)sender;
			sender = (TransformationAgent)eResolveProxy(oldSender);
			if (sender != oldSender) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OrganicdesignhelperPackage.NEXT_AGENT_FOR_RESOURCE_CONTEXT__SENDER, oldSender, sender));
			}
		}
		return sender;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationAgent basicGetSender() {
		return sender;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSender(TransformationAgent newSender) {
		TransformationAgent oldSender = sender;
		sender = newSender;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OrganicdesignhelperPackage.NEXT_AGENT_FOR_RESOURCE_CONTEXT__SENDER, oldSender, sender));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Cart getReceiver() {
		if (receiver != null && receiver.eIsProxy()) {
			InternalEObject oldReceiver = (InternalEObject)receiver;
			receiver = (Cart)eResolveProxy(oldReceiver);
			if (receiver != oldReceiver) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OrganicdesignhelperPackage.NEXT_AGENT_FOR_RESOURCE_CONTEXT__RECEIVER, oldReceiver, receiver));
			}
		}
		return receiver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Cart basicGetReceiver() {
		return receiver;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setReceiver(Cart newReceiver) {
		Cart oldReceiver = receiver;
		receiver = newReceiver;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OrganicdesignhelperPackage.NEXT_AGENT_FOR_RESOURCE_CONTEXT__RECEIVER, oldReceiver, receiver));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Resource getResource() {
		if (resource != null && resource.eIsProxy()) {
			InternalEObject oldResource = (InternalEObject)resource;
			resource = (Resource)eResolveProxy(oldResource);
			if (resource != oldResource) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OrganicdesignhelperPackage.NEXT_AGENT_FOR_RESOURCE_CONTEXT__RESOURCE, oldResource, resource));
			}
		}
		return resource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Resource basicGetResource() {
		return resource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResource(Resource newResource) {
		Resource oldResource = resource;
		resource = newResource;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OrganicdesignhelperPackage.NEXT_AGENT_FOR_RESOURCE_CONTEXT__RESOURCE, oldResource, resource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TransformationAgent> getConnectedAgentThatCanPerformTask() {
		EList<TransformationAgent> agentList = new BasicEList<TransformationAgent>();
		Cart _receiver = this.getReceiver();
		EList<Location> _connectedLocations = _receiver.getConnectedLocations();
		for (final Location loc : _connectedLocations) {
			if ((loc instanceof TransformationAgent)) {
				Resource _resource = this.getResource();
				EList<Task> _tasksThatAgentCanPerform = this.tasksThatAgentCanPerform(((TransformationAgent)loc), _resource);
				boolean _isEmpty = _tasksThatAgentCanPerform.isEmpty();
				boolean _not = (!_isEmpty);
				if (_not) {
					agentList.add(((TransformationAgent)loc));
				}
			}
		}
		return agentList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSetConnectedAgentThatCanPerformTask() {
		// TODO: implement this method to return whether the 'Connected Agent That Can Perform Task' reference list is set
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OrganicdesignhelperPackage.NEXT_AGENT_FOR_RESOURCE_CONTEXT__SENDER:
				if (resolve) return getSender();
				return basicGetSender();
			case OrganicdesignhelperPackage.NEXT_AGENT_FOR_RESOURCE_CONTEXT__RECEIVER:
				if (resolve) return getReceiver();
				return basicGetReceiver();
			case OrganicdesignhelperPackage.NEXT_AGENT_FOR_RESOURCE_CONTEXT__RESOURCE:
				if (resolve) return getResource();
				return basicGetResource();
			case OrganicdesignhelperPackage.NEXT_AGENT_FOR_RESOURCE_CONTEXT__CONNECTED_AGENT_THAT_CAN_PERFORM_TASK:
				return getConnectedAgentThatCanPerformTask();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OrganicdesignhelperPackage.NEXT_AGENT_FOR_RESOURCE_CONTEXT__SENDER:
				setSender((TransformationAgent)newValue);
				return;
			case OrganicdesignhelperPackage.NEXT_AGENT_FOR_RESOURCE_CONTEXT__RECEIVER:
				setReceiver((Cart)newValue);
				return;
			case OrganicdesignhelperPackage.NEXT_AGENT_FOR_RESOURCE_CONTEXT__RESOURCE:
				setResource((Resource)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OrganicdesignhelperPackage.NEXT_AGENT_FOR_RESOURCE_CONTEXT__SENDER:
				setSender((TransformationAgent)null);
				return;
			case OrganicdesignhelperPackage.NEXT_AGENT_FOR_RESOURCE_CONTEXT__RECEIVER:
				setReceiver((Cart)null);
				return;
			case OrganicdesignhelperPackage.NEXT_AGENT_FOR_RESOURCE_CONTEXT__RESOURCE:
				setResource((Resource)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OrganicdesignhelperPackage.NEXT_AGENT_FOR_RESOURCE_CONTEXT__SENDER:
				return sender != null;
			case OrganicdesignhelperPackage.NEXT_AGENT_FOR_RESOURCE_CONTEXT__RECEIVER:
				return receiver != null;
			case OrganicdesignhelperPackage.NEXT_AGENT_FOR_RESOURCE_CONTEXT__RESOURCE:
				return resource != null;
			case OrganicdesignhelperPackage.NEXT_AGENT_FOR_RESOURCE_CONTEXT__CONNECTED_AGENT_THAT_CAN_PERFORM_TASK:
				return isSetConnectedAgentThatCanPerformTask();
		}
		return super.eIsSet(featureID);
	}

} //NextAgentForResourceContextImpl
