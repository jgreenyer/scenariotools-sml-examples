/**
 */
package organicdesignhelper;

import org.eclipse.emf.common.util.EList;

import organicdesign.Environment;
import organicdesign.Resource;
import organicdesign.Task;
import organicdesign.TransformationAgent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Perform Task Context</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link organicdesignhelper.PerformTaskContext#getReceiver <em>Receiver</em>}</li>
 *   <li>{@link organicdesignhelper.PerformTaskContext#getSender <em>Sender</em>}</li>
 *   <li>{@link organicdesignhelper.PerformTaskContext#getResource <em>Resource</em>}</li>
 *   <li>{@link organicdesignhelper.PerformTaskContext#getTasksThatAgentCanPerform <em>Tasks That Agent Can Perform</em>}</li>
 *   <li>{@link organicdesignhelper.PerformTaskContext#isMatches <em>Matches</em>}</li>
 * </ul>
 *
 * @see organicdesignhelper.OrganicdesignhelperPackage#getPerformTaskContext()
 * @model
 * @generated
 */
public interface PerformTaskContext extends AgentAndResourceContext {
	/**
	 * Returns the value of the '<em><b>Receiver</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Receiver</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Receiver</em>' reference.
	 * @see #setReceiver(TransformationAgent)
	 * @see organicdesignhelper.OrganicdesignhelperPackage#getPerformTaskContext_Receiver()
	 * @model
	 * @generated
	 */
	TransformationAgent getReceiver();

	/**
	 * Sets the value of the '{@link organicdesignhelper.PerformTaskContext#getReceiver <em>Receiver</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Receiver</em>' reference.
	 * @see #getReceiver()
	 * @generated
	 */
	void setReceiver(TransformationAgent value);

	/**
	 * Returns the value of the '<em><b>Sender</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sender</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sender</em>' reference.
	 * @see #setSender(Environment)
	 * @see organicdesignhelper.OrganicdesignhelperPackage#getPerformTaskContext_Sender()
	 * @model
	 * @generated
	 */
	Environment getSender();

	/**
	 * Sets the value of the '{@link organicdesignhelper.PerformTaskContext#getSender <em>Sender</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sender</em>' reference.
	 * @see #getSender()
	 * @generated
	 */
	void setSender(Environment value);

	/**
	 * Returns the value of the '<em><b>Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource</em>' reference.
	 * @see #setResource(Resource)
	 * @see organicdesignhelper.OrganicdesignhelperPackage#getPerformTaskContext_Resource()
	 * @model
	 * @generated
	 */
	Resource getResource();

	/**
	 * Sets the value of the '{@link organicdesignhelper.PerformTaskContext#getResource <em>Resource</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resource</em>' reference.
	 * @see #getResource()
	 * @generated
	 */
	void setResource(Resource value);

	/**
	 * Returns the value of the '<em><b>Tasks That Agent Can Perform</b></em>' reference list.
	 * The list contents are of type {@link organicdesign.Task}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tasks That Agent Can Perform</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tasks That Agent Can Perform</em>' reference list.
	 * @see #isSetTasksThatAgentCanPerform()
	 * @see organicdesignhelper.OrganicdesignhelperPackage#getPerformTaskContext_TasksThatAgentCanPerform()
	 * @model unsettable="true" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='<%organicdesign.TransformationAgent%> _receiver = this.getReceiver();\n<%organicdesign.Resource%> _resource = this.getResource();\nreturn this.tasksThatAgentCanPerform(_receiver, _resource);'"
	 * @generated
	 */
	EList<Task> getTasksThatAgentCanPerform();

	/**
	 * Returns whether the value of the '{@link organicdesignhelper.PerformTaskContext#getTasksThatAgentCanPerform <em>Tasks That Agent Can Perform</em>}' reference list is set.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return whether the value of the '<em>Tasks That Agent Can Perform</em>' reference list is set.
	 * @see #getTasksThatAgentCanPerform()
	 * @generated
	 */
	boolean isSetTasksThatAgentCanPerform();

	/**
	 * Returns the value of the '<em><b>Matches</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Matches</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Matches</em>' attribute.
	 * @see organicdesignhelper.OrganicdesignhelperPackage#getPerformTaskContext_Matches()
	 * @model unique="false" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='return true;'"
	 * @generated
	 */
	boolean isMatches();

} // PerformTaskContext
