/**
 */
package organicdesignhelper;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see organicdesignhelper.OrganicdesignhelperFactory
 * @model kind="package"
 * @generated
 */
public interface OrganicdesignhelperPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "organicdesignhelper";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "organicdesignhelper";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "organicdesignhelper";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OrganicdesignhelperPackage eINSTANCE = organicdesignhelper.impl.OrganicdesignhelperPackageImpl.init();

	/**
	 * The meta object id for the '{@link organicdesignhelper.impl.AgentAndResourceContextImpl <em>Agent And Resource Context</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see organicdesignhelper.impl.AgentAndResourceContextImpl
	 * @see organicdesignhelper.impl.OrganicdesignhelperPackageImpl#getAgentAndResourceContext()
	 * @generated
	 */
	int AGENT_AND_RESOURCE_CONTEXT = 0;

	/**
	 * The number of structural features of the '<em>Agent And Resource Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_AND_RESOURCE_CONTEXT_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Tasks That Agent Can Perform</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_AND_RESOURCE_CONTEXT___TASKS_THAT_AGENT_CAN_PERFORM__TRANSFORMATIONAGENT_RESOURCE = 0;

	/**
	 * The number of operations of the '<em>Agent And Resource Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_AND_RESOURCE_CONTEXT_OPERATION_COUNT = 1;

	/**
	 * The meta object id for the '{@link organicdesignhelper.impl.PerformTaskContextImpl <em>Perform Task Context</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see organicdesignhelper.impl.PerformTaskContextImpl
	 * @see organicdesignhelper.impl.OrganicdesignhelperPackageImpl#getPerformTaskContext()
	 * @generated
	 */
	int PERFORM_TASK_CONTEXT = 1;

	/**
	 * The feature id for the '<em><b>Receiver</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERFORM_TASK_CONTEXT__RECEIVER = AGENT_AND_RESOURCE_CONTEXT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sender</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERFORM_TASK_CONTEXT__SENDER = AGENT_AND_RESOURCE_CONTEXT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERFORM_TASK_CONTEXT__RESOURCE = AGENT_AND_RESOURCE_CONTEXT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Tasks That Agent Can Perform</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERFORM_TASK_CONTEXT__TASKS_THAT_AGENT_CAN_PERFORM = AGENT_AND_RESOURCE_CONTEXT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Matches</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERFORM_TASK_CONTEXT__MATCHES = AGENT_AND_RESOURCE_CONTEXT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Perform Task Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERFORM_TASK_CONTEXT_FEATURE_COUNT = AGENT_AND_RESOURCE_CONTEXT_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Tasks That Agent Can Perform</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERFORM_TASK_CONTEXT___TASKS_THAT_AGENT_CAN_PERFORM__TRANSFORMATIONAGENT_RESOURCE = AGENT_AND_RESOURCE_CONTEXT___TASKS_THAT_AGENT_CAN_PERFORM__TRANSFORMATIONAGENT_RESOURCE;

	/**
	 * The number of operations of the '<em>Perform Task Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERFORM_TASK_CONTEXT_OPERATION_COUNT = AGENT_AND_RESOURCE_CONTEXT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link organicdesignhelper.impl.NextAgentForResourceContextImpl <em>Next Agent For Resource Context</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see organicdesignhelper.impl.NextAgentForResourceContextImpl
	 * @see organicdesignhelper.impl.OrganicdesignhelperPackageImpl#getNextAgentForResourceContext()
	 * @generated
	 */
	int NEXT_AGENT_FOR_RESOURCE_CONTEXT = 2;

	/**
	 * The feature id for the '<em><b>Sender</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEXT_AGENT_FOR_RESOURCE_CONTEXT__SENDER = AGENT_AND_RESOURCE_CONTEXT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Receiver</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEXT_AGENT_FOR_RESOURCE_CONTEXT__RECEIVER = AGENT_AND_RESOURCE_CONTEXT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Resource</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEXT_AGENT_FOR_RESOURCE_CONTEXT__RESOURCE = AGENT_AND_RESOURCE_CONTEXT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Connected Agent That Can Perform Task</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEXT_AGENT_FOR_RESOURCE_CONTEXT__CONNECTED_AGENT_THAT_CAN_PERFORM_TASK = AGENT_AND_RESOURCE_CONTEXT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Next Agent For Resource Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEXT_AGENT_FOR_RESOURCE_CONTEXT_FEATURE_COUNT = AGENT_AND_RESOURCE_CONTEXT_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Tasks That Agent Can Perform</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEXT_AGENT_FOR_RESOURCE_CONTEXT___TASKS_THAT_AGENT_CAN_PERFORM__TRANSFORMATIONAGENT_RESOURCE = AGENT_AND_RESOURCE_CONTEXT___TASKS_THAT_AGENT_CAN_PERFORM__TRANSFORMATIONAGENT_RESOURCE;

	/**
	 * The number of operations of the '<em>Next Agent For Resource Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEXT_AGENT_FOR_RESOURCE_CONTEXT_OPERATION_COUNT = AGENT_AND_RESOURCE_CONTEXT_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link organicdesignhelper.AgentAndResourceContext <em>Agent And Resource Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Agent And Resource Context</em>'.
	 * @see organicdesignhelper.AgentAndResourceContext
	 * @generated
	 */
	EClass getAgentAndResourceContext();

	/**
	 * Returns the meta object for the '{@link organicdesignhelper.AgentAndResourceContext#tasksThatAgentCanPerform(organicdesign.TransformationAgent, organicdesign.Resource) <em>Tasks That Agent Can Perform</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Tasks That Agent Can Perform</em>' operation.
	 * @see organicdesignhelper.AgentAndResourceContext#tasksThatAgentCanPerform(organicdesign.TransformationAgent, organicdesign.Resource)
	 * @generated
	 */
	EOperation getAgentAndResourceContext__TasksThatAgentCanPerform__TransformationAgent_Resource();

	/**
	 * Returns the meta object for class '{@link organicdesignhelper.PerformTaskContext <em>Perform Task Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Perform Task Context</em>'.
	 * @see organicdesignhelper.PerformTaskContext
	 * @generated
	 */
	EClass getPerformTaskContext();

	/**
	 * Returns the meta object for the reference '{@link organicdesignhelper.PerformTaskContext#getReceiver <em>Receiver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Receiver</em>'.
	 * @see organicdesignhelper.PerformTaskContext#getReceiver()
	 * @see #getPerformTaskContext()
	 * @generated
	 */
	EReference getPerformTaskContext_Receiver();

	/**
	 * Returns the meta object for the reference '{@link organicdesignhelper.PerformTaskContext#getSender <em>Sender</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sender</em>'.
	 * @see organicdesignhelper.PerformTaskContext#getSender()
	 * @see #getPerformTaskContext()
	 * @generated
	 */
	EReference getPerformTaskContext_Sender();

	/**
	 * Returns the meta object for the reference '{@link organicdesignhelper.PerformTaskContext#getResource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Resource</em>'.
	 * @see organicdesignhelper.PerformTaskContext#getResource()
	 * @see #getPerformTaskContext()
	 * @generated
	 */
	EReference getPerformTaskContext_Resource();

	/**
	 * Returns the meta object for the reference list '{@link organicdesignhelper.PerformTaskContext#getTasksThatAgentCanPerform <em>Tasks That Agent Can Perform</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Tasks That Agent Can Perform</em>'.
	 * @see organicdesignhelper.PerformTaskContext#getTasksThatAgentCanPerform()
	 * @see #getPerformTaskContext()
	 * @generated
	 */
	EReference getPerformTaskContext_TasksThatAgentCanPerform();

	/**
	 * Returns the meta object for the attribute '{@link organicdesignhelper.PerformTaskContext#isMatches <em>Matches</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Matches</em>'.
	 * @see organicdesignhelper.PerformTaskContext#isMatches()
	 * @see #getPerformTaskContext()
	 * @generated
	 */
	EAttribute getPerformTaskContext_Matches();

	/**
	 * Returns the meta object for class '{@link organicdesignhelper.NextAgentForResourceContext <em>Next Agent For Resource Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Next Agent For Resource Context</em>'.
	 * @see organicdesignhelper.NextAgentForResourceContext
	 * @generated
	 */
	EClass getNextAgentForResourceContext();

	/**
	 * Returns the meta object for the reference '{@link organicdesignhelper.NextAgentForResourceContext#getSender <em>Sender</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sender</em>'.
	 * @see organicdesignhelper.NextAgentForResourceContext#getSender()
	 * @see #getNextAgentForResourceContext()
	 * @generated
	 */
	EReference getNextAgentForResourceContext_Sender();

	/**
	 * Returns the meta object for the reference '{@link organicdesignhelper.NextAgentForResourceContext#getReceiver <em>Receiver</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Receiver</em>'.
	 * @see organicdesignhelper.NextAgentForResourceContext#getReceiver()
	 * @see #getNextAgentForResourceContext()
	 * @generated
	 */
	EReference getNextAgentForResourceContext_Receiver();

	/**
	 * Returns the meta object for the reference '{@link organicdesignhelper.NextAgentForResourceContext#getResource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Resource</em>'.
	 * @see organicdesignhelper.NextAgentForResourceContext#getResource()
	 * @see #getNextAgentForResourceContext()
	 * @generated
	 */
	EReference getNextAgentForResourceContext_Resource();

	/**
	 * Returns the meta object for the reference list '{@link organicdesignhelper.NextAgentForResourceContext#getConnectedAgentThatCanPerformTask <em>Connected Agent That Can Perform Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Connected Agent That Can Perform Task</em>'.
	 * @see organicdesignhelper.NextAgentForResourceContext#getConnectedAgentThatCanPerformTask()
	 * @see #getNextAgentForResourceContext()
	 * @generated
	 */
	EReference getNextAgentForResourceContext_ConnectedAgentThatCanPerformTask();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OrganicdesignhelperFactory getOrganicdesignhelperFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link organicdesignhelper.impl.AgentAndResourceContextImpl <em>Agent And Resource Context</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see organicdesignhelper.impl.AgentAndResourceContextImpl
		 * @see organicdesignhelper.impl.OrganicdesignhelperPackageImpl#getAgentAndResourceContext()
		 * @generated
		 */
		EClass AGENT_AND_RESOURCE_CONTEXT = eINSTANCE.getAgentAndResourceContext();

		/**
		 * The meta object literal for the '<em><b>Tasks That Agent Can Perform</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AGENT_AND_RESOURCE_CONTEXT___TASKS_THAT_AGENT_CAN_PERFORM__TRANSFORMATIONAGENT_RESOURCE = eINSTANCE.getAgentAndResourceContext__TasksThatAgentCanPerform__TransformationAgent_Resource();

		/**
		 * The meta object literal for the '{@link organicdesignhelper.impl.PerformTaskContextImpl <em>Perform Task Context</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see organicdesignhelper.impl.PerformTaskContextImpl
		 * @see organicdesignhelper.impl.OrganicdesignhelperPackageImpl#getPerformTaskContext()
		 * @generated
		 */
		EClass PERFORM_TASK_CONTEXT = eINSTANCE.getPerformTaskContext();

		/**
		 * The meta object literal for the '<em><b>Receiver</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERFORM_TASK_CONTEXT__RECEIVER = eINSTANCE.getPerformTaskContext_Receiver();

		/**
		 * The meta object literal for the '<em><b>Sender</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERFORM_TASK_CONTEXT__SENDER = eINSTANCE.getPerformTaskContext_Sender();

		/**
		 * The meta object literal for the '<em><b>Resource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERFORM_TASK_CONTEXT__RESOURCE = eINSTANCE.getPerformTaskContext_Resource();

		/**
		 * The meta object literal for the '<em><b>Tasks That Agent Can Perform</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERFORM_TASK_CONTEXT__TASKS_THAT_AGENT_CAN_PERFORM = eINSTANCE.getPerformTaskContext_TasksThatAgentCanPerform();

		/**
		 * The meta object literal for the '<em><b>Matches</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERFORM_TASK_CONTEXT__MATCHES = eINSTANCE.getPerformTaskContext_Matches();

		/**
		 * The meta object literal for the '{@link organicdesignhelper.impl.NextAgentForResourceContextImpl <em>Next Agent For Resource Context</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see organicdesignhelper.impl.NextAgentForResourceContextImpl
		 * @see organicdesignhelper.impl.OrganicdesignhelperPackageImpl#getNextAgentForResourceContext()
		 * @generated
		 */
		EClass NEXT_AGENT_FOR_RESOURCE_CONTEXT = eINSTANCE.getNextAgentForResourceContext();

		/**
		 * The meta object literal for the '<em><b>Sender</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NEXT_AGENT_FOR_RESOURCE_CONTEXT__SENDER = eINSTANCE.getNextAgentForResourceContext_Sender();

		/**
		 * The meta object literal for the '<em><b>Receiver</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NEXT_AGENT_FOR_RESOURCE_CONTEXT__RECEIVER = eINSTANCE.getNextAgentForResourceContext_Receiver();

		/**
		 * The meta object literal for the '<em><b>Resource</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NEXT_AGENT_FOR_RESOURCE_CONTEXT__RESOURCE = eINSTANCE.getNextAgentForResourceContext_Resource();

		/**
		 * The meta object literal for the '<em><b>Connected Agent That Can Perform Task</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NEXT_AGENT_FOR_RESOURCE_CONTEXT__CONNECTED_AGENT_THAT_CAN_PERFORM_TASK = eINSTANCE.getNextAgentForResourceContext_ConnectedAgentThatCanPerformTask();

	}

} //OrganicdesignhelperPackage
