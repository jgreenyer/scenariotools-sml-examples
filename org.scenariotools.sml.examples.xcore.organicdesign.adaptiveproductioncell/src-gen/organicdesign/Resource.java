/**
 */
package organicdesign;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resource</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link organicdesign.Resource#getRecipe <em>Recipe</em>}</li>
 *   <li>{@link organicdesign.Resource#getFulfilledTask <em>Fulfilled Task</em>}</li>
 *   <li>{@link organicdesign.Resource#getLocation <em>Location</em>}</li>
 * </ul>
 *
 * @see organicdesign.OrganicdesignPackage#getResource()
 * @model
 * @generated
 */
public interface Resource extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Recipe</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Recipe</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Recipe</em>' reference.
	 * @see #setRecipe(Recipe)
	 * @see organicdesign.OrganicdesignPackage#getResource_Recipe()
	 * @model
	 * @generated
	 */
	Recipe getRecipe();

	/**
	 * Sets the value of the '{@link organicdesign.Resource#getRecipe <em>Recipe</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Recipe</em>' reference.
	 * @see #getRecipe()
	 * @generated
	 */
	void setRecipe(Recipe value);

	/**
	 * Returns the value of the '<em><b>Fulfilled Task</b></em>' reference list.
	 * The list contents are of type {@link organicdesign.Task}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Fulfilled Task</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Fulfilled Task</em>' reference list.
	 * @see organicdesign.OrganicdesignPackage#getResource_FulfilledTask()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Task> getFulfilledTask();

	/**
	 * Returns the value of the '<em><b>Location</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link organicdesign.Location#getLocatedResources <em>Located Resources</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Location</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location</em>' reference.
	 * @see #setLocation(Location)
	 * @see organicdesign.OrganicdesignPackage#getResource_Location()
	 * @see organicdesign.Location#getLocatedResources
	 * @model opposite="locatedResources"
	 * @generated
	 */
	Location getLocation();

	/**
	 * Sets the value of the '{@link organicdesign.Resource#getLocation <em>Location</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Location</em>' reference.
	 * @see #getLocation()
	 * @generated
	 */
	void setLocation(Location value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" taskUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return (this.getRecipe().getRequiredTasks().contains(task) && (!this.getFulfilledTask().contains(task)));'"
	 * @generated
	 */
	boolean CAN_performTask(Task task);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model taskUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='<%org.eclipse.emf.common.util.EList%><<%organicdesign.Task%>> _fulfilledTask = this.getFulfilledTask();\n_fulfilledTask.add(task);'"
	 * @generated
	 */
	void performTask(Task task);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model envUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='<%organicdesign.EnvironmentLocation%> _environmentLocation = env.getEnvironmentLocation();\nthis.setLocation(_environmentLocation);\n<%org.eclipse.emf.common.util.EList%><<%organicdesign.Task%>> _fulfilledTask = this.getFulfilledTask();\n_fulfilledTask.clear();'"
	 * @generated
	 */
	void reset(Environment env);

} // Resource
