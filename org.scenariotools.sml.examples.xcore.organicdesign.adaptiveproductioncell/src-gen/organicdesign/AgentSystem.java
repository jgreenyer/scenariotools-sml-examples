/**
 */
package organicdesign;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Agent System</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link organicdesign.AgentSystem#getResource <em>Resource</em>}</li>
 *   <li>{@link organicdesign.AgentSystem#getAgents <em>Agents</em>}</li>
 *   <li>{@link organicdesign.AgentSystem#getTasks <em>Tasks</em>}</li>
 *   <li>{@link organicdesign.AgentSystem#getCapabilities <em>Capabilities</em>}</li>
 *   <li>{@link organicdesign.AgentSystem#getRoles <em>Roles</em>}</li>
 *   <li>{@link organicdesign.AgentSystem#getRecipes <em>Recipes</em>}</li>
 *   <li>{@link organicdesign.AgentSystem#getEnvironment <em>Environment</em>}</li>
 *   <li>{@link organicdesign.AgentSystem#getEnvironmentlocation <em>Environmentlocation</em>}</li>
 * </ul>
 *
 * @see organicdesign.OrganicdesignPackage#getAgentSystem()
 * @model
 * @generated
 */
public interface AgentSystem extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Resource</b></em>' containment reference list.
	 * The list contents are of type {@link organicdesign.Resource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource</em>' containment reference list.
	 * @see organicdesign.OrganicdesignPackage#getAgentSystem_Resource()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Resource> getResource();

	/**
	 * Returns the value of the '<em><b>Agents</b></em>' containment reference list.
	 * The list contents are of type {@link organicdesign.Agent}.
	 * It is bidirectional and its opposite is '{@link organicdesign.Agent#getAgentSystem <em>Agent System</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Agents</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Agents</em>' containment reference list.
	 * @see organicdesign.OrganicdesignPackage#getAgentSystem_Agents()
	 * @see organicdesign.Agent#getAgentSystem
	 * @model opposite="agentSystem" containment="true" ordered="false"
	 * @generated
	 */
	EList<Agent> getAgents();

	/**
	 * Returns the value of the '<em><b>Tasks</b></em>' containment reference list.
	 * The list contents are of type {@link organicdesign.Task}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Tasks</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Tasks</em>' containment reference list.
	 * @see organicdesign.OrganicdesignPackage#getAgentSystem_Tasks()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Task> getTasks();

	/**
	 * Returns the value of the '<em><b>Capabilities</b></em>' containment reference list.
	 * The list contents are of type {@link organicdesign.Capability}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Capabilities</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Capabilities</em>' containment reference list.
	 * @see organicdesign.OrganicdesignPackage#getAgentSystem_Capabilities()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Capability> getCapabilities();

	/**
	 * Returns the value of the '<em><b>Roles</b></em>' containment reference list.
	 * The list contents are of type {@link organicdesign.Role}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Roles</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Roles</em>' containment reference list.
	 * @see organicdesign.OrganicdesignPackage#getAgentSystem_Roles()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Role> getRoles();

	/**
	 * Returns the value of the '<em><b>Recipes</b></em>' containment reference list.
	 * The list contents are of type {@link organicdesign.Recipe}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Recipes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Recipes</em>' containment reference list.
	 * @see organicdesign.OrganicdesignPackage#getAgentSystem_Recipes()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Recipe> getRecipes();

	/**
	 * Returns the value of the '<em><b>Environment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Environment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Environment</em>' containment reference.
	 * @see #setEnvironment(Environment)
	 * @see organicdesign.OrganicdesignPackage#getAgentSystem_Environment()
	 * @model containment="true"
	 * @generated
	 */
	Environment getEnvironment();

	/**
	 * Sets the value of the '{@link organicdesign.AgentSystem#getEnvironment <em>Environment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Environment</em>' containment reference.
	 * @see #getEnvironment()
	 * @generated
	 */
	void setEnvironment(Environment value);

	/**
	 * Returns the value of the '<em><b>Environmentlocation</b></em>' containment reference list.
	 * The list contents are of type {@link organicdesign.EnvironmentLocation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Environmentlocation</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Environmentlocation</em>' containment reference list.
	 * @see organicdesign.OrganicdesignPackage#getAgentSystem_Environmentlocation()
	 * @model containment="true"
	 * @generated
	 */
	EList<EnvironmentLocation> getEnvironmentlocation();

} // AgentSystem
