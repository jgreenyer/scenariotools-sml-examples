/**
 */
package organicdesign;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Role</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link organicdesign.Role#getProvidedCapabilities <em>Provided Capabilities</em>}</li>
 * </ul>
 *
 * @see organicdesign.OrganicdesignPackage#getRole()
 * @model
 * @generated
 */
public interface Role extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Provided Capabilities</b></em>' reference list.
	 * The list contents are of type {@link organicdesign.Capability}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Provided Capabilities</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provided Capabilities</em>' reference list.
	 * @see organicdesign.OrganicdesignPackage#getRole_ProvidedCapabilities()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Capability> getProvidedCapabilities();

} // Role
