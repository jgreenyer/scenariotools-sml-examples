/**
 */
package organicdesign;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Capability</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see organicdesign.OrganicdesignPackage#getCapability()
 * @model
 * @generated
 */
public interface Capability extends NamedElement {
} // Capability
