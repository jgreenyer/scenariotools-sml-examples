/**
 */
package organicdesign;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Recipe</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link organicdesign.Recipe#getRequiredTasks <em>Required Tasks</em>}</li>
 *   <li>{@link organicdesign.Recipe#getTaskdependency <em>Taskdependency</em>}</li>
 * </ul>
 *
 * @see organicdesign.OrganicdesignPackage#getRecipe()
 * @model
 * @generated
 */
public interface Recipe extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Required Tasks</b></em>' reference list.
	 * The list contents are of type {@link organicdesign.Task}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Tasks</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Tasks</em>' reference list.
	 * @see organicdesign.OrganicdesignPackage#getRecipe_RequiredTasks()
	 * @model
	 * @generated
	 */
	EList<Task> getRequiredTasks();

	/**
	 * Returns the value of the '<em><b>Taskdependency</b></em>' containment reference list.
	 * The list contents are of type {@link organicdesign.TaskDependency}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Taskdependency</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Taskdependency</em>' containment reference list.
	 * @see organicdesign.OrganicdesignPackage#getRecipe_Taskdependency()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<TaskDependency> getTaskdependency();

} // Recipe
