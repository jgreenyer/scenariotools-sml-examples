/**
 */
package organicdesign;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Task</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link organicdesign.Task#getRequiredCapability <em>Required Capability</em>}</li>
 * </ul>
 *
 * @see organicdesign.OrganicdesignPackage#getTask()
 * @model
 * @generated
 */
public interface Task extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Required Capability</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Capability</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Capability</em>' reference.
	 * @see #setRequiredCapability(Capability)
	 * @see organicdesign.OrganicdesignPackage#getTask_RequiredCapability()
	 * @model
	 * @generated
	 */
	Capability getRequiredCapability();

	/**
	 * Sets the value of the '{@link organicdesign.Task#getRequiredCapability <em>Required Capability</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required Capability</em>' reference.
	 * @see #getRequiredCapability()
	 * @generated
	 */
	void setRequiredCapability(Capability value);

} // Task
