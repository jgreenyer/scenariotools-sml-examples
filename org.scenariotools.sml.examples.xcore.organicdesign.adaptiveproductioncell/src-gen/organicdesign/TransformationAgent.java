/**
 */
package organicdesign;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Transformation Agent</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link organicdesign.TransformationAgent#getAllRoles <em>All Roles</em>}</li>
 *   <li>{@link organicdesign.TransformationAgent#getCurrentRole <em>Current Role</em>}</li>
 *   <li>{@link organicdesign.TransformationAgent#getAllCapabilities <em>All Capabilities</em>}</li>
 *   <li>{@link organicdesign.TransformationAgent#getActivatedCapabilities <em>Activated Capabilities</em>}</li>
 *   <li>{@link organicdesign.TransformationAgent#getUnavailableCapabilities <em>Unavailable Capabilities</em>}</li>
 * </ul>
 *
 * @see organicdesign.OrganicdesignPackage#getTransformationAgent()
 * @model
 * @generated
 */
public interface TransformationAgent extends Agent {
	/**
	 * Returns the value of the '<em><b>All Roles</b></em>' reference list.
	 * The list contents are of type {@link organicdesign.Role}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Roles</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Roles</em>' reference list.
	 * @see organicdesign.OrganicdesignPackage#getTransformationAgent_AllRoles()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Role> getAllRoles();

	/**
	 * Returns the value of the '<em><b>Current Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Current Role</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current Role</em>' reference.
	 * @see #setCurrentRole(Role)
	 * @see organicdesign.OrganicdesignPackage#getTransformationAgent_CurrentRole()
	 * @model
	 * @generated
	 */
	Role getCurrentRole();

	/**
	 * Sets the value of the '{@link organicdesign.TransformationAgent#getCurrentRole <em>Current Role</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current Role</em>' reference.
	 * @see #getCurrentRole()
	 * @generated
	 */
	void setCurrentRole(Role value);

	/**
	 * Returns the value of the '<em><b>All Capabilities</b></em>' reference list.
	 * The list contents are of type {@link organicdesign.Capability}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Capabilities</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Capabilities</em>' reference list.
	 * @see organicdesign.OrganicdesignPackage#getTransformationAgent_AllCapabilities()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Capability> getAllCapabilities();

	/**
	 * Returns the value of the '<em><b>Activated Capabilities</b></em>' reference list.
	 * The list contents are of type {@link organicdesign.Capability}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Activated Capabilities</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Activated Capabilities</em>' reference list.
	 * @see organicdesign.OrganicdesignPackage#getTransformationAgent_ActivatedCapabilities()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Capability> getActivatedCapabilities();

	/**
	 * Returns the value of the '<em><b>Unavailable Capabilities</b></em>' reference list.
	 * The list contents are of type {@link organicdesign.Capability}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unavailable Capabilities</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unavailable Capabilities</em>' reference list.
	 * @see organicdesign.OrganicdesignPackage#getTransformationAgent_UnavailableCapabilities()
	 * @model ordered="false"
	 * @generated
	 */
	EList<Capability> getUnavailableCapabilities();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model resourceUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='<%organicdesign.AgentSystem%> _agentSystem = this.getAgentSystem();\n<%organicdesign.Environment%> _environment = _agentSystem.getEnvironment();\n<%organicdesign.EnvironmentLocation%> _environmentLocation = _environment.getEnvironmentLocation();\n<%org.eclipse.emf.common.util.EList%><<%organicdesign.Resource%>> _locatedResources = _environmentLocation.getLocatedResources();\n_locatedResources.remove(resource);\n<%org.eclipse.emf.common.util.EList%><<%organicdesign.Resource%>> _locatedResources_1 = this.getLocatedResources();\n_locatedResources_1.add(resource);'"
	 * @generated
	 */
	void resourceArrived(Resource resource);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" resourceUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return ((this.getAgentSystem().getEnvironment().getEnvironmentLocation().getLocatedResources().contains(resource) && this.getLocatedResources().isEmpty()) && this.getAgentSystem().getEnvironment().getEnvironmentLocation().getConnectedAgent().contains(this));'"
	 * @generated
	 */
	boolean CAN_resourceArrived(Resource resource);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void taskPerformed();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model resourceUnique="false" locationUnique="false"
	 * @generated
	 */
	void availableForPickUpOfResourceAtLocation(Resource resource, Location location);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model resourceUnique="false" locationUnique="false"
	 * @generated
	 */
	void unavailableForPickUpOfResourceAtLocation(Resource resource, Location location);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model resourceUnique="false"
	 * @generated
	 */
	void pickUpArrangedForResource(Resource resource);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model resourceUnique="false"
	 * @generated
	 */
	void readyToLoadResource(Resource resource);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model resourceUnique="false"
	 * @generated
	 */
	void readyToUnLoadResource(Resource resource);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" resourceUnique="false" cartUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return (<%com.google.common.base.Objects%>.equal(cart.getLocation(), this) && this.getLocatedResources().contains(resource));'"
	 * @generated
	 */
	boolean CAN_resourceLoadedOntoCart(Resource resource, Cart cart);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model resourceUnique="false" cartUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='<%org.eclipse.emf.common.util.EList%><<%organicdesign.Resource%>> _locatedResources = this.getLocatedResources();\n_locatedResources.remove(resource);\n<%org.eclipse.emf.common.util.EList%><<%organicdesign.Resource%>> _locatedResources_1 = cart.getLocatedResources();\n_locatedResources_1.add(resource);'"
	 * @generated
	 */
	void resourceLoadedOntoCart(Resource resource, Cart cart);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model unique="false" resourceUnique="false" cartUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='return (<%com.google.common.base.Objects%>.equal(cart.getLocation(), this) && cart.getLocatedResources().contains(resource));'"
	 * @generated
	 */
	boolean CAN_resourceUnloadedFromCart(Resource resource, Cart cart);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model resourceUnique="false" cartUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='<%org.eclipse.emf.common.util.EList%><<%organicdesign.Resource%>> _locatedResources = cart.getLocatedResources();\n_locatedResources.remove(resource);\n<%org.eclipse.emf.common.util.EList%><<%organicdesign.Resource%>> _locatedResources_1 = this.getLocatedResources();\n_locatedResources_1.add(resource);'"
	 * @generated
	 */
	void resourceUnloadedFromCart(Resource resource, Cart cart);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model resourceUnique="false"
	 * @generated
	 */
	void transformResourceRequest(Resource resource);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void destinationTransformationAgentFound();

} // TransformationAgent
