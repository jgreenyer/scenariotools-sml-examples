/**
 */
package organicdesign;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see organicdesign.OrganicdesignFactory
 * @model kind="package"
 * @generated
 */
public interface OrganicdesignPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "organicdesign";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "organicdesign";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "organicdesign";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OrganicdesignPackage eINSTANCE = organicdesign.impl.OrganicdesignPackageImpl.init();

	/**
	 * The meta object id for the '{@link organicdesign.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see organicdesign.impl.NamedElementImpl
	 * @see organicdesign.impl.OrganicdesignPackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link organicdesign.impl.AgentSystemImpl <em>Agent System</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see organicdesign.impl.AgentSystemImpl
	 * @see organicdesign.impl.OrganicdesignPackageImpl#getAgentSystem()
	 * @generated
	 */
	int AGENT_SYSTEM = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_SYSTEM__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Resource</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_SYSTEM__RESOURCE = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Agents</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_SYSTEM__AGENTS = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Tasks</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_SYSTEM__TASKS = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Capabilities</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_SYSTEM__CAPABILITIES = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Roles</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_SYSTEM__ROLES = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Recipes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_SYSTEM__RECIPES = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Environment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_SYSTEM__ENVIRONMENT = NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Environmentlocation</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_SYSTEM__ENVIRONMENTLOCATION = NAMED_ELEMENT_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Agent System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_SYSTEM_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 8;

	/**
	 * The number of operations of the '<em>Agent System</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_SYSTEM_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link organicdesign.impl.TaskImpl <em>Task</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see organicdesign.impl.TaskImpl
	 * @see organicdesign.impl.OrganicdesignPackageImpl#getTask()
	 * @generated
	 */
	int TASK = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Required Capability</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK__REQUIRED_CAPABILITY = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Task</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link organicdesign.impl.ResourceImpl <em>Resource</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see organicdesign.impl.ResourceImpl
	 * @see organicdesign.impl.OrganicdesignPackageImpl#getResource()
	 * @generated
	 */
	int RESOURCE = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Recipe</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__RECIPE = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Fulfilled Task</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__FULFILLED_TASK = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Location</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE__LOCATION = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>CAN perform Task</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE___CAN_PERFORM_TASK__TASK = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Perform Task</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE___PERFORM_TASK__TASK = NAMED_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Reset</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE___RESET__ENVIRONMENT = NAMED_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>Resource</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link organicdesign.impl.RecipeImpl <em>Recipe</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see organicdesign.impl.RecipeImpl
	 * @see organicdesign.impl.OrganicdesignPackageImpl#getRecipe()
	 * @generated
	 */
	int RECIPE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECIPE__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Required Tasks</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECIPE__REQUIRED_TASKS = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Taskdependency</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECIPE__TASKDEPENDENCY = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Recipe</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECIPE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Recipe</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RECIPE_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link organicdesign.impl.LocationImpl <em>Location</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see organicdesign.impl.LocationImpl
	 * @see organicdesign.impl.OrganicdesignPackageImpl#getLocation()
	 * @generated
	 */
	int LOCATION = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Located Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION__LOCATED_RESOURCES = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Connected By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION__CONNECTED_BY = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Location</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Location</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link organicdesign.impl.AgentImpl <em>Agent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see organicdesign.impl.AgentImpl
	 * @see organicdesign.impl.OrganicdesignPackageImpl#getAgent()
	 * @generated
	 */
	int AGENT = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT__NAME = LOCATION__NAME;

	/**
	 * The feature id for the '<em><b>Located Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT__LOCATED_RESOURCES = LOCATION__LOCATED_RESOURCES;

	/**
	 * The feature id for the '<em><b>Connected By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT__CONNECTED_BY = LOCATION__CONNECTED_BY;

	/**
	 * The feature id for the '<em><b>Agent System</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT__AGENT_SYSTEM = LOCATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Agent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_FEATURE_COUNT = LOCATION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>Arrange Pick Up For</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT___ARRANGE_PICK_UP_FOR__RESOURCE = LOCATION_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Agent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AGENT_OPERATION_COUNT = LOCATION_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link organicdesign.impl.RoleImpl <em>Role</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see organicdesign.impl.RoleImpl
	 * @see organicdesign.impl.OrganicdesignPackageImpl#getRole()
	 * @generated
	 */
	int ROLE = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Provided Capabilities</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE__PROVIDED_CAPABILITIES = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link organicdesign.impl.CapabilityImpl <em>Capability</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see organicdesign.impl.CapabilityImpl
	 * @see organicdesign.impl.OrganicdesignPackageImpl#getCapability()
	 * @generated
	 */
	int CAPABILITY = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The number of structural features of the '<em>Capability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The number of operations of the '<em>Capability</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAPABILITY_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link organicdesign.impl.CartImpl <em>Cart</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see organicdesign.impl.CartImpl
	 * @see organicdesign.impl.OrganicdesignPackageImpl#getCart()
	 * @generated
	 */
	int CART = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CART__NAME = AGENT__NAME;

	/**
	 * The feature id for the '<em><b>Located Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CART__LOCATED_RESOURCES = AGENT__LOCATED_RESOURCES;

	/**
	 * The feature id for the '<em><b>Connected By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CART__CONNECTED_BY = AGENT__CONNECTED_BY;

	/**
	 * The feature id for the '<em><b>Agent System</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CART__AGENT_SYSTEM = AGENT__AGENT_SYSTEM;

	/**
	 * The feature id for the '<em><b>Location</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CART__LOCATION = AGENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Connected Locations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CART__CONNECTED_LOCATIONS = AGENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Cart</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CART_FEATURE_COUNT = AGENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Arrange Pick Up For</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CART___ARRANGE_PICK_UP_FOR__RESOURCE = AGENT___ARRANGE_PICK_UP_FOR__RESOURCE;

	/**
	 * The operation id for the '<em>Pick Up Resource At Location Request</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CART___PICK_UP_RESOURCE_AT_LOCATION_REQUEST__RESOURCE_LOCATION = AGENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Arrives At Location</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CART___ARRIVES_AT_LOCATION__LOCATION = AGENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Resource Loaded</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CART___RESOURCE_LOADED__RESOURCE = AGENT_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Resource Unloaded</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CART___RESOURCE_UNLOADED__RESOURCE = AGENT_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Available For Transformation Of Resource</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CART___AVAILABLE_FOR_TRANSFORMATION_OF_RESOURCE__RESOURCE = AGENT_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Unavailable For Transformation Of Resource</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CART___UNAVAILABLE_FOR_TRANSFORMATION_OF_RESOURCE__RESOURCE = AGENT_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Destination Transformation Agent Found For Resource</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CART___DESTINATION_TRANSFORMATION_AGENT_FOUND_FOR_RESOURCE__TRANSFORMATIONAGENT_RESOURCE = AGENT_OPERATION_COUNT + 6;

	/**
	 * The number of operations of the '<em>Cart</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CART_OPERATION_COUNT = AGENT_OPERATION_COUNT + 7;

	/**
	 * The meta object id for the '{@link organicdesign.impl.TransformationAgentImpl <em>Transformation Agent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see organicdesign.impl.TransformationAgentImpl
	 * @see organicdesign.impl.OrganicdesignPackageImpl#getTransformationAgent()
	 * @generated
	 */
	int TRANSFORMATION_AGENT = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_AGENT__NAME = AGENT__NAME;

	/**
	 * The feature id for the '<em><b>Located Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_AGENT__LOCATED_RESOURCES = AGENT__LOCATED_RESOURCES;

	/**
	 * The feature id for the '<em><b>Connected By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_AGENT__CONNECTED_BY = AGENT__CONNECTED_BY;

	/**
	 * The feature id for the '<em><b>Agent System</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_AGENT__AGENT_SYSTEM = AGENT__AGENT_SYSTEM;

	/**
	 * The feature id for the '<em><b>All Roles</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_AGENT__ALL_ROLES = AGENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Current Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_AGENT__CURRENT_ROLE = AGENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>All Capabilities</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_AGENT__ALL_CAPABILITIES = AGENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Activated Capabilities</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_AGENT__ACTIVATED_CAPABILITIES = AGENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Unavailable Capabilities</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_AGENT__UNAVAILABLE_CAPABILITIES = AGENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Transformation Agent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_AGENT_FEATURE_COUNT = AGENT_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>Arrange Pick Up For</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_AGENT___ARRANGE_PICK_UP_FOR__RESOURCE = AGENT___ARRANGE_PICK_UP_FOR__RESOURCE;

	/**
	 * The operation id for the '<em>Resource Arrived</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_AGENT___RESOURCE_ARRIVED__RESOURCE = AGENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>CAN resource Arrived</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_AGENT___CAN_RESOURCE_ARRIVED__RESOURCE = AGENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Task Performed</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_AGENT___TASK_PERFORMED = AGENT_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Available For Pick Up Of Resource At Location</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_AGENT___AVAILABLE_FOR_PICK_UP_OF_RESOURCE_AT_LOCATION__RESOURCE_LOCATION = AGENT_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>Unavailable For Pick Up Of Resource At Location</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_AGENT___UNAVAILABLE_FOR_PICK_UP_OF_RESOURCE_AT_LOCATION__RESOURCE_LOCATION = AGENT_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>Pick Up Arranged For Resource</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_AGENT___PICK_UP_ARRANGED_FOR_RESOURCE__RESOURCE = AGENT_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Ready To Load Resource</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_AGENT___READY_TO_LOAD_RESOURCE__RESOURCE = AGENT_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Ready To Un Load Resource</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_AGENT___READY_TO_UN_LOAD_RESOURCE__RESOURCE = AGENT_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>CAN resource Loaded Onto Cart</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_AGENT___CAN_RESOURCE_LOADED_ONTO_CART__RESOURCE_CART = AGENT_OPERATION_COUNT + 8;

	/**
	 * The operation id for the '<em>Resource Loaded Onto Cart</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_AGENT___RESOURCE_LOADED_ONTO_CART__RESOURCE_CART = AGENT_OPERATION_COUNT + 9;

	/**
	 * The operation id for the '<em>CAN resource Unloaded From Cart</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_AGENT___CAN_RESOURCE_UNLOADED_FROM_CART__RESOURCE_CART = AGENT_OPERATION_COUNT + 10;

	/**
	 * The operation id for the '<em>Resource Unloaded From Cart</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_AGENT___RESOURCE_UNLOADED_FROM_CART__RESOURCE_CART = AGENT_OPERATION_COUNT + 11;

	/**
	 * The operation id for the '<em>Transform Resource Request</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_AGENT___TRANSFORM_RESOURCE_REQUEST__RESOURCE = AGENT_OPERATION_COUNT + 12;

	/**
	 * The operation id for the '<em>Destination Transformation Agent Found</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_AGENT___DESTINATION_TRANSFORMATION_AGENT_FOUND = AGENT_OPERATION_COUNT + 13;

	/**
	 * The number of operations of the '<em>Transformation Agent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TRANSFORMATION_AGENT_OPERATION_COUNT = AGENT_OPERATION_COUNT + 14;

	/**
	 * The meta object id for the '{@link organicdesign.impl.TaskDependencyImpl <em>Task Dependency</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see organicdesign.impl.TaskDependencyImpl
	 * @see organicdesign.impl.OrganicdesignPackageImpl#getTaskDependency()
	 * @generated
	 */
	int TASK_DEPENDENCY = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DEPENDENCY__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Task</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DEPENDENCY__TASK = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Precondition</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DEPENDENCY__PRECONDITION = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Task Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DEPENDENCY_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Task Dependency</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TASK_DEPENDENCY_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link organicdesign.impl.EnvironmentImpl <em>Environment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see organicdesign.impl.EnvironmentImpl
	 * @see organicdesign.impl.OrganicdesignPackageImpl#getEnvironment()
	 * @generated
	 */
	int ENVIRONMENT = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Deposit Location</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT__DEPOSIT_LOCATION = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Environment Location</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT__ENVIRONMENT_LOCATION = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Environment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Move To Location</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT___MOVE_TO_LOCATION__LOCATION = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Load Resource Onto Cart</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT___LOAD_RESOURCE_ONTO_CART__RESOURCE_CART = NAMED_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Unload Resource From Cart</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT___UNLOAD_RESOURCE_FROM_CART__RESOURCE_CART = NAMED_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Reset Resource</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT___RESET_RESOURCE__RESOURCE = NAMED_ELEMENT_OPERATION_COUNT + 3;

	/**
	 * The number of operations of the '<em>Environment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 4;

	/**
	 * The meta object id for the '{@link organicdesign.impl.EnvironmentLocationImpl <em>Environment Location</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see organicdesign.impl.EnvironmentLocationImpl
	 * @see organicdesign.impl.OrganicdesignPackageImpl#getEnvironmentLocation()
	 * @generated
	 */
	int ENVIRONMENT_LOCATION = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_LOCATION__NAME = LOCATION__NAME;

	/**
	 * The feature id for the '<em><b>Located Resources</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_LOCATION__LOCATED_RESOURCES = LOCATION__LOCATED_RESOURCES;

	/**
	 * The feature id for the '<em><b>Connected By</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_LOCATION__CONNECTED_BY = LOCATION__CONNECTED_BY;

	/**
	 * The feature id for the '<em><b>Connected Agent</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_LOCATION__CONNECTED_AGENT = LOCATION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Environment Location</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_LOCATION_FEATURE_COUNT = LOCATION_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Environment Location</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_LOCATION_OPERATION_COUNT = LOCATION_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link organicdesign.AgentSystem <em>Agent System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Agent System</em>'.
	 * @see organicdesign.AgentSystem
	 * @generated
	 */
	EClass getAgentSystem();

	/**
	 * Returns the meta object for the containment reference list '{@link organicdesign.AgentSystem#getResource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Resource</em>'.
	 * @see organicdesign.AgentSystem#getResource()
	 * @see #getAgentSystem()
	 * @generated
	 */
	EReference getAgentSystem_Resource();

	/**
	 * Returns the meta object for the containment reference list '{@link organicdesign.AgentSystem#getAgents <em>Agents</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Agents</em>'.
	 * @see organicdesign.AgentSystem#getAgents()
	 * @see #getAgentSystem()
	 * @generated
	 */
	EReference getAgentSystem_Agents();

	/**
	 * Returns the meta object for the containment reference list '{@link organicdesign.AgentSystem#getTasks <em>Tasks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Tasks</em>'.
	 * @see organicdesign.AgentSystem#getTasks()
	 * @see #getAgentSystem()
	 * @generated
	 */
	EReference getAgentSystem_Tasks();

	/**
	 * Returns the meta object for the containment reference list '{@link organicdesign.AgentSystem#getCapabilities <em>Capabilities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Capabilities</em>'.
	 * @see organicdesign.AgentSystem#getCapabilities()
	 * @see #getAgentSystem()
	 * @generated
	 */
	EReference getAgentSystem_Capabilities();

	/**
	 * Returns the meta object for the containment reference list '{@link organicdesign.AgentSystem#getRoles <em>Roles</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Roles</em>'.
	 * @see organicdesign.AgentSystem#getRoles()
	 * @see #getAgentSystem()
	 * @generated
	 */
	EReference getAgentSystem_Roles();

	/**
	 * Returns the meta object for the containment reference list '{@link organicdesign.AgentSystem#getRecipes <em>Recipes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Recipes</em>'.
	 * @see organicdesign.AgentSystem#getRecipes()
	 * @see #getAgentSystem()
	 * @generated
	 */
	EReference getAgentSystem_Recipes();

	/**
	 * Returns the meta object for the containment reference '{@link organicdesign.AgentSystem#getEnvironment <em>Environment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Environment</em>'.
	 * @see organicdesign.AgentSystem#getEnvironment()
	 * @see #getAgentSystem()
	 * @generated
	 */
	EReference getAgentSystem_Environment();

	/**
	 * Returns the meta object for the containment reference list '{@link organicdesign.AgentSystem#getEnvironmentlocation <em>Environmentlocation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Environmentlocation</em>'.
	 * @see organicdesign.AgentSystem#getEnvironmentlocation()
	 * @see #getAgentSystem()
	 * @generated
	 */
	EReference getAgentSystem_Environmentlocation();

	/**
	 * Returns the meta object for class '{@link organicdesign.Task <em>Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Task</em>'.
	 * @see organicdesign.Task
	 * @generated
	 */
	EClass getTask();

	/**
	 * Returns the meta object for the reference '{@link organicdesign.Task#getRequiredCapability <em>Required Capability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Required Capability</em>'.
	 * @see organicdesign.Task#getRequiredCapability()
	 * @see #getTask()
	 * @generated
	 */
	EReference getTask_RequiredCapability();

	/**
	 * Returns the meta object for class '{@link organicdesign.Resource <em>Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Resource</em>'.
	 * @see organicdesign.Resource
	 * @generated
	 */
	EClass getResource();

	/**
	 * Returns the meta object for the reference '{@link organicdesign.Resource#getRecipe <em>Recipe</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Recipe</em>'.
	 * @see organicdesign.Resource#getRecipe()
	 * @see #getResource()
	 * @generated
	 */
	EReference getResource_Recipe();

	/**
	 * Returns the meta object for the reference list '{@link organicdesign.Resource#getFulfilledTask <em>Fulfilled Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Fulfilled Task</em>'.
	 * @see organicdesign.Resource#getFulfilledTask()
	 * @see #getResource()
	 * @generated
	 */
	EReference getResource_FulfilledTask();

	/**
	 * Returns the meta object for the reference '{@link organicdesign.Resource#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Location</em>'.
	 * @see organicdesign.Resource#getLocation()
	 * @see #getResource()
	 * @generated
	 */
	EReference getResource_Location();

	/**
	 * Returns the meta object for the '{@link organicdesign.Resource#CAN_performTask(organicdesign.Task) <em>CAN perform Task</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>CAN perform Task</em>' operation.
	 * @see organicdesign.Resource#CAN_performTask(organicdesign.Task)
	 * @generated
	 */
	EOperation getResource__CAN_performTask__Task();

	/**
	 * Returns the meta object for the '{@link organicdesign.Resource#performTask(organicdesign.Task) <em>Perform Task</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Perform Task</em>' operation.
	 * @see organicdesign.Resource#performTask(organicdesign.Task)
	 * @generated
	 */
	EOperation getResource__PerformTask__Task();

	/**
	 * Returns the meta object for the '{@link organicdesign.Resource#reset(organicdesign.Environment) <em>Reset</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Reset</em>' operation.
	 * @see organicdesign.Resource#reset(organicdesign.Environment)
	 * @generated
	 */
	EOperation getResource__Reset__Environment();

	/**
	 * Returns the meta object for class '{@link organicdesign.Recipe <em>Recipe</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Recipe</em>'.
	 * @see organicdesign.Recipe
	 * @generated
	 */
	EClass getRecipe();

	/**
	 * Returns the meta object for the reference list '{@link organicdesign.Recipe#getRequiredTasks <em>Required Tasks</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Required Tasks</em>'.
	 * @see organicdesign.Recipe#getRequiredTasks()
	 * @see #getRecipe()
	 * @generated
	 */
	EReference getRecipe_RequiredTasks();

	/**
	 * Returns the meta object for the containment reference list '{@link organicdesign.Recipe#getTaskdependency <em>Taskdependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Taskdependency</em>'.
	 * @see organicdesign.Recipe#getTaskdependency()
	 * @see #getRecipe()
	 * @generated
	 */
	EReference getRecipe_Taskdependency();

	/**
	 * Returns the meta object for class '{@link organicdesign.Agent <em>Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Agent</em>'.
	 * @see organicdesign.Agent
	 * @generated
	 */
	EClass getAgent();

	/**
	 * Returns the meta object for the container reference '{@link organicdesign.Agent#getAgentSystem <em>Agent System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Agent System</em>'.
	 * @see organicdesign.Agent#getAgentSystem()
	 * @see #getAgent()
	 * @generated
	 */
	EReference getAgent_AgentSystem();

	/**
	 * Returns the meta object for the '{@link organicdesign.Agent#arrangePickUpFor(organicdesign.Resource) <em>Arrange Pick Up For</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Arrange Pick Up For</em>' operation.
	 * @see organicdesign.Agent#arrangePickUpFor(organicdesign.Resource)
	 * @generated
	 */
	EOperation getAgent__ArrangePickUpFor__Resource();

	/**
	 * Returns the meta object for class '{@link organicdesign.Role <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role</em>'.
	 * @see organicdesign.Role
	 * @generated
	 */
	EClass getRole();

	/**
	 * Returns the meta object for the reference list '{@link organicdesign.Role#getProvidedCapabilities <em>Provided Capabilities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Provided Capabilities</em>'.
	 * @see organicdesign.Role#getProvidedCapabilities()
	 * @see #getRole()
	 * @generated
	 */
	EReference getRole_ProvidedCapabilities();

	/**
	 * Returns the meta object for class '{@link organicdesign.Capability <em>Capability</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Capability</em>'.
	 * @see organicdesign.Capability
	 * @generated
	 */
	EClass getCapability();

	/**
	 * Returns the meta object for class '{@link organicdesign.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see organicdesign.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link organicdesign.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see organicdesign.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link organicdesign.Cart <em>Cart</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cart</em>'.
	 * @see organicdesign.Cart
	 * @generated
	 */
	EClass getCart();

	/**
	 * Returns the meta object for the reference '{@link organicdesign.Cart#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Location</em>'.
	 * @see organicdesign.Cart#getLocation()
	 * @see #getCart()
	 * @generated
	 */
	EReference getCart_Location();

	/**
	 * Returns the meta object for the reference list '{@link organicdesign.Cart#getConnectedLocations <em>Connected Locations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Connected Locations</em>'.
	 * @see organicdesign.Cart#getConnectedLocations()
	 * @see #getCart()
	 * @generated
	 */
	EReference getCart_ConnectedLocations();

	/**
	 * Returns the meta object for the '{@link organicdesign.Cart#pickUpResourceAtLocationRequest(organicdesign.Resource, organicdesign.Location) <em>Pick Up Resource At Location Request</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pick Up Resource At Location Request</em>' operation.
	 * @see organicdesign.Cart#pickUpResourceAtLocationRequest(organicdesign.Resource, organicdesign.Location)
	 * @generated
	 */
	EOperation getCart__PickUpResourceAtLocationRequest__Resource_Location();

	/**
	 * Returns the meta object for the '{@link organicdesign.Cart#arrivesAtLocation(organicdesign.Location) <em>Arrives At Location</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Arrives At Location</em>' operation.
	 * @see organicdesign.Cart#arrivesAtLocation(organicdesign.Location)
	 * @generated
	 */
	EOperation getCart__ArrivesAtLocation__Location();

	/**
	 * Returns the meta object for the '{@link organicdesign.Cart#resourceLoaded(organicdesign.Resource) <em>Resource Loaded</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Resource Loaded</em>' operation.
	 * @see organicdesign.Cart#resourceLoaded(organicdesign.Resource)
	 * @generated
	 */
	EOperation getCart__ResourceLoaded__Resource();

	/**
	 * Returns the meta object for the '{@link organicdesign.Cart#resourceUnloaded(organicdesign.Resource) <em>Resource Unloaded</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Resource Unloaded</em>' operation.
	 * @see organicdesign.Cart#resourceUnloaded(organicdesign.Resource)
	 * @generated
	 */
	EOperation getCart__ResourceUnloaded__Resource();

	/**
	 * Returns the meta object for the '{@link organicdesign.Cart#availableForTransformationOfResource(organicdesign.Resource) <em>Available For Transformation Of Resource</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Available For Transformation Of Resource</em>' operation.
	 * @see organicdesign.Cart#availableForTransformationOfResource(organicdesign.Resource)
	 * @generated
	 */
	EOperation getCart__AvailableForTransformationOfResource__Resource();

	/**
	 * Returns the meta object for the '{@link organicdesign.Cart#unavailableForTransformationOfResource(organicdesign.Resource) <em>Unavailable For Transformation Of Resource</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Unavailable For Transformation Of Resource</em>' operation.
	 * @see organicdesign.Cart#unavailableForTransformationOfResource(organicdesign.Resource)
	 * @generated
	 */
	EOperation getCart__UnavailableForTransformationOfResource__Resource();

	/**
	 * Returns the meta object for the '{@link organicdesign.Cart#destinationTransformationAgentFoundForResource(organicdesign.TransformationAgent, organicdesign.Resource) <em>Destination Transformation Agent Found For Resource</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Destination Transformation Agent Found For Resource</em>' operation.
	 * @see organicdesign.Cart#destinationTransformationAgentFoundForResource(organicdesign.TransformationAgent, organicdesign.Resource)
	 * @generated
	 */
	EOperation getCart__DestinationTransformationAgentFoundForResource__TransformationAgent_Resource();

	/**
	 * Returns the meta object for class '{@link organicdesign.TransformationAgent <em>Transformation Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Transformation Agent</em>'.
	 * @see organicdesign.TransformationAgent
	 * @generated
	 */
	EClass getTransformationAgent();

	/**
	 * Returns the meta object for the reference list '{@link organicdesign.TransformationAgent#getAllRoles <em>All Roles</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All Roles</em>'.
	 * @see organicdesign.TransformationAgent#getAllRoles()
	 * @see #getTransformationAgent()
	 * @generated
	 */
	EReference getTransformationAgent_AllRoles();

	/**
	 * Returns the meta object for the reference '{@link organicdesign.TransformationAgent#getCurrentRole <em>Current Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Current Role</em>'.
	 * @see organicdesign.TransformationAgent#getCurrentRole()
	 * @see #getTransformationAgent()
	 * @generated
	 */
	EReference getTransformationAgent_CurrentRole();

	/**
	 * Returns the meta object for the reference list '{@link organicdesign.TransformationAgent#getAllCapabilities <em>All Capabilities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>All Capabilities</em>'.
	 * @see organicdesign.TransformationAgent#getAllCapabilities()
	 * @see #getTransformationAgent()
	 * @generated
	 */
	EReference getTransformationAgent_AllCapabilities();

	/**
	 * Returns the meta object for the reference list '{@link organicdesign.TransformationAgent#getActivatedCapabilities <em>Activated Capabilities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Activated Capabilities</em>'.
	 * @see organicdesign.TransformationAgent#getActivatedCapabilities()
	 * @see #getTransformationAgent()
	 * @generated
	 */
	EReference getTransformationAgent_ActivatedCapabilities();

	/**
	 * Returns the meta object for the reference list '{@link organicdesign.TransformationAgent#getUnavailableCapabilities <em>Unavailable Capabilities</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Unavailable Capabilities</em>'.
	 * @see organicdesign.TransformationAgent#getUnavailableCapabilities()
	 * @see #getTransformationAgent()
	 * @generated
	 */
	EReference getTransformationAgent_UnavailableCapabilities();

	/**
	 * Returns the meta object for the '{@link organicdesign.TransformationAgent#resourceArrived(organicdesign.Resource) <em>Resource Arrived</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Resource Arrived</em>' operation.
	 * @see organicdesign.TransformationAgent#resourceArrived(organicdesign.Resource)
	 * @generated
	 */
	EOperation getTransformationAgent__ResourceArrived__Resource();

	/**
	 * Returns the meta object for the '{@link organicdesign.TransformationAgent#CAN_resourceArrived(organicdesign.Resource) <em>CAN resource Arrived</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>CAN resource Arrived</em>' operation.
	 * @see organicdesign.TransformationAgent#CAN_resourceArrived(organicdesign.Resource)
	 * @generated
	 */
	EOperation getTransformationAgent__CAN_resourceArrived__Resource();

	/**
	 * Returns the meta object for the '{@link organicdesign.TransformationAgent#taskPerformed() <em>Task Performed</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Task Performed</em>' operation.
	 * @see organicdesign.TransformationAgent#taskPerformed()
	 * @generated
	 */
	EOperation getTransformationAgent__TaskPerformed();

	/**
	 * Returns the meta object for the '{@link organicdesign.TransformationAgent#availableForPickUpOfResourceAtLocation(organicdesign.Resource, organicdesign.Location) <em>Available For Pick Up Of Resource At Location</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Available For Pick Up Of Resource At Location</em>' operation.
	 * @see organicdesign.TransformationAgent#availableForPickUpOfResourceAtLocation(organicdesign.Resource, organicdesign.Location)
	 * @generated
	 */
	EOperation getTransformationAgent__AvailableForPickUpOfResourceAtLocation__Resource_Location();

	/**
	 * Returns the meta object for the '{@link organicdesign.TransformationAgent#unavailableForPickUpOfResourceAtLocation(organicdesign.Resource, organicdesign.Location) <em>Unavailable For Pick Up Of Resource At Location</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Unavailable For Pick Up Of Resource At Location</em>' operation.
	 * @see organicdesign.TransformationAgent#unavailableForPickUpOfResourceAtLocation(organicdesign.Resource, organicdesign.Location)
	 * @generated
	 */
	EOperation getTransformationAgent__UnavailableForPickUpOfResourceAtLocation__Resource_Location();

	/**
	 * Returns the meta object for the '{@link organicdesign.TransformationAgent#pickUpArrangedForResource(organicdesign.Resource) <em>Pick Up Arranged For Resource</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pick Up Arranged For Resource</em>' operation.
	 * @see organicdesign.TransformationAgent#pickUpArrangedForResource(organicdesign.Resource)
	 * @generated
	 */
	EOperation getTransformationAgent__PickUpArrangedForResource__Resource();

	/**
	 * Returns the meta object for the '{@link organicdesign.TransformationAgent#readyToLoadResource(organicdesign.Resource) <em>Ready To Load Resource</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Ready To Load Resource</em>' operation.
	 * @see organicdesign.TransformationAgent#readyToLoadResource(organicdesign.Resource)
	 * @generated
	 */
	EOperation getTransformationAgent__ReadyToLoadResource__Resource();

	/**
	 * Returns the meta object for the '{@link organicdesign.TransformationAgent#readyToUnLoadResource(organicdesign.Resource) <em>Ready To Un Load Resource</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Ready To Un Load Resource</em>' operation.
	 * @see organicdesign.TransformationAgent#readyToUnLoadResource(organicdesign.Resource)
	 * @generated
	 */
	EOperation getTransformationAgent__ReadyToUnLoadResource__Resource();

	/**
	 * Returns the meta object for the '{@link organicdesign.TransformationAgent#CAN_resourceLoadedOntoCart(organicdesign.Resource, organicdesign.Cart) <em>CAN resource Loaded Onto Cart</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>CAN resource Loaded Onto Cart</em>' operation.
	 * @see organicdesign.TransformationAgent#CAN_resourceLoadedOntoCart(organicdesign.Resource, organicdesign.Cart)
	 * @generated
	 */
	EOperation getTransformationAgent__CAN_resourceLoadedOntoCart__Resource_Cart();

	/**
	 * Returns the meta object for the '{@link organicdesign.TransformationAgent#resourceLoadedOntoCart(organicdesign.Resource, organicdesign.Cart) <em>Resource Loaded Onto Cart</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Resource Loaded Onto Cart</em>' operation.
	 * @see organicdesign.TransformationAgent#resourceLoadedOntoCart(organicdesign.Resource, organicdesign.Cart)
	 * @generated
	 */
	EOperation getTransformationAgent__ResourceLoadedOntoCart__Resource_Cart();

	/**
	 * Returns the meta object for the '{@link organicdesign.TransformationAgent#CAN_resourceUnloadedFromCart(organicdesign.Resource, organicdesign.Cart) <em>CAN resource Unloaded From Cart</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>CAN resource Unloaded From Cart</em>' operation.
	 * @see organicdesign.TransformationAgent#CAN_resourceUnloadedFromCart(organicdesign.Resource, organicdesign.Cart)
	 * @generated
	 */
	EOperation getTransformationAgent__CAN_resourceUnloadedFromCart__Resource_Cart();

	/**
	 * Returns the meta object for the '{@link organicdesign.TransformationAgent#resourceUnloadedFromCart(organicdesign.Resource, organicdesign.Cart) <em>Resource Unloaded From Cart</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Resource Unloaded From Cart</em>' operation.
	 * @see organicdesign.TransformationAgent#resourceUnloadedFromCart(organicdesign.Resource, organicdesign.Cart)
	 * @generated
	 */
	EOperation getTransformationAgent__ResourceUnloadedFromCart__Resource_Cart();

	/**
	 * Returns the meta object for the '{@link organicdesign.TransformationAgent#transformResourceRequest(organicdesign.Resource) <em>Transform Resource Request</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Transform Resource Request</em>' operation.
	 * @see organicdesign.TransformationAgent#transformResourceRequest(organicdesign.Resource)
	 * @generated
	 */
	EOperation getTransformationAgent__TransformResourceRequest__Resource();

	/**
	 * Returns the meta object for the '{@link organicdesign.TransformationAgent#destinationTransformationAgentFound() <em>Destination Transformation Agent Found</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Destination Transformation Agent Found</em>' operation.
	 * @see organicdesign.TransformationAgent#destinationTransformationAgentFound()
	 * @generated
	 */
	EOperation getTransformationAgent__DestinationTransformationAgentFound();

	/**
	 * Returns the meta object for class '{@link organicdesign.TaskDependency <em>Task Dependency</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Task Dependency</em>'.
	 * @see organicdesign.TaskDependency
	 * @generated
	 */
	EClass getTaskDependency();

	/**
	 * Returns the meta object for the reference '{@link organicdesign.TaskDependency#getTask <em>Task</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Task</em>'.
	 * @see organicdesign.TaskDependency#getTask()
	 * @see #getTaskDependency()
	 * @generated
	 */
	EReference getTaskDependency_Task();

	/**
	 * Returns the meta object for the reference '{@link organicdesign.TaskDependency#getPrecondition <em>Precondition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Precondition</em>'.
	 * @see organicdesign.TaskDependency#getPrecondition()
	 * @see #getTaskDependency()
	 * @generated
	 */
	EReference getTaskDependency_Precondition();

	/**
	 * Returns the meta object for class '{@link organicdesign.Environment <em>Environment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Environment</em>'.
	 * @see organicdesign.Environment
	 * @generated
	 */
	EClass getEnvironment();

	/**
	 * Returns the meta object for the reference '{@link organicdesign.Environment#getDepositLocation <em>Deposit Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Deposit Location</em>'.
	 * @see organicdesign.Environment#getDepositLocation()
	 * @see #getEnvironment()
	 * @generated
	 */
	EReference getEnvironment_DepositLocation();

	/**
	 * Returns the meta object for the reference '{@link organicdesign.Environment#getEnvironmentLocation <em>Environment Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Environment Location</em>'.
	 * @see organicdesign.Environment#getEnvironmentLocation()
	 * @see #getEnvironment()
	 * @generated
	 */
	EReference getEnvironment_EnvironmentLocation();

	/**
	 * Returns the meta object for the '{@link organicdesign.Environment#moveToLocation(organicdesign.Location) <em>Move To Location</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Move To Location</em>' operation.
	 * @see organicdesign.Environment#moveToLocation(organicdesign.Location)
	 * @generated
	 */
	EOperation getEnvironment__MoveToLocation__Location();

	/**
	 * Returns the meta object for the '{@link organicdesign.Environment#loadResourceOntoCart(organicdesign.Resource, organicdesign.Cart) <em>Load Resource Onto Cart</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Load Resource Onto Cart</em>' operation.
	 * @see organicdesign.Environment#loadResourceOntoCart(organicdesign.Resource, organicdesign.Cart)
	 * @generated
	 */
	EOperation getEnvironment__LoadResourceOntoCart__Resource_Cart();

	/**
	 * Returns the meta object for the '{@link organicdesign.Environment#unloadResourceFromCart(organicdesign.Resource, organicdesign.Cart) <em>Unload Resource From Cart</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Unload Resource From Cart</em>' operation.
	 * @see organicdesign.Environment#unloadResourceFromCart(organicdesign.Resource, organicdesign.Cart)
	 * @generated
	 */
	EOperation getEnvironment__UnloadResourceFromCart__Resource_Cart();

	/**
	 * Returns the meta object for the '{@link organicdesign.Environment#resetResource(organicdesign.Resource) <em>Reset Resource</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Reset Resource</em>' operation.
	 * @see organicdesign.Environment#resetResource(organicdesign.Resource)
	 * @generated
	 */
	EOperation getEnvironment__ResetResource__Resource();

	/**
	 * Returns the meta object for class '{@link organicdesign.EnvironmentLocation <em>Environment Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Environment Location</em>'.
	 * @see organicdesign.EnvironmentLocation
	 * @generated
	 */
	EClass getEnvironmentLocation();

	/**
	 * Returns the meta object for the reference list '{@link organicdesign.EnvironmentLocation#getConnectedAgent <em>Connected Agent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Connected Agent</em>'.
	 * @see organicdesign.EnvironmentLocation#getConnectedAgent()
	 * @see #getEnvironmentLocation()
	 * @generated
	 */
	EReference getEnvironmentLocation_ConnectedAgent();

	/**
	 * Returns the meta object for class '{@link organicdesign.Location <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Location</em>'.
	 * @see organicdesign.Location
	 * @generated
	 */
	EClass getLocation();

	/**
	 * Returns the meta object for the reference list '{@link organicdesign.Location#getLocatedResources <em>Located Resources</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Located Resources</em>'.
	 * @see organicdesign.Location#getLocatedResources()
	 * @see #getLocation()
	 * @generated
	 */
	EReference getLocation_LocatedResources();

	/**
	 * Returns the meta object for the reference list '{@link organicdesign.Location#getConnectedBy <em>Connected By</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Connected By</em>'.
	 * @see organicdesign.Location#getConnectedBy()
	 * @see #getLocation()
	 * @generated
	 */
	EReference getLocation_ConnectedBy();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OrganicdesignFactory getOrganicdesignFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link organicdesign.impl.AgentSystemImpl <em>Agent System</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see organicdesign.impl.AgentSystemImpl
		 * @see organicdesign.impl.OrganicdesignPackageImpl#getAgentSystem()
		 * @generated
		 */
		EClass AGENT_SYSTEM = eINSTANCE.getAgentSystem();

		/**
		 * The meta object literal for the '<em><b>Resource</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AGENT_SYSTEM__RESOURCE = eINSTANCE.getAgentSystem_Resource();

		/**
		 * The meta object literal for the '<em><b>Agents</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AGENT_SYSTEM__AGENTS = eINSTANCE.getAgentSystem_Agents();

		/**
		 * The meta object literal for the '<em><b>Tasks</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AGENT_SYSTEM__TASKS = eINSTANCE.getAgentSystem_Tasks();

		/**
		 * The meta object literal for the '<em><b>Capabilities</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AGENT_SYSTEM__CAPABILITIES = eINSTANCE.getAgentSystem_Capabilities();

		/**
		 * The meta object literal for the '<em><b>Roles</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AGENT_SYSTEM__ROLES = eINSTANCE.getAgentSystem_Roles();

		/**
		 * The meta object literal for the '<em><b>Recipes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AGENT_SYSTEM__RECIPES = eINSTANCE.getAgentSystem_Recipes();

		/**
		 * The meta object literal for the '<em><b>Environment</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AGENT_SYSTEM__ENVIRONMENT = eINSTANCE.getAgentSystem_Environment();

		/**
		 * The meta object literal for the '<em><b>Environmentlocation</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AGENT_SYSTEM__ENVIRONMENTLOCATION = eINSTANCE.getAgentSystem_Environmentlocation();

		/**
		 * The meta object literal for the '{@link organicdesign.impl.TaskImpl <em>Task</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see organicdesign.impl.TaskImpl
		 * @see organicdesign.impl.OrganicdesignPackageImpl#getTask()
		 * @generated
		 */
		EClass TASK = eINSTANCE.getTask();

		/**
		 * The meta object literal for the '<em><b>Required Capability</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK__REQUIRED_CAPABILITY = eINSTANCE.getTask_RequiredCapability();

		/**
		 * The meta object literal for the '{@link organicdesign.impl.ResourceImpl <em>Resource</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see organicdesign.impl.ResourceImpl
		 * @see organicdesign.impl.OrganicdesignPackageImpl#getResource()
		 * @generated
		 */
		EClass RESOURCE = eINSTANCE.getResource();

		/**
		 * The meta object literal for the '<em><b>Recipe</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE__RECIPE = eINSTANCE.getResource_Recipe();

		/**
		 * The meta object literal for the '<em><b>Fulfilled Task</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE__FULFILLED_TASK = eINSTANCE.getResource_FulfilledTask();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESOURCE__LOCATION = eINSTANCE.getResource_Location();

		/**
		 * The meta object literal for the '<em><b>CAN perform Task</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RESOURCE___CAN_PERFORM_TASK__TASK = eINSTANCE.getResource__CAN_performTask__Task();

		/**
		 * The meta object literal for the '<em><b>Perform Task</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RESOURCE___PERFORM_TASK__TASK = eINSTANCE.getResource__PerformTask__Task();

		/**
		 * The meta object literal for the '<em><b>Reset</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation RESOURCE___RESET__ENVIRONMENT = eINSTANCE.getResource__Reset__Environment();

		/**
		 * The meta object literal for the '{@link organicdesign.impl.RecipeImpl <em>Recipe</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see organicdesign.impl.RecipeImpl
		 * @see organicdesign.impl.OrganicdesignPackageImpl#getRecipe()
		 * @generated
		 */
		EClass RECIPE = eINSTANCE.getRecipe();

		/**
		 * The meta object literal for the '<em><b>Required Tasks</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RECIPE__REQUIRED_TASKS = eINSTANCE.getRecipe_RequiredTasks();

		/**
		 * The meta object literal for the '<em><b>Taskdependency</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RECIPE__TASKDEPENDENCY = eINSTANCE.getRecipe_Taskdependency();

		/**
		 * The meta object literal for the '{@link organicdesign.impl.AgentImpl <em>Agent</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see organicdesign.impl.AgentImpl
		 * @see organicdesign.impl.OrganicdesignPackageImpl#getAgent()
		 * @generated
		 */
		EClass AGENT = eINSTANCE.getAgent();

		/**
		 * The meta object literal for the '<em><b>Agent System</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AGENT__AGENT_SYSTEM = eINSTANCE.getAgent_AgentSystem();

		/**
		 * The meta object literal for the '<em><b>Arrange Pick Up For</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation AGENT___ARRANGE_PICK_UP_FOR__RESOURCE = eINSTANCE.getAgent__ArrangePickUpFor__Resource();

		/**
		 * The meta object literal for the '{@link organicdesign.impl.RoleImpl <em>Role</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see organicdesign.impl.RoleImpl
		 * @see organicdesign.impl.OrganicdesignPackageImpl#getRole()
		 * @generated
		 */
		EClass ROLE = eINSTANCE.getRole();

		/**
		 * The meta object literal for the '<em><b>Provided Capabilities</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE__PROVIDED_CAPABILITIES = eINSTANCE.getRole_ProvidedCapabilities();

		/**
		 * The meta object literal for the '{@link organicdesign.impl.CapabilityImpl <em>Capability</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see organicdesign.impl.CapabilityImpl
		 * @see organicdesign.impl.OrganicdesignPackageImpl#getCapability()
		 * @generated
		 */
		EClass CAPABILITY = eINSTANCE.getCapability();

		/**
		 * The meta object literal for the '{@link organicdesign.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see organicdesign.impl.NamedElementImpl
		 * @see organicdesign.impl.OrganicdesignPackageImpl#getNamedElement()
		 * @generated
		 */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '{@link organicdesign.impl.CartImpl <em>Cart</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see organicdesign.impl.CartImpl
		 * @see organicdesign.impl.OrganicdesignPackageImpl#getCart()
		 * @generated
		 */
		EClass CART = eINSTANCE.getCart();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CART__LOCATION = eINSTANCE.getCart_Location();

		/**
		 * The meta object literal for the '<em><b>Connected Locations</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CART__CONNECTED_LOCATIONS = eINSTANCE.getCart_ConnectedLocations();

		/**
		 * The meta object literal for the '<em><b>Pick Up Resource At Location Request</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CART___PICK_UP_RESOURCE_AT_LOCATION_REQUEST__RESOURCE_LOCATION = eINSTANCE.getCart__PickUpResourceAtLocationRequest__Resource_Location();

		/**
		 * The meta object literal for the '<em><b>Arrives At Location</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CART___ARRIVES_AT_LOCATION__LOCATION = eINSTANCE.getCart__ArrivesAtLocation__Location();

		/**
		 * The meta object literal for the '<em><b>Resource Loaded</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CART___RESOURCE_LOADED__RESOURCE = eINSTANCE.getCart__ResourceLoaded__Resource();

		/**
		 * The meta object literal for the '<em><b>Resource Unloaded</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CART___RESOURCE_UNLOADED__RESOURCE = eINSTANCE.getCart__ResourceUnloaded__Resource();

		/**
		 * The meta object literal for the '<em><b>Available For Transformation Of Resource</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CART___AVAILABLE_FOR_TRANSFORMATION_OF_RESOURCE__RESOURCE = eINSTANCE.getCart__AvailableForTransformationOfResource__Resource();

		/**
		 * The meta object literal for the '<em><b>Unavailable For Transformation Of Resource</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CART___UNAVAILABLE_FOR_TRANSFORMATION_OF_RESOURCE__RESOURCE = eINSTANCE.getCart__UnavailableForTransformationOfResource__Resource();

		/**
		 * The meta object literal for the '<em><b>Destination Transformation Agent Found For Resource</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation CART___DESTINATION_TRANSFORMATION_AGENT_FOUND_FOR_RESOURCE__TRANSFORMATIONAGENT_RESOURCE = eINSTANCE.getCart__DestinationTransformationAgentFoundForResource__TransformationAgent_Resource();

		/**
		 * The meta object literal for the '{@link organicdesign.impl.TransformationAgentImpl <em>Transformation Agent</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see organicdesign.impl.TransformationAgentImpl
		 * @see organicdesign.impl.OrganicdesignPackageImpl#getTransformationAgent()
		 * @generated
		 */
		EClass TRANSFORMATION_AGENT = eINSTANCE.getTransformationAgent();

		/**
		 * The meta object literal for the '<em><b>All Roles</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSFORMATION_AGENT__ALL_ROLES = eINSTANCE.getTransformationAgent_AllRoles();

		/**
		 * The meta object literal for the '<em><b>Current Role</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSFORMATION_AGENT__CURRENT_ROLE = eINSTANCE.getTransformationAgent_CurrentRole();

		/**
		 * The meta object literal for the '<em><b>All Capabilities</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSFORMATION_AGENT__ALL_CAPABILITIES = eINSTANCE.getTransformationAgent_AllCapabilities();

		/**
		 * The meta object literal for the '<em><b>Activated Capabilities</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSFORMATION_AGENT__ACTIVATED_CAPABILITIES = eINSTANCE.getTransformationAgent_ActivatedCapabilities();

		/**
		 * The meta object literal for the '<em><b>Unavailable Capabilities</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TRANSFORMATION_AGENT__UNAVAILABLE_CAPABILITIES = eINSTANCE.getTransformationAgent_UnavailableCapabilities();

		/**
		 * The meta object literal for the '<em><b>Resource Arrived</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TRANSFORMATION_AGENT___RESOURCE_ARRIVED__RESOURCE = eINSTANCE.getTransformationAgent__ResourceArrived__Resource();

		/**
		 * The meta object literal for the '<em><b>CAN resource Arrived</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TRANSFORMATION_AGENT___CAN_RESOURCE_ARRIVED__RESOURCE = eINSTANCE.getTransformationAgent__CAN_resourceArrived__Resource();

		/**
		 * The meta object literal for the '<em><b>Task Performed</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TRANSFORMATION_AGENT___TASK_PERFORMED = eINSTANCE.getTransformationAgent__TaskPerformed();

		/**
		 * The meta object literal for the '<em><b>Available For Pick Up Of Resource At Location</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TRANSFORMATION_AGENT___AVAILABLE_FOR_PICK_UP_OF_RESOURCE_AT_LOCATION__RESOURCE_LOCATION = eINSTANCE.getTransformationAgent__AvailableForPickUpOfResourceAtLocation__Resource_Location();

		/**
		 * The meta object literal for the '<em><b>Unavailable For Pick Up Of Resource At Location</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TRANSFORMATION_AGENT___UNAVAILABLE_FOR_PICK_UP_OF_RESOURCE_AT_LOCATION__RESOURCE_LOCATION = eINSTANCE.getTransformationAgent__UnavailableForPickUpOfResourceAtLocation__Resource_Location();

		/**
		 * The meta object literal for the '<em><b>Pick Up Arranged For Resource</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TRANSFORMATION_AGENT___PICK_UP_ARRANGED_FOR_RESOURCE__RESOURCE = eINSTANCE.getTransformationAgent__PickUpArrangedForResource__Resource();

		/**
		 * The meta object literal for the '<em><b>Ready To Load Resource</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TRANSFORMATION_AGENT___READY_TO_LOAD_RESOURCE__RESOURCE = eINSTANCE.getTransformationAgent__ReadyToLoadResource__Resource();

		/**
		 * The meta object literal for the '<em><b>Ready To Un Load Resource</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TRANSFORMATION_AGENT___READY_TO_UN_LOAD_RESOURCE__RESOURCE = eINSTANCE.getTransformationAgent__ReadyToUnLoadResource__Resource();

		/**
		 * The meta object literal for the '<em><b>CAN resource Loaded Onto Cart</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TRANSFORMATION_AGENT___CAN_RESOURCE_LOADED_ONTO_CART__RESOURCE_CART = eINSTANCE.getTransformationAgent__CAN_resourceLoadedOntoCart__Resource_Cart();

		/**
		 * The meta object literal for the '<em><b>Resource Loaded Onto Cart</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TRANSFORMATION_AGENT___RESOURCE_LOADED_ONTO_CART__RESOURCE_CART = eINSTANCE.getTransformationAgent__ResourceLoadedOntoCart__Resource_Cart();

		/**
		 * The meta object literal for the '<em><b>CAN resource Unloaded From Cart</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TRANSFORMATION_AGENT___CAN_RESOURCE_UNLOADED_FROM_CART__RESOURCE_CART = eINSTANCE.getTransformationAgent__CAN_resourceUnloadedFromCart__Resource_Cart();

		/**
		 * The meta object literal for the '<em><b>Resource Unloaded From Cart</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TRANSFORMATION_AGENT___RESOURCE_UNLOADED_FROM_CART__RESOURCE_CART = eINSTANCE.getTransformationAgent__ResourceUnloadedFromCart__Resource_Cart();

		/**
		 * The meta object literal for the '<em><b>Transform Resource Request</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TRANSFORMATION_AGENT___TRANSFORM_RESOURCE_REQUEST__RESOURCE = eINSTANCE.getTransformationAgent__TransformResourceRequest__Resource();

		/**
		 * The meta object literal for the '<em><b>Destination Transformation Agent Found</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation TRANSFORMATION_AGENT___DESTINATION_TRANSFORMATION_AGENT_FOUND = eINSTANCE.getTransformationAgent__DestinationTransformationAgentFound();

		/**
		 * The meta object literal for the '{@link organicdesign.impl.TaskDependencyImpl <em>Task Dependency</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see organicdesign.impl.TaskDependencyImpl
		 * @see organicdesign.impl.OrganicdesignPackageImpl#getTaskDependency()
		 * @generated
		 */
		EClass TASK_DEPENDENCY = eINSTANCE.getTaskDependency();

		/**
		 * The meta object literal for the '<em><b>Task</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK_DEPENDENCY__TASK = eINSTANCE.getTaskDependency_Task();

		/**
		 * The meta object literal for the '<em><b>Precondition</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TASK_DEPENDENCY__PRECONDITION = eINSTANCE.getTaskDependency_Precondition();

		/**
		 * The meta object literal for the '{@link organicdesign.impl.EnvironmentImpl <em>Environment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see organicdesign.impl.EnvironmentImpl
		 * @see organicdesign.impl.OrganicdesignPackageImpl#getEnvironment()
		 * @generated
		 */
		EClass ENVIRONMENT = eINSTANCE.getEnvironment();

		/**
		 * The meta object literal for the '<em><b>Deposit Location</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENVIRONMENT__DEPOSIT_LOCATION = eINSTANCE.getEnvironment_DepositLocation();

		/**
		 * The meta object literal for the '<em><b>Environment Location</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENVIRONMENT__ENVIRONMENT_LOCATION = eINSTANCE.getEnvironment_EnvironmentLocation();

		/**
		 * The meta object literal for the '<em><b>Move To Location</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ENVIRONMENT___MOVE_TO_LOCATION__LOCATION = eINSTANCE.getEnvironment__MoveToLocation__Location();

		/**
		 * The meta object literal for the '<em><b>Load Resource Onto Cart</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ENVIRONMENT___LOAD_RESOURCE_ONTO_CART__RESOURCE_CART = eINSTANCE.getEnvironment__LoadResourceOntoCart__Resource_Cart();

		/**
		 * The meta object literal for the '<em><b>Unload Resource From Cart</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ENVIRONMENT___UNLOAD_RESOURCE_FROM_CART__RESOURCE_CART = eINSTANCE.getEnvironment__UnloadResourceFromCart__Resource_Cart();

		/**
		 * The meta object literal for the '<em><b>Reset Resource</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ENVIRONMENT___RESET_RESOURCE__RESOURCE = eINSTANCE.getEnvironment__ResetResource__Resource();

		/**
		 * The meta object literal for the '{@link organicdesign.impl.EnvironmentLocationImpl <em>Environment Location</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see organicdesign.impl.EnvironmentLocationImpl
		 * @see organicdesign.impl.OrganicdesignPackageImpl#getEnvironmentLocation()
		 * @generated
		 */
		EClass ENVIRONMENT_LOCATION = eINSTANCE.getEnvironmentLocation();

		/**
		 * The meta object literal for the '<em><b>Connected Agent</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENVIRONMENT_LOCATION__CONNECTED_AGENT = eINSTANCE.getEnvironmentLocation_ConnectedAgent();

		/**
		 * The meta object literal for the '{@link organicdesign.impl.LocationImpl <em>Location</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see organicdesign.impl.LocationImpl
		 * @see organicdesign.impl.OrganicdesignPackageImpl#getLocation()
		 * @generated
		 */
		EClass LOCATION = eINSTANCE.getLocation();

		/**
		 * The meta object literal for the '<em><b>Located Resources</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOCATION__LOCATED_RESOURCES = eINSTANCE.getLocation_LocatedResources();

		/**
		 * The meta object literal for the '<em><b>Connected By</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOCATION__CONNECTED_BY = eINSTANCE.getLocation_ConnectedBy();

	}

} //OrganicdesignPackage
