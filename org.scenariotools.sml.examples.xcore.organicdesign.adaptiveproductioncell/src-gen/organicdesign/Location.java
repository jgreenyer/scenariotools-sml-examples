/**
 */
package organicdesign;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Location</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link organicdesign.Location#getLocatedResources <em>Located Resources</em>}</li>
 *   <li>{@link organicdesign.Location#getConnectedBy <em>Connected By</em>}</li>
 * </ul>
 *
 * @see organicdesign.OrganicdesignPackage#getLocation()
 * @model abstract="true"
 * @generated
 */
public interface Location extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Located Resources</b></em>' reference list.
	 * The list contents are of type {@link organicdesign.Resource}.
	 * It is bidirectional and its opposite is '{@link organicdesign.Resource#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Located Resources</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Located Resources</em>' reference list.
	 * @see organicdesign.OrganicdesignPackage#getLocation_LocatedResources()
	 * @see organicdesign.Resource#getLocation
	 * @model opposite="location" ordered="false"
	 * @generated
	 */
	EList<Resource> getLocatedResources();

	/**
	 * Returns the value of the '<em><b>Connected By</b></em>' reference list.
	 * The list contents are of type {@link organicdesign.Cart}.
	 * It is bidirectional and its opposite is '{@link organicdesign.Cart#getConnectedLocations <em>Connected Locations</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connected By</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connected By</em>' reference list.
	 * @see organicdesign.OrganicdesignPackage#getLocation_ConnectedBy()
	 * @see organicdesign.Cart#getConnectedLocations
	 * @model opposite="connectedLocations" ordered="false"
	 * @generated
	 */
	EList<Cart> getConnectedBy();

} // Location
