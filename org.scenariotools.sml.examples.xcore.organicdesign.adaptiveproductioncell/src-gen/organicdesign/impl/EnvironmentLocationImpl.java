/**
 */
package organicdesign.impl;

import java.util.Collection;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import organicdesign.EnvironmentLocation;
import organicdesign.OrganicdesignPackage;
import organicdesign.TransformationAgent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Environment Location</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link organicdesign.impl.EnvironmentLocationImpl#getConnectedAgent <em>Connected Agent</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EnvironmentLocationImpl extends LocationImpl implements EnvironmentLocation {
	/**
	 * The cached value of the '{@link #getConnectedAgent() <em>Connected Agent</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnectedAgent()
	 * @generated
	 * @ordered
	 */
	protected EList<TransformationAgent> connectedAgent;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EnvironmentLocationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OrganicdesignPackage.Literals.ENVIRONMENT_LOCATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TransformationAgent> getConnectedAgent() {
		if (connectedAgent == null) {
			connectedAgent = new EObjectResolvingEList<TransformationAgent>(TransformationAgent.class, this, OrganicdesignPackage.ENVIRONMENT_LOCATION__CONNECTED_AGENT);
		}
		return connectedAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OrganicdesignPackage.ENVIRONMENT_LOCATION__CONNECTED_AGENT:
				return getConnectedAgent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OrganicdesignPackage.ENVIRONMENT_LOCATION__CONNECTED_AGENT:
				getConnectedAgent().clear();
				getConnectedAgent().addAll((Collection<? extends TransformationAgent>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OrganicdesignPackage.ENVIRONMENT_LOCATION__CONNECTED_AGENT:
				getConnectedAgent().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OrganicdesignPackage.ENVIRONMENT_LOCATION__CONNECTED_AGENT:
				return connectedAgent != null && !connectedAgent.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //EnvironmentLocationImpl
