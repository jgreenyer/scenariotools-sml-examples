/**
 */
package organicdesign.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import organicdesign.Cart;
import organicdesign.Location;
import organicdesign.OrganicdesignPackage;
import organicdesign.Resource;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Location</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link organicdesign.impl.LocationImpl#getLocatedResources <em>Located Resources</em>}</li>
 *   <li>{@link organicdesign.impl.LocationImpl#getConnectedBy <em>Connected By</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class LocationImpl extends NamedElementImpl implements Location {
	/**
	 * The cached value of the '{@link #getLocatedResources() <em>Located Resources</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocatedResources()
	 * @generated
	 * @ordered
	 */
	protected EList<Resource> locatedResources;

	/**
	 * The cached value of the '{@link #getConnectedBy() <em>Connected By</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnectedBy()
	 * @generated
	 * @ordered
	 */
	protected EList<Cart> connectedBy;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LocationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OrganicdesignPackage.Literals.LOCATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Resource> getLocatedResources() {
		if (locatedResources == null) {
			locatedResources = new EObjectWithInverseResolvingEList<Resource>(Resource.class, this, OrganicdesignPackage.LOCATION__LOCATED_RESOURCES, OrganicdesignPackage.RESOURCE__LOCATION);
		}
		return locatedResources;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Cart> getConnectedBy() {
		if (connectedBy == null) {
			connectedBy = new EObjectWithInverseResolvingEList.ManyInverse<Cart>(Cart.class, this, OrganicdesignPackage.LOCATION__CONNECTED_BY, OrganicdesignPackage.CART__CONNECTED_LOCATIONS);
		}
		return connectedBy;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OrganicdesignPackage.LOCATION__LOCATED_RESOURCES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getLocatedResources()).basicAdd(otherEnd, msgs);
			case OrganicdesignPackage.LOCATION__CONNECTED_BY:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getConnectedBy()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OrganicdesignPackage.LOCATION__LOCATED_RESOURCES:
				return ((InternalEList<?>)getLocatedResources()).basicRemove(otherEnd, msgs);
			case OrganicdesignPackage.LOCATION__CONNECTED_BY:
				return ((InternalEList<?>)getConnectedBy()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OrganicdesignPackage.LOCATION__LOCATED_RESOURCES:
				return getLocatedResources();
			case OrganicdesignPackage.LOCATION__CONNECTED_BY:
				return getConnectedBy();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OrganicdesignPackage.LOCATION__LOCATED_RESOURCES:
				getLocatedResources().clear();
				getLocatedResources().addAll((Collection<? extends Resource>)newValue);
				return;
			case OrganicdesignPackage.LOCATION__CONNECTED_BY:
				getConnectedBy().clear();
				getConnectedBy().addAll((Collection<? extends Cart>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OrganicdesignPackage.LOCATION__LOCATED_RESOURCES:
				getLocatedResources().clear();
				return;
			case OrganicdesignPackage.LOCATION__CONNECTED_BY:
				getConnectedBy().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OrganicdesignPackage.LOCATION__LOCATED_RESOURCES:
				return locatedResources != null && !locatedResources.isEmpty();
			case OrganicdesignPackage.LOCATION__CONNECTED_BY:
				return connectedBy != null && !connectedBy.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //LocationImpl
