/**
 */
package organicdesign.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import organicdesign.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OrganicdesignFactoryImpl extends EFactoryImpl implements OrganicdesignFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static OrganicdesignFactory init() {
		try {
			OrganicdesignFactory theOrganicdesignFactory = (OrganicdesignFactory)EPackage.Registry.INSTANCE.getEFactory(OrganicdesignPackage.eNS_URI);
			if (theOrganicdesignFactory != null) {
				return theOrganicdesignFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new OrganicdesignFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrganicdesignFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case OrganicdesignPackage.AGENT_SYSTEM: return createAgentSystem();
			case OrganicdesignPackage.TASK: return createTask();
			case OrganicdesignPackage.RESOURCE: return createResource();
			case OrganicdesignPackage.RECIPE: return createRecipe();
			case OrganicdesignPackage.ROLE: return createRole();
			case OrganicdesignPackage.CAPABILITY: return createCapability();
			case OrganicdesignPackage.CART: return createCart();
			case OrganicdesignPackage.TRANSFORMATION_AGENT: return createTransformationAgent();
			case OrganicdesignPackage.TASK_DEPENDENCY: return createTaskDependency();
			case OrganicdesignPackage.ENVIRONMENT: return createEnvironment();
			case OrganicdesignPackage.ENVIRONMENT_LOCATION: return createEnvironmentLocation();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AgentSystem createAgentSystem() {
		AgentSystemImpl agentSystem = new AgentSystemImpl();
		return agentSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Task createTask() {
		TaskImpl task = new TaskImpl();
		return task;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Resource createResource() {
		ResourceImpl resource = new ResourceImpl();
		return resource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Recipe createRecipe() {
		RecipeImpl recipe = new RecipeImpl();
		return recipe;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role createRole() {
		RoleImpl role = new RoleImpl();
		return role;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Capability createCapability() {
		CapabilityImpl capability = new CapabilityImpl();
		return capability;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Cart createCart() {
		CartImpl cart = new CartImpl();
		return cart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TransformationAgent createTransformationAgent() {
		TransformationAgentImpl transformationAgent = new TransformationAgentImpl();
		return transformationAgent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TaskDependency createTaskDependency() {
		TaskDependencyImpl taskDependency = new TaskDependencyImpl();
		return taskDependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Environment createEnvironment() {
		EnvironmentImpl environment = new EnvironmentImpl();
		return environment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnvironmentLocation createEnvironmentLocation() {
		EnvironmentLocationImpl environmentLocation = new EnvironmentLocationImpl();
		return environmentLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrganicdesignPackage getOrganicdesignPackage() {
		return (OrganicdesignPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static OrganicdesignPackage getPackage() {
		return OrganicdesignPackage.eINSTANCE;
	}

} //OrganicdesignFactoryImpl
