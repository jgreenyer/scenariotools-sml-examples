/**
 */
package organicdesign.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import organicdesign.Cart;
import organicdesign.Environment;
import organicdesign.EnvironmentLocation;
import organicdesign.Location;
import organicdesign.OrganicdesignPackage;
import organicdesign.Resource;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Environment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link organicdesign.impl.EnvironmentImpl#getDepositLocation <em>Deposit Location</em>}</li>
 *   <li>{@link organicdesign.impl.EnvironmentImpl#getEnvironmentLocation <em>Environment Location</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EnvironmentImpl extends NamedElementImpl implements Environment {
	/**
	 * The cached value of the '{@link #getDepositLocation() <em>Deposit Location</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDepositLocation()
	 * @generated
	 * @ordered
	 */
	protected Location depositLocation;

	/**
	 * The cached value of the '{@link #getEnvironmentLocation() <em>Environment Location</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnvironmentLocation()
	 * @generated
	 * @ordered
	 */
	protected EnvironmentLocation environmentLocation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EnvironmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OrganicdesignPackage.Literals.ENVIRONMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Location getDepositLocation() {
		if (depositLocation != null && depositLocation.eIsProxy()) {
			InternalEObject oldDepositLocation = (InternalEObject)depositLocation;
			depositLocation = (Location)eResolveProxy(oldDepositLocation);
			if (depositLocation != oldDepositLocation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OrganicdesignPackage.ENVIRONMENT__DEPOSIT_LOCATION, oldDepositLocation, depositLocation));
			}
		}
		return depositLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Location basicGetDepositLocation() {
		return depositLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDepositLocation(Location newDepositLocation) {
		Location oldDepositLocation = depositLocation;
		depositLocation = newDepositLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OrganicdesignPackage.ENVIRONMENT__DEPOSIT_LOCATION, oldDepositLocation, depositLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnvironmentLocation getEnvironmentLocation() {
		if (environmentLocation != null && environmentLocation.eIsProxy()) {
			InternalEObject oldEnvironmentLocation = (InternalEObject)environmentLocation;
			environmentLocation = (EnvironmentLocation)eResolveProxy(oldEnvironmentLocation);
			if (environmentLocation != oldEnvironmentLocation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OrganicdesignPackage.ENVIRONMENT__ENVIRONMENT_LOCATION, oldEnvironmentLocation, environmentLocation));
			}
		}
		return environmentLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EnvironmentLocation basicGetEnvironmentLocation() {
		return environmentLocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnvironmentLocation(EnvironmentLocation newEnvironmentLocation) {
		EnvironmentLocation oldEnvironmentLocation = environmentLocation;
		environmentLocation = newEnvironmentLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OrganicdesignPackage.ENVIRONMENT__ENVIRONMENT_LOCATION, oldEnvironmentLocation, environmentLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void moveToLocation(Location location) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void loadResourceOntoCart(Resource resource, Cart cart) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unloadResourceFromCart(Resource resource, Cart cart) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void resetResource(Resource resource) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OrganicdesignPackage.ENVIRONMENT__DEPOSIT_LOCATION:
				if (resolve) return getDepositLocation();
				return basicGetDepositLocation();
			case OrganicdesignPackage.ENVIRONMENT__ENVIRONMENT_LOCATION:
				if (resolve) return getEnvironmentLocation();
				return basicGetEnvironmentLocation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OrganicdesignPackage.ENVIRONMENT__DEPOSIT_LOCATION:
				setDepositLocation((Location)newValue);
				return;
			case OrganicdesignPackage.ENVIRONMENT__ENVIRONMENT_LOCATION:
				setEnvironmentLocation((EnvironmentLocation)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OrganicdesignPackage.ENVIRONMENT__DEPOSIT_LOCATION:
				setDepositLocation((Location)null);
				return;
			case OrganicdesignPackage.ENVIRONMENT__ENVIRONMENT_LOCATION:
				setEnvironmentLocation((EnvironmentLocation)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OrganicdesignPackage.ENVIRONMENT__DEPOSIT_LOCATION:
				return depositLocation != null;
			case OrganicdesignPackage.ENVIRONMENT__ENVIRONMENT_LOCATION:
				return environmentLocation != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case OrganicdesignPackage.ENVIRONMENT___MOVE_TO_LOCATION__LOCATION:
				moveToLocation((Location)arguments.get(0));
				return null;
			case OrganicdesignPackage.ENVIRONMENT___LOAD_RESOURCE_ONTO_CART__RESOURCE_CART:
				loadResourceOntoCart((Resource)arguments.get(0), (Cart)arguments.get(1));
				return null;
			case OrganicdesignPackage.ENVIRONMENT___UNLOAD_RESOURCE_FROM_CART__RESOURCE_CART:
				unloadResourceFromCart((Resource)arguments.get(0), (Cart)arguments.get(1));
				return null;
			case OrganicdesignPackage.ENVIRONMENT___RESET_RESOURCE__RESOURCE:
				resetResource((Resource)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //EnvironmentImpl
