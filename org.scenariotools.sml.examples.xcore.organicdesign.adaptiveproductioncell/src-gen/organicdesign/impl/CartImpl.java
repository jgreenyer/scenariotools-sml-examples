/**
 */
package organicdesign.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import organicdesign.Cart;
import organicdesign.Location;
import organicdesign.OrganicdesignPackage;
import organicdesign.Resource;
import organicdesign.TransformationAgent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cart</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link organicdesign.impl.CartImpl#getLocation <em>Location</em>}</li>
 *   <li>{@link organicdesign.impl.CartImpl#getConnectedLocations <em>Connected Locations</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CartImpl extends AgentImpl implements Cart {
	/**
	 * The cached value of the '{@link #getLocation() <em>Location</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected Location location;

	/**
	 * The cached value of the '{@link #getConnectedLocations() <em>Connected Locations</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConnectedLocations()
	 * @generated
	 * @ordered
	 */
	protected EList<Location> connectedLocations;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CartImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OrganicdesignPackage.Literals.CART;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Location getLocation() {
		if (location != null && location.eIsProxy()) {
			InternalEObject oldLocation = (InternalEObject)location;
			location = (Location)eResolveProxy(oldLocation);
			if (location != oldLocation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OrganicdesignPackage.CART__LOCATION, oldLocation, location));
			}
		}
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Location basicGetLocation() {
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocation(Location newLocation) {
		Location oldLocation = location;
		location = newLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OrganicdesignPackage.CART__LOCATION, oldLocation, location));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Location> getConnectedLocations() {
		if (connectedLocations == null) {
			connectedLocations = new EObjectWithInverseResolvingEList.ManyInverse<Location>(Location.class, this, OrganicdesignPackage.CART__CONNECTED_LOCATIONS, OrganicdesignPackage.LOCATION__CONNECTED_BY);
		}
		return connectedLocations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void pickUpResourceAtLocationRequest(Resource resource, Location location) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void arrivesAtLocation(final Location location) {
		this.setLocation(location);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void resourceLoaded(Resource resource) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void resourceUnloaded(Resource resource) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void availableForTransformationOfResource(Resource resource) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unavailableForTransformationOfResource(Resource resource) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void destinationTransformationAgentFoundForResource(TransformationAgent transformationAgent, Resource resource) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OrganicdesignPackage.CART__CONNECTED_LOCATIONS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getConnectedLocations()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OrganicdesignPackage.CART__CONNECTED_LOCATIONS:
				return ((InternalEList<?>)getConnectedLocations()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OrganicdesignPackage.CART__LOCATION:
				if (resolve) return getLocation();
				return basicGetLocation();
			case OrganicdesignPackage.CART__CONNECTED_LOCATIONS:
				return getConnectedLocations();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OrganicdesignPackage.CART__LOCATION:
				setLocation((Location)newValue);
				return;
			case OrganicdesignPackage.CART__CONNECTED_LOCATIONS:
				getConnectedLocations().clear();
				getConnectedLocations().addAll((Collection<? extends Location>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OrganicdesignPackage.CART__LOCATION:
				setLocation((Location)null);
				return;
			case OrganicdesignPackage.CART__CONNECTED_LOCATIONS:
				getConnectedLocations().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OrganicdesignPackage.CART__LOCATION:
				return location != null;
			case OrganicdesignPackage.CART__CONNECTED_LOCATIONS:
				return connectedLocations != null && !connectedLocations.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case OrganicdesignPackage.CART___PICK_UP_RESOURCE_AT_LOCATION_REQUEST__RESOURCE_LOCATION:
				pickUpResourceAtLocationRequest((Resource)arguments.get(0), (Location)arguments.get(1));
				return null;
			case OrganicdesignPackage.CART___ARRIVES_AT_LOCATION__LOCATION:
				arrivesAtLocation((Location)arguments.get(0));
				return null;
			case OrganicdesignPackage.CART___RESOURCE_LOADED__RESOURCE:
				resourceLoaded((Resource)arguments.get(0));
				return null;
			case OrganicdesignPackage.CART___RESOURCE_UNLOADED__RESOURCE:
				resourceUnloaded((Resource)arguments.get(0));
				return null;
			case OrganicdesignPackage.CART___AVAILABLE_FOR_TRANSFORMATION_OF_RESOURCE__RESOURCE:
				availableForTransformationOfResource((Resource)arguments.get(0));
				return null;
			case OrganicdesignPackage.CART___UNAVAILABLE_FOR_TRANSFORMATION_OF_RESOURCE__RESOURCE:
				unavailableForTransformationOfResource((Resource)arguments.get(0));
				return null;
			case OrganicdesignPackage.CART___DESTINATION_TRANSFORMATION_AGENT_FOUND_FOR_RESOURCE__TRANSFORMATIONAGENT_RESOURCE:
				destinationTransformationAgentFoundForResource((TransformationAgent)arguments.get(0), (Resource)arguments.get(1));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //CartImpl
