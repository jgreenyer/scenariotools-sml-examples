/**
 */
package organicdesign.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import organicdesign.Agent;
import organicdesign.AgentSystem;
import organicdesign.Capability;
import organicdesign.Environment;
import organicdesign.EnvironmentLocation;
import organicdesign.OrganicdesignPackage;
import organicdesign.Recipe;
import organicdesign.Resource;
import organicdesign.Role;
import organicdesign.Task;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Agent System</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link organicdesign.impl.AgentSystemImpl#getResource <em>Resource</em>}</li>
 *   <li>{@link organicdesign.impl.AgentSystemImpl#getAgents <em>Agents</em>}</li>
 *   <li>{@link organicdesign.impl.AgentSystemImpl#getTasks <em>Tasks</em>}</li>
 *   <li>{@link organicdesign.impl.AgentSystemImpl#getCapabilities <em>Capabilities</em>}</li>
 *   <li>{@link organicdesign.impl.AgentSystemImpl#getRoles <em>Roles</em>}</li>
 *   <li>{@link organicdesign.impl.AgentSystemImpl#getRecipes <em>Recipes</em>}</li>
 *   <li>{@link organicdesign.impl.AgentSystemImpl#getEnvironment <em>Environment</em>}</li>
 *   <li>{@link organicdesign.impl.AgentSystemImpl#getEnvironmentlocation <em>Environmentlocation</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AgentSystemImpl extends NamedElementImpl implements AgentSystem {
	/**
	 * The cached value of the '{@link #getResource() <em>Resource</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getResource()
	 * @generated
	 * @ordered
	 */
	protected EList<Resource> resource;

	/**
	 * The cached value of the '{@link #getAgents() <em>Agents</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAgents()
	 * @generated
	 * @ordered
	 */
	protected EList<Agent> agents;

	/**
	 * The cached value of the '{@link #getTasks() <em>Tasks</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTasks()
	 * @generated
	 * @ordered
	 */
	protected EList<Task> tasks;

	/**
	 * The cached value of the '{@link #getCapabilities() <em>Capabilities</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCapabilities()
	 * @generated
	 * @ordered
	 */
	protected EList<Capability> capabilities;

	/**
	 * The cached value of the '{@link #getRoles() <em>Roles</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoles()
	 * @generated
	 * @ordered
	 */
	protected EList<Role> roles;

	/**
	 * The cached value of the '{@link #getRecipes() <em>Recipes</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRecipes()
	 * @generated
	 * @ordered
	 */
	protected EList<Recipe> recipes;

	/**
	 * The cached value of the '{@link #getEnvironment() <em>Environment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnvironment()
	 * @generated
	 * @ordered
	 */
	protected Environment environment;

	/**
	 * The cached value of the '{@link #getEnvironmentlocation() <em>Environmentlocation</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnvironmentlocation()
	 * @generated
	 * @ordered
	 */
	protected EList<EnvironmentLocation> environmentlocation;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AgentSystemImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OrganicdesignPackage.Literals.AGENT_SYSTEM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Resource> getResource() {
		if (resource == null) {
			resource = new EObjectContainmentEList<Resource>(Resource.class, this, OrganicdesignPackage.AGENT_SYSTEM__RESOURCE);
		}
		return resource;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Agent> getAgents() {
		if (agents == null) {
			agents = new EObjectContainmentWithInverseEList<Agent>(Agent.class, this, OrganicdesignPackage.AGENT_SYSTEM__AGENTS, OrganicdesignPackage.AGENT__AGENT_SYSTEM);
		}
		return agents;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Task> getTasks() {
		if (tasks == null) {
			tasks = new EObjectContainmentEList<Task>(Task.class, this, OrganicdesignPackage.AGENT_SYSTEM__TASKS);
		}
		return tasks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Capability> getCapabilities() {
		if (capabilities == null) {
			capabilities = new EObjectContainmentEList<Capability>(Capability.class, this, OrganicdesignPackage.AGENT_SYSTEM__CAPABILITIES);
		}
		return capabilities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Role> getRoles() {
		if (roles == null) {
			roles = new EObjectContainmentEList<Role>(Role.class, this, OrganicdesignPackage.AGENT_SYSTEM__ROLES);
		}
		return roles;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Recipe> getRecipes() {
		if (recipes == null) {
			recipes = new EObjectContainmentEList<Recipe>(Recipe.class, this, OrganicdesignPackage.AGENT_SYSTEM__RECIPES);
		}
		return recipes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Environment getEnvironment() {
		return environment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEnvironment(Environment newEnvironment, NotificationChain msgs) {
		Environment oldEnvironment = environment;
		environment = newEnvironment;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OrganicdesignPackage.AGENT_SYSTEM__ENVIRONMENT, oldEnvironment, newEnvironment);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnvironment(Environment newEnvironment) {
		if (newEnvironment != environment) {
			NotificationChain msgs = null;
			if (environment != null)
				msgs = ((InternalEObject)environment).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - OrganicdesignPackage.AGENT_SYSTEM__ENVIRONMENT, null, msgs);
			if (newEnvironment != null)
				msgs = ((InternalEObject)newEnvironment).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - OrganicdesignPackage.AGENT_SYSTEM__ENVIRONMENT, null, msgs);
			msgs = basicSetEnvironment(newEnvironment, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OrganicdesignPackage.AGENT_SYSTEM__ENVIRONMENT, newEnvironment, newEnvironment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<EnvironmentLocation> getEnvironmentlocation() {
		if (environmentlocation == null) {
			environmentlocation = new EObjectContainmentEList<EnvironmentLocation>(EnvironmentLocation.class, this, OrganicdesignPackage.AGENT_SYSTEM__ENVIRONMENTLOCATION);
		}
		return environmentlocation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OrganicdesignPackage.AGENT_SYSTEM__AGENTS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getAgents()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OrganicdesignPackage.AGENT_SYSTEM__RESOURCE:
				return ((InternalEList<?>)getResource()).basicRemove(otherEnd, msgs);
			case OrganicdesignPackage.AGENT_SYSTEM__AGENTS:
				return ((InternalEList<?>)getAgents()).basicRemove(otherEnd, msgs);
			case OrganicdesignPackage.AGENT_SYSTEM__TASKS:
				return ((InternalEList<?>)getTasks()).basicRemove(otherEnd, msgs);
			case OrganicdesignPackage.AGENT_SYSTEM__CAPABILITIES:
				return ((InternalEList<?>)getCapabilities()).basicRemove(otherEnd, msgs);
			case OrganicdesignPackage.AGENT_SYSTEM__ROLES:
				return ((InternalEList<?>)getRoles()).basicRemove(otherEnd, msgs);
			case OrganicdesignPackage.AGENT_SYSTEM__RECIPES:
				return ((InternalEList<?>)getRecipes()).basicRemove(otherEnd, msgs);
			case OrganicdesignPackage.AGENT_SYSTEM__ENVIRONMENT:
				return basicSetEnvironment(null, msgs);
			case OrganicdesignPackage.AGENT_SYSTEM__ENVIRONMENTLOCATION:
				return ((InternalEList<?>)getEnvironmentlocation()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OrganicdesignPackage.AGENT_SYSTEM__RESOURCE:
				return getResource();
			case OrganicdesignPackage.AGENT_SYSTEM__AGENTS:
				return getAgents();
			case OrganicdesignPackage.AGENT_SYSTEM__TASKS:
				return getTasks();
			case OrganicdesignPackage.AGENT_SYSTEM__CAPABILITIES:
				return getCapabilities();
			case OrganicdesignPackage.AGENT_SYSTEM__ROLES:
				return getRoles();
			case OrganicdesignPackage.AGENT_SYSTEM__RECIPES:
				return getRecipes();
			case OrganicdesignPackage.AGENT_SYSTEM__ENVIRONMENT:
				return getEnvironment();
			case OrganicdesignPackage.AGENT_SYSTEM__ENVIRONMENTLOCATION:
				return getEnvironmentlocation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OrganicdesignPackage.AGENT_SYSTEM__RESOURCE:
				getResource().clear();
				getResource().addAll((Collection<? extends Resource>)newValue);
				return;
			case OrganicdesignPackage.AGENT_SYSTEM__AGENTS:
				getAgents().clear();
				getAgents().addAll((Collection<? extends Agent>)newValue);
				return;
			case OrganicdesignPackage.AGENT_SYSTEM__TASKS:
				getTasks().clear();
				getTasks().addAll((Collection<? extends Task>)newValue);
				return;
			case OrganicdesignPackage.AGENT_SYSTEM__CAPABILITIES:
				getCapabilities().clear();
				getCapabilities().addAll((Collection<? extends Capability>)newValue);
				return;
			case OrganicdesignPackage.AGENT_SYSTEM__ROLES:
				getRoles().clear();
				getRoles().addAll((Collection<? extends Role>)newValue);
				return;
			case OrganicdesignPackage.AGENT_SYSTEM__RECIPES:
				getRecipes().clear();
				getRecipes().addAll((Collection<? extends Recipe>)newValue);
				return;
			case OrganicdesignPackage.AGENT_SYSTEM__ENVIRONMENT:
				setEnvironment((Environment)newValue);
				return;
			case OrganicdesignPackage.AGENT_SYSTEM__ENVIRONMENTLOCATION:
				getEnvironmentlocation().clear();
				getEnvironmentlocation().addAll((Collection<? extends EnvironmentLocation>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OrganicdesignPackage.AGENT_SYSTEM__RESOURCE:
				getResource().clear();
				return;
			case OrganicdesignPackage.AGENT_SYSTEM__AGENTS:
				getAgents().clear();
				return;
			case OrganicdesignPackage.AGENT_SYSTEM__TASKS:
				getTasks().clear();
				return;
			case OrganicdesignPackage.AGENT_SYSTEM__CAPABILITIES:
				getCapabilities().clear();
				return;
			case OrganicdesignPackage.AGENT_SYSTEM__ROLES:
				getRoles().clear();
				return;
			case OrganicdesignPackage.AGENT_SYSTEM__RECIPES:
				getRecipes().clear();
				return;
			case OrganicdesignPackage.AGENT_SYSTEM__ENVIRONMENT:
				setEnvironment((Environment)null);
				return;
			case OrganicdesignPackage.AGENT_SYSTEM__ENVIRONMENTLOCATION:
				getEnvironmentlocation().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OrganicdesignPackage.AGENT_SYSTEM__RESOURCE:
				return resource != null && !resource.isEmpty();
			case OrganicdesignPackage.AGENT_SYSTEM__AGENTS:
				return agents != null && !agents.isEmpty();
			case OrganicdesignPackage.AGENT_SYSTEM__TASKS:
				return tasks != null && !tasks.isEmpty();
			case OrganicdesignPackage.AGENT_SYSTEM__CAPABILITIES:
				return capabilities != null && !capabilities.isEmpty();
			case OrganicdesignPackage.AGENT_SYSTEM__ROLES:
				return roles != null && !roles.isEmpty();
			case OrganicdesignPackage.AGENT_SYSTEM__RECIPES:
				return recipes != null && !recipes.isEmpty();
			case OrganicdesignPackage.AGENT_SYSTEM__ENVIRONMENT:
				return environment != null;
			case OrganicdesignPackage.AGENT_SYSTEM__ENVIRONMENTLOCATION:
				return environmentlocation != null && !environmentlocation.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //AgentSystemImpl
