/**
 */
package organicdesign.impl;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import organicdesign.Environment;
import organicdesign.EnvironmentLocation;
import organicdesign.Location;
import organicdesign.OrganicdesignPackage;
import organicdesign.Recipe;
import organicdesign.Resource;
import organicdesign.Task;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Resource</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link organicdesign.impl.ResourceImpl#getRecipe <em>Recipe</em>}</li>
 *   <li>{@link organicdesign.impl.ResourceImpl#getFulfilledTask <em>Fulfilled Task</em>}</li>
 *   <li>{@link organicdesign.impl.ResourceImpl#getLocation <em>Location</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ResourceImpl extends NamedElementImpl implements Resource {
	/**
	 * The cached value of the '{@link #getRecipe() <em>Recipe</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRecipe()
	 * @generated
	 * @ordered
	 */
	protected Recipe recipe;

	/**
	 * The cached value of the '{@link #getFulfilledTask() <em>Fulfilled Task</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFulfilledTask()
	 * @generated
	 * @ordered
	 */
	protected EList<Task> fulfilledTask;

	/**
	 * The cached value of the '{@link #getLocation() <em>Location</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected Location location;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResourceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OrganicdesignPackage.Literals.RESOURCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Recipe getRecipe() {
		if (recipe != null && recipe.eIsProxy()) {
			InternalEObject oldRecipe = (InternalEObject)recipe;
			recipe = (Recipe)eResolveProxy(oldRecipe);
			if (recipe != oldRecipe) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OrganicdesignPackage.RESOURCE__RECIPE, oldRecipe, recipe));
			}
		}
		return recipe;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Recipe basicGetRecipe() {
		return recipe;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRecipe(Recipe newRecipe) {
		Recipe oldRecipe = recipe;
		recipe = newRecipe;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OrganicdesignPackage.RESOURCE__RECIPE, oldRecipe, recipe));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Task> getFulfilledTask() {
		if (fulfilledTask == null) {
			fulfilledTask = new EObjectResolvingEList<Task>(Task.class, this, OrganicdesignPackage.RESOURCE__FULFILLED_TASK);
		}
		return fulfilledTask;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Location getLocation() {
		if (location != null && location.eIsProxy()) {
			InternalEObject oldLocation = (InternalEObject)location;
			location = (Location)eResolveProxy(oldLocation);
			if (location != oldLocation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OrganicdesignPackage.RESOURCE__LOCATION, oldLocation, location));
			}
		}
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Location basicGetLocation() {
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLocation(Location newLocation, NotificationChain msgs) {
		Location oldLocation = location;
		location = newLocation;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, OrganicdesignPackage.RESOURCE__LOCATION, oldLocation, newLocation);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocation(Location newLocation) {
		if (newLocation != location) {
			NotificationChain msgs = null;
			if (location != null)
				msgs = ((InternalEObject)location).eInverseRemove(this, OrganicdesignPackage.LOCATION__LOCATED_RESOURCES, Location.class, msgs);
			if (newLocation != null)
				msgs = ((InternalEObject)newLocation).eInverseAdd(this, OrganicdesignPackage.LOCATION__LOCATED_RESOURCES, Location.class, msgs);
			msgs = basicSetLocation(newLocation, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OrganicdesignPackage.RESOURCE__LOCATION, newLocation, newLocation));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean CAN_performTask(final Task task) {
		return (this.getRecipe().getRequiredTasks().contains(task) && (!this.getFulfilledTask().contains(task)));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void performTask(final Task task) {
		EList<Task> _fulfilledTask = this.getFulfilledTask();
		_fulfilledTask.add(task);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void reset(final Environment env) {
		EnvironmentLocation _environmentLocation = env.getEnvironmentLocation();
		this.setLocation(_environmentLocation);
		EList<Task> _fulfilledTask = this.getFulfilledTask();
		_fulfilledTask.clear();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OrganicdesignPackage.RESOURCE__LOCATION:
				if (location != null)
					msgs = ((InternalEObject)location).eInverseRemove(this, OrganicdesignPackage.LOCATION__LOCATED_RESOURCES, Location.class, msgs);
				return basicSetLocation((Location)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OrganicdesignPackage.RESOURCE__LOCATION:
				return basicSetLocation(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OrganicdesignPackage.RESOURCE__RECIPE:
				if (resolve) return getRecipe();
				return basicGetRecipe();
			case OrganicdesignPackage.RESOURCE__FULFILLED_TASK:
				return getFulfilledTask();
			case OrganicdesignPackage.RESOURCE__LOCATION:
				if (resolve) return getLocation();
				return basicGetLocation();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OrganicdesignPackage.RESOURCE__RECIPE:
				setRecipe((Recipe)newValue);
				return;
			case OrganicdesignPackage.RESOURCE__FULFILLED_TASK:
				getFulfilledTask().clear();
				getFulfilledTask().addAll((Collection<? extends Task>)newValue);
				return;
			case OrganicdesignPackage.RESOURCE__LOCATION:
				setLocation((Location)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OrganicdesignPackage.RESOURCE__RECIPE:
				setRecipe((Recipe)null);
				return;
			case OrganicdesignPackage.RESOURCE__FULFILLED_TASK:
				getFulfilledTask().clear();
				return;
			case OrganicdesignPackage.RESOURCE__LOCATION:
				setLocation((Location)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OrganicdesignPackage.RESOURCE__RECIPE:
				return recipe != null;
			case OrganicdesignPackage.RESOURCE__FULFILLED_TASK:
				return fulfilledTask != null && !fulfilledTask.isEmpty();
			case OrganicdesignPackage.RESOURCE__LOCATION:
				return location != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case OrganicdesignPackage.RESOURCE___CAN_PERFORM_TASK__TASK:
				return CAN_performTask((Task)arguments.get(0));
			case OrganicdesignPackage.RESOURCE___PERFORM_TASK__TASK:
				performTask((Task)arguments.get(0));
				return null;
			case OrganicdesignPackage.RESOURCE___RESET__ENVIRONMENT:
				reset((Environment)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //ResourceImpl
