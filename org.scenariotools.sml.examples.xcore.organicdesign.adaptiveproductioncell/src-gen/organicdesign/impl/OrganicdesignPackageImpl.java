/**
 */
package organicdesign.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import organicdesign.Agent;
import organicdesign.AgentSystem;
import organicdesign.Capability;
import organicdesign.Cart;
import organicdesign.Environment;
import organicdesign.EnvironmentLocation;
import organicdesign.Location;
import organicdesign.NamedElement;
import organicdesign.OrganicdesignFactory;
import organicdesign.OrganicdesignPackage;
import organicdesign.Recipe;
import organicdesign.Resource;
import organicdesign.Role;
import organicdesign.Task;
import organicdesign.TaskDependency;
import organicdesign.TransformationAgent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class OrganicdesignPackageImpl extends EPackageImpl implements OrganicdesignPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass agentSystemEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass taskEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass resourceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass recipeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass agentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass capabilityEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass cartEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass transformationAgentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass taskDependencyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass environmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass environmentLocationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass locationEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see organicdesign.OrganicdesignPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private OrganicdesignPackageImpl() {
		super(eNS_URI, OrganicdesignFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link OrganicdesignPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static OrganicdesignPackage init() {
		if (isInited) return (OrganicdesignPackage)EPackage.Registry.INSTANCE.getEPackage(OrganicdesignPackage.eNS_URI);

		// Obtain or create and register package
		OrganicdesignPackageImpl theOrganicdesignPackage = (OrganicdesignPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof OrganicdesignPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new OrganicdesignPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theOrganicdesignPackage.createPackageContents();

		// Initialize created meta-data
		theOrganicdesignPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theOrganicdesignPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(OrganicdesignPackage.eNS_URI, theOrganicdesignPackage);
		return theOrganicdesignPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAgentSystem() {
		return agentSystemEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAgentSystem_Resource() {
		return (EReference)agentSystemEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAgentSystem_Agents() {
		return (EReference)agentSystemEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAgentSystem_Tasks() {
		return (EReference)agentSystemEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAgentSystem_Capabilities() {
		return (EReference)agentSystemEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAgentSystem_Roles() {
		return (EReference)agentSystemEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAgentSystem_Recipes() {
		return (EReference)agentSystemEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAgentSystem_Environment() {
		return (EReference)agentSystemEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAgentSystem_Environmentlocation() {
		return (EReference)agentSystemEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTask() {
		return taskEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTask_RequiredCapability() {
		return (EReference)taskEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getResource() {
		return resourceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResource_Recipe() {
		return (EReference)resourceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResource_FulfilledTask() {
		return (EReference)resourceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getResource_Location() {
		return (EReference)resourceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getResource__CAN_performTask__Task() {
		return resourceEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getResource__PerformTask__Task() {
		return resourceEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getResource__Reset__Environment() {
		return resourceEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRecipe() {
		return recipeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRecipe_RequiredTasks() {
		return (EReference)recipeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRecipe_Taskdependency() {
		return (EReference)recipeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAgent() {
		return agentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAgent_AgentSystem() {
		return (EReference)agentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getAgent__ArrangePickUpFor__Resource() {
		return agentEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRole() {
		return roleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRole_ProvidedCapabilities() {
		return (EReference)roleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCapability() {
		return capabilityEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamedElement() {
		return namedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedElement_Name() {
		return (EAttribute)namedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCart() {
		return cartEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCart_Location() {
		return (EReference)cartEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCart_ConnectedLocations() {
		return (EReference)cartEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCart__PickUpResourceAtLocationRequest__Resource_Location() {
		return cartEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCart__ArrivesAtLocation__Location() {
		return cartEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCart__ResourceLoaded__Resource() {
		return cartEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCart__ResourceUnloaded__Resource() {
		return cartEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCart__AvailableForTransformationOfResource__Resource() {
		return cartEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCart__UnavailableForTransformationOfResource__Resource() {
		return cartEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCart__DestinationTransformationAgentFoundForResource__TransformationAgent_Resource() {
		return cartEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTransformationAgent() {
		return transformationAgentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransformationAgent_AllRoles() {
		return (EReference)transformationAgentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransformationAgent_CurrentRole() {
		return (EReference)transformationAgentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransformationAgent_AllCapabilities() {
		return (EReference)transformationAgentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransformationAgent_ActivatedCapabilities() {
		return (EReference)transformationAgentEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTransformationAgent_UnavailableCapabilities() {
		return (EReference)transformationAgentEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTransformationAgent__ResourceArrived__Resource() {
		return transformationAgentEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTransformationAgent__CAN_resourceArrived__Resource() {
		return transformationAgentEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTransformationAgent__TaskPerformed() {
		return transformationAgentEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTransformationAgent__AvailableForPickUpOfResourceAtLocation__Resource_Location() {
		return transformationAgentEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTransformationAgent__UnavailableForPickUpOfResourceAtLocation__Resource_Location() {
		return transformationAgentEClass.getEOperations().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTransformationAgent__PickUpArrangedForResource__Resource() {
		return transformationAgentEClass.getEOperations().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTransformationAgent__ReadyToLoadResource__Resource() {
		return transformationAgentEClass.getEOperations().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTransformationAgent__ReadyToUnLoadResource__Resource() {
		return transformationAgentEClass.getEOperations().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTransformationAgent__CAN_resourceLoadedOntoCart__Resource_Cart() {
		return transformationAgentEClass.getEOperations().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTransformationAgent__ResourceLoadedOntoCart__Resource_Cart() {
		return transformationAgentEClass.getEOperations().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTransformationAgent__CAN_resourceUnloadedFromCart__Resource_Cart() {
		return transformationAgentEClass.getEOperations().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTransformationAgent__ResourceUnloadedFromCart__Resource_Cart() {
		return transformationAgentEClass.getEOperations().get(11);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTransformationAgent__TransformResourceRequest__Resource() {
		return transformationAgentEClass.getEOperations().get(12);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getTransformationAgent__DestinationTransformationAgentFound() {
		return transformationAgentEClass.getEOperations().get(13);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTaskDependency() {
		return taskDependencyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTaskDependency_Task() {
		return (EReference)taskDependencyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTaskDependency_Precondition() {
		return (EReference)taskDependencyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEnvironment() {
		return environmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEnvironment_DepositLocation() {
		return (EReference)environmentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEnvironment_EnvironmentLocation() {
		return (EReference)environmentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getEnvironment__MoveToLocation__Location() {
		return environmentEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getEnvironment__LoadResourceOntoCart__Resource_Cart() {
		return environmentEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getEnvironment__UnloadResourceFromCart__Resource_Cart() {
		return environmentEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getEnvironment__ResetResource__Resource() {
		return environmentEClass.getEOperations().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEnvironmentLocation() {
		return environmentLocationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEnvironmentLocation_ConnectedAgent() {
		return (EReference)environmentLocationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLocation() {
		return locationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLocation_LocatedResources() {
		return (EReference)locationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLocation_ConnectedBy() {
		return (EReference)locationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrganicdesignFactory getOrganicdesignFactory() {
		return (OrganicdesignFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		agentSystemEClass = createEClass(AGENT_SYSTEM);
		createEReference(agentSystemEClass, AGENT_SYSTEM__RESOURCE);
		createEReference(agentSystemEClass, AGENT_SYSTEM__AGENTS);
		createEReference(agentSystemEClass, AGENT_SYSTEM__TASKS);
		createEReference(agentSystemEClass, AGENT_SYSTEM__CAPABILITIES);
		createEReference(agentSystemEClass, AGENT_SYSTEM__ROLES);
		createEReference(agentSystemEClass, AGENT_SYSTEM__RECIPES);
		createEReference(agentSystemEClass, AGENT_SYSTEM__ENVIRONMENT);
		createEReference(agentSystemEClass, AGENT_SYSTEM__ENVIRONMENTLOCATION);

		taskEClass = createEClass(TASK);
		createEReference(taskEClass, TASK__REQUIRED_CAPABILITY);

		resourceEClass = createEClass(RESOURCE);
		createEReference(resourceEClass, RESOURCE__RECIPE);
		createEReference(resourceEClass, RESOURCE__FULFILLED_TASK);
		createEReference(resourceEClass, RESOURCE__LOCATION);
		createEOperation(resourceEClass, RESOURCE___CAN_PERFORM_TASK__TASK);
		createEOperation(resourceEClass, RESOURCE___PERFORM_TASK__TASK);
		createEOperation(resourceEClass, RESOURCE___RESET__ENVIRONMENT);

		recipeEClass = createEClass(RECIPE);
		createEReference(recipeEClass, RECIPE__REQUIRED_TASKS);
		createEReference(recipeEClass, RECIPE__TASKDEPENDENCY);

		agentEClass = createEClass(AGENT);
		createEReference(agentEClass, AGENT__AGENT_SYSTEM);
		createEOperation(agentEClass, AGENT___ARRANGE_PICK_UP_FOR__RESOURCE);

		roleEClass = createEClass(ROLE);
		createEReference(roleEClass, ROLE__PROVIDED_CAPABILITIES);

		capabilityEClass = createEClass(CAPABILITY);

		namedElementEClass = createEClass(NAMED_ELEMENT);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__NAME);

		cartEClass = createEClass(CART);
		createEReference(cartEClass, CART__LOCATION);
		createEReference(cartEClass, CART__CONNECTED_LOCATIONS);
		createEOperation(cartEClass, CART___PICK_UP_RESOURCE_AT_LOCATION_REQUEST__RESOURCE_LOCATION);
		createEOperation(cartEClass, CART___ARRIVES_AT_LOCATION__LOCATION);
		createEOperation(cartEClass, CART___RESOURCE_LOADED__RESOURCE);
		createEOperation(cartEClass, CART___RESOURCE_UNLOADED__RESOURCE);
		createEOperation(cartEClass, CART___AVAILABLE_FOR_TRANSFORMATION_OF_RESOURCE__RESOURCE);
		createEOperation(cartEClass, CART___UNAVAILABLE_FOR_TRANSFORMATION_OF_RESOURCE__RESOURCE);
		createEOperation(cartEClass, CART___DESTINATION_TRANSFORMATION_AGENT_FOUND_FOR_RESOURCE__TRANSFORMATIONAGENT_RESOURCE);

		transformationAgentEClass = createEClass(TRANSFORMATION_AGENT);
		createEReference(transformationAgentEClass, TRANSFORMATION_AGENT__ALL_ROLES);
		createEReference(transformationAgentEClass, TRANSFORMATION_AGENT__CURRENT_ROLE);
		createEReference(transformationAgentEClass, TRANSFORMATION_AGENT__ALL_CAPABILITIES);
		createEReference(transformationAgentEClass, TRANSFORMATION_AGENT__ACTIVATED_CAPABILITIES);
		createEReference(transformationAgentEClass, TRANSFORMATION_AGENT__UNAVAILABLE_CAPABILITIES);
		createEOperation(transformationAgentEClass, TRANSFORMATION_AGENT___RESOURCE_ARRIVED__RESOURCE);
		createEOperation(transformationAgentEClass, TRANSFORMATION_AGENT___CAN_RESOURCE_ARRIVED__RESOURCE);
		createEOperation(transformationAgentEClass, TRANSFORMATION_AGENT___TASK_PERFORMED);
		createEOperation(transformationAgentEClass, TRANSFORMATION_AGENT___AVAILABLE_FOR_PICK_UP_OF_RESOURCE_AT_LOCATION__RESOURCE_LOCATION);
		createEOperation(transformationAgentEClass, TRANSFORMATION_AGENT___UNAVAILABLE_FOR_PICK_UP_OF_RESOURCE_AT_LOCATION__RESOURCE_LOCATION);
		createEOperation(transformationAgentEClass, TRANSFORMATION_AGENT___PICK_UP_ARRANGED_FOR_RESOURCE__RESOURCE);
		createEOperation(transformationAgentEClass, TRANSFORMATION_AGENT___READY_TO_LOAD_RESOURCE__RESOURCE);
		createEOperation(transformationAgentEClass, TRANSFORMATION_AGENT___READY_TO_UN_LOAD_RESOURCE__RESOURCE);
		createEOperation(transformationAgentEClass, TRANSFORMATION_AGENT___CAN_RESOURCE_LOADED_ONTO_CART__RESOURCE_CART);
		createEOperation(transformationAgentEClass, TRANSFORMATION_AGENT___RESOURCE_LOADED_ONTO_CART__RESOURCE_CART);
		createEOperation(transformationAgentEClass, TRANSFORMATION_AGENT___CAN_RESOURCE_UNLOADED_FROM_CART__RESOURCE_CART);
		createEOperation(transformationAgentEClass, TRANSFORMATION_AGENT___RESOURCE_UNLOADED_FROM_CART__RESOURCE_CART);
		createEOperation(transformationAgentEClass, TRANSFORMATION_AGENT___TRANSFORM_RESOURCE_REQUEST__RESOURCE);
		createEOperation(transformationAgentEClass, TRANSFORMATION_AGENT___DESTINATION_TRANSFORMATION_AGENT_FOUND);

		taskDependencyEClass = createEClass(TASK_DEPENDENCY);
		createEReference(taskDependencyEClass, TASK_DEPENDENCY__TASK);
		createEReference(taskDependencyEClass, TASK_DEPENDENCY__PRECONDITION);

		environmentEClass = createEClass(ENVIRONMENT);
		createEReference(environmentEClass, ENVIRONMENT__DEPOSIT_LOCATION);
		createEReference(environmentEClass, ENVIRONMENT__ENVIRONMENT_LOCATION);
		createEOperation(environmentEClass, ENVIRONMENT___MOVE_TO_LOCATION__LOCATION);
		createEOperation(environmentEClass, ENVIRONMENT___LOAD_RESOURCE_ONTO_CART__RESOURCE_CART);
		createEOperation(environmentEClass, ENVIRONMENT___UNLOAD_RESOURCE_FROM_CART__RESOURCE_CART);
		createEOperation(environmentEClass, ENVIRONMENT___RESET_RESOURCE__RESOURCE);

		environmentLocationEClass = createEClass(ENVIRONMENT_LOCATION);
		createEReference(environmentLocationEClass, ENVIRONMENT_LOCATION__CONNECTED_AGENT);

		locationEClass = createEClass(LOCATION);
		createEReference(locationEClass, LOCATION__LOCATED_RESOURCES);
		createEReference(locationEClass, LOCATION__CONNECTED_BY);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		agentSystemEClass.getESuperTypes().add(this.getNamedElement());
		taskEClass.getESuperTypes().add(this.getNamedElement());
		resourceEClass.getESuperTypes().add(this.getNamedElement());
		recipeEClass.getESuperTypes().add(this.getNamedElement());
		agentEClass.getESuperTypes().add(this.getLocation());
		roleEClass.getESuperTypes().add(this.getNamedElement());
		capabilityEClass.getESuperTypes().add(this.getNamedElement());
		cartEClass.getESuperTypes().add(this.getAgent());
		transformationAgentEClass.getESuperTypes().add(this.getAgent());
		taskDependencyEClass.getESuperTypes().add(this.getNamedElement());
		environmentEClass.getESuperTypes().add(this.getNamedElement());
		environmentLocationEClass.getESuperTypes().add(this.getLocation());
		locationEClass.getESuperTypes().add(this.getNamedElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(agentSystemEClass, AgentSystem.class, "AgentSystem", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAgentSystem_Resource(), this.getResource(), null, "resource", null, 0, -1, AgentSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAgentSystem_Agents(), this.getAgent(), this.getAgent_AgentSystem(), "agents", null, 0, -1, AgentSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAgentSystem_Tasks(), this.getTask(), null, "tasks", null, 0, -1, AgentSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAgentSystem_Capabilities(), this.getCapability(), null, "capabilities", null, 0, -1, AgentSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAgentSystem_Roles(), this.getRole(), null, "roles", null, 0, -1, AgentSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAgentSystem_Recipes(), this.getRecipe(), null, "recipes", null, 0, -1, AgentSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getAgentSystem_Environment(), this.getEnvironment(), null, "environment", null, 0, 1, AgentSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAgentSystem_Environmentlocation(), this.getEnvironmentLocation(), null, "environmentlocation", null, 0, -1, AgentSystem.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(taskEClass, Task.class, "Task", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTask_RequiredCapability(), this.getCapability(), null, "requiredCapability", null, 0, 1, Task.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(resourceEClass, Resource.class, "Resource", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getResource_Recipe(), this.getRecipe(), null, "recipe", null, 0, 1, Resource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getResource_FulfilledTask(), this.getTask(), null, "fulfilledTask", null, 0, -1, Resource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getResource_Location(), this.getLocation(), this.getLocation_LocatedResources(), "location", null, 0, 1, Resource.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getResource__CAN_performTask__Task(), theEcorePackage.getEBoolean(), "CAN_performTask", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTask(), "task", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getResource__PerformTask__Task(), null, "performTask", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTask(), "task", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getResource__Reset__Environment(), null, "reset", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getEnvironment(), "env", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(recipeEClass, Recipe.class, "Recipe", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRecipe_RequiredTasks(), this.getTask(), null, "requiredTasks", null, 0, -1, Recipe.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRecipe_Taskdependency(), this.getTaskDependency(), null, "taskdependency", null, 0, -1, Recipe.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(agentEClass, Agent.class, "Agent", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAgent_AgentSystem(), this.getAgentSystem(), this.getAgentSystem_Agents(), "agentSystem", null, 0, 1, Agent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getAgent__ArrangePickUpFor__Resource(), null, "arrangePickUpFor", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getResource(), "resourceParameter", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(roleEClass, Role.class, "Role", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRole_ProvidedCapabilities(), this.getCapability(), null, "providedCapabilities", null, 0, -1, Role.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(capabilityEClass, Capability.class, "Capability", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(namedElementEClass, NamedElement.class, "NamedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamedElement_Name(), theEcorePackage.getEString(), "name", null, 0, 1, NamedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(cartEClass, Cart.class, "Cart", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCart_Location(), this.getLocation(), null, "location", null, 0, 1, Cart.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCart_ConnectedLocations(), this.getLocation(), this.getLocation_ConnectedBy(), "connectedLocations", null, 0, -1, Cart.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		op = initEOperation(getCart__PickUpResourceAtLocationRequest__Resource_Location(), null, "pickUpResourceAtLocationRequest", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getResource(), "resource", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getLocation(), "location", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getCart__ArrivesAtLocation__Location(), null, "arrivesAtLocation", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getLocation(), "location", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getCart__ResourceLoaded__Resource(), null, "resourceLoaded", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getResource(), "resource", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getCart__ResourceUnloaded__Resource(), null, "resourceUnloaded", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getResource(), "resource", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getCart__AvailableForTransformationOfResource__Resource(), null, "availableForTransformationOfResource", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getResource(), "resource", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getCart__UnavailableForTransformationOfResource__Resource(), null, "unavailableForTransformationOfResource", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getResource(), "resource", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getCart__DestinationTransformationAgentFoundForResource__TransformationAgent_Resource(), null, "destinationTransformationAgentFoundForResource", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getTransformationAgent(), "transformationAgent", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getResource(), "resource", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(transformationAgentEClass, TransformationAgent.class, "TransformationAgent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTransformationAgent_AllRoles(), this.getRole(), null, "allRoles", null, 0, -1, TransformationAgent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTransformationAgent_CurrentRole(), this.getRole(), null, "currentRole", null, 0, 1, TransformationAgent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTransformationAgent_AllCapabilities(), this.getCapability(), null, "allCapabilities", null, 0, -1, TransformationAgent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTransformationAgent_ActivatedCapabilities(), this.getCapability(), null, "activatedCapabilities", null, 0, -1, TransformationAgent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getTransformationAgent_UnavailableCapabilities(), this.getCapability(), null, "unavailableCapabilities", null, 0, -1, TransformationAgent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		op = initEOperation(getTransformationAgent__ResourceArrived__Resource(), null, "resourceArrived", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getResource(), "resource", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getTransformationAgent__CAN_resourceArrived__Resource(), theEcorePackage.getEBoolean(), "CAN_resourceArrived", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getResource(), "resource", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getTransformationAgent__TaskPerformed(), null, "taskPerformed", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getTransformationAgent__AvailableForPickUpOfResourceAtLocation__Resource_Location(), null, "availableForPickUpOfResourceAtLocation", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getResource(), "resource", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getLocation(), "location", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getTransformationAgent__UnavailableForPickUpOfResourceAtLocation__Resource_Location(), null, "unavailableForPickUpOfResourceAtLocation", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getResource(), "resource", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getLocation(), "location", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getTransformationAgent__PickUpArrangedForResource__Resource(), null, "pickUpArrangedForResource", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getResource(), "resource", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getTransformationAgent__ReadyToLoadResource__Resource(), null, "readyToLoadResource", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getResource(), "resource", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getTransformationAgent__ReadyToUnLoadResource__Resource(), null, "readyToUnLoadResource", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getResource(), "resource", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getTransformationAgent__CAN_resourceLoadedOntoCart__Resource_Cart(), theEcorePackage.getEBoolean(), "CAN_resourceLoadedOntoCart", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getResource(), "resource", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCart(), "cart", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getTransformationAgent__ResourceLoadedOntoCart__Resource_Cart(), null, "resourceLoadedOntoCart", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getResource(), "resource", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCart(), "cart", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getTransformationAgent__CAN_resourceUnloadedFromCart__Resource_Cart(), theEcorePackage.getEBoolean(), "CAN_resourceUnloadedFromCart", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getResource(), "resource", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCart(), "cart", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getTransformationAgent__ResourceUnloadedFromCart__Resource_Cart(), null, "resourceUnloadedFromCart", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getResource(), "resource", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCart(), "cart", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getTransformationAgent__TransformResourceRequest__Resource(), null, "transformResourceRequest", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getResource(), "resource", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getTransformationAgent__DestinationTransformationAgentFound(), null, "destinationTransformationAgentFound", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(taskDependencyEClass, TaskDependency.class, "TaskDependency", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTaskDependency_Task(), this.getTask(), null, "task", null, 0, 1, TaskDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTaskDependency_Precondition(), this.getTask(), null, "precondition", null, 0, 1, TaskDependency.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(environmentEClass, Environment.class, "Environment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEnvironment_DepositLocation(), this.getLocation(), null, "depositLocation", null, 0, 1, Environment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEnvironment_EnvironmentLocation(), this.getEnvironmentLocation(), null, "environmentLocation", null, 0, 1, Environment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getEnvironment__MoveToLocation__Location(), null, "moveToLocation", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getLocation(), "location", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getEnvironment__LoadResourceOntoCart__Resource_Cart(), null, "loadResourceOntoCart", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getResource(), "resource", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCart(), "cart", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getEnvironment__UnloadResourceFromCart__Resource_Cart(), null, "unloadResourceFromCart", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getResource(), "resource", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getCart(), "cart", 0, 1, !IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getEnvironment__ResetResource__Resource(), null, "resetResource", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getResource(), "resource", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(environmentLocationEClass, EnvironmentLocation.class, "EnvironmentLocation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEnvironmentLocation_ConnectedAgent(), this.getTransformationAgent(), null, "connectedAgent", null, 0, -1, EnvironmentLocation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(locationEClass, Location.class, "Location", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLocation_LocatedResources(), this.getResource(), this.getResource_Location(), "locatedResources", null, 0, -1, Location.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getLocation_ConnectedBy(), this.getCart(), this.getCart_ConnectedLocations(), "connectedBy", null, 0, -1, Location.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //OrganicdesignPackageImpl
