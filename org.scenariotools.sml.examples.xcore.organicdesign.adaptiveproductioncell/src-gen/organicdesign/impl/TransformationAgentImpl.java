/**
 */
package organicdesign.impl;

import com.google.common.base.Objects;

import java.lang.reflect.InvocationTargetException;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import organicdesign.AgentSystem;
import organicdesign.Capability;
import organicdesign.Cart;
import organicdesign.Environment;
import organicdesign.EnvironmentLocation;
import organicdesign.Location;
import organicdesign.OrganicdesignPackage;
import organicdesign.Resource;
import organicdesign.Role;
import organicdesign.TransformationAgent;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Transformation Agent</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link organicdesign.impl.TransformationAgentImpl#getAllRoles <em>All Roles</em>}</li>
 *   <li>{@link organicdesign.impl.TransformationAgentImpl#getCurrentRole <em>Current Role</em>}</li>
 *   <li>{@link organicdesign.impl.TransformationAgentImpl#getAllCapabilities <em>All Capabilities</em>}</li>
 *   <li>{@link organicdesign.impl.TransformationAgentImpl#getActivatedCapabilities <em>Activated Capabilities</em>}</li>
 *   <li>{@link organicdesign.impl.TransformationAgentImpl#getUnavailableCapabilities <em>Unavailable Capabilities</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TransformationAgentImpl extends AgentImpl implements TransformationAgent {
	/**
	 * The cached value of the '{@link #getAllRoles() <em>All Roles</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllRoles()
	 * @generated
	 * @ordered
	 */
	protected EList<Role> allRoles;

	/**
	 * The cached value of the '{@link #getCurrentRole() <em>Current Role</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurrentRole()
	 * @generated
	 * @ordered
	 */
	protected Role currentRole;

	/**
	 * The cached value of the '{@link #getAllCapabilities() <em>All Capabilities</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAllCapabilities()
	 * @generated
	 * @ordered
	 */
	protected EList<Capability> allCapabilities;

	/**
	 * The cached value of the '{@link #getActivatedCapabilities() <em>Activated Capabilities</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getActivatedCapabilities()
	 * @generated
	 * @ordered
	 */
	protected EList<Capability> activatedCapabilities;

	/**
	 * The cached value of the '{@link #getUnavailableCapabilities() <em>Unavailable Capabilities</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getUnavailableCapabilities()
	 * @generated
	 * @ordered
	 */
	protected EList<Capability> unavailableCapabilities;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TransformationAgentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OrganicdesignPackage.Literals.TRANSFORMATION_AGENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Role> getAllRoles() {
		if (allRoles == null) {
			allRoles = new EObjectResolvingEList<Role>(Role.class, this, OrganicdesignPackage.TRANSFORMATION_AGENT__ALL_ROLES);
		}
		return allRoles;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role getCurrentRole() {
		if (currentRole != null && currentRole.eIsProxy()) {
			InternalEObject oldCurrentRole = (InternalEObject)currentRole;
			currentRole = (Role)eResolveProxy(oldCurrentRole);
			if (currentRole != oldCurrentRole) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, OrganicdesignPackage.TRANSFORMATION_AGENT__CURRENT_ROLE, oldCurrentRole, currentRole));
			}
		}
		return currentRole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Role basicGetCurrentRole() {
		return currentRole;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentRole(Role newCurrentRole) {
		Role oldCurrentRole = currentRole;
		currentRole = newCurrentRole;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OrganicdesignPackage.TRANSFORMATION_AGENT__CURRENT_ROLE, oldCurrentRole, currentRole));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Capability> getAllCapabilities() {
		if (allCapabilities == null) {
			allCapabilities = new EObjectResolvingEList<Capability>(Capability.class, this, OrganicdesignPackage.TRANSFORMATION_AGENT__ALL_CAPABILITIES);
		}
		return allCapabilities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Capability> getActivatedCapabilities() {
		if (activatedCapabilities == null) {
			activatedCapabilities = new EObjectResolvingEList<Capability>(Capability.class, this, OrganicdesignPackage.TRANSFORMATION_AGENT__ACTIVATED_CAPABILITIES);
		}
		return activatedCapabilities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Capability> getUnavailableCapabilities() {
		if (unavailableCapabilities == null) {
			unavailableCapabilities = new EObjectResolvingEList<Capability>(Capability.class, this, OrganicdesignPackage.TRANSFORMATION_AGENT__UNAVAILABLE_CAPABILITIES);
		}
		return unavailableCapabilities;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void resourceArrived(final Resource resource) {
		AgentSystem _agentSystem = this.getAgentSystem();
		Environment _environment = _agentSystem.getEnvironment();
		EnvironmentLocation _environmentLocation = _environment.getEnvironmentLocation();
		EList<Resource> _locatedResources = _environmentLocation.getLocatedResources();
		_locatedResources.remove(resource);
		EList<Resource> _locatedResources_1 = this.getLocatedResources();
		_locatedResources_1.add(resource);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean CAN_resourceArrived(final Resource resource) {
		return ((this.getAgentSystem().getEnvironment().getEnvironmentLocation().getLocatedResources().contains(resource) && this.getLocatedResources().isEmpty()) && this.getAgentSystem().getEnvironment().getEnvironmentLocation().getConnectedAgent().contains(this));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void taskPerformed() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void availableForPickUpOfResourceAtLocation(Resource resource, Location location) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void unavailableForPickUpOfResourceAtLocation(Resource resource, Location location) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void pickUpArrangedForResource(Resource resource) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void readyToLoadResource(Resource resource) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void readyToUnLoadResource(Resource resource) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean CAN_resourceLoadedOntoCart(final Resource resource, final Cart cart) {
		return (Objects.equal(cart.getLocation(), this) && this.getLocatedResources().contains(resource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void resourceLoadedOntoCart(final Resource resource, final Cart cart) {
		EList<Resource> _locatedResources = this.getLocatedResources();
		_locatedResources.remove(resource);
		EList<Resource> _locatedResources_1 = cart.getLocatedResources();
		_locatedResources_1.add(resource);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean CAN_resourceUnloadedFromCart(final Resource resource, final Cart cart) {
		return (Objects.equal(cart.getLocation(), this) && cart.getLocatedResources().contains(resource));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void resourceUnloadedFromCart(final Resource resource, final Cart cart) {
		EList<Resource> _locatedResources = cart.getLocatedResources();
		_locatedResources.remove(resource);
		EList<Resource> _locatedResources_1 = this.getLocatedResources();
		_locatedResources_1.add(resource);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void transformResourceRequest(Resource resource) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void destinationTransformationAgentFound() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OrganicdesignPackage.TRANSFORMATION_AGENT__ALL_ROLES:
				return getAllRoles();
			case OrganicdesignPackage.TRANSFORMATION_AGENT__CURRENT_ROLE:
				if (resolve) return getCurrentRole();
				return basicGetCurrentRole();
			case OrganicdesignPackage.TRANSFORMATION_AGENT__ALL_CAPABILITIES:
				return getAllCapabilities();
			case OrganicdesignPackage.TRANSFORMATION_AGENT__ACTIVATED_CAPABILITIES:
				return getActivatedCapabilities();
			case OrganicdesignPackage.TRANSFORMATION_AGENT__UNAVAILABLE_CAPABILITIES:
				return getUnavailableCapabilities();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OrganicdesignPackage.TRANSFORMATION_AGENT__ALL_ROLES:
				getAllRoles().clear();
				getAllRoles().addAll((Collection<? extends Role>)newValue);
				return;
			case OrganicdesignPackage.TRANSFORMATION_AGENT__CURRENT_ROLE:
				setCurrentRole((Role)newValue);
				return;
			case OrganicdesignPackage.TRANSFORMATION_AGENT__ALL_CAPABILITIES:
				getAllCapabilities().clear();
				getAllCapabilities().addAll((Collection<? extends Capability>)newValue);
				return;
			case OrganicdesignPackage.TRANSFORMATION_AGENT__ACTIVATED_CAPABILITIES:
				getActivatedCapabilities().clear();
				getActivatedCapabilities().addAll((Collection<? extends Capability>)newValue);
				return;
			case OrganicdesignPackage.TRANSFORMATION_AGENT__UNAVAILABLE_CAPABILITIES:
				getUnavailableCapabilities().clear();
				getUnavailableCapabilities().addAll((Collection<? extends Capability>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OrganicdesignPackage.TRANSFORMATION_AGENT__ALL_ROLES:
				getAllRoles().clear();
				return;
			case OrganicdesignPackage.TRANSFORMATION_AGENT__CURRENT_ROLE:
				setCurrentRole((Role)null);
				return;
			case OrganicdesignPackage.TRANSFORMATION_AGENT__ALL_CAPABILITIES:
				getAllCapabilities().clear();
				return;
			case OrganicdesignPackage.TRANSFORMATION_AGENT__ACTIVATED_CAPABILITIES:
				getActivatedCapabilities().clear();
				return;
			case OrganicdesignPackage.TRANSFORMATION_AGENT__UNAVAILABLE_CAPABILITIES:
				getUnavailableCapabilities().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OrganicdesignPackage.TRANSFORMATION_AGENT__ALL_ROLES:
				return allRoles != null && !allRoles.isEmpty();
			case OrganicdesignPackage.TRANSFORMATION_AGENT__CURRENT_ROLE:
				return currentRole != null;
			case OrganicdesignPackage.TRANSFORMATION_AGENT__ALL_CAPABILITIES:
				return allCapabilities != null && !allCapabilities.isEmpty();
			case OrganicdesignPackage.TRANSFORMATION_AGENT__ACTIVATED_CAPABILITIES:
				return activatedCapabilities != null && !activatedCapabilities.isEmpty();
			case OrganicdesignPackage.TRANSFORMATION_AGENT__UNAVAILABLE_CAPABILITIES:
				return unavailableCapabilities != null && !unavailableCapabilities.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case OrganicdesignPackage.TRANSFORMATION_AGENT___RESOURCE_ARRIVED__RESOURCE:
				resourceArrived((Resource)arguments.get(0));
				return null;
			case OrganicdesignPackage.TRANSFORMATION_AGENT___CAN_RESOURCE_ARRIVED__RESOURCE:
				return CAN_resourceArrived((Resource)arguments.get(0));
			case OrganicdesignPackage.TRANSFORMATION_AGENT___TASK_PERFORMED:
				taskPerformed();
				return null;
			case OrganicdesignPackage.TRANSFORMATION_AGENT___AVAILABLE_FOR_PICK_UP_OF_RESOURCE_AT_LOCATION__RESOURCE_LOCATION:
				availableForPickUpOfResourceAtLocation((Resource)arguments.get(0), (Location)arguments.get(1));
				return null;
			case OrganicdesignPackage.TRANSFORMATION_AGENT___UNAVAILABLE_FOR_PICK_UP_OF_RESOURCE_AT_LOCATION__RESOURCE_LOCATION:
				unavailableForPickUpOfResourceAtLocation((Resource)arguments.get(0), (Location)arguments.get(1));
				return null;
			case OrganicdesignPackage.TRANSFORMATION_AGENT___PICK_UP_ARRANGED_FOR_RESOURCE__RESOURCE:
				pickUpArrangedForResource((Resource)arguments.get(0));
				return null;
			case OrganicdesignPackage.TRANSFORMATION_AGENT___READY_TO_LOAD_RESOURCE__RESOURCE:
				readyToLoadResource((Resource)arguments.get(0));
				return null;
			case OrganicdesignPackage.TRANSFORMATION_AGENT___READY_TO_UN_LOAD_RESOURCE__RESOURCE:
				readyToUnLoadResource((Resource)arguments.get(0));
				return null;
			case OrganicdesignPackage.TRANSFORMATION_AGENT___CAN_RESOURCE_LOADED_ONTO_CART__RESOURCE_CART:
				return CAN_resourceLoadedOntoCart((Resource)arguments.get(0), (Cart)arguments.get(1));
			case OrganicdesignPackage.TRANSFORMATION_AGENT___RESOURCE_LOADED_ONTO_CART__RESOURCE_CART:
				resourceLoadedOntoCart((Resource)arguments.get(0), (Cart)arguments.get(1));
				return null;
			case OrganicdesignPackage.TRANSFORMATION_AGENT___CAN_RESOURCE_UNLOADED_FROM_CART__RESOURCE_CART:
				return CAN_resourceUnloadedFromCart((Resource)arguments.get(0), (Cart)arguments.get(1));
			case OrganicdesignPackage.TRANSFORMATION_AGENT___RESOURCE_UNLOADED_FROM_CART__RESOURCE_CART:
				resourceUnloadedFromCart((Resource)arguments.get(0), (Cart)arguments.get(1));
				return null;
			case OrganicdesignPackage.TRANSFORMATION_AGENT___TRANSFORM_RESOURCE_REQUEST__RESOURCE:
				transformResourceRequest((Resource)arguments.get(0));
				return null;
			case OrganicdesignPackage.TRANSFORMATION_AGENT___DESTINATION_TRANSFORMATION_AGENT_FOUND:
				destinationTransformationAgentFound();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //TransformationAgentImpl
