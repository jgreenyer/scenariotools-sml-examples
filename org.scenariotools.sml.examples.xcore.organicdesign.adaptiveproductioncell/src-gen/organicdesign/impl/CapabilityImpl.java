/**
 */
package organicdesign.impl;

import org.eclipse.emf.ecore.EClass;

import organicdesign.Capability;
import organicdesign.OrganicdesignPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Capability</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CapabilityImpl extends NamedElementImpl implements Capability {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CapabilityImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OrganicdesignPackage.Literals.CAPABILITY;
	}

} //CapabilityImpl
