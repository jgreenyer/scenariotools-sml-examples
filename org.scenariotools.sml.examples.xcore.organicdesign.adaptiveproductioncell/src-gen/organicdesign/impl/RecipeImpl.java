/**
 */
package organicdesign.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import organicdesign.OrganicdesignPackage;
import organicdesign.Recipe;
import organicdesign.Task;
import organicdesign.TaskDependency;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Recipe</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link organicdesign.impl.RecipeImpl#getRequiredTasks <em>Required Tasks</em>}</li>
 *   <li>{@link organicdesign.impl.RecipeImpl#getTaskdependency <em>Taskdependency</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RecipeImpl extends NamedElementImpl implements Recipe {
	/**
	 * The cached value of the '{@link #getRequiredTasks() <em>Required Tasks</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRequiredTasks()
	 * @generated
	 * @ordered
	 */
	protected EList<Task> requiredTasks;

	/**
	 * The cached value of the '{@link #getTaskdependency() <em>Taskdependency</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTaskdependency()
	 * @generated
	 * @ordered
	 */
	protected EList<TaskDependency> taskdependency;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RecipeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OrganicdesignPackage.Literals.RECIPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Task> getRequiredTasks() {
		if (requiredTasks == null) {
			requiredTasks = new EObjectResolvingEList<Task>(Task.class, this, OrganicdesignPackage.RECIPE__REQUIRED_TASKS);
		}
		return requiredTasks;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<TaskDependency> getTaskdependency() {
		if (taskdependency == null) {
			taskdependency = new EObjectContainmentEList<TaskDependency>(TaskDependency.class, this, OrganicdesignPackage.RECIPE__TASKDEPENDENCY);
		}
		return taskdependency;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OrganicdesignPackage.RECIPE__TASKDEPENDENCY:
				return ((InternalEList<?>)getTaskdependency()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OrganicdesignPackage.RECIPE__REQUIRED_TASKS:
				return getRequiredTasks();
			case OrganicdesignPackage.RECIPE__TASKDEPENDENCY:
				return getTaskdependency();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OrganicdesignPackage.RECIPE__REQUIRED_TASKS:
				getRequiredTasks().clear();
				getRequiredTasks().addAll((Collection<? extends Task>)newValue);
				return;
			case OrganicdesignPackage.RECIPE__TASKDEPENDENCY:
				getTaskdependency().clear();
				getTaskdependency().addAll((Collection<? extends TaskDependency>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OrganicdesignPackage.RECIPE__REQUIRED_TASKS:
				getRequiredTasks().clear();
				return;
			case OrganicdesignPackage.RECIPE__TASKDEPENDENCY:
				getTaskdependency().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OrganicdesignPackage.RECIPE__REQUIRED_TASKS:
				return requiredTasks != null && !requiredTasks.isEmpty();
			case OrganicdesignPackage.RECIPE__TASKDEPENDENCY:
				return taskdependency != null && !taskdependency.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //RecipeImpl
