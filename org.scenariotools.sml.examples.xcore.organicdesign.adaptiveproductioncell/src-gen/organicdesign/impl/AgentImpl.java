/**
 */
package organicdesign.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

import organicdesign.Agent;
import organicdesign.AgentSystem;
import organicdesign.OrganicdesignPackage;
import organicdesign.Resource;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Agent</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link organicdesign.impl.AgentImpl#getAgentSystem <em>Agent System</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AgentImpl extends LocationImpl implements Agent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AgentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OrganicdesignPackage.Literals.AGENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AgentSystem getAgentSystem() {
		if (eContainerFeatureID() != OrganicdesignPackage.AGENT__AGENT_SYSTEM) return null;
		return (AgentSystem)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AgentSystem basicGetAgentSystem() {
		if (eContainerFeatureID() != OrganicdesignPackage.AGENT__AGENT_SYSTEM) return null;
		return (AgentSystem)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAgentSystem(AgentSystem newAgentSystem, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newAgentSystem, OrganicdesignPackage.AGENT__AGENT_SYSTEM, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAgentSystem(AgentSystem newAgentSystem) {
		if (newAgentSystem != eInternalContainer() || (eContainerFeatureID() != OrganicdesignPackage.AGENT__AGENT_SYSTEM && newAgentSystem != null)) {
			if (EcoreUtil.isAncestor(this, newAgentSystem))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newAgentSystem != null)
				msgs = ((InternalEObject)newAgentSystem).eInverseAdd(this, OrganicdesignPackage.AGENT_SYSTEM__AGENTS, AgentSystem.class, msgs);
			msgs = basicSetAgentSystem(newAgentSystem, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, OrganicdesignPackage.AGENT__AGENT_SYSTEM, newAgentSystem, newAgentSystem));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void arrangePickUpFor(Resource resourceParameter) {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OrganicdesignPackage.AGENT__AGENT_SYSTEM:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetAgentSystem((AgentSystem)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case OrganicdesignPackage.AGENT__AGENT_SYSTEM:
				return basicSetAgentSystem(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case OrganicdesignPackage.AGENT__AGENT_SYSTEM:
				return eInternalContainer().eInverseRemove(this, OrganicdesignPackage.AGENT_SYSTEM__AGENTS, AgentSystem.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case OrganicdesignPackage.AGENT__AGENT_SYSTEM:
				if (resolve) return getAgentSystem();
				return basicGetAgentSystem();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case OrganicdesignPackage.AGENT__AGENT_SYSTEM:
				setAgentSystem((AgentSystem)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case OrganicdesignPackage.AGENT__AGENT_SYSTEM:
				setAgentSystem((AgentSystem)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case OrganicdesignPackage.AGENT__AGENT_SYSTEM:
				return basicGetAgentSystem() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case OrganicdesignPackage.AGENT___ARRANGE_PICK_UP_FOR__RESOURCE:
				arrangePickUpFor((Resource)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //AgentImpl
