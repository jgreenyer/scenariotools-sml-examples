/**
 */
package organicdesign;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cart</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link organicdesign.Cart#getLocation <em>Location</em>}</li>
 *   <li>{@link organicdesign.Cart#getConnectedLocations <em>Connected Locations</em>}</li>
 * </ul>
 *
 * @see organicdesign.OrganicdesignPackage#getCart()
 * @model
 * @generated
 */
public interface Cart extends Agent {
	/**
	 * Returns the value of the '<em><b>Location</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Location</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location</em>' reference.
	 * @see #setLocation(Location)
	 * @see organicdesign.OrganicdesignPackage#getCart_Location()
	 * @model
	 * @generated
	 */
	Location getLocation();

	/**
	 * Sets the value of the '{@link organicdesign.Cart#getLocation <em>Location</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Location</em>' reference.
	 * @see #getLocation()
	 * @generated
	 */
	void setLocation(Location value);

	/**
	 * Returns the value of the '<em><b>Connected Locations</b></em>' reference list.
	 * The list contents are of type {@link organicdesign.Location}.
	 * It is bidirectional and its opposite is '{@link organicdesign.Location#getConnectedBy <em>Connected By</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connected Locations</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connected Locations</em>' reference list.
	 * @see organicdesign.OrganicdesignPackage#getCart_ConnectedLocations()
	 * @see organicdesign.Location#getConnectedBy
	 * @model opposite="connectedBy" ordered="false"
	 * @generated
	 */
	EList<Location> getConnectedLocations();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model resourceUnique="false" locationUnique="false"
	 * @generated
	 */
	void pickUpResourceAtLocationRequest(Resource resource, Location location);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model locationUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.setLocation(location);'"
	 * @generated
	 */
	void arrivesAtLocation(Location location);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model resourceUnique="false"
	 * @generated
	 */
	void resourceLoaded(Resource resource);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model resourceUnique="false"
	 * @generated
	 */
	void resourceUnloaded(Resource resource);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model resourceUnique="false"
	 * @generated
	 */
	void availableForTransformationOfResource(Resource resource);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model resourceUnique="false"
	 * @generated
	 */
	void unavailableForTransformationOfResource(Resource resource);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model transformationAgentUnique="false" resourceUnique="false"
	 * @generated
	 */
	void destinationTransformationAgentFoundForResource(TransformationAgent transformationAgent, Resource resource);

} // Cart
