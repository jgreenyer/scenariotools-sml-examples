/**
 */
package organicdesign;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Agent</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link organicdesign.Agent#getAgentSystem <em>Agent System</em>}</li>
 * </ul>
 *
 * @see organicdesign.OrganicdesignPackage#getAgent()
 * @model abstract="true"
 * @generated
 */
public interface Agent extends Location {
	/**
	 * Returns the value of the '<em><b>Agent System</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link organicdesign.AgentSystem#getAgents <em>Agents</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Agent System</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Agent System</em>' container reference.
	 * @see #setAgentSystem(AgentSystem)
	 * @see organicdesign.OrganicdesignPackage#getAgent_AgentSystem()
	 * @see organicdesign.AgentSystem#getAgents
	 * @model opposite="agents" transient="false"
	 * @generated
	 */
	AgentSystem getAgentSystem();

	/**
	 * Sets the value of the '{@link organicdesign.Agent#getAgentSystem <em>Agent System</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Agent System</em>' container reference.
	 * @see #getAgentSystem()
	 * @generated
	 */
	void setAgentSystem(AgentSystem value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model resourceParameterUnique="false"
	 * @generated
	 */
	void arrangePickUpFor(Resource resourceParameter);

} // Agent
