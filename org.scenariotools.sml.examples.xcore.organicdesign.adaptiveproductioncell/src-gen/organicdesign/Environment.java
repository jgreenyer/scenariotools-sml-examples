/**
 */
package organicdesign;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Environment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link organicdesign.Environment#getDepositLocation <em>Deposit Location</em>}</li>
 *   <li>{@link organicdesign.Environment#getEnvironmentLocation <em>Environment Location</em>}</li>
 * </ul>
 *
 * @see organicdesign.OrganicdesignPackage#getEnvironment()
 * @model
 * @generated
 */
public interface Environment extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Deposit Location</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deposit Location</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deposit Location</em>' reference.
	 * @see #setDepositLocation(Location)
	 * @see organicdesign.OrganicdesignPackage#getEnvironment_DepositLocation()
	 * @model
	 * @generated
	 */
	Location getDepositLocation();

	/**
	 * Sets the value of the '{@link organicdesign.Environment#getDepositLocation <em>Deposit Location</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deposit Location</em>' reference.
	 * @see #getDepositLocation()
	 * @generated
	 */
	void setDepositLocation(Location value);

	/**
	 * Returns the value of the '<em><b>Environment Location</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Environment Location</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Environment Location</em>' reference.
	 * @see #setEnvironmentLocation(EnvironmentLocation)
	 * @see organicdesign.OrganicdesignPackage#getEnvironment_EnvironmentLocation()
	 * @model
	 * @generated
	 */
	EnvironmentLocation getEnvironmentLocation();

	/**
	 * Sets the value of the '{@link organicdesign.Environment#getEnvironmentLocation <em>Environment Location</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Environment Location</em>' reference.
	 * @see #getEnvironmentLocation()
	 * @generated
	 */
	void setEnvironmentLocation(EnvironmentLocation value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model locationUnique="false"
	 * @generated
	 */
	void moveToLocation(Location location);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model resourceUnique="false" cartUnique="false"
	 * @generated
	 */
	void loadResourceOntoCart(Resource resource, Cart cart);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model resourceUnique="false" cartUnique="false"
	 * @generated
	 */
	void unloadResourceFromCart(Resource resource, Cart cart);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model resourceUnique="false"
	 * @generated
	 */
	void resetResource(Resource resource);

} // Environment
