/**
 */
package organicdesign;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Environment Location</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link organicdesign.EnvironmentLocation#getConnectedAgent <em>Connected Agent</em>}</li>
 * </ul>
 *
 * @see organicdesign.OrganicdesignPackage#getEnvironmentLocation()
 * @model
 * @generated
 */
public interface EnvironmentLocation extends Location {
	/**
	 * Returns the value of the '<em><b>Connected Agent</b></em>' reference list.
	 * The list contents are of type {@link organicdesign.TransformationAgent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Connected Agent</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Connected Agent</em>' reference list.
	 * @see organicdesign.OrganicdesignPackage#getEnvironmentLocation_ConnectedAgent()
	 * @model ordered="false"
	 * @generated
	 */
	EList<TransformationAgent> getConnectedAgent();

} // EnvironmentLocation
