import "../model/Organicdesign.xcore"
import "../model/Organicdesignhelper.xcore"

specification OrganicDesign{
	 
	domain organicdesign

    contexts organicdesignhelper

	controllable {
		TransformationAgent
		Cart
	}
	
	non-spontaneous events {
		TransformationAgent.resourceLoadedOntoCart
		TransformationAgent.resourceUnloadedFromCart
		TransformationAgent.taskPerformed
	}

	collaboration OrganicDesign {

		static role Environment env
		dynamic role TransformationAgent trAgent
		dynamic role Resource resource
		dynamic multi role Cart cart
		dynamic multi role TransformationAgent nextAgent

		guarantee scenario ResourceArrives 
		context PerformTaskContext
		{
			alternative {
				env->trAgent.resourceArrived(bind resource)	
			}or{
				env->trAgent.resourceUnloadedFromCart(bind resource, *)	
			}
			
			while [!PerformTaskContext.tasksThatAgentCanPerform.isEmpty()]{
				strict urgent trAgent->resource.performTask(PerformTaskContext.tasksThatAgentCanPerform.any())
				resource->trAgent.taskPerformed()
			}
			alternative [env.depositLocation != trAgent]{
				strict urgent trAgent->trAgent.arrangePickUpFor(resource)
			}
		}constraints[
			ignore env->trAgent.resourceArrived(*)
			ignore trAgent->trAgent.arrangePickUpFor(*)
			ignore resource->trAgent.taskPerformed()
		]
		

		guarantee scenario ArrangePickUpForResource
		bindings [
			cart = trAgent.connectedBy
		]
		{
			trAgent->trAgent.arrangePickUpFor(bind resource)
			strict urgent trAgent->cart.pickUpResourceAtLocationRequest(resource,trAgent)
			alternative [cart.locatedResources.isEmpty()]{
				strict urgent cart->trAgent.availableForPickUpOfResourceAtLocation(resource, trAgent)
				strict committed trAgent->trAgent.pickUpArrangedForResource(resource)
			} or [!cart.locatedResources.isEmpty()]{
				strict urgent cart->trAgent.unavailableForPickUpOfResourceAtLocation(resource, trAgent)
			}			
		}constraints[
			interrupt trAgent->trAgent.pickUpArrangedForResource(resource)
		]

		guarantee scenario CartMovesToPickUpResourceUponRequest
		{
			cart->trAgent.availableForPickUpOfResourceAtLocation(bind resource, trAgent)
			alternative [cart.location != trAgent]{
				strict urgent cart->env.moveToLocation(trAgent)
				env->cart.arrivesAtLocation(trAgent)
			}
			strict urgent cart->trAgent.readyToLoadResource(resource)
		}		

		guarantee scenario CartPicksUpResource
		{
			cart->trAgent.readyToLoadResource(bind resource)
			strict urgent trAgent->env.loadResourceOntoCart(resource, cart)
			env->trAgent.resourceLoadedOntoCart(resource, cart)
			strict urgent trAgent->cart.resourceLoaded(resource)
		}		

		guarantee scenario CartSeeksNextAgentForResource
		context NextAgentForResourceContext
		bindings [
			// TODO fix bug that this is not picked up by the validator
			//bind nextAgent to cart.connectedAgentThatCanPerformTask
			nextAgent = NextAgentForResourceContext.connectedAgentThatCanPerformTask
		]
		{
			trAgent->cart.resourceLoaded(bind resource)
			strict urgent cart->nextAgent.transformResourceRequest(resource)
		}constraints[
			interrupt cart->cart.destinationTransformationAgentFoundForResource(*, resource)
		]
		
		guarantee scenario TransformationAgentCanTransformResourceResponse
		{
			cart->nextAgent.transformResourceRequest(bind resource)
			alternative [nextAgent.locatedResources.isEmpty()] {
				strict urgent nextAgent->cart.availableForTransformationOfResource(resource)
				strict committed cart->cart.destinationTransformationAgentFoundForResource(nextAgent, resource)
			} or [!nextAgent.locatedResources.isEmpty()]{ 
				strict urgent nextAgent->cart.unavailableForTransformationOfResource(resource)
			}
		}constraints[
			interrupt cart->cart.destinationTransformationAgentFoundForResource(*, resource)
		]

		guarantee scenario CartBringsResourceToNextTransformationAgent{
			cart->cart.destinationTransformationAgentFoundForResource(bind nextAgent, bind resource)
			strict urgent cart->env.moveToLocation(nextAgent)
			env->cart.arrivesAtLocation(nextAgent)
			strict urgent cart->nextAgent.readyToUnLoadResource(resource)
		}

		guarantee scenario TransformationAgentUnloadsResource{
			cart->nextAgent.readyToUnLoadResource(bind resource)
			strict urgent nextAgent->env.unloadResourceFromCart(resource, cart)
			env->nextAgent.resourceUnloadedFromCart(resource, cart)
			strict urgent nextAgent->cart.resourceUnloaded(resource)
		}

		assumption scenario CartFullfillsMoveToLocationCommand
		{
			cart->env.moveToLocation(bind trAgent)
			eventually env->cart.arrivesAtLocation(trAgent)
//			committed env->cart.setLocation(trAgent)
		}

		assumption scenario ResourceWillBeLoadedOntoCart
		{
			trAgent->env.loadResourceOntoCart(bind resource, bind cart)
			eventually env->trAgent.resourceLoadedOntoCart(resource, cart)
		}
		
		assumption scenario ResourceWillBeUnloadedOntoCart
		{
			trAgent->env.unloadResourceFromCart(bind resource, bind cart)
			eventually env->trAgent.resourceUnloadedFromCart(resource, cart)
		}

		assumption scenario TasksWillBePerfomed
		{
			trAgent->resource.performTask(*)
			eventually resource->trAgent.taskPerformed()
		}
		
		
		assumption scenario ResetDepositedResourceAsNew
		{
			resource->trAgent.taskPerformed()
			interrupt [env.depositLocation != resource.location]
			// reset to environment location
			env->resource.reset(env)
		}
		
	}
	
}