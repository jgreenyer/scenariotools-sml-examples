package organicdesignhelper

import organicdesign.Environment
import organicdesign.Resource
import organicdesign.Task
import organicdesign.TransformationAgent
import organicdesign.Cart
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.common.util.BasicEList
import organicdesign.Location

abstract class AgentAndResourceContext{
	op Task[] tasksThatAgentCanPerform(TransformationAgent transformationAgent, Resource resource){
		return resource.recipe.requiredTasks
		.filter(t|!resource.fulfilledTask.contains(t))
		.filter(t|!resource.recipe.taskdependency.exists(d| d.task==t && !resource.fulfilledTask.contains(d.precondition)))
		.filter(t|
			transformationAgent.currentRole != null
			&& transformationAgent.currentRole.providedCapabilities.contains(t.requiredCapability)
		)
		.toEList
	}
}

class PerformTaskContext extends AgentAndResourceContext{
	refers TransformationAgent receiver
	refers Environment sender
	refers Resource resource
	refers derived volatile unsettable Task[] tasksThatAgentCanPerform get{
		return tasksThatAgentCanPerform(receiver, resource)
	}
	
	derived boolean matches get { true }
}

class NextAgentForResourceContext extends AgentAndResourceContext{

	refers TransformationAgent sender
	refers Cart receiver
	refers Resource resource

	refers derived volatile unsettable TransformationAgent[] connectedAgentThatCanPerformTask get{
		var EList<TransformationAgent> agentList = new BasicEList<TransformationAgent>()
		for(Location loc : receiver.connectedLocations){
			if(loc instanceof TransformationAgent){
				if(!tasksThatAgentCanPerform(loc, resource).empty)
				agentList.add(loc)
			}
		}
		return agentList
		
		//typeof not supported when working with dynamic objects, see https://bugs.eclipse.org/bugs/show_bug.cgi?id=510033 
//		receiver.connectedLocations
//		.filter(typeof(TransformationAgent))
//		.filter(a | !tasksThatAgentCanPerform(a, resource).empty)
//		.toEList()
	}


}