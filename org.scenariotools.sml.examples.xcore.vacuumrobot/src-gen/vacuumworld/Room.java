/**
 */
package vacuumworld;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Room</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link vacuumworld.Room#getEnv <em>Env</em>}</li>
 *   <li>{@link vacuumworld.Room#isHasChargingStation <em>Has Charging Station</em>}</li>
 *   <li>{@link vacuumworld.Room#isDirty <em>Dirty</em>}</li>
 *   <li>{@link vacuumworld.Room#getAdjacentRooms <em>Adjacent Rooms</em>}</li>
 * </ul>
 *
 * @see vacuumworld.VacuumworldPackage#getRoom()
 * @model
 * @generated
 */
public interface Room extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Env</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link vacuumworld.Environment#getRooms <em>Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Env</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Env</em>' container reference.
	 * @see #setEnv(Environment)
	 * @see vacuumworld.VacuumworldPackage#getRoom_Env()
	 * @see vacuumworld.Environment#getRooms
	 * @model opposite="rooms" transient="false"
	 * @generated
	 */
	Environment getEnv();

	/**
	 * Sets the value of the '{@link vacuumworld.Room#getEnv <em>Env</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Env</em>' container reference.
	 * @see #getEnv()
	 * @generated
	 */
	void setEnv(Environment value);

	/**
	 * Returns the value of the '<em><b>Has Charging Station</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has Charging Station</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has Charging Station</em>' attribute.
	 * @see #setHasChargingStation(boolean)
	 * @see vacuumworld.VacuumworldPackage#getRoom_HasChargingStation()
	 * @model unique="false" required="true"
	 * @generated
	 */
	boolean isHasChargingStation();

	/**
	 * Sets the value of the '{@link vacuumworld.Room#isHasChargingStation <em>Has Charging Station</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has Charging Station</em>' attribute.
	 * @see #isHasChargingStation()
	 * @generated
	 */
	void setHasChargingStation(boolean value);

	/**
	 * Returns the value of the '<em><b>Dirty</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dirty</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dirty</em>' attribute.
	 * @see #setDirty(boolean)
	 * @see vacuumworld.VacuumworldPackage#getRoom_Dirty()
	 * @model unique="false" required="true"
	 * @generated
	 */
	boolean isDirty();

	/**
	 * Sets the value of the '{@link vacuumworld.Room#isDirty <em>Dirty</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dirty</em>' attribute.
	 * @see #isDirty()
	 * @generated
	 */
	void setDirty(boolean value);

	/**
	 * Returns the value of the '<em><b>Adjacent Rooms</b></em>' reference list.
	 * The list contents are of type {@link vacuumworld.Room}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Adjacent Rooms</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Adjacent Rooms</em>' reference list.
	 * @see vacuumworld.VacuumworldPackage#getRoom_AdjacentRooms()
	 * @model transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='<%vacuumworld.Environment%> _env = this.getEnv();\n<%org.eclipse.emf.common.util.EList%><<%vacuumworld.Room%>> _rooms = _env.getRooms();\nfinal <%org.eclipse.xtext.xbase.lib.Functions.Function1%><<%vacuumworld.Room%>, <%java.lang.Boolean%>> _function = new <%org.eclipse.xtext.xbase.lib.Functions.Function1%><<%vacuumworld.Room%>, <%java.lang.Boolean%>>()\n{\n\tpublic <%java.lang.Boolean%> apply(final <%vacuumworld.Room%> room)\n\t{\n\t\treturn <%java.lang.Boolean%>.valueOf(((!<%com.google.common.base.Objects%>.equal(room, this)) && <%org.eclipse.xtext.xbase.lib.IterableExtensions%>.<<%vacuumworld.Door%>>exists(<%this%>.getEnv().getDoors(), new <%org.eclipse.xtext.xbase.lib.Functions.Function1%><<%vacuumworld.Door%>, <%java.lang.Boolean%>>()\n\t\t{\n\t\t\tpublic <%java.lang.Boolean%> apply(final <%vacuumworld.Door%> it)\n\t\t\t{\n\t\t\t\treturn <%java.lang.Boolean%>.valueOf((it.getConnectedRooms().contains(room) && it.getConnectedRooms().contains(this)));\n\t\t\t}\n\t\t})));\n\t}\n};\n<%java.lang.Iterable%><<%vacuumworld.Room%>> _filter = <%org.eclipse.xtext.xbase.lib.IterableExtensions%>.<<%vacuumworld.Room%>>filter(_rooms, _function);\nreturn <%org.eclipse.emf.common.util.ECollections%>.<<%vacuumworld.Room%>>toEList(_filter);'"
	 * @generated
	 */
	EList<Room> getAdjacentRooms();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='this.setDirty(true);'"
	 * @generated
	 */
	void makeDirty();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='this.setDirty(false);'"
	 * @generated
	 */
	void clean();

} // Room
