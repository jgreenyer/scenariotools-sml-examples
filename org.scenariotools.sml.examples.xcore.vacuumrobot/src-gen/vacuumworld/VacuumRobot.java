/**
 */
package vacuumworld;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vacuum Robot</b></em>'.
 * <!-- end-user-doc -->
 *
 * <!-- begin-model-doc -->
 * the robot may perform these moves, each costing 1 unit of energy(battery)
 * - move to an adjacent room
 * - clean a room
 * 
 * the robot begins each cleaning iteration with a full battery (in a room where charging is possible)
 * and must end each iteration in a room in which charging is possible
 * <!-- end-model-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link vacuumworld.VacuumRobot#getEnv <em>Env</em>}</li>
 *   <li>{@link vacuumworld.VacuumRobot#getLocation <em>Location</em>}</li>
 *   <li>{@link vacuumworld.VacuumRobot#getBatteryCapacity <em>Battery Capacity</em>}</li>
 *   <li>{@link vacuumworld.VacuumRobot#getBatteryLevel <em>Battery Level</em>}</li>
 *   <li>{@link vacuumworld.VacuumRobot#isPerformCleaning <em>Perform Cleaning</em>}</li>
 *   <li>{@link vacuumworld.VacuumRobot#isBatteryEmpty <em>Battery Empty</em>}</li>
 * </ul>
 *
 * @see vacuumworld.VacuumworldPackage#getVacuumRobot()
 * @model
 * @generated
 */
public interface VacuumRobot extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Env</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Env</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Env</em>' reference.
	 * @see #setEnv(Environment)
	 * @see vacuumworld.VacuumworldPackage#getVacuumRobot_Env()
	 * @model required="true"
	 * @generated
	 */
	Environment getEnv();

	/**
	 * Sets the value of the '{@link vacuumworld.VacuumRobot#getEnv <em>Env</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Env</em>' reference.
	 * @see #getEnv()
	 * @generated
	 */
	void setEnv(Environment value);

	/**
	 * Returns the value of the '<em><b>Location</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Location</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location</em>' reference.
	 * @see #setLocation(Room)
	 * @see vacuumworld.VacuumworldPackage#getVacuumRobot_Location()
	 * @model required="true"
	 * @generated
	 */
	Room getLocation();

	/**
	 * Sets the value of the '{@link vacuumworld.VacuumRobot#getLocation <em>Location</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Location</em>' reference.
	 * @see #getLocation()
	 * @generated
	 */
	void setLocation(Room value);

	/**
	 * Returns the value of the '<em><b>Battery Capacity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Battery Capacity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Battery Capacity</em>' attribute.
	 * @see #setBatteryCapacity(int)
	 * @see vacuumworld.VacuumworldPackage#getVacuumRobot_BatteryCapacity()
	 * @model unique="false" required="true"
	 * @generated
	 */
	int getBatteryCapacity();

	/**
	 * Sets the value of the '{@link vacuumworld.VacuumRobot#getBatteryCapacity <em>Battery Capacity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Battery Capacity</em>' attribute.
	 * @see #getBatteryCapacity()
	 * @generated
	 */
	void setBatteryCapacity(int value);

	/**
	 * Returns the value of the '<em><b>Battery Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Battery Level</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Battery Level</em>' attribute.
	 * @see #setBatteryLevel(int)
	 * @see vacuumworld.VacuumworldPackage#getVacuumRobot_BatteryLevel()
	 * @model unique="false" required="true"
	 * @generated
	 */
	int getBatteryLevel();

	/**
	 * Sets the value of the '{@link vacuumworld.VacuumRobot#getBatteryLevel <em>Battery Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Battery Level</em>' attribute.
	 * @see #getBatteryLevel()
	 * @generated
	 */
	void setBatteryLevel(int value);

	/**
	 * Returns the value of the '<em><b>Perform Cleaning</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Perform Cleaning</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Perform Cleaning</em>' attribute.
	 * @see #setPerformCleaning(boolean)
	 * @see vacuumworld.VacuumworldPackage#getVacuumRobot_PerformCleaning()
	 * @model unique="false" required="true"
	 * @generated
	 */
	boolean isPerformCleaning();

	/**
	 * Sets the value of the '{@link vacuumworld.VacuumRobot#isPerformCleaning <em>Perform Cleaning</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Perform Cleaning</em>' attribute.
	 * @see #isPerformCleaning()
	 * @generated
	 */
	void setPerformCleaning(boolean value);

	/**
	 * Returns the value of the '<em><b>Battery Empty</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Battery Empty</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Battery Empty</em>' attribute.
	 * @see vacuumworld.VacuumworldPackage#getVacuumRobot_BatteryEmpty()
	 * @model unique="false" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='int _batteryLevel = this.getBatteryLevel();\nreturn (_batteryLevel == 0);'"
	 * @generated
	 */
	boolean isBatteryEmpty();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='this.setPerformCleaning(true);\nint _batteryCapacity = this.getBatteryCapacity();\nthis.setBatteryLevel(_batteryCapacity);'"
	 * @generated
	 */
	void startCleaning();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model annotation="http://www.eclipse.org/emf/2002/GenModel body='this.setPerformCleaning(false);\nthis.setBatteryLevel(0);'"
	 * @generated
	 */
	void stopCleaning();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomUnique="false"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='this.setLocation(room);\nint _batteryLevel = this.getBatteryLevel();\nint _minus = (_batteryLevel - 1);\nthis.setBatteryLevel(_minus);'"
	 * @generated
	 */
	void moveToRoom(Room room);

} // VacuumRobot
