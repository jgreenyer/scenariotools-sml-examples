/**
 */
package vacuumworld.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import vacuumworld.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class VacuumworldFactoryImpl extends EFactoryImpl implements VacuumworldFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static VacuumworldFactory init() {
		try {
			VacuumworldFactory theVacuumworldFactory = (VacuumworldFactory)EPackage.Registry.INSTANCE.getEFactory(VacuumworldPackage.eNS_URI);
			if (theVacuumworldFactory != null) {
				return theVacuumworldFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new VacuumworldFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VacuumworldFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case VacuumworldPackage.VACUUM_WORLD: return createVacuumWorld();
			case VacuumworldPackage.ENVIRONMENT: return createEnvironment();
			case VacuumworldPackage.VACUUM_ROBOT: return createVacuumRobot();
			case VacuumworldPackage.ROOM: return createRoom();
			case VacuumworldPackage.DOOR: return createDoor();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VacuumWorld createVacuumWorld() {
		VacuumWorldImpl vacuumWorld = new VacuumWorldImpl();
		return vacuumWorld;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Environment createEnvironment() {
		EnvironmentImpl environment = new EnvironmentImpl();
		return environment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VacuumRobot createVacuumRobot() {
		VacuumRobotImpl vacuumRobot = new VacuumRobotImpl();
		return vacuumRobot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Room createRoom() {
		RoomImpl room = new RoomImpl();
		return room;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Door createDoor() {
		DoorImpl door = new DoorImpl();
		return door;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VacuumworldPackage getVacuumworldPackage() {
		return (VacuumworldPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static VacuumworldPackage getPackage() {
		return VacuumworldPackage.eINSTANCE;
	}

} //VacuumworldFactoryImpl
