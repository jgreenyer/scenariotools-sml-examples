/**
 */
package vacuumworld.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import vacuumworld.Environment;
import vacuumworld.Room;
import vacuumworld.VacuumRobot;
import vacuumworld.VacuumworldPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Vacuum Robot</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link vacuumworld.impl.VacuumRobotImpl#getEnv <em>Env</em>}</li>
 *   <li>{@link vacuumworld.impl.VacuumRobotImpl#getLocation <em>Location</em>}</li>
 *   <li>{@link vacuumworld.impl.VacuumRobotImpl#getBatteryCapacity <em>Battery Capacity</em>}</li>
 *   <li>{@link vacuumworld.impl.VacuumRobotImpl#getBatteryLevel <em>Battery Level</em>}</li>
 *   <li>{@link vacuumworld.impl.VacuumRobotImpl#isPerformCleaning <em>Perform Cleaning</em>}</li>
 *   <li>{@link vacuumworld.impl.VacuumRobotImpl#isBatteryEmpty <em>Battery Empty</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VacuumRobotImpl extends NamedElementImpl implements VacuumRobot {
	/**
	 * The cached value of the '{@link #getEnv() <em>Env</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnv()
	 * @generated
	 * @ordered
	 */
	protected Environment env;

	/**
	 * The cached value of the '{@link #getLocation() <em>Location</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLocation()
	 * @generated
	 * @ordered
	 */
	protected Room location;

	/**
	 * The default value of the '{@link #getBatteryCapacity() <em>Battery Capacity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBatteryCapacity()
	 * @generated
	 * @ordered
	 */
	protected static final int BATTERY_CAPACITY_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getBatteryCapacity() <em>Battery Capacity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBatteryCapacity()
	 * @generated
	 * @ordered
	 */
	protected int batteryCapacity = BATTERY_CAPACITY_EDEFAULT;

	/**
	 * The default value of the '{@link #getBatteryLevel() <em>Battery Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBatteryLevel()
	 * @generated
	 * @ordered
	 */
	protected static final int BATTERY_LEVEL_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getBatteryLevel() <em>Battery Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBatteryLevel()
	 * @generated
	 * @ordered
	 */
	protected int batteryLevel = BATTERY_LEVEL_EDEFAULT;

	/**
	 * The default value of the '{@link #isPerformCleaning() <em>Perform Cleaning</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPerformCleaning()
	 * @generated
	 * @ordered
	 */
	protected static final boolean PERFORM_CLEANING_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isPerformCleaning() <em>Perform Cleaning</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isPerformCleaning()
	 * @generated
	 * @ordered
	 */
	protected boolean performCleaning = PERFORM_CLEANING_EDEFAULT;

	/**
	 * The default value of the '{@link #isBatteryEmpty() <em>Battery Empty</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isBatteryEmpty()
	 * @generated
	 * @ordered
	 */
	protected static final boolean BATTERY_EMPTY_EDEFAULT = false;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VacuumRobotImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VacuumworldPackage.Literals.VACUUM_ROBOT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Environment getEnv() {
		if (env != null && env.eIsProxy()) {
			InternalEObject oldEnv = (InternalEObject)env;
			env = (Environment)eResolveProxy(oldEnv);
			if (env != oldEnv) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VacuumworldPackage.VACUUM_ROBOT__ENV, oldEnv, env));
			}
		}
		return env;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Environment basicGetEnv() {
		return env;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnv(Environment newEnv) {
		Environment oldEnv = env;
		env = newEnv;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VacuumworldPackage.VACUUM_ROBOT__ENV, oldEnv, env));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Room getLocation() {
		if (location != null && location.eIsProxy()) {
			InternalEObject oldLocation = (InternalEObject)location;
			location = (Room)eResolveProxy(oldLocation);
			if (location != oldLocation) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, VacuumworldPackage.VACUUM_ROBOT__LOCATION, oldLocation, location));
			}
		}
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Room basicGetLocation() {
		return location;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocation(Room newLocation) {
		Room oldLocation = location;
		location = newLocation;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VacuumworldPackage.VACUUM_ROBOT__LOCATION, oldLocation, location));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getBatteryCapacity() {
		return batteryCapacity;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBatteryCapacity(int newBatteryCapacity) {
		int oldBatteryCapacity = batteryCapacity;
		batteryCapacity = newBatteryCapacity;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VacuumworldPackage.VACUUM_ROBOT__BATTERY_CAPACITY, oldBatteryCapacity, batteryCapacity));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getBatteryLevel() {
		return batteryLevel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBatteryLevel(int newBatteryLevel) {
		int oldBatteryLevel = batteryLevel;
		batteryLevel = newBatteryLevel;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VacuumworldPackage.VACUUM_ROBOT__BATTERY_LEVEL, oldBatteryLevel, batteryLevel));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isPerformCleaning() {
		return performCleaning;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPerformCleaning(boolean newPerformCleaning) {
		boolean oldPerformCleaning = performCleaning;
		performCleaning = newPerformCleaning;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VacuumworldPackage.VACUUM_ROBOT__PERFORM_CLEANING, oldPerformCleaning, performCleaning));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isBatteryEmpty() {
		int _batteryLevel = this.getBatteryLevel();
		return (_batteryLevel == 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void startCleaning() {
		this.setPerformCleaning(true);
		int _batteryCapacity = this.getBatteryCapacity();
		this.setBatteryLevel(_batteryCapacity);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void stopCleaning() {
		this.setPerformCleaning(false);
		this.setBatteryLevel(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void moveToRoom(final Room room) {
		this.setLocation(room);
		int _batteryLevel = this.getBatteryLevel();
		int _minus = (_batteryLevel - 1);
		this.setBatteryLevel(_minus);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VacuumworldPackage.VACUUM_ROBOT__ENV:
				if (resolve) return getEnv();
				return basicGetEnv();
			case VacuumworldPackage.VACUUM_ROBOT__LOCATION:
				if (resolve) return getLocation();
				return basicGetLocation();
			case VacuumworldPackage.VACUUM_ROBOT__BATTERY_CAPACITY:
				return getBatteryCapacity();
			case VacuumworldPackage.VACUUM_ROBOT__BATTERY_LEVEL:
				return getBatteryLevel();
			case VacuumworldPackage.VACUUM_ROBOT__PERFORM_CLEANING:
				return isPerformCleaning();
			case VacuumworldPackage.VACUUM_ROBOT__BATTERY_EMPTY:
				return isBatteryEmpty();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VacuumworldPackage.VACUUM_ROBOT__ENV:
				setEnv((Environment)newValue);
				return;
			case VacuumworldPackage.VACUUM_ROBOT__LOCATION:
				setLocation((Room)newValue);
				return;
			case VacuumworldPackage.VACUUM_ROBOT__BATTERY_CAPACITY:
				setBatteryCapacity((Integer)newValue);
				return;
			case VacuumworldPackage.VACUUM_ROBOT__BATTERY_LEVEL:
				setBatteryLevel((Integer)newValue);
				return;
			case VacuumworldPackage.VACUUM_ROBOT__PERFORM_CLEANING:
				setPerformCleaning((Boolean)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VacuumworldPackage.VACUUM_ROBOT__ENV:
				setEnv((Environment)null);
				return;
			case VacuumworldPackage.VACUUM_ROBOT__LOCATION:
				setLocation((Room)null);
				return;
			case VacuumworldPackage.VACUUM_ROBOT__BATTERY_CAPACITY:
				setBatteryCapacity(BATTERY_CAPACITY_EDEFAULT);
				return;
			case VacuumworldPackage.VACUUM_ROBOT__BATTERY_LEVEL:
				setBatteryLevel(BATTERY_LEVEL_EDEFAULT);
				return;
			case VacuumworldPackage.VACUUM_ROBOT__PERFORM_CLEANING:
				setPerformCleaning(PERFORM_CLEANING_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VacuumworldPackage.VACUUM_ROBOT__ENV:
				return env != null;
			case VacuumworldPackage.VACUUM_ROBOT__LOCATION:
				return location != null;
			case VacuumworldPackage.VACUUM_ROBOT__BATTERY_CAPACITY:
				return batteryCapacity != BATTERY_CAPACITY_EDEFAULT;
			case VacuumworldPackage.VACUUM_ROBOT__BATTERY_LEVEL:
				return batteryLevel != BATTERY_LEVEL_EDEFAULT;
			case VacuumworldPackage.VACUUM_ROBOT__PERFORM_CLEANING:
				return performCleaning != PERFORM_CLEANING_EDEFAULT;
			case VacuumworldPackage.VACUUM_ROBOT__BATTERY_EMPTY:
				return isBatteryEmpty() != BATTERY_EMPTY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case VacuumworldPackage.VACUUM_ROBOT___START_CLEANING:
				startCleaning();
				return null;
			case VacuumworldPackage.VACUUM_ROBOT___STOP_CLEANING:
				stopCleaning();
				return null;
			case VacuumworldPackage.VACUUM_ROBOT___MOVE_TO_ROOM__ROOM:
				moveToRoom((Room)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (batteryCapacity: ");
		result.append(batteryCapacity);
		result.append(", batteryLevel: ");
		result.append(batteryLevel);
		result.append(", performCleaning: ");
		result.append(performCleaning);
		result.append(')');
		return result.toString();
	}

} //VacuumRobotImpl
