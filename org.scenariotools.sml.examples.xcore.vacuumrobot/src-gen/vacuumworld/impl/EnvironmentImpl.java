/**
 */
package vacuumworld.impl;

import java.lang.Iterable;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

import org.eclipse.xtext.xbase.lib.Functions.Function1;

import org.eclipse.xtext.xbase.lib.IterableExtensions;

import vacuumworld.Door;
import vacuumworld.Environment;
import vacuumworld.Room;
import vacuumworld.VacuumworldPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Environment</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link vacuumworld.impl.EnvironmentImpl#getMinDirtPerIteration <em>Min Dirt Per Iteration</em>}</li>
 *   <li>{@link vacuumworld.impl.EnvironmentImpl#getMaxDirtPerIteration <em>Max Dirt Per Iteration</em>}</li>
 *   <li>{@link vacuumworld.impl.EnvironmentImpl#getRooms <em>Rooms</em>}</li>
 *   <li>{@link vacuumworld.impl.EnvironmentImpl#getDoors <em>Doors</em>}</li>
 *   <li>{@link vacuumworld.impl.EnvironmentImpl#getNumDirtyRooms <em>Num Dirty Rooms</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EnvironmentImpl extends NamedElementImpl implements Environment {
	/**
	 * The default value of the '{@link #getMinDirtPerIteration() <em>Min Dirt Per Iteration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinDirtPerIteration()
	 * @generated
	 * @ordered
	 */
	protected static final int MIN_DIRT_PER_ITERATION_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMinDirtPerIteration() <em>Min Dirt Per Iteration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMinDirtPerIteration()
	 * @generated
	 * @ordered
	 */
	protected int minDirtPerIteration = MIN_DIRT_PER_ITERATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getMaxDirtPerIteration() <em>Max Dirt Per Iteration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxDirtPerIteration()
	 * @generated
	 * @ordered
	 */
	protected static final int MAX_DIRT_PER_ITERATION_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getMaxDirtPerIteration() <em>Max Dirt Per Iteration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getMaxDirtPerIteration()
	 * @generated
	 * @ordered
	 */
	protected int maxDirtPerIteration = MAX_DIRT_PER_ITERATION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRooms() <em>Rooms</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRooms()
	 * @generated
	 * @ordered
	 */
	protected EList<Room> rooms;

	/**
	 * The cached value of the '{@link #getDoors() <em>Doors</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDoors()
	 * @generated
	 * @ordered
	 */
	protected EList<Door> doors;

	/**
	 * The default value of the '{@link #getNumDirtyRooms() <em>Num Dirty Rooms</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumDirtyRooms()
	 * @generated
	 * @ordered
	 */
	protected static final int NUM_DIRTY_ROOMS_EDEFAULT = 0;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EnvironmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VacuumworldPackage.Literals.ENVIRONMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMinDirtPerIteration() {
		return minDirtPerIteration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinDirtPerIteration(int newMinDirtPerIteration) {
		int oldMinDirtPerIteration = minDirtPerIteration;
		minDirtPerIteration = newMinDirtPerIteration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VacuumworldPackage.ENVIRONMENT__MIN_DIRT_PER_ITERATION, oldMinDirtPerIteration, minDirtPerIteration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMaxDirtPerIteration() {
		return maxDirtPerIteration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxDirtPerIteration(int newMaxDirtPerIteration) {
		int oldMaxDirtPerIteration = maxDirtPerIteration;
		maxDirtPerIteration = newMaxDirtPerIteration;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VacuumworldPackage.ENVIRONMENT__MAX_DIRT_PER_ITERATION, oldMaxDirtPerIteration, maxDirtPerIteration));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Room> getRooms() {
		if (rooms == null) {
			rooms = new EObjectContainmentWithInverseEList<Room>(Room.class, this, VacuumworldPackage.ENVIRONMENT__ROOMS, VacuumworldPackage.ROOM__ENV);
		}
		return rooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Door> getDoors() {
		if (doors == null) {
			doors = new EObjectContainmentEList<Door>(Door.class, this, VacuumworldPackage.ENVIRONMENT__DOORS);
		}
		return doors;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNumDirtyRooms() {
		EList<Room> _rooms = this.getRooms();
		final Function1<Room, Boolean> _function = new Function1<Room, Boolean>() {
			public Boolean apply(final Room room) {
				return Boolean.valueOf(room.isDirty());
			}
		};
		Iterable<Room> _filter = IterableExtensions.<Room>filter(_rooms, _function);
		return IterableExtensions.size(_filter);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VacuumworldPackage.ENVIRONMENT__ROOMS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getRooms()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VacuumworldPackage.ENVIRONMENT__ROOMS:
				return ((InternalEList<?>)getRooms()).basicRemove(otherEnd, msgs);
			case VacuumworldPackage.ENVIRONMENT__DOORS:
				return ((InternalEList<?>)getDoors()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VacuumworldPackage.ENVIRONMENT__MIN_DIRT_PER_ITERATION:
				return getMinDirtPerIteration();
			case VacuumworldPackage.ENVIRONMENT__MAX_DIRT_PER_ITERATION:
				return getMaxDirtPerIteration();
			case VacuumworldPackage.ENVIRONMENT__ROOMS:
				return getRooms();
			case VacuumworldPackage.ENVIRONMENT__DOORS:
				return getDoors();
			case VacuumworldPackage.ENVIRONMENT__NUM_DIRTY_ROOMS:
				return getNumDirtyRooms();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VacuumworldPackage.ENVIRONMENT__MIN_DIRT_PER_ITERATION:
				setMinDirtPerIteration((Integer)newValue);
				return;
			case VacuumworldPackage.ENVIRONMENT__MAX_DIRT_PER_ITERATION:
				setMaxDirtPerIteration((Integer)newValue);
				return;
			case VacuumworldPackage.ENVIRONMENT__ROOMS:
				getRooms().clear();
				getRooms().addAll((Collection<? extends Room>)newValue);
				return;
			case VacuumworldPackage.ENVIRONMENT__DOORS:
				getDoors().clear();
				getDoors().addAll((Collection<? extends Door>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VacuumworldPackage.ENVIRONMENT__MIN_DIRT_PER_ITERATION:
				setMinDirtPerIteration(MIN_DIRT_PER_ITERATION_EDEFAULT);
				return;
			case VacuumworldPackage.ENVIRONMENT__MAX_DIRT_PER_ITERATION:
				setMaxDirtPerIteration(MAX_DIRT_PER_ITERATION_EDEFAULT);
				return;
			case VacuumworldPackage.ENVIRONMENT__ROOMS:
				getRooms().clear();
				return;
			case VacuumworldPackage.ENVIRONMENT__DOORS:
				getDoors().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VacuumworldPackage.ENVIRONMENT__MIN_DIRT_PER_ITERATION:
				return minDirtPerIteration != MIN_DIRT_PER_ITERATION_EDEFAULT;
			case VacuumworldPackage.ENVIRONMENT__MAX_DIRT_PER_ITERATION:
				return maxDirtPerIteration != MAX_DIRT_PER_ITERATION_EDEFAULT;
			case VacuumworldPackage.ENVIRONMENT__ROOMS:
				return rooms != null && !rooms.isEmpty();
			case VacuumworldPackage.ENVIRONMENT__DOORS:
				return doors != null && !doors.isEmpty();
			case VacuumworldPackage.ENVIRONMENT__NUM_DIRTY_ROOMS:
				return getNumDirtyRooms() != NUM_DIRTY_ROOMS_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (minDirtPerIteration: ");
		result.append(minDirtPerIteration);
		result.append(", maxDirtPerIteration: ");
		result.append(maxDirtPerIteration);
		result.append(')');
		return result.toString();
	}

} //EnvironmentImpl
