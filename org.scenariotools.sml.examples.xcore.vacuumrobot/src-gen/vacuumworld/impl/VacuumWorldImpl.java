/**
 */
package vacuumworld.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import vacuumworld.Environment;
import vacuumworld.VacuumRobot;
import vacuumworld.VacuumWorld;
import vacuumworld.VacuumworldPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Vacuum World</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link vacuumworld.impl.VacuumWorldImpl#getEnvironment <em>Environment</em>}</li>
 *   <li>{@link vacuumworld.impl.VacuumWorldImpl#getVacuumRobot <em>Vacuum Robot</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VacuumWorldImpl extends NamedElementImpl implements VacuumWorld {
	/**
	 * The cached value of the '{@link #getEnvironment() <em>Environment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnvironment()
	 * @generated
	 * @ordered
	 */
	protected Environment environment;

	/**
	 * The cached value of the '{@link #getVacuumRobot() <em>Vacuum Robot</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getVacuumRobot()
	 * @generated
	 * @ordered
	 */
	protected VacuumRobot vacuumRobot;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VacuumWorldImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return VacuumworldPackage.Literals.VACUUM_WORLD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Environment getEnvironment() {
		return environment;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetEnvironment(Environment newEnvironment, NotificationChain msgs) {
		Environment oldEnvironment = environment;
		environment = newEnvironment;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VacuumworldPackage.VACUUM_WORLD__ENVIRONMENT, oldEnvironment, newEnvironment);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnvironment(Environment newEnvironment) {
		if (newEnvironment != environment) {
			NotificationChain msgs = null;
			if (environment != null)
				msgs = ((InternalEObject)environment).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VacuumworldPackage.VACUUM_WORLD__ENVIRONMENT, null, msgs);
			if (newEnvironment != null)
				msgs = ((InternalEObject)newEnvironment).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VacuumworldPackage.VACUUM_WORLD__ENVIRONMENT, null, msgs);
			msgs = basicSetEnvironment(newEnvironment, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VacuumworldPackage.VACUUM_WORLD__ENVIRONMENT, newEnvironment, newEnvironment));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VacuumRobot getVacuumRobot() {
		return vacuumRobot;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetVacuumRobot(VacuumRobot newVacuumRobot, NotificationChain msgs) {
		VacuumRobot oldVacuumRobot = vacuumRobot;
		vacuumRobot = newVacuumRobot;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, VacuumworldPackage.VACUUM_WORLD__VACUUM_ROBOT, oldVacuumRobot, newVacuumRobot);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVacuumRobot(VacuumRobot newVacuumRobot) {
		if (newVacuumRobot != vacuumRobot) {
			NotificationChain msgs = null;
			if (vacuumRobot != null)
				msgs = ((InternalEObject)vacuumRobot).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - VacuumworldPackage.VACUUM_WORLD__VACUUM_ROBOT, null, msgs);
			if (newVacuumRobot != null)
				msgs = ((InternalEObject)newVacuumRobot).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - VacuumworldPackage.VACUUM_WORLD__VACUUM_ROBOT, null, msgs);
			msgs = basicSetVacuumRobot(newVacuumRobot, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, VacuumworldPackage.VACUUM_WORLD__VACUUM_ROBOT, newVacuumRobot, newVacuumRobot));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case VacuumworldPackage.VACUUM_WORLD__ENVIRONMENT:
				return basicSetEnvironment(null, msgs);
			case VacuumworldPackage.VACUUM_WORLD__VACUUM_ROBOT:
				return basicSetVacuumRobot(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case VacuumworldPackage.VACUUM_WORLD__ENVIRONMENT:
				return getEnvironment();
			case VacuumworldPackage.VACUUM_WORLD__VACUUM_ROBOT:
				return getVacuumRobot();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case VacuumworldPackage.VACUUM_WORLD__ENVIRONMENT:
				setEnvironment((Environment)newValue);
				return;
			case VacuumworldPackage.VACUUM_WORLD__VACUUM_ROBOT:
				setVacuumRobot((VacuumRobot)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case VacuumworldPackage.VACUUM_WORLD__ENVIRONMENT:
				setEnvironment((Environment)null);
				return;
			case VacuumworldPackage.VACUUM_WORLD__VACUUM_ROBOT:
				setVacuumRobot((VacuumRobot)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case VacuumworldPackage.VACUUM_WORLD__ENVIRONMENT:
				return environment != null;
			case VacuumworldPackage.VACUUM_WORLD__VACUUM_ROBOT:
				return vacuumRobot != null;
		}
		return super.eIsSet(featureID);
	}

} //VacuumWorldImpl
