/**
 */
package vacuumworld.impl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import vacuumworld.Door;
import vacuumworld.Environment;
import vacuumworld.NamedElement;
import vacuumworld.Room;
import vacuumworld.VacuumRobot;
import vacuumworld.VacuumWorld;
import vacuumworld.VacuumworldFactory;
import vacuumworld.VacuumworldPackage;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class VacuumworldPackageImpl extends EPackageImpl implements VacuumworldPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass namedElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vacuumWorldEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass environmentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vacuumRobotEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass roomEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass doorEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see vacuumworld.VacuumworldPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private VacuumworldPackageImpl() {
		super(eNS_URI, VacuumworldFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link VacuumworldPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static VacuumworldPackage init() {
		if (isInited) return (VacuumworldPackage)EPackage.Registry.INSTANCE.getEPackage(VacuumworldPackage.eNS_URI);

		// Obtain or create and register package
		VacuumworldPackageImpl theVacuumworldPackage = (VacuumworldPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof VacuumworldPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new VacuumworldPackageImpl());

		isInited = true;

		// Initialize simple dependencies
		EcorePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theVacuumworldPackage.createPackageContents();

		// Initialize created meta-data
		theVacuumworldPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theVacuumworldPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(VacuumworldPackage.eNS_URI, theVacuumworldPackage);
		return theVacuumworldPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNamedElement() {
		return namedElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNamedElement_Name() {
		return (EAttribute)namedElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVacuumWorld() {
		return vacuumWorldEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVacuumWorld_Environment() {
		return (EReference)vacuumWorldEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVacuumWorld_VacuumRobot() {
		return (EReference)vacuumWorldEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEnvironment() {
		return environmentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEnvironment_MinDirtPerIteration() {
		return (EAttribute)environmentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEnvironment_MaxDirtPerIteration() {
		return (EAttribute)environmentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEnvironment_Rooms() {
		return (EReference)environmentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEnvironment_Doors() {
		return (EReference)environmentEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEnvironment_NumDirtyRooms() {
		return (EAttribute)environmentEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVacuumRobot() {
		return vacuumRobotEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVacuumRobot_Env() {
		return (EReference)vacuumRobotEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVacuumRobot_Location() {
		return (EReference)vacuumRobotEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVacuumRobot_BatteryCapacity() {
		return (EAttribute)vacuumRobotEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVacuumRobot_BatteryLevel() {
		return (EAttribute)vacuumRobotEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVacuumRobot_PerformCleaning() {
		return (EAttribute)vacuumRobotEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getVacuumRobot_BatteryEmpty() {
		return (EAttribute)vacuumRobotEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getVacuumRobot__StartCleaning() {
		return vacuumRobotEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getVacuumRobot__StopCleaning() {
		return vacuumRobotEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getVacuumRobot__MoveToRoom__Room() {
		return vacuumRobotEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRoom() {
		return roomEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoom_Env() {
		return (EReference)roomEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoom_HasChargingStation() {
		return (EAttribute)roomEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRoom_Dirty() {
		return (EAttribute)roomEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRoom_AdjacentRooms() {
		return (EReference)roomEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoom__MakeDirty() {
		return roomEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRoom__Clean() {
		return roomEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDoor() {
		return doorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDoor_ConnectedRooms() {
		return (EReference)doorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VacuumworldFactory getVacuumworldFactory() {
		return (VacuumworldFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		namedElementEClass = createEClass(NAMED_ELEMENT);
		createEAttribute(namedElementEClass, NAMED_ELEMENT__NAME);

		vacuumWorldEClass = createEClass(VACUUM_WORLD);
		createEReference(vacuumWorldEClass, VACUUM_WORLD__ENVIRONMENT);
		createEReference(vacuumWorldEClass, VACUUM_WORLD__VACUUM_ROBOT);

		environmentEClass = createEClass(ENVIRONMENT);
		createEAttribute(environmentEClass, ENVIRONMENT__MIN_DIRT_PER_ITERATION);
		createEAttribute(environmentEClass, ENVIRONMENT__MAX_DIRT_PER_ITERATION);
		createEReference(environmentEClass, ENVIRONMENT__ROOMS);
		createEReference(environmentEClass, ENVIRONMENT__DOORS);
		createEAttribute(environmentEClass, ENVIRONMENT__NUM_DIRTY_ROOMS);

		vacuumRobotEClass = createEClass(VACUUM_ROBOT);
		createEReference(vacuumRobotEClass, VACUUM_ROBOT__ENV);
		createEReference(vacuumRobotEClass, VACUUM_ROBOT__LOCATION);
		createEAttribute(vacuumRobotEClass, VACUUM_ROBOT__BATTERY_CAPACITY);
		createEAttribute(vacuumRobotEClass, VACUUM_ROBOT__BATTERY_LEVEL);
		createEAttribute(vacuumRobotEClass, VACUUM_ROBOT__PERFORM_CLEANING);
		createEAttribute(vacuumRobotEClass, VACUUM_ROBOT__BATTERY_EMPTY);
		createEOperation(vacuumRobotEClass, VACUUM_ROBOT___START_CLEANING);
		createEOperation(vacuumRobotEClass, VACUUM_ROBOT___STOP_CLEANING);
		createEOperation(vacuumRobotEClass, VACUUM_ROBOT___MOVE_TO_ROOM__ROOM);

		roomEClass = createEClass(ROOM);
		createEReference(roomEClass, ROOM__ENV);
		createEAttribute(roomEClass, ROOM__HAS_CHARGING_STATION);
		createEAttribute(roomEClass, ROOM__DIRTY);
		createEReference(roomEClass, ROOM__ADJACENT_ROOMS);
		createEOperation(roomEClass, ROOM___MAKE_DIRTY);
		createEOperation(roomEClass, ROOM___CLEAN);

		doorEClass = createEClass(DOOR);
		createEReference(doorEClass, DOOR__CONNECTED_ROOMS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		EcorePackage theEcorePackage = (EcorePackage)EPackage.Registry.INSTANCE.getEPackage(EcorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		vacuumWorldEClass.getESuperTypes().add(this.getNamedElement());
		environmentEClass.getESuperTypes().add(this.getNamedElement());
		vacuumRobotEClass.getESuperTypes().add(this.getNamedElement());
		roomEClass.getESuperTypes().add(this.getNamedElement());

		// Initialize classes, features, and operations; add parameters
		initEClass(namedElementEClass, NamedElement.class, "NamedElement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getNamedElement_Name(), theEcorePackage.getEString(), "name", null, 1, 1, NamedElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(vacuumWorldEClass, VacuumWorld.class, "VacuumWorld", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVacuumWorld_Environment(), this.getEnvironment(), null, "environment", null, 1, 1, VacuumWorld.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getVacuumWorld_VacuumRobot(), this.getVacuumRobot(), null, "vacuumRobot", null, 1, 1, VacuumWorld.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(environmentEClass, Environment.class, "Environment", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEnvironment_MinDirtPerIteration(), theEcorePackage.getEInt(), "minDirtPerIteration", null, 1, 1, Environment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEnvironment_MaxDirtPerIteration(), theEcorePackage.getEInt(), "maxDirtPerIteration", null, 1, 1, Environment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEnvironment_Rooms(), this.getRoom(), this.getRoom_Env(), "rooms", null, 0, -1, Environment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEnvironment_Doors(), this.getDoor(), null, "doors", null, 0, -1, Environment.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEnvironment_NumDirtyRooms(), theEcorePackage.getEInt(), "numDirtyRooms", null, 0, 1, Environment.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEClass(vacuumRobotEClass, VacuumRobot.class, "VacuumRobot", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVacuumRobot_Env(), this.getEnvironment(), null, "env", null, 1, 1, VacuumRobot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getVacuumRobot_Location(), this.getRoom(), null, "location", null, 1, 1, VacuumRobot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVacuumRobot_BatteryCapacity(), theEcorePackage.getEInt(), "batteryCapacity", null, 1, 1, VacuumRobot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVacuumRobot_BatteryLevel(), theEcorePackage.getEInt(), "batteryLevel", null, 1, 1, VacuumRobot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVacuumRobot_PerformCleaning(), theEcorePackage.getEBoolean(), "performCleaning", null, 1, 1, VacuumRobot.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getVacuumRobot_BatteryEmpty(), theEcorePackage.getEBoolean(), "batteryEmpty", null, 0, 1, VacuumRobot.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEOperation(getVacuumRobot__StartCleaning(), null, "startCleaning", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getVacuumRobot__StopCleaning(), null, "stopCleaning", 0, 1, !IS_UNIQUE, IS_ORDERED);

		EOperation op = initEOperation(getVacuumRobot__MoveToRoom__Room(), null, "moveToRoom", 0, 1, !IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getRoom(), "room", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(roomEClass, Room.class, "Room", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRoom_Env(), this.getEnvironment(), this.getEnvironment_Rooms(), "env", null, 0, 1, Room.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRoom_HasChargingStation(), theEcorePackage.getEBoolean(), "hasChargingStation", null, 1, 1, Room.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRoom_Dirty(), theEcorePackage.getEBoolean(), "dirty", null, 1, 1, Room.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, !IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRoom_AdjacentRooms(), this.getRoom(), null, "adjacentRooms", null, 0, -1, Room.class, IS_TRANSIENT, IS_VOLATILE, !IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, IS_DERIVED, IS_ORDERED);

		initEOperation(getRoom__MakeDirty(), null, "makeDirty", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEOperation(getRoom__Clean(), null, "clean", 0, 1, !IS_UNIQUE, IS_ORDERED);

		initEClass(doorEClass, Door.class, "Door", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDoor_ConnectedRooms(), this.getRoom(), null, "connectedRooms", null, 2, 2, Door.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //VacuumworldPackageImpl
