/**
 */
package vacuumworld;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vacuum World</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link vacuumworld.VacuumWorld#getEnvironment <em>Environment</em>}</li>
 *   <li>{@link vacuumworld.VacuumWorld#getVacuumRobot <em>Vacuum Robot</em>}</li>
 * </ul>
 *
 * @see vacuumworld.VacuumworldPackage#getVacuumWorld()
 * @model
 * @generated
 */
public interface VacuumWorld extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Environment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Environment</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Environment</em>' containment reference.
	 * @see #setEnvironment(Environment)
	 * @see vacuumworld.VacuumworldPackage#getVacuumWorld_Environment()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Environment getEnvironment();

	/**
	 * Sets the value of the '{@link vacuumworld.VacuumWorld#getEnvironment <em>Environment</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Environment</em>' containment reference.
	 * @see #getEnvironment()
	 * @generated
	 */
	void setEnvironment(Environment value);

	/**
	 * Returns the value of the '<em><b>Vacuum Robot</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vacuum Robot</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vacuum Robot</em>' containment reference.
	 * @see #setVacuumRobot(VacuumRobot)
	 * @see vacuumworld.VacuumworldPackage#getVacuumWorld_VacuumRobot()
	 * @model containment="true" required="true"
	 * @generated
	 */
	VacuumRobot getVacuumRobot();

	/**
	 * Sets the value of the '{@link vacuumworld.VacuumWorld#getVacuumRobot <em>Vacuum Robot</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Vacuum Robot</em>' containment reference.
	 * @see #getVacuumRobot()
	 * @generated
	 */
	void setVacuumRobot(VacuumRobot value);

} // VacuumWorld
