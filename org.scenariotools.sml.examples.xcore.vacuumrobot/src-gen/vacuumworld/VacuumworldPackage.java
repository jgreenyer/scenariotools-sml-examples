/**
 */
package vacuumworld;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see vacuumworld.VacuumworldFactory
 * @model kind="package"
 * @generated
 */
public interface VacuumworldPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "vacuumworld";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "vacuumworld";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "vacuumworld";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	VacuumworldPackage eINSTANCE = vacuumworld.impl.VacuumworldPackageImpl.init();

	/**
	 * The meta object id for the '{@link vacuumworld.impl.NamedElementImpl <em>Named Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see vacuumworld.impl.NamedElementImpl
	 * @see vacuumworld.impl.VacuumworldPackageImpl#getNamedElement()
	 * @generated
	 */
	int NAMED_ELEMENT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT__NAME = 0;

	/**
	 * The number of structural features of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Named Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NAMED_ELEMENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link vacuumworld.impl.VacuumWorldImpl <em>Vacuum World</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see vacuumworld.impl.VacuumWorldImpl
	 * @see vacuumworld.impl.VacuumworldPackageImpl#getVacuumWorld()
	 * @generated
	 */
	int VACUUM_WORLD = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_WORLD__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Environment</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_WORLD__ENVIRONMENT = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Vacuum Robot</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_WORLD__VACUUM_ROBOT = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Vacuum World</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_WORLD_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>Vacuum World</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_WORLD_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link vacuumworld.impl.EnvironmentImpl <em>Environment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see vacuumworld.impl.EnvironmentImpl
	 * @see vacuumworld.impl.VacuumworldPackageImpl#getEnvironment()
	 * @generated
	 */
	int ENVIRONMENT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Min Dirt Per Iteration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT__MIN_DIRT_PER_ITERATION = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Max Dirt Per Iteration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT__MAX_DIRT_PER_ITERATION = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Rooms</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT__ROOMS = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Doors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT__DOORS = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Num Dirty Rooms</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT__NUM_DIRTY_ROOMS = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Environment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>Environment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENVIRONMENT_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link vacuumworld.impl.VacuumRobotImpl <em>Vacuum Robot</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see vacuumworld.impl.VacuumRobotImpl
	 * @see vacuumworld.impl.VacuumworldPackageImpl#getVacuumRobot()
	 * @generated
	 */
	int VACUUM_ROBOT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Env</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT__ENV = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Location</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT__LOCATION = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Battery Capacity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT__BATTERY_CAPACITY = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Battery Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT__BATTERY_LEVEL = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Perform Cleaning</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT__PERFORM_CLEANING = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Battery Empty</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT__BATTERY_EMPTY = NAMED_ELEMENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Vacuum Robot</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>Start Cleaning</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT___START_CLEANING = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Stop Cleaning</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT___STOP_CLEANING = NAMED_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Move To Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT___MOVE_TO_ROOM__ROOM = NAMED_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>Vacuum Robot</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VACUUM_ROBOT_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link vacuumworld.impl.RoomImpl <em>Room</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see vacuumworld.impl.RoomImpl
	 * @see vacuumworld.impl.VacuumworldPackageImpl#getRoom()
	 * @generated
	 */
	int ROOM = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__NAME = NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Env</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__ENV = NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Has Charging Station</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__HAS_CHARGING_STATION = NAMED_ELEMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Dirty</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__DIRTY = NAMED_ELEMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Adjacent Rooms</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__ADJACENT_ROOMS = NAMED_ELEMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_FEATURE_COUNT = NAMED_ELEMENT_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Make Dirty</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM___MAKE_DIRTY = NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Clean</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM___CLEAN = NAMED_ELEMENT_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_OPERATION_COUNT = NAMED_ELEMENT_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link vacuumworld.impl.DoorImpl <em>Door</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see vacuumworld.impl.DoorImpl
	 * @see vacuumworld.impl.VacuumworldPackageImpl#getDoor()
	 * @generated
	 */
	int DOOR = 5;

	/**
	 * The feature id for the '<em><b>Connected Rooms</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOOR__CONNECTED_ROOMS = 0;

	/**
	 * The number of structural features of the '<em>Door</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOOR_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>Door</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DOOR_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link vacuumworld.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Named Element</em>'.
	 * @see vacuumworld.NamedElement
	 * @generated
	 */
	EClass getNamedElement();

	/**
	 * Returns the meta object for the attribute '{@link vacuumworld.NamedElement#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see vacuumworld.NamedElement#getName()
	 * @see #getNamedElement()
	 * @generated
	 */
	EAttribute getNamedElement_Name();

	/**
	 * Returns the meta object for class '{@link vacuumworld.VacuumWorld <em>Vacuum World</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vacuum World</em>'.
	 * @see vacuumworld.VacuumWorld
	 * @generated
	 */
	EClass getVacuumWorld();

	/**
	 * Returns the meta object for the containment reference '{@link vacuumworld.VacuumWorld#getEnvironment <em>Environment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Environment</em>'.
	 * @see vacuumworld.VacuumWorld#getEnvironment()
	 * @see #getVacuumWorld()
	 * @generated
	 */
	EReference getVacuumWorld_Environment();

	/**
	 * Returns the meta object for the containment reference '{@link vacuumworld.VacuumWorld#getVacuumRobot <em>Vacuum Robot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Vacuum Robot</em>'.
	 * @see vacuumworld.VacuumWorld#getVacuumRobot()
	 * @see #getVacuumWorld()
	 * @generated
	 */
	EReference getVacuumWorld_VacuumRobot();

	/**
	 * Returns the meta object for class '{@link vacuumworld.Environment <em>Environment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Environment</em>'.
	 * @see vacuumworld.Environment
	 * @generated
	 */
	EClass getEnvironment();

	/**
	 * Returns the meta object for the attribute '{@link vacuumworld.Environment#getMinDirtPerIteration <em>Min Dirt Per Iteration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Dirt Per Iteration</em>'.
	 * @see vacuumworld.Environment#getMinDirtPerIteration()
	 * @see #getEnvironment()
	 * @generated
	 */
	EAttribute getEnvironment_MinDirtPerIteration();

	/**
	 * Returns the meta object for the attribute '{@link vacuumworld.Environment#getMaxDirtPerIteration <em>Max Dirt Per Iteration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Dirt Per Iteration</em>'.
	 * @see vacuumworld.Environment#getMaxDirtPerIteration()
	 * @see #getEnvironment()
	 * @generated
	 */
	EAttribute getEnvironment_MaxDirtPerIteration();

	/**
	 * Returns the meta object for the containment reference list '{@link vacuumworld.Environment#getRooms <em>Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rooms</em>'.
	 * @see vacuumworld.Environment#getRooms()
	 * @see #getEnvironment()
	 * @generated
	 */
	EReference getEnvironment_Rooms();

	/**
	 * Returns the meta object for the containment reference list '{@link vacuumworld.Environment#getDoors <em>Doors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Doors</em>'.
	 * @see vacuumworld.Environment#getDoors()
	 * @see #getEnvironment()
	 * @generated
	 */
	EReference getEnvironment_Doors();

	/**
	 * Returns the meta object for the attribute '{@link vacuumworld.Environment#getNumDirtyRooms <em>Num Dirty Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num Dirty Rooms</em>'.
	 * @see vacuumworld.Environment#getNumDirtyRooms()
	 * @see #getEnvironment()
	 * @generated
	 */
	EAttribute getEnvironment_NumDirtyRooms();

	/**
	 * Returns the meta object for class '{@link vacuumworld.VacuumRobot <em>Vacuum Robot</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vacuum Robot</em>'.
	 * @see vacuumworld.VacuumRobot
	 * @generated
	 */
	EClass getVacuumRobot();

	/**
	 * Returns the meta object for the reference '{@link vacuumworld.VacuumRobot#getEnv <em>Env</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Env</em>'.
	 * @see vacuumworld.VacuumRobot#getEnv()
	 * @see #getVacuumRobot()
	 * @generated
	 */
	EReference getVacuumRobot_Env();

	/**
	 * Returns the meta object for the reference '{@link vacuumworld.VacuumRobot#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Location</em>'.
	 * @see vacuumworld.VacuumRobot#getLocation()
	 * @see #getVacuumRobot()
	 * @generated
	 */
	EReference getVacuumRobot_Location();

	/**
	 * Returns the meta object for the attribute '{@link vacuumworld.VacuumRobot#getBatteryCapacity <em>Battery Capacity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Battery Capacity</em>'.
	 * @see vacuumworld.VacuumRobot#getBatteryCapacity()
	 * @see #getVacuumRobot()
	 * @generated
	 */
	EAttribute getVacuumRobot_BatteryCapacity();

	/**
	 * Returns the meta object for the attribute '{@link vacuumworld.VacuumRobot#getBatteryLevel <em>Battery Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Battery Level</em>'.
	 * @see vacuumworld.VacuumRobot#getBatteryLevel()
	 * @see #getVacuumRobot()
	 * @generated
	 */
	EAttribute getVacuumRobot_BatteryLevel();

	/**
	 * Returns the meta object for the attribute '{@link vacuumworld.VacuumRobot#isPerformCleaning <em>Perform Cleaning</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Perform Cleaning</em>'.
	 * @see vacuumworld.VacuumRobot#isPerformCleaning()
	 * @see #getVacuumRobot()
	 * @generated
	 */
	EAttribute getVacuumRobot_PerformCleaning();

	/**
	 * Returns the meta object for the attribute '{@link vacuumworld.VacuumRobot#isBatteryEmpty <em>Battery Empty</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Battery Empty</em>'.
	 * @see vacuumworld.VacuumRobot#isBatteryEmpty()
	 * @see #getVacuumRobot()
	 * @generated
	 */
	EAttribute getVacuumRobot_BatteryEmpty();

	/**
	 * Returns the meta object for the '{@link vacuumworld.VacuumRobot#startCleaning() <em>Start Cleaning</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Start Cleaning</em>' operation.
	 * @see vacuumworld.VacuumRobot#startCleaning()
	 * @generated
	 */
	EOperation getVacuumRobot__StartCleaning();

	/**
	 * Returns the meta object for the '{@link vacuumworld.VacuumRobot#stopCleaning() <em>Stop Cleaning</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Stop Cleaning</em>' operation.
	 * @see vacuumworld.VacuumRobot#stopCleaning()
	 * @generated
	 */
	EOperation getVacuumRobot__StopCleaning();

	/**
	 * Returns the meta object for the '{@link vacuumworld.VacuumRobot#moveToRoom(vacuumworld.Room) <em>Move To Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Move To Room</em>' operation.
	 * @see vacuumworld.VacuumRobot#moveToRoom(vacuumworld.Room)
	 * @generated
	 */
	EOperation getVacuumRobot__MoveToRoom__Room();

	/**
	 * Returns the meta object for class '{@link vacuumworld.Room <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room</em>'.
	 * @see vacuumworld.Room
	 * @generated
	 */
	EClass getRoom();

	/**
	 * Returns the meta object for the container reference '{@link vacuumworld.Room#getEnv <em>Env</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Env</em>'.
	 * @see vacuumworld.Room#getEnv()
	 * @see #getRoom()
	 * @generated
	 */
	EReference getRoom_Env();

	/**
	 * Returns the meta object for the attribute '{@link vacuumworld.Room#isHasChargingStation <em>Has Charging Station</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Has Charging Station</em>'.
	 * @see vacuumworld.Room#isHasChargingStation()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_HasChargingStation();

	/**
	 * Returns the meta object for the attribute '{@link vacuumworld.Room#isDirty <em>Dirty</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Dirty</em>'.
	 * @see vacuumworld.Room#isDirty()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_Dirty();

	/**
	 * Returns the meta object for the reference list '{@link vacuumworld.Room#getAdjacentRooms <em>Adjacent Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Adjacent Rooms</em>'.
	 * @see vacuumworld.Room#getAdjacentRooms()
	 * @see #getRoom()
	 * @generated
	 */
	EReference getRoom_AdjacentRooms();

	/**
	 * Returns the meta object for the '{@link vacuumworld.Room#makeDirty() <em>Make Dirty</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Make Dirty</em>' operation.
	 * @see vacuumworld.Room#makeDirty()
	 * @generated
	 */
	EOperation getRoom__MakeDirty();

	/**
	 * Returns the meta object for the '{@link vacuumworld.Room#clean() <em>Clean</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Clean</em>' operation.
	 * @see vacuumworld.Room#clean()
	 * @generated
	 */
	EOperation getRoom__Clean();

	/**
	 * Returns the meta object for class '{@link vacuumworld.Door <em>Door</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Door</em>'.
	 * @see vacuumworld.Door
	 * @generated
	 */
	EClass getDoor();

	/**
	 * Returns the meta object for the reference list '{@link vacuumworld.Door#getConnectedRooms <em>Connected Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Connected Rooms</em>'.
	 * @see vacuumworld.Door#getConnectedRooms()
	 * @see #getDoor()
	 * @generated
	 */
	EReference getDoor_ConnectedRooms();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	VacuumworldFactory getVacuumworldFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link vacuumworld.impl.NamedElementImpl <em>Named Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see vacuumworld.impl.NamedElementImpl
		 * @see vacuumworld.impl.VacuumworldPackageImpl#getNamedElement()
		 * @generated
		 */
		EClass NAMED_ELEMENT = eINSTANCE.getNamedElement();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NAMED_ELEMENT__NAME = eINSTANCE.getNamedElement_Name();

		/**
		 * The meta object literal for the '{@link vacuumworld.impl.VacuumWorldImpl <em>Vacuum World</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see vacuumworld.impl.VacuumWorldImpl
		 * @see vacuumworld.impl.VacuumworldPackageImpl#getVacuumWorld()
		 * @generated
		 */
		EClass VACUUM_WORLD = eINSTANCE.getVacuumWorld();

		/**
		 * The meta object literal for the '<em><b>Environment</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VACUUM_WORLD__ENVIRONMENT = eINSTANCE.getVacuumWorld_Environment();

		/**
		 * The meta object literal for the '<em><b>Vacuum Robot</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VACUUM_WORLD__VACUUM_ROBOT = eINSTANCE.getVacuumWorld_VacuumRobot();

		/**
		 * The meta object literal for the '{@link vacuumworld.impl.EnvironmentImpl <em>Environment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see vacuumworld.impl.EnvironmentImpl
		 * @see vacuumworld.impl.VacuumworldPackageImpl#getEnvironment()
		 * @generated
		 */
		EClass ENVIRONMENT = eINSTANCE.getEnvironment();

		/**
		 * The meta object literal for the '<em><b>Min Dirt Per Iteration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENVIRONMENT__MIN_DIRT_PER_ITERATION = eINSTANCE.getEnvironment_MinDirtPerIteration();

		/**
		 * The meta object literal for the '<em><b>Max Dirt Per Iteration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENVIRONMENT__MAX_DIRT_PER_ITERATION = eINSTANCE.getEnvironment_MaxDirtPerIteration();

		/**
		 * The meta object literal for the '<em><b>Rooms</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENVIRONMENT__ROOMS = eINSTANCE.getEnvironment_Rooms();

		/**
		 * The meta object literal for the '<em><b>Doors</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ENVIRONMENT__DOORS = eINSTANCE.getEnvironment_Doors();

		/**
		 * The meta object literal for the '<em><b>Num Dirty Rooms</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ENVIRONMENT__NUM_DIRTY_ROOMS = eINSTANCE.getEnvironment_NumDirtyRooms();

		/**
		 * The meta object literal for the '{@link vacuumworld.impl.VacuumRobotImpl <em>Vacuum Robot</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see vacuumworld.impl.VacuumRobotImpl
		 * @see vacuumworld.impl.VacuumworldPackageImpl#getVacuumRobot()
		 * @generated
		 */
		EClass VACUUM_ROBOT = eINSTANCE.getVacuumRobot();

		/**
		 * The meta object literal for the '<em><b>Env</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VACUUM_ROBOT__ENV = eINSTANCE.getVacuumRobot_Env();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VACUUM_ROBOT__LOCATION = eINSTANCE.getVacuumRobot_Location();

		/**
		 * The meta object literal for the '<em><b>Battery Capacity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VACUUM_ROBOT__BATTERY_CAPACITY = eINSTANCE.getVacuumRobot_BatteryCapacity();

		/**
		 * The meta object literal for the '<em><b>Battery Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VACUUM_ROBOT__BATTERY_LEVEL = eINSTANCE.getVacuumRobot_BatteryLevel();

		/**
		 * The meta object literal for the '<em><b>Perform Cleaning</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VACUUM_ROBOT__PERFORM_CLEANING = eINSTANCE.getVacuumRobot_PerformCleaning();

		/**
		 * The meta object literal for the '<em><b>Battery Empty</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute VACUUM_ROBOT__BATTERY_EMPTY = eINSTANCE.getVacuumRobot_BatteryEmpty();

		/**
		 * The meta object literal for the '<em><b>Start Cleaning</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation VACUUM_ROBOT___START_CLEANING = eINSTANCE.getVacuumRobot__StartCleaning();

		/**
		 * The meta object literal for the '<em><b>Stop Cleaning</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation VACUUM_ROBOT___STOP_CLEANING = eINSTANCE.getVacuumRobot__StopCleaning();

		/**
		 * The meta object literal for the '<em><b>Move To Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation VACUUM_ROBOT___MOVE_TO_ROOM__ROOM = eINSTANCE.getVacuumRobot__MoveToRoom__Room();

		/**
		 * The meta object literal for the '{@link vacuumworld.impl.RoomImpl <em>Room</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see vacuumworld.impl.RoomImpl
		 * @see vacuumworld.impl.VacuumworldPackageImpl#getRoom()
		 * @generated
		 */
		EClass ROOM = eINSTANCE.getRoom();

		/**
		 * The meta object literal for the '<em><b>Env</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM__ENV = eINSTANCE.getRoom_Env();

		/**
		 * The meta object literal for the '<em><b>Has Charging Station</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__HAS_CHARGING_STATION = eINSTANCE.getRoom_HasChargingStation();

		/**
		 * The meta object literal for the '<em><b>Dirty</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__DIRTY = eINSTANCE.getRoom_Dirty();

		/**
		 * The meta object literal for the '<em><b>Adjacent Rooms</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM__ADJACENT_ROOMS = eINSTANCE.getRoom_AdjacentRooms();

		/**
		 * The meta object literal for the '<em><b>Make Dirty</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM___MAKE_DIRTY = eINSTANCE.getRoom__MakeDirty();

		/**
		 * The meta object literal for the '<em><b>Clean</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROOM___CLEAN = eINSTANCE.getRoom__Clean();

		/**
		 * The meta object literal for the '{@link vacuumworld.impl.DoorImpl <em>Door</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see vacuumworld.impl.DoorImpl
		 * @see vacuumworld.impl.VacuumworldPackageImpl#getDoor()
		 * @generated
		 */
		EClass DOOR = eINSTANCE.getDoor();

		/**
		 * The meta object literal for the '<em><b>Connected Rooms</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DOOR__CONNECTED_ROOMS = eINSTANCE.getDoor_ConnectedRooms();

	}

} //VacuumworldPackage
