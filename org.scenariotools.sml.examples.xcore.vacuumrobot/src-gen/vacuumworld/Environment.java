/**
 */
package vacuumworld;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Environment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link vacuumworld.Environment#getMinDirtPerIteration <em>Min Dirt Per Iteration</em>}</li>
 *   <li>{@link vacuumworld.Environment#getMaxDirtPerIteration <em>Max Dirt Per Iteration</em>}</li>
 *   <li>{@link vacuumworld.Environment#getRooms <em>Rooms</em>}</li>
 *   <li>{@link vacuumworld.Environment#getDoors <em>Doors</em>}</li>
 *   <li>{@link vacuumworld.Environment#getNumDirtyRooms <em>Num Dirty Rooms</em>}</li>
 * </ul>
 *
 * @see vacuumworld.VacuumworldPackage#getEnvironment()
 * @model
 * @generated
 */
public interface Environment extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Min Dirt Per Iteration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Dirt Per Iteration</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Dirt Per Iteration</em>' attribute.
	 * @see #setMinDirtPerIteration(int)
	 * @see vacuumworld.VacuumworldPackage#getEnvironment_MinDirtPerIteration()
	 * @model unique="false" required="true"
	 * @generated
	 */
	int getMinDirtPerIteration();

	/**
	 * Sets the value of the '{@link vacuumworld.Environment#getMinDirtPerIteration <em>Min Dirt Per Iteration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Dirt Per Iteration</em>' attribute.
	 * @see #getMinDirtPerIteration()
	 * @generated
	 */
	void setMinDirtPerIteration(int value);

	/**
	 * Returns the value of the '<em><b>Max Dirt Per Iteration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Dirt Per Iteration</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Dirt Per Iteration</em>' attribute.
	 * @see #setMaxDirtPerIteration(int)
	 * @see vacuumworld.VacuumworldPackage#getEnvironment_MaxDirtPerIteration()
	 * @model unique="false" required="true"
	 * @generated
	 */
	int getMaxDirtPerIteration();

	/**
	 * Sets the value of the '{@link vacuumworld.Environment#getMaxDirtPerIteration <em>Max Dirt Per Iteration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Dirt Per Iteration</em>' attribute.
	 * @see #getMaxDirtPerIteration()
	 * @generated
	 */
	void setMaxDirtPerIteration(int value);

	/**
	 * Returns the value of the '<em><b>Rooms</b></em>' containment reference list.
	 * The list contents are of type {@link vacuumworld.Room}.
	 * It is bidirectional and its opposite is '{@link vacuumworld.Room#getEnv <em>Env</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rooms</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rooms</em>' containment reference list.
	 * @see vacuumworld.VacuumworldPackage#getEnvironment_Rooms()
	 * @see vacuumworld.Room#getEnv
	 * @model opposite="env" containment="true"
	 * @generated
	 */
	EList<Room> getRooms();

	/**
	 * Returns the value of the '<em><b>Doors</b></em>' containment reference list.
	 * The list contents are of type {@link vacuumworld.Door}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Doors</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Doors</em>' containment reference list.
	 * @see vacuumworld.VacuumworldPackage#getEnvironment_Doors()
	 * @model containment="true"
	 * @generated
	 */
	EList<Door> getDoors();

	/**
	 * Returns the value of the '<em><b>Num Dirty Rooms</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Num Dirty Rooms</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Num Dirty Rooms</em>' attribute.
	 * @see vacuumworld.VacuumworldPackage#getEnvironment_NumDirtyRooms()
	 * @model unique="false" transient="true" changeable="false" volatile="true" derived="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel get='<%org.eclipse.emf.common.util.EList%><<%vacuumworld.Room%>> _rooms = this.getRooms();\nfinal <%org.eclipse.xtext.xbase.lib.Functions.Function1%><<%vacuumworld.Room%>, <%java.lang.Boolean%>> _function = new <%org.eclipse.xtext.xbase.lib.Functions.Function1%><<%vacuumworld.Room%>, <%java.lang.Boolean%>>()\n{\n\tpublic <%java.lang.Boolean%> apply(final <%vacuumworld.Room%> room)\n\t{\n\t\treturn <%java.lang.Boolean%>.valueOf(room.isDirty());\n\t}\n};\n<%java.lang.Iterable%><<%vacuumworld.Room%>> _filter = <%org.eclipse.xtext.xbase.lib.IterableExtensions%>.<<%vacuumworld.Room%>>filter(_rooms, _function);\nreturn <%org.eclipse.xtext.xbase.lib.IterableExtensions%>.size(_filter);'"
	 * @generated
	 */
	int getNumDirtyRooms();

} // Environment
