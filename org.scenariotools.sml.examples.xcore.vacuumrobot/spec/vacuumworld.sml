import "../model/vacuumworld.xcore"

specification VacuumWorld {
	domain vacuumworld
	
	controllable {
		VacuumRobot
	}
	
	non-spontaneous events {
		VacuumRobot.startCleaning
	}
	
	collaboration RoomsGetDirty {
		static role Environment env
		static role VacuumRobot robot
		dynamic role Room room
		
		assumption scenario RoomsCannotGetEvenMoreDirty {
			env -> room.makeDirty()
			wait [!room.dirty]
		} constraints [
			forbidden env -> room.makeDirty()
		]
		
		assumption scenario StartCleaning {
			env -> room.makeDirty()
			interrupt [env.numDirtyRooms < env.minDirtPerIteration]
			env -> robot.startCleaning()
		}
		
		assumption scenario ForceStartCleaning {
			env -> room.makeDirty()
			interrupt [env.numDirtyRooms < env.maxDirtPerIteration]
			committed env -> robot.startCleaning()
		}
		
		assumption scenario RestartCleaning {
			robot -> robot.stopCleaning()
			interrupt [env.numDirtyRooms < env.minDirtPerIteration]
			env -> robot.startCleaning()
		}

		assumption scenario ForceRestartCleaning {
			robot -> robot.stopCleaning()
			interrupt [env.numDirtyRooms < env.maxDirtPerIteration]
			committed env -> robot.startCleaning()
		}		
	}
	
	collaboration CleaningIteration {
		static role Environment env
		static role VacuumRobot robot
		dynamic role Room room
		
		guarantee scenario PerformCleaning {
			env -> robot.startCleaning()
			var Room currentRoom
			while [robot.performCleaning] {
				currentRoom = robot.location
				alternative [!robot.batteryEmpty] {
					urgent robot -> robot.moveToRoom(currentRoom.adjacentRooms.any())
				} or {
					urgent robot -> robot.stopCleaning()
				}
			}
		}
		
		// the scenario below is here for technical reasons: can't do robot -> currentRoom.clean() in PerformCleaning
		guarantee scenario CleanCurrentRoom
		bindings [
			room = robot.location
		] {
			alternative {
				env -> robot.startCleaning()
			} or {
				robot -> robot.moveToRoom(*)
			}
			interrupt [robot.batteryEmpty || !room.dirty]
			urgent robot -> room.clean()
		} constraints [
			interrupt robot -> robot.moveToRoom(*)
			interrupt robot -> robot.stopCleaning()
		]
	}
	
	collaboration Goals {
		static role Environment env
		static role VacuumRobot robot
		dynamic role Room room

		guarantee scenario DirtyRoomsGetCleanedEventually {
			env -> room.makeDirty()
			wait eventually [!room.dirty]
		}

//		dynamic multi role Room anyRoom
//		guarantee scenario DirtyRoomsGetCleanedEventually
//		with dynamic bindings [
//			anyRoom = env.rooms
//		] {
//			robot -> robot.stopCleaning()
//			violation [anyRoom.dirty]
//		}
		
		guarantee scenario VacuumRobotEndsCleaningIterationAtChargingStation
		bindings [
			room = robot.location
		] {
			robot -> robot.stopCleaning()
			violation [!room.hasChargingStation]
		}
	}
}