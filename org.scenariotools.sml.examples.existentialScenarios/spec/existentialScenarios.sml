import "../model/existentialScenarios.ecore"

specification ExistentialScenariosSpecification {
	/*
	 * Refer to a package in the imported ecore model.
	 * Hint: Use ctrl+alt+R to rename packages and classes.
	 */
	domain existentialScenarios

	/* 
	 * Define classes of objects that are controllable
	 * or uncontrollable.
	 */
	controllable {
		B
		
	}
	

	/*
	 * Collaborations describe how objects interact in a certain
	 * context to collectively accomplish some desired functionality.
	 */
	collaboration ExistentialScenariosCollaboration {

		static role A a
		static role B b

		guarantee scenario Scenario1 {
			a -> b.doBThing()
			requested b -> b.doBnotherThing()
		}
		
		guarantee scenario Scenario2 {
			b -> b.doBnotherThing()
			requested b -> a.doAnotherThing()
		}
		
		existential scenario existentialScenarioSuccesful{
			a -> b.doBThing()
			b -> b.doBnotherThing()
			b -> a.doAnotherThing()
		}
		
		existential scenario existentialScenarioFirstSuccesful{
			a -> b.doBThing()
			a -> a.doAnotherThing()
		}
		
	}

}
