import "../../model/highvoltagecoupling.ecore"
 
specification HighvoltagecouplingSpecification {
 
	domain highvoltagecoupling
 
	controllable {
		Controller
	}
	
	non-spontaneous events {
		Controller.unplug
	}
	
	collaboration PlugInAndUnplug {		
		static role Socket socket
		static role Controller controller
 
		/*
		 * When the plug is plugged into the socket, 
		 * then the socket must be locked.
		 */
		guarantee scenario WhenPluggedThenLock{
			socket->controller.plug()
			strict urgent controller->socket.lockSocket()
		}
 
		guarantee scenario SetPlugged{
			socket->controller.plug()
			strict urgent controller->controller.setPlugged(true)
		}		
 
		/*
		 * When the relays are closed, the plug 
		 * must not be unplugged.
		 */
		guarantee scenario UnplugForbiddenWhenRelaysClosed {
			socket->controller.unplug()
			violation [controller.relaysClosed]
		}
 
		assumption scenario NoUnplugBetweenLockAndUnlock{
			controller->socket.lockSocket()
			controller->socket.unlockSocket()
		}constraints[
			forbidden socket->controller.unplug()
		]
		
		/*
		 * Perform unplugging
		 */
		guarantee scenario SetUnplugged {
			socket->controller.unplug()
			strict urgent controller->controller.setPlugged(false)
		}
		
		/*
		 * user has to unplug plug before plugging it back in
		 */
		assumption scenario NoRepeatedPluggingIn{
			socket->controller.plug()
			socket->controller.unplug()
		}constraints[
			forbidden socket->controller.plug()
		]

		/*
		 * user eventually unplugs plug
		 */
		assumption scenario UserUnplugsPlugAfterPressingStop {
			socket->controller.plug()
			socket->controller.unplug()
		}		
	}
	
	collaboration SocketControl {
		static role Socket socket
		static role StopButton stopButton
		static role Controller controller

		/*
		 * perform locking of socket
		 */
		guarantee scenario SetLocked{
			controller->socket.lockSocket()
			strict urgent controller->controller.setSocketLocked(true)
		}		
 
		/*
		 * Unlock socket when stopping the flow of electricity
		 */
		guarantee scenario ReleaseLockWhenStopping {
			stopButton->controller.stop()
			interrupt [!controller.socketLocked]
			strict urgent controller->socket.unlockSocket()
		}

		guarantee scenario SetUnlocked {
			controller->socket.unlockSocket()
			strict urgent controller->controller.setSocketLocked(false)			
		}		
	}
	
	collaboration RelaysControl {
		static role Relays relays
		static role StartButton startButton
		static role StopButton stopButton
		static role Controller controller

		/*
		 * When the start-button is pressed, 
		 * then the relays have to be closed.
		 */
		guarantee scenario WhenStartPressedThenCloseContact{
			startButton->controller.start()
			interrupt [!controller.plugged || !controller.socketLocked || controller.relaysClosed]
			strict urgent controller->relays.closeRelays()
		}
 
		/*
		 * When the plug is not plugged or not 
		 * locked then the relays must not be closed.
		 * -> Modeled as an anti-scenario: closing the relays
		 *    leads to a violation if not plugged or not locked
		 */
		guarantee scenario OnlyCloseRelaysWhenPlugPluggedAndLocked {
			controller->relays.closeRelays()
			violation [!controller.plugged || !controller.socketLocked]
		}
		
		guarantee scenario SetRelaysClosed{
			controller->relays.closeRelays()
			strict urgent controller->controller.setRelaysClosed(true)
		}		
		
		/*
		 * Stop the flow of electricity to when pressing the
		 * stop button to allow unplugging
		 */
		guarantee scenario OpenRelaysWhenStopping {
			stopButton->controller.stop()
			interrupt [!controller.relaysClosed]
			strict urgent controller->relays.openRelays()
		}
		
		guarantee scenario SetRelaysOpen {
			controller->relays.openRelays()
			strict urgent controller->controller.setRelaysClosed(false)
		}		
	}
}