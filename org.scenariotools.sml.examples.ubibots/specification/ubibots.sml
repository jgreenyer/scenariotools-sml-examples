import "../model/ubibots.ecore"

specification UbiBots {

	domain ubibots
	domain ubibots.system
	domain ubibots.world

	controllable {
		RoadSystem
		Car
		ObstacleControl
	}

	non-spontaneous events {
		Driver.driveAllowed
		Driver.driveForbidden
		BasicVehicle.drive
		BasicVehicle.stop
		SmartVehicle.driveAllowed
		SmartVehicle.driveForbidden
		SmartVehicle.approachingObstacle
		SmartVehicle.overtakingObstacle
		SmartVehicle.overtakingObstacleFinished
		SmartVehicle.obstacleAreaLeft
		SmartVehicle.approachingNarrowArea
		SmartVehicle.waitBeforeNarrowArea
		SmartVehicle.narrowAreaEntered
		SmartVehicle.narrowAreaLeft
		SmartVehicle.setCurrentLane
	}

	collaboration Car_Drives_On_Road {

		dynamic role Environment env
		dynamic role Driver driver
		dynamic role Car car
		dynamic role Lane currentLane
		dynamic role Lane nextLane

		guarantee scenario carEntersNextLane bindings [
			currentLane = car.currentLane
			nextLane = currentLane.next
			driver = car.driver
		] {
			env -> car.roadSectionEntered()
			strict eventually car -> car.setCurrentLane(nextLane)
		}

	}

	collaboration Car_Driver_Communication {

		dynamic role Environment env
		dynamic role RoadSystem roadsystem
		dynamic role Driver driver
		dynamic role Car car

		guarantee scenario carInformsDriverRoadSectionEntered bindings [
			driver = car.driver
		] {
			env -> car.roadSectionEntered()
			strict urgent car -> driver.nextLaneEntered()
		}

		assumption scenario driverStopsCarWhenDriveForbidden bindings [
			roadsystem = car.roadsystem
			env = roadsystem.environment
		] {
			car -> driver.driveForbidden()
			strict eventually driver -> car.stop()
		} constraints [
			consider env -> car.overtakingObstacle()
			consider env -> car.narrowAreaEntered()
		]

		assumption scenario driverDrivesWhenDriveAllowed bindings [
			roadsystem = car.roadsystem
			env = roadsystem.environment
		] {
			car -> driver.driveAllowed()
			strict eventually driver -> car.drive()
		} constraints [
			consider env -> car.overtakingObstacle()
			consider env -> car.narrowAreaEntered()
		]

	}

	collaboration Car_Meets_Construction {

		dynamic role Environment env
		dynamic role RoadSystem roadsystem
		dynamic role Driver driver
		dynamic role Car car
		dynamic role Construction construction
		dynamic role Lane currentLane
		dynamic role Lane nextLane
		dynamic role Lane oppositeLane
		dynamic role ObstacleControl obstacleControl

		assumption scenario OvertakingOnlyPossibleWhileDriving bindings [
			roadsystem = car.roadsystem
			env = roadsystem.environment
		] {
			car -> driver.driveForbidden()
			car -> driver.driveAllowed()
		} constraints [
			forbidden env -> car.overtakingObstacle()
			forbidden env -> car.narrowAreaEntered()
		]

		// better:
		//	assumption scenario  OvertakingOnlyPossibleWhileDriving bindings [
		//		roadsystem = car.roadsystem
		//		env = roadsystem.environment	
		//		]{
		//			 driver -> car.stop()
		//			 driver -> car.drive()
		//		} constraints[
		//			forbidden env -> car.overtakingObstacle()
		//			forbidden env -> car.narrowAreaEntered()
		//		]
		guarantee scenario carMeetsConstructionAndRequestsDrive bindings [
			driver = car.driver
			currentLane = car.currentLane
			construction = currentLane.obstacle
			obstacleControl = construction.obstacleControl
		] {
			env -> car.approachingObstacle()
			strict urgent car -> driver.approachingObstacle()
			strict urgent car -> obstacleControl.requestDrive()
			alternative [ obstacleControl.narrowAreaFree ] {
				strict urgent obstacleControl -> obstacleControl.setNarrowAreaFree(false)
				strict urgent obstacleControl -> car.driveAllowed()
			} or [ ! obstacleControl.narrowAreaFree ] {
				strict urgent obstacleControl -> car.driveForbidden()
			}
		}

		assumption scenario approachingObstacleOnlyWhenObstacleExists bindings [
			currentLane = car.currentLane
			nextLane = currentLane.next
		] {
			env -> car.roadSectionEntered()
			interrupt [ nextLane.obstacle == null ]
			strict eventually env -> car.approachingObstacle()
			strict eventually env -> car.overtakingObstacle()
			strict eventually env -> car.overtakingObstacleFinished()
			strict eventually env -> car.obstacleAreaLeft()
		}

		assumption scenario approachingNarrowAreaOnlyWhenObstacleExists bindings [
			currentLane = car.currentLane
			nextLane = currentLane.next
			oppositeLane = nextLane.opposite
		] {
			env -> car.roadSectionEntered()
			interrupt [ oppositeLane.obstacle == null ]
			strict eventually env -> car.approachingNarrowArea()
			strict eventually env -> car.narrowAreaEntered()
			strict eventually env -> car.narrowAreaLeft()
		}

		guarantee scenario carDrivesThroughConstruction bindings [
			currentLane = car.currentLane
			construction = currentLane.obstacle
			obstacleControl = construction.obstacleControl
		] {
			env -> car.overtakingObstacle()
			strict urgent car -> construction.setPassingVehicle(car)
			strict env -> car.overtakingObstacleFinished()
			strict env -> car.obstacleAreaLeft()
			strict urgent obstacleControl -> obstacleControl.setNarrowAreaFree(true)
			strict urgent car -> construction.setPassingVehicle(null)
		}

		guarantee scenario carDrivesThroughNarrowArea bindings [
			currentLane = car.currentLane
			oppositeLane = currentLane.opposite
			construction = oppositeLane.obstacle
			obstacleControl = construction.obstacleControl
		] {
			env -> car.narrowAreaEntered()
			strict eventually car -> construction.setPassingVehicle(car)
			strict env -> car.narrowAreaLeft()
//			parallel {
//				strict urgent obstacleControl -> obstacleControl.setNarrowAreaFree(true)
//			} and {
//				strict urgent car -> construction.setPassingVehicle(null)
//			}
			strict urgent obstacleControl -> obstacleControl.setNarrowAreaFree(true)
			strict urgent car -> construction.setPassingVehicle(null)
		}

		guarantee scenario carNeedsToStopAtConstruction bindings [
			driver = car.driver
			roadsystem = car.roadsystem
			env = roadsystem.environment
		] {
			obstacleControl -> car.driveForbidden()
			wait strict [ obstacleControl.narrowAreaFree && obstacleControl.carWaitingBeforeNarrowArea == null]
			strict urgent car -> obstacleControl.setNarrowAreaFree(false)
			strict urgent obstacleControl -> car.driveAllowed()
		} constraints [
			consider env -> car.overtakingObstacle()
		]

		guarantee scenario carNeedsToStopAtNarrowArea bindings [
			driver = car.driver
			roadsystem = car.roadsystem
			env = roadsystem.environment
		] {
			obstacleControl -> car.waitBeforeNarrowArea()
			wait [ obstacleControl.narrowAreaFree ]
			strict urgent car -> obstacleControl.setNarrowAreaFree(false)
			strict urgent obstacleControl -> car.driveAllowed()
			strict urgent car -> obstacleControl.setCarWaitingBeforeNarrowArea(null)
		} constraints [
			forbidden env -> car.narrowAreaEntered()
		]

		guarantee scenario carMeetsNarrowAreaAndRequestsDrive bindings [
			driver = car.driver
			currentLane = car.currentLane
			oppositeLane = currentLane.opposite
			construction = oppositeLane.obstacle
			obstacleControl = construction.obstacleControl
		] {
			env -> car.approachingNarrowArea()
			strict urgent car -> driver.approachingNarrowArea()
			strict urgent car -> obstacleControl.requestDrive()
			strict urgent car -> obstacleControl.setCarWaitingBeforeNarrowArea(car)
			alternative [ obstacleControl.narrowAreaFree ] {
				strict urgent obstacleControl -> obstacleControl.setNarrowAreaFree(false)
				strict urgent obstacleControl -> car.driveAllowed()
				strict urgent car -> obstacleControl.setCarWaitingBeforeNarrowArea(null)
			} or [ ! obstacleControl.narrowAreaFree ] {
				strict urgent obstacleControl -> car.waitBeforeNarrowArea()
			}
		}

		guarantee scenario carInformsDriverDriveAllowed bindings [
			driver = car.driver
		] {
			obstacleControl -> car.driveAllowed()
			strict urgent car -> driver.driveAllowed()
		}

		guarantee scenario carInformsDriverDriveForbidden bindings [
			driver = car.driver
		] {
			obstacleControl -> car.driveForbidden()
			strict urgent car -> driver.driveForbidden()
		}

		guarantee scenario carInformsDriverWaitBeforeNarrowArea bindings [
			driver = car.driver
		] {
			obstacleControl -> car.waitBeforeNarrowArea()
			strict urgent car -> driver.driveForbidden()
		}

	}

}