<topology> / <room configuration> / <realizable>



"full" topology room configuration:
<num. of rooms>_<num. of dirtyable rooms>

"star" topology room configuration:
<num. of rooms>_<num. of dirtyable rooms>

"ring" topology room configuration:
<num. of dirtyable rooms>

"hierarchy" topology room configuration:
<num. of corridors in middle level>_<num. of rooms per corridor>_<num. of dirtyable rooms>
