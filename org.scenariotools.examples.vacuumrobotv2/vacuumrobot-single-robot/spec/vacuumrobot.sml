import "vacuumrobot.ecore"

specification VacuumrobotSpecification {

	domain vacuumrobot
	
	controllable {
		VacuumRobotController
	}

	non-spontaneous events {
		VacuumRobot.setCharge
		VacuumRobot.setRoom
		VacuumRobotController.arrivedInRoom
	}
	
	collaboration VacuumrobotCollaboration {

		static role Environment env
		static role VacuumRobot robot
		static role VacuumRobotController robotCtrl 
		dynamic role DirtyableRoom dirtyableRoom
		dynamic role Room room

		guarantee scenario EventuallyCleanDirtyRoom{
			env->dirtyableRoom.setDirty(true)
			wait eventually [!dirtyableRoom.dirty]
		}

		guarantee scenario MoveRobotWhenSomeRoomGetsDirty {
			env->dirtyableRoom.setDirty(true)
			interrupt [robotCtrl.moving || robot.room == dirtyableRoom]
			var Room currentRoom = robot.room
			strict requested robotCtrl->robot.moveTo(currentRoom.adjacentRooms.any())
			strict requested robotCtrl->robotCtrl.setMoving(true)
		}

		guarantee scenario RobotMustNotRunOutOfCharge{
			env->robot.setCharge(*)
			violation [robot.charge <= 0]
		}

		guarantee scenario RobotMustEventuallyRecharge{
			env->robot.setCharge(*)
			wait eventually [robot.charge == robot.maxCharge]
		}

		guarantee scenario KeepMovingOrStopAtChargingStation {
			robot->robotCtrl.arrivedInRoom(bind room)
			alternative { 
			 	strict requested robotCtrl->robot.moveTo(room.adjacentRooms.any())
			} or [room.hasChargingStation]{
				strict requested robotCtrl->robotCtrl.setMoving(false)
		 	} 
		}
		 
		assumption scenario RobotArrivesInRoom {
			robotCtrl->robot.moveTo(bind room)
			var Room currentRoom = robot.room
			interrupt [!currentRoom.adjacentRooms.contains(room)]
			eventually env->robot.setRoom(room)
			strict committed robot->robotCtrl.arrivedInRoom(room)
		}

		assumption scenario RobotDischargesOrRecharges {
			robot->robotCtrl.arrivedInRoom(bind room)
			alternative [room.hasChargingStation]{
				strict committed env->robot.setCharge(robot.maxCharge)
			} or [!room.hasChargingStation]{
				strict committed env->robot.setCharge(robot.charge - 1)
			}
		}
		 
		assumption scenario RobotCleansDirtyRoom {
			env->robot.setRoom(bind dirtyableRoom)
			alternative [dirtyableRoom.dirty]{
				strict eventually env->dirtyableRoom.setDirty(false)
			}
		}

		assumption scenario RoomCannotGetDirtyWhenRobotIsInIt{
			env->dirtyableRoom.setDirty(true)
			violation [robot.room == dirtyableRoom]
		}
		 
//		assumption scenario DoNothing {
//			env->env.doNothing
//		}
		
	}

}