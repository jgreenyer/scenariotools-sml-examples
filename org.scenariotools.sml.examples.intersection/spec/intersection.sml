import "../model/intersection.ecore"

system specification IntersectionSpecification {

	/*
	 * Refer to a package in the imported ecore model.
	 * Hint: Use ctrl+alt+R to rename packages and classes.
	 */
	domain intersection

	/* 
	 * Define classes of objects that are controllable
	 * or uncontrollable.
	 */
	define IntersectionController as controllable
	define Car as controllable
	define IntersectionPiece as uncontrollable
	define Environment as uncontrollable
	define RoadSection as uncontrollable
	define Road as uncontrollable
	define InfluenceArea as uncontrollable
	define DecisionArea as uncontrollable

	/*
	 * Collaborations describe how objects interact in a certain
	 * context to collectively accomplish some desired functionality.
	 */
	collaboration IntersectionCollaboration {

		static role IntersectionController intersectionController
		dynamic role Environment env
		dynamic multi role Car car
		dynamic multi role IntersectionPiece intersectionPiece
		dynamic multi role InfluenceArea influenceArea
		dynamic multi role DecisionArea decisionArea

		
		//specification scenario carConnectsAndDeconnects {
			//message car -> intersectionController.connectedCars(car)
			//message car -> intersectionController.removeConnectedCars(car)
		
		
		specification scenario carCrossesIntersection 
//		with dynamic bindings[
//			bind intersectionPiece to intersectionController.intersectionPieces
//		]
		{
			message intersectionPiece -> car.setCurrentRoadSection(intersectionPiece)
			requested wait until[car.currentRoadSection == car.destinationRoadSection]
			message requested intersectionController -> intersectionPiece.setFree(true)
		}
		
		specification scenario intersectionHasToBeFree 
//		with dynamic bindings[
//			bind intersectionPiece to intersectionController.intersectionPieces
//		]
		{
			message intersectionPiece -> car.setCurrentRoadSection(intersectionPiece)
			violation if [!intersectionPiece.free]
		}
		
		specification scenario blockIntersection 
		with dynamic bindings[
			bind intersectionPiece to intersectionController.intersectionPieces
		]
		{
			message intersectionController -> car.drive()
			message strict requested intersectionController -> intersectionPiece.setFree(false)			
		}
		
		specification scenario requestDecision 
//		with dynamic bindings[
//			bind intersectionPiece to intersectionController.intersectionPieces
//		]
		{
			//How can you say Environment and not influenceArea
			message decisionArea -> car.setCurrentRoadSection(decisionArea)
			message strict requested car -> intersectionController.requestCrossing()
		}
		
		specification scenario giveDecision
		with dynamic bindings[
			bind intersectionPiece to intersectionController.intersectionPieces
		]
		{
			message car -> intersectionController.requestCrossing()
			alternative if[intersectionPiece.free]
			{
				message requested intersectionController -> car.drive()
			}or if[!intersectionPiece.free]
			{
				message requested intersectionController -> car.stop()
				message strict requested car -> intersectionController.waitingCars.add(car)
				
			}
		}
		
		specification scenario letWaitingCarsDrive
		with dynamic bindings[
			bind car to intersectionController.waitingCars
		]
		{
			message intersectionController -> intersectionPiece.setFree(true)
			interrupt if[intersectionController.waitingCars.isEmpty() | car != intersectionController.waitingCars.first()]
			message intersectionController -> car.drive() 
		}
		
		
		

	}

}
