package pi2go.runtime;

import vacuumrobotspecification.pi2go.runtime.observers.RobotObserver;
import vacuumrobotspecification.runtime.DistributedRunconfig_Robot;

public class DistributedRunconfig_Robot_PI2GO extends DistributedRunconfig_Robot {

	@Override
	protected void registerObservers() {
		// Add Observers here.
		// addScenarioObserver(scenarioObserver);
		// addScenarioObserver(new RobotObserver());
		addScenarioObserver(new RobotObserver(getAdapter()));
	}

}
