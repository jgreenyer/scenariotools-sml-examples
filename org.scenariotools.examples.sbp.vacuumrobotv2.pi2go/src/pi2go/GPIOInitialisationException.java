package pi2go;

public class GPIOInitialisationException extends RuntimeException {
	/**
	 * auto generated serialVersionUID
	 */
	private static final long serialVersionUID = 9157088397428436397L;
	private Pi2Go pi2go;

	GPIOInitialisationException(String msg, Pi2Go pi2go) {
		super(msg);
		this.pi2go = pi2go;
	}

	public Pi2Go getPi2Go() {
		return pi2go;
	}
}
