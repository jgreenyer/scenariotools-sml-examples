package pi2go;

import com.pi4j.wiringpi.Gpio;
import com.pi4j.wiringpi.SoftPwm;

import topology.Room;
import topology.Room.CardinalDirection;

/**
 * 
 * *******************************************************************************
 * This file is modified by: Jianwei Shi
 * 
 * simple API to set the speed(go forward or backward) and spin (left or right)
 * *******************************************************************************
 * 
 */

public class Pi2Go {

	static final int pinLeftFwd = 11;
	static final int pinLeftBwd = 10;
	static final int pinRightFwd = 12;
	static final int pinRightBwd = 13;

	private int rightSpeed;
	private int leftSpeed;

	// if lineLeft == true, it means there is a line on the left.
	private boolean lineLeft;
	private boolean lineRight;

	// if crossingReached == true, it means the robot has reached the crossing
	private boolean crossingReached;

	// if isTurning == true, it means the robot is turning left/right
	private boolean isTurning;

	// the Cardinal Direction of the robot
	// It is the direction which the head of the robot points
	private CardinalDirection robotCardinalDirection;

	private Room lastRoom;
	private Room currentRoom;
	private Room nextRoom;

	public Pi2Go() {
		// Initialise wiringPi library
		// If this function returns a value of '-1' then an error has occurred
		// and the initialization of the GPIO has failed. A return value of '0'
		// indicates a successful GPIO initialization.
		int isSuccess = Gpio.wiringPiSetup();
		if (isSuccess == 0) {
			// Hardware Initialisation (for motors)
			SoftPwm.softPwmCreate(pinLeftFwd, 0, 100);
			SoftPwm.softPwmCreate(pinLeftBwd, 0, 100);
			SoftPwm.softPwmCreate(pinRightFwd, 0, 100);
			SoftPwm.softPwmCreate(pinRightBwd, 0, 100);

			// variables Initialisation
			lineLeft = false;
			lineRight = false;
			crossingReached = false;
			isTurning = false;
			robotCardinalDirection = CardinalDirection.EAST;
		} else {
			throw new GPIOInitialisationException("FATAL ERROR OF GPIO! Programm stopped!", this);
		}
	}

	public void go(int speed) {
		setLeftSpeed(speed);
		setRightSpeed(speed);
	}

	public void spinLeft(int speed) {
		setLeftSpeed(-speed);
		setRightSpeed(speed);
	}

	public void spinRight(int speed) {
		setLeftSpeed(speed);
		setRightSpeed(-speed);
	}

	/**
	 * Sets the speed of the left wheel
	 * 
	 * @param speed
	 *            -100 <= speed <= 100
	 */
	public void setLeftSpeed(int speed) {
		if (speed < -100)
			throw new IllegalArgumentException("Parameter 'speed' must not be smaller than -100.");
		if (speed < -100 || speed > 100)
			throw new IllegalArgumentException("Parameter 'speed' must not be greater than 100.");
		if (speed > 0) {
			SoftPwm.softPwmWrite(pinLeftBwd, 0);
			SoftPwm.softPwmWrite(pinLeftFwd, speed);
		} else {
			SoftPwm.softPwmWrite(pinLeftFwd, 0);
			SoftPwm.softPwmWrite(pinLeftBwd, -speed);
		}
		leftSpeed = speed;
		System.out.println("set left speed to : " + speed);
	}

	/**
	 * Sets the speed of the right wheel
	 * 
	 * @param speed
	 *            -100 <= speed <= 100
	 */
	public void setRightSpeed(int speed) throws IllegalArgumentException {
		if (speed < -100)
			throw new IllegalArgumentException("Parameter 'speed' must not be smaller than -100.");
		if (speed < -100 || speed > 100)
			throw new IllegalArgumentException("Parameter 'speed' must not be greater than 100.");
		if (speed > 0) {
			SoftPwm.softPwmWrite(pinRightBwd, 0);
			SoftPwm.softPwmWrite(pinRightFwd, speed);
		} else {
			SoftPwm.softPwmWrite(pinRightFwd, 0);
			SoftPwm.softPwmWrite(pinRightBwd, -speed);
		}
		rightSpeed = speed;
		System.out.println("set right speed to : " + speed);
	}

	public void shutdown() {
		setLeftSpeed(0);
		setRightSpeed(0);
	}

	public int getRightSpeed() {
		return rightSpeed;
	}

	public int getLeftSpeed() {
		return leftSpeed;
	}

	public boolean isLineLeft() {
		return lineLeft;
	}

	public void setLineLeft(boolean lineLeft) {
		this.lineLeft = lineLeft;
	}

	public boolean isLineRight() {
		return lineRight;
	}

	public void setLineRight(boolean lineRight) {
		this.lineRight = lineRight;
	}

	public boolean isCrossingReached() {
		return crossingReached;
	}

	public void setCrossingReached(boolean crossingReached) {
		this.crossingReached = crossingReached;
	}

	public boolean isTurning() {
		return isTurning;
	}

	public void setTurning(boolean isTurning) {
		this.isTurning = isTurning;
	}

	public CardinalDirection getRobotCardinalDirection() {
		return robotCardinalDirection;
	}

	public void setRobotCardinalDirection(CardinalDirection robotCardinalDirection) {
		this.robotCardinalDirection = robotCardinalDirection;
	}

	public Room getLastRoom() {
		return lastRoom;
	}

	public void setLastRoom(Room lastRoom) {
		this.lastRoom = lastRoom;
	}

	public Room getCurrentRoom() {
		return currentRoom;
	}

	public void setCurrentRoom(Room currentRoom) {
		this.currentRoom = currentRoom;
	}

	public Room getNextRoom() {
		return nextRoom;
	}

	public void setNextRoom(Room nextRoom) {
		this.nextRoom = nextRoom;
	}

}
