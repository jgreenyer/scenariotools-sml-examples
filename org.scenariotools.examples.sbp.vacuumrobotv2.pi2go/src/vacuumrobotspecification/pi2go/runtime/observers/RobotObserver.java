package vacuumrobotspecification.pi2go.runtime.observers;

import java.util.ArrayList;
import java.util.List;

import pi2go.MovingLogic;
import pi2go.MovingLogic.RobotMovingDirection;
import sbp.runtime.adapter.RuntimeAdapter;
import sbp.runtime.settings.Settings;
import sbp.specification.events.Message;
import sbp.specification.role.Role;
import sbp.specification.scenarios.Scenario;
import sbp.specification.scenarios.utils.ScenarioObserver;
import topology.DirectionUtil;
import vacuumrobot.Room;
import vacuumrobot.VacuumRobot;
import vacuumrobot.VacuumRobotController;

public class RobotObserver extends ScenarioObserver {

	private RuntimeAdapter runtimeAdapter;

	public RobotObserver(RuntimeAdapter ra) {
		this.setRuntimeAdapter(ra);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void trigger(Scenario scenario, Message messageEvent) {
		if (messageEvent.getReceiver().getBinding() instanceof VacuumRobot) {
			// TODO: messageEvent.getMessage().equals("moveToAdjacentRoom")
			if (messageEvent.getMessage().equals("moveToAdjacentRoom")) {
				String theMessage = messageEvent.getName();
				Room room = (Room) messageEvent.getParameters().get(0);
				Settings.NETWORK_OUT.println("Observer got : '" + theMessage + "'");

				MovingLogic.GPIOInitialisation();

				topology.Room nextRoom = (topology.Room) MovingLogic.map.get(room);
				MovingLogic.pi2go_no_2.setNextRoom(nextRoom);

				RobotMovingDirection robotDir;

				// robotDir = DirectionUtil.DecideDirection(threeRooms.get(2),
				// threeRooms.get(0), threeRooms.get(1));

				// MovingLogic.pi2go_no_2.setCurrentRoom(threeRooms.get(0));

				robotDir = DirectionUtil.DecideDirection(MovingLogic.pi2go_no_2.getRobotCardinalDirection(),
						MovingLogic.pi2go_no_2.getCurrentRoom(), MovingLogic.pi2go_no_2.getNextRoom());

				System.out.println("TRIGGER robotDir: " + robotDir);

				try {
					MovingLogic.MoveToNextRoom(MovingLogic.pi2go_no_2, robotDir, runtimeAdapter, messageEvent, room);
					MovingLogic.pi2go_no_2.setCurrentRoom(nextRoom);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				MovingLogic.GPIOShutdown();
			}
		}
	}

	public RuntimeAdapter getRuntimeAdapter() {
		return runtimeAdapter;
	}

	public void setRuntimeAdapter(RuntimeAdapter runtimeAdapter) {
		this.runtimeAdapter = runtimeAdapter;
	}

}
