package topology;

import java.util.ArrayList;
import java.util.List;

import pi2go.MovingLogic.RobotMovingDirection;
import topology.Room.CardinalDirection;

public class RoomsTopo {

	List<Room> rooms = new ArrayList<Room>();

	// create a three room topology
	// room 03 <--> room 01 <--> room 02
	// the east side of room 03 is room 01
	// the east side of room 01 is room 02
	// the west side of room 02 is room 01
	public void createThreeRooms() {

		Room room_01 = new Room();
		Room room_02 = new Room();
		Room room_03 = new Room();

		rooms.add(room_01);
		rooms.add(room_02);
		rooms.add(room_03);

		room_01.setEastRoom(room_02);
		room_01.setWestRoom(room_03);

		room_02.setWestRoom(room_01);

		room_03.setEastRoom(room_01);
	}

	// small test function
	public void test() {

		RoomsTopo threeRoomsTopo = new RoomsTopo();
		threeRoomsTopo.createThreeRooms();
		List<Room> threeRooms = threeRoomsTopo.rooms;

		RobotMovingDirection robotDir;
		// robotDir = DirectionUtil.DecideDirection(threeRooms.get(2),
		// threeRooms.get(0), threeRooms.get(1));
		robotDir = DirectionUtil.DecideDirection(CardinalDirection.EAST, threeRooms.get(2), threeRooms.get(0));

		System.out.println(robotDir);
	}

	public List<Room> getRooms() {
		return rooms;
	}

	public void setRooms(List<Room> rooms) {
		this.rooms = rooms;
	}

}
