package topology;

public class Room {
	private Room eastRoom;
	private Room westRoom;
	private Room northRoom;
	private Room southRoom;

	// the direction between two rooms
	public enum CardinalDirection {
		EAST, WEST, NORTH, SOUTH
	};

	public Room() {
		this.setEastRoom(null);
		this.setWestRoom(null);
		this.setNorthRoom(null);
		this.setSouthRoom(null);
	}

	public Room getRoom(CardinalDirection dir) {

		Room room;
		switch (dir) {
		case EAST:
			room = this.getEastRoom();
			break;

		case WEST:
			room = this.getWestRoom();
			break;

		case NORTH:
			room = this.getNorthRoom();
			break;

		case SOUTH:
			room = this.getSouthRoom();
			break;

		default:
			room = null;
		}

		return room;
	}

	public Room getEastRoom() {
		return eastRoom;
	}

	public void setEastRoom(Room eastRoom) {
		this.eastRoom = eastRoom;
	}

	public Room getWestRoom() {
		return westRoom;
	}

	public void setWestRoom(Room westRoom) {
		this.westRoom = westRoom;
	}

	public Room getNorthRoom() {
		return northRoom;
	}

	public void setNorthRoom(Room northRoom) {
		this.northRoom = northRoom;
	}

	public Room getSouthRoom() {
		return southRoom;
	}

	public void setSouthRoom(Room southRoom) {
		this.southRoom = southRoom;
	}
}
