package topology;

import pi2go.MovingLogic.RobotMovingDirection;
import topology.Room.CardinalDirection;

public class DirectionUtil {
	/**
	 * Decide the direction of the robot with information only from the rooms
	 * 
	 * It is good for the situation where the lastRoom is already known.
	 * 
	 * @param lastRoom
	 *            the last room which the robot has visited
	 * @param currentRoom
	 *            the room in which the robot is
	 * @param nextRoom
	 *            the room which the robot will go
	 * @return the direction that the robot should follow. If null is returned,
	 *         the robot does not move
	 */
	public static RobotMovingDirection DecideDirection(Room lastRoom, Room currentRoom, Room nextRoom) {
		RobotMovingDirection robotMovingDir = null;

		// the direction from the lastRoom to the currentRoom
		CardinalDirection dirA = getDirectionBetweenTwoRooms(lastRoom, currentRoom);
		// the direction from the currentRoom to the newRoom
		CardinalDirection dirB = getDirectionBetweenTwoRooms(currentRoom, nextRoom);

		if (dirA == dirB) {

			robotMovingDir = RobotMovingDirection.FORWARD;

		} else if ((dirA == CardinalDirection.WEST) && (dirB == CardinalDirection.EAST)
				|| (dirA == CardinalDirection.EAST) && (dirB == CardinalDirection.WEST)
				|| (dirA == CardinalDirection.NORTH) && (dirB == CardinalDirection.SOUTH)
				|| (dirA == CardinalDirection.SOUTH) && (dirB == CardinalDirection.NORTH)) {

			robotMovingDir = RobotMovingDirection.BACKWARD;

		} else if ((dirA == CardinalDirection.NORTH) && (dirB == CardinalDirection.EAST)
				|| (dirA == CardinalDirection.WEST) && (dirB == CardinalDirection.NORTH)
				|| (dirA == CardinalDirection.SOUTH) && (dirB == CardinalDirection.WEST)
				|| (dirA == CardinalDirection.EAST) && (dirB == CardinalDirection.SOUTH)) {

			robotMovingDir = RobotMovingDirection.RIGHT;

		} else if ((dirA == null) || (dirB == null)) {
			// if there is no direct connection
			// between lastRoom and currentRoom
			// or between currentRoom and newRoom
			robotMovingDir = null;

		} else {

			robotMovingDir = RobotMovingDirection.LEFT;

		}
		return robotMovingDir;
	}

	/**
	 * Decide the direction of the robot with three information from the rooms
	 * and the direction of the robot now
	 * 
	 * It is good for the situation where the lastRoom is not known.
	 * 
	 * the default cardinal direction for robot is east
	 * 
	 * @param robotCardinalDirection
	 * @param currentRoom
	 * @param newRoom
	 * @return
	 */
	public static RobotMovingDirection DecideDirection(CardinalDirection robotCardinalDirection, Room currentRoom,
			Room newRoom) {
		RobotMovingDirection robotMovingDir = null;
		
		// the direction from the currentRoom to the newRoom
		CardinalDirection roomDir = getDirectionBetweenTwoRooms(currentRoom, newRoom);
		
		if (robotCardinalDirection == roomDir) {

			robotMovingDir = RobotMovingDirection.FORWARD;

		} else if ((robotCardinalDirection == CardinalDirection.WEST) && (roomDir == CardinalDirection.NORTH)
				|| (robotCardinalDirection == CardinalDirection.NORTH) && (roomDir == CardinalDirection.EAST)
				|| (robotCardinalDirection == CardinalDirection.EAST) && (roomDir == CardinalDirection.SOUTH)
				|| (robotCardinalDirection == CardinalDirection.SOUTH) && (roomDir == CardinalDirection.WEST)) {

			robotMovingDir = RobotMovingDirection.RIGHT;

		} else if ((robotCardinalDirection == CardinalDirection.WEST) && (roomDir == CardinalDirection.SOUTH)
				|| (robotCardinalDirection == CardinalDirection.SOUTH) && (roomDir == CardinalDirection.EAST)
				|| (robotCardinalDirection == CardinalDirection.EAST) && (roomDir == CardinalDirection.NORTH)
				|| (robotCardinalDirection == CardinalDirection.NORTH) && (roomDir == CardinalDirection.WEST)) {

			robotMovingDir = RobotMovingDirection.LEFT;

		} else if ((robotCardinalDirection == CardinalDirection.WEST) && (roomDir == CardinalDirection.EAST)
				|| (robotCardinalDirection == CardinalDirection.EAST) && (roomDir == CardinalDirection.WEST)
				|| (robotCardinalDirection == CardinalDirection.NORTH) && (roomDir == CardinalDirection.SOUTH)
				|| (robotCardinalDirection == CardinalDirection.SOUTH) && (roomDir == CardinalDirection.NORTH)) {

			robotMovingDir = RobotMovingDirection.BACKWARD;

		}

		return robotMovingDir;
	}

	private static CardinalDirection getDirectionBetweenTwoRooms(Room roomA, Room roomB) {
		CardinalDirection roomDir = null;
		for (CardinalDirection dir : CardinalDirection.values()) {
			if (roomA.getRoom(dir) != null) {
				if (roomA.getRoom(dir).equals(roomB)) {
					roomDir = dir;
					break;
				}
			}
		}
		return roomDir;
	}
}
