This project runs with special software. This documentation tells you how.

For Raspberry Pi:
1.Install Raspbian on Pi
  https://www.raspberrypi.org/downloads/raspbian/

2.Screen and keyboard are required in following steps 
  2.0 connect the screen and keyboard to pi
  2.1 Change keyboard layout to de, or whatever it fits
      2.1.1 Edit /etc/default/keyboard with your favorite editor (vim, nano,). Remember to use sudo:
      
            sudo nano /etc/default/keyboard

            Make the file look like this:
            
            # KEYBOARD CONFIGURATION FILE
            # Consult the keyboard(5) manual page.
            XKBMODEL="pc105"
            XKBLAYOUT="de"
            XKBVARIANT=""
            XKBOPTIONS="terminate:ctrl_alt_bksp"
            BACKSPACE="guess"

      2.1.2 Save the file, then reboot.
      
  2.2. set the network connection
       https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md

       important commands:
       to see ip address
        
       ifconfig wlan0
       
       force to reread configuration file
       
       sudo wpa_cli reconfigure
       
  2.3. change the hostname of the pi
  
       sudo raspi-config
       
       then change the name in a GUI
       
  2.4. install samba in pi
  
       sudo apt-get update
       
       sudo apt-get install Samba
       
  2.5.enable SSH in pi
  
       sudo raspi-config
       
       then enable SSH in GUI
       
  And then you can connect to your pi with putty on windows. 
  You will NOT need a screen and a keyboard anymore!

3. install java (here java 8)

   sudo apt-get update
   
   sudo apt-get install oracle-java8-jdk

4. install pi4j

   curl -s get.pi4j.com | sudo bash

5. enable SPI
   The SPI master driver is disabled by default on Raspbian. To enable it, 
   use raspi-config, or ensure the line dtparam=spi=on isn't commented out in
   /boot/config.txt, and reboot. If the SPI driver was loaded, you should see 
   the device /dev/spidev0.0.
   (https://www.raspberrypi.org/documentation/hardware/raspberrypi/spi/README.md)


For Eclipse:
1. Install LauchPi Plug-in (for remote debugging of pi)
   http://tsvetan-stoyanov.github.io/launchpi/
   