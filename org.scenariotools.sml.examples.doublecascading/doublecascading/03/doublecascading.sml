import "../doublecascading.ecore"
specification doublecascading03{ 
	domain Cascade
	
	controllable {
		A
		B
	}
	
	collaboration Doublecascading {
		
		static role Environment e
		static role A a
		static role B b
		
		guarantee scenario Cascade1 {
			e->a.startCascade()
			strict urgent a->b.msg1()
			strict urgent a->b.msg1()
		}

		guarantee scenario Cascade2 {
			a->b.msg1()
			strict urgent a->b.msg2()
			strict urgent a->b.msg2()
		}

		guarantee scenario Cascade3 {
			a->b.msg2()
			strict urgent a->b.msg3()
			strict urgent a->b.msg3()
		}
		
	}
	
}