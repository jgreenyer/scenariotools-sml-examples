import "../model/pathfinding.ecore"

specification PathfindingSpecification {

	domain pathfinding

	controllable {
		Robot
	}
	
	non-spontaneous events {
		Robot.setLocation
	}

	collaboration PathfindingCollaboration {

		static role Environment env
		static role Robot robot
		dynamic role Room room
		dynamic role TargetRoom targetRoom
		static role Room extraCostRoom1
		static role Room extraCostRoom2

		guarantee scenario RobotEventuallyReachesTargetRoom
		optimize cost {
			targetRoom -> robot.setTargetLocation(targetRoom)
			wait eventually [robot.location == robot.targetLocation]
		}
		
		guarantee scenario RobotStartsMovingUponTargetBeingSet {
			targetRoom -> robot.setTargetLocation(targetRoom)
			urgent robot -> robot.move()
		}
		
		guarantee scenario RobotChoosesWhereToMove
		bindings [
			room = robot.location
		] {
			robot -> robot.move()
			urgent robot -> robot.moveToRoom(room.adjacentRooms.any())
		}
		
		guarantee scenario RobotKeepsMovingWhenNotAtTargetLocation {
			env -> robot.setLocation(*)
			interrupt [robot.location == robot.targetLocation]
			urgent robot -> robot.move()
		}

		assumption scenario RobotArrivesInRoom
		cost [1.0] {
			robot -> robot.moveToRoom(bind room)
			committed env -> robot.setLocation(room)
		}
		
		assumption scenario RobotArrivesInRoom_extraCost
		cost [10.0] {
			robot -> robot.moveToRoom(bind room)
			interrupt [robot.location != extraCostRoom1 && robot.location != extraCostRoom2]
			interrupt [room != extraCostRoom1 && room != extraCostRoom2]
			env -> robot.setLocation(room)
		}
		
		assumption scenario RobotIdlesWhenReachingTarget {
			env -> robot.setLocation(*)
			interrupt [robot.location != robot.targetLocation]
			while [true] {
				committed env -> robot.idle()
			}
		}		
	}
}
