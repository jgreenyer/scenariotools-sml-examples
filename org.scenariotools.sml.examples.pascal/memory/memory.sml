import "../model.ecore"

specification test_scenario {
	domain model
	
	controllable {
		A
		B
	}

	non-spontaneous events {
		A.opA2		
	}
	
	collaboration test_scenario{
		
		static role A a 
		static role B b 
		static role Environment env
		
 		assumption scenario Environment {
			env -> a.opA1()
			while [true] {
				strict env -> a.opA2()
			}
		}
		
		guarantee scenario req1 {
			a -> a.opA3()
			monitored eventually a -> a.opA1()
		}
		
		guarantee scenario req2 {
			a -> a.opA3()
			monitored eventually a -> a.opA2()
		}
		
		guarantee scenario start {
			env -> a.opA1()
			strict urgent a -> a.opA3()
			strict urgent a -> b.opB1()
		}

		guarantee scenario loop {
			a -> b.opB1()
			
			while [true] {
				env -> a.opA2()
				alternative {
					strict urgent a -> a.opA1()
				} or {
					strict urgent a -> a.opA2()
				}
				strict urgent a -> a.opA3()
			}
		}
	}
}