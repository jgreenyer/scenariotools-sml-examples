import "../model.ecore"

specification test_scenario {
	domain model
	
	controllable {
		A
		B
	}

	non-spontaneous events {
		A.opA2		
	}
	
	collaboration test_scenario{
		
		static role A a 
		static role B b 
		static role Environment env
		
 		assumption scenario Environment {
			env -> a.opA1()
			while [true] {
				strict env -> a.opA2()
			}
		} constraints [interrupt a -> b.opB1()]

 		assumption scenario Environment2 {
			a -> b.opB1()
			strict eventually env -> a.opA1()
		}
		
		guarantee scenario req {
			env -> a.opA1()
			strict monitored eventually a -> b.opB1()
		}
		
		guarantee scenario loop {
			env -> a.opA2()
			alternative {
				strict urgent b -> a.opA1()
			} or {
				strict urgent a -> b.opB1()
				strict  b -> b.opB1()
			}
			strict urgent a -> a.opA1()
		}

		guarantee scenario loop2 {
			a -> b.opB1()
			strict env -> a.opA1()
			strict urgent b -> b.opB1()
		}
		
		guarantee scenario loop3 {
			 a -> a.opA1()
			strict urgent a -> a.opA2()
		}
	}
}