import "../model.ecore"

specification test_scenario {
	domain model
	
	controllable {
		A
		B
	}

	non-spontaneous events {
		A.opA2
		A.opA3
		B.opB3
	}
	
	collaboration test_scenario{
		
		static role A a 
		static role B b 
		static role Environment env
		
 		assumption scenario Environment {
			env -> a.opA1()
			while [true] {
				strict env -> a.opA2()
				strict env -> a.opA3()
				strict env -> b.opB3()
			}
		}
		
		guarantee scenario req {
			a -> b.opB1()
			strict monitored eventually  b -> a.opA1()
		}

		guarantee scenario start {
			env -> a.opA1()
			strict urgent a -> b.opB1()
		}

		guarantee scenario loop1 {
			env -> a.opA2()
			strict urgent a -> b.opBP1(true)
		}
		
		guarantee scenario loop2 {
			a -> b.opBP1(true)
			env -> a.opA3()
			strict urgent b -> a.opA1()
			strict urgent a -> b.opB1()
			strict urgent a -> b.opBP1(false)
		}
		
		guarantee scenario loop3 {
			a -> b.opBP1(false)
			env -> b.opB3()
			strict urgent a -> b.opB2()
		}
	}
}