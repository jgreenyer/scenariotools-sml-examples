import "../model.ecore"

specification test_scenario {
	domain model
	
	controllable {
		A
		B
	}

	non-spontaneous events {
		A.opA2
		B.opB1
		B.opB2
	}
	
	collaboration test_scenario{
		
		static role A a 
		static role B b 
		static role Environment env
		
 		assumption scenario BlockingAssumption {
			b -> b.opB1()
			strict eventually env -> a.opA2()
		}
		
		assumption scenario Environment {
			env -> a.opA1()
			while [true] {
				alternative {
					strict env -> a.opA2()
				} or {
					strict env -> b.opB1()
				}
			}
		}
		
		guarantee scenario req {
			env -> a.opA1()
			strict monitored eventually b -> b.opB2()
		}
		
		guarantee scenario start {
			env -> a.opA1()
			strict urgent b -> b.opB1()
		}

		guarantee scenario assumptionFulfilled {
			env -> a.opA2()
			strict urgent b -> b.opB1()
		}
		
		guarantee scenario assumptionUnfulfilled {
			env -> b.opB1()
			strict urgent a -> b.opB1()
		}
	}
}