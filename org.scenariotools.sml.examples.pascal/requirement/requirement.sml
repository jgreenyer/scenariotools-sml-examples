import "../model.ecore"

specification test_scenario {
	domain model

	controllable {
		A
		B
	}	

	non-spontaneous events {
		A.opA2
		B.opB1
		B.opB2
	}
	
	collaboration test_scenario{
		
		static role A a 
		static role B b 
		static role Environment env
	
		guarantee scenario loop {
			env -> a.opA1()
			strict urgent a -> b.opB1()
			strict urgent b -> a.opA2()			 
		}
		
		guarantee scenario req {
			 a -> b.opB1()
			strict monitored eventually b -> a.opA2()			 
		}
	}
}