import "../model/car-to-x.ecore"

specification CarToXSpecification {

	domain CarToX
	
	controllable {
		Obstacle
	}
	
	non-spontaneous events {
		Environment.tick
		Obstacle.register
		Car.setLocation
		Car.setPermission
	}

	collaboration CarsPassObstacle {
		static role Environment env
		static role Obstacle obstacle
		dynamic role Car car
		
		assumption scenario CarApproachesObstacle {
			env -> car.approachingObstacle()
			committed env -> car.setLocation(CarLocation:ApproachingObstacle)
			committed car -> obstacle.register()
			eventually env -> car.setLocation(CarLocation:ReachedObstacle)
		}
		
		assumption scenario CarPassesObstacle {
			obstacle -> car.setPermission(*)
			wait [car.location == CarLocation:ReachedObstacle && car.permission == CarPermission:MayPass]
			eventually env -> car.setLocation(CarLocation:PassingObstacle)
			eventually env -> car.setLocation(CarLocation:BehindObstacle)
		}
		
		assumption scenario CarCantApproachMultipleTimes {
			env -> car.approachingObstacle()
			wait strict [car.allCarsBehindObstacle]
			while [true] {
				strict env -> env.tick()			
			}
		}
		
		guarantee scenario ObstacleRegistersCar {
			car -> obstacle.register()
			alternative {
				eventually obstacle -> car.setPermission(CarPermission:MayPass)
			} or {
				eventually obstacle -> car.setPermission(CarPermission:MustWait)
			}
		}
		
		guarantee scenario EventuallyAllowCarToPass {
			obstacle -> car.setPermission(*)
			interrupt [car.permission == CarPermission:MayPass]
			eventually obstacle -> car.setPermission(CarPermission:MayPass)
		}
		
		guarantee scenario CarReceivesAnswerBeforeReachingObstacle {
			env -> car.setLocation(*)
			violation [!(car.permission == CarPermission:Undefined => car.location != CarLocation:ReachedObstacle)]
		}

		guarantee scenario OnlyAllowCarsDrivingInTheSameDirectionToPassAtTheSameTime {
			env -> car.setLocation(*)
			violation [!(car.carsPassingInOppositeDirection.size() > 0 => car.location != CarLocation:PassingObstacle)]
		}
	}
}
