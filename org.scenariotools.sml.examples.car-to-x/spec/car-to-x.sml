import "../model/car-to-x.ecore"

specification CarToXSpecification {

	domain CarToX
	
	controllable {
		Obstacle
	}
	
	non-spontaneous events {
		Environment.tick
		Obstacle.register
		Car.setLocation
		Car.setPermission
		Car.obstacleReached
		Car.enterNarrowPassage
		Car.leaveNarrowPassage
	}

	collaboration CarsPassObstacle {
		static role Environment env
		static role Obstacle obstacle
		dynamic role Car car
		
		assumption scenario CarApproachesObstacle {
			env -> car.approachingObstacle()
			committed car -> car.setLocation(CarLocation:ApproachingObstacle)
			committed car -> obstacle.register()
			eventually env -> car.obstacleReached()
			committed car -> car.setLocation(CarLocation:ReachedObstacle)
		}
		
		assumption scenario CarMayPassObstacle {
			obstacle -> car.enteringAllowed()
			committed car -> car.setPermission(CarPermission:MayPass)
		}
		
		assumption scenario CarMustWait {
			obstacle -> car.enteringDisallowed()
			committed car -> car.setPermission(CarPermission:MustWait)
		}
		
		assumption scenario CarPassesObstacle {
			obstacle -> car.enteringAllowed()
			wait [car.location == CarLocation:ReachedObstacle]
			eventually env -> car.enterNarrowPassage()
			committed car -> car.setLocation(CarLocation:PassingObstacle)
			eventually env -> car.leaveNarrowPassage()
			committed car -> car.setLocation(CarLocation:BehindObstacle)
//			committed car -> obstacle.deregister()
		}
		
		assumption scenario CarCantApproachMultipleTimes {
			env -> car.approachingObstacle()
			wait strict [car.allCarsBehindObstacle]
			while [true] {
				strict env -> env.tick()			
			}
		}
		
		guarantee scenario ObstacleRegistersCar {
			car -> obstacle.register()
			alternative {
				eventually obstacle -> car.enteringAllowed()
			} or {
				eventually obstacle -> car.enteringDisallowed()
			}
		}
		
		guarantee scenario EventuallyAllowCarToPass {
			obstacle -> car.enteringDisallowed()
			eventually obstacle -> car.enteringAllowed()
		}
		
		guarantee scenario CarReceivesAnswerBeforeReachingObstacle {
			env -> car.obstacleReached()
			violation [car.permission == CarPermission:Undefined]
		}

		guarantee scenario OnlyAllowCarsDrivingInTheSameDirectionToPassAtTheSameTime {
			//Note: reacting to car -> car.setLocation(CarLocation:PassingObstacle) does not work properly; bug in runtime?
			env -> car.enterNarrowPassage()
			wait [car.location == CarLocation:PassingObstacle]
			violation [car.carsPassingInOppositeDirection.size() > 0]
		}
	}
}
