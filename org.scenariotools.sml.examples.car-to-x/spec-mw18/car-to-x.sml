import "../model/car-to-x.ecore"

specification CarToXSpecification {

	domain CarToX
	
	controllable {
		Obstacle
	}
	
	non-spontaneous events {
		Environment.tick
		Obstacle.register
		Car.setLocation
		Car.setPermission
	}

	collaboration CarsPassObstacle {
		static role Environment env
		static role Obstacle obstacle
		dynamic role Car car
		dynamic multi role Car otherCar
		
		assumption scenario CarApproachesAndPassesObstacle {
			env -> car.approachingObstacle()
			committed env -> car.setLocation(CarLocation:ApproachingObstacle)
			committed car -> obstacle.register()
			env -> car.setLocation(CarLocation:ReachedObstacle)
			env -> car.setLocation(CarLocation:PassingObstacle)
			env -> car.setLocation(CarLocation:BehindObstacle)
		}
		
		assumption scenario DriverObeysSignals { // defect 2: missing assumption
			obstacle -> car.setPermission(CarPermission:MustWait)
			obstacle -> car.setPermission(CarPermission:MayPass)			
		} constraints [
			forbidden env -> car.setLocation(CarLocation:PassingObstacle)
		]
		
		assumption scenario CarCantApproachMultipleTimes {
			env -> car.approachingObstacle()
			while [true] {
				wait strict [env.noMoreMovesPossible]
				{
					env -> env.tick()
				} constraints [
					forbidden env -> car.setLocation(*)
				]
			}
		} constraints [
			forbidden env -> car.approachingObstacle()
		]
		
		guarantee scenario ObstacleRegistersCar {
			car -> obstacle.register()
			alternative [car.carsInOppositeDirectionThatSoonPass.size() == 0] {
				committed obstacle -> car.setPermission(CarPermission:MayPass)
			} or [car.carsInOppositeDirectionThatSoonPass.size() > 0] {
//			} or [car.carsInOppositeDirectionThatSoonPass.size() > 1] { // defect 1: off by one
				committed obstacle -> car.setPermission(CarPermission:MustWait)
			}
		}

		guarantee scenario EventuallyAllowCarToPass { // defect 3: missing behavior (causes liveness violation)
			obstacle -> car.setPermission(CarPermission:MustWait)
			wait [car.carsInOppositeDirectionThatSoonPass.size() == 0]
			committed obstacle -> car.setPermission(CarPermission:MayPass)
		}
		
		guarantee scenario CarReceivesAnswerBeforeReachingObstacle {
			env -> car.setLocation(CarLocation:ReachedObstacle)
			violation [car.permission == CarPermission:Undefined]
		}
		
		guarantee scenario AllApproachingCarsEventuallyPassObstacle {
			env -> car.setLocation(CarLocation:ReachedObstacle)
			monitored eventually env -> car.setLocation(CarLocation:BehindObstacle)
		} constraints [
			ignore env -> car.setLocation(*)
		]

		guarantee scenario OnlyAllowCarsDrivingInTheSameDirectionToPassAtTheSameTime
		bindings [
			otherCar = car.carsInOppositeDirection
		] {
			env -> car.setLocation(CarLocation:PassingObstacle)
			env -> car.setLocation(CarLocation:BehindObstacle)
		} constraints [
			forbidden env -> otherCar.setLocation(CarLocation:PassingObstacle)
		]
	}
}
