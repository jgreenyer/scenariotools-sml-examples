import "../model/elevatorSystem.ecore"

specification ElevatorSystemSpecification {

	/*
	 * Refer to a package in the imported ecore model.
	 * Hint: Use ctrl+alt+R to rename packages and classes.
	 */
	domain elevatorSystem

	/* 
	 * Define classes of objects that are controllable
	 * or uncontrollable.
	 */
	controllable {
		Controller
	}
	

	non-spontaneous events
	{
		LevelSensor.setLevel
		Motor.setMoving
		Motor.setState
		Motor.setLastState
		Elevator.setDoorOpen
		Controller.timerExpired
	}
	

	/*
	 * Collaborations describe how objects interact in a certain
	 * context to collectively accomplish some desired functionality.
	 */
	collaboration ElevatorSystemCollaboration {

		static role Controller controller
		dynamic multi role CallButton callButton
		dynamic role Elevator elevator
		dynamic role Motor motor
		dynamic role LevelSensor levelSensor
		dynamic multi role LevelButton levelButton
		dynamic multi role Button button
		dynamic role Timer timer
		

		
		// the user presses one Button (levelButton or callButton)
//		specification scenario pressButton
//		with dynamic bindings[
//			bind elevator to controller.elevator
//		]
//		{
//			message button -> controller.requestElevator(button.level)
//			message strict requested controller -> button.setPressed(true)
//			while [button.pressed == true]
//			{
//				message button -> controller.requestElevator(button.level)
//			}
//		} 
//		
//		// the user presses one levelButton
//		specification scenario chooseLevel
//		with dynamic bindings[
//			bind elevator to controller.elevator
//		]
//		{
//			message levelButton -> controller.requestElevator(levelButton.level)
//			message strict requested controller -> levelButton.setPressed(true)
//			//requested wait until [levelSensor.level == levelButton.level]
//			//message controller -> elevator.openDoor()
//			while [levelButton.pressed == true]
//			{
//				message levelButton -> controller.requestElevator(levelButton.level)
//			}
//			//message controller -> levelButton.setPressed(false)
//		} 
//			
//		// the user presses one callButton
//		specification scenario callElevator
//		with dynamic bindings[
//			bind elevator to controller.elevator
//		]
//		{
//			message callButton -> controller.requestElevator(callButton.level)
//			message strict requested controller -> callButton.setPressed(true)
//			//requested wait until [levelSensor.level == callButton.level]
//			//message controller -> elevator.openDoor()
//			while [callButton.pressed == true]
//			{
//				message callButton -> controller.requestElevator(callButton.level)
//			}
//			//message controller -> callButton.setPressed(false)
//		}
		
		// the system inactivate the levelButton 
		// according to the level
		guarantee scenario inactivateLevelButton
		bindings [ 
			levelSensor = elevator.levelSensor
			levelButton = elevator.levelButton
		]
		{	
			//message controller -> elevator.openDoor()
			elevator -> elevator.setDoorOpen(true)
			alternative [levelSensor.level == levelButton.level]
			{
				strict requested controller -> levelButton.setPressed(false)
			}
		}
		
		// the system inactivate the callButton 
		guarantee scenario inactivateCallButton
		bindings [ 
			levelSensor = elevator.levelSensor
			callButton = elevator.callButton
		]
		{	
			//message controller -> elevator.openDoor()
			elevator -> elevator.setDoorOpen(true)
			alternative [levelSensor.level == callButton.level]
			{
				strict eventually elevator -> callButton.setPressed(false)
			}
		}
		
		// the system controls the motor (moveDown, moveUp and stop)
		// according to the level 
		guarantee scenario motorStateChanging
		bindings [ 
			elevator = controller.elevator
			motor = elevator.motor
			levelSensor = elevator.levelSensor
		]
		{	
			button -> controller.requestElevator(button.level)
			
			strict requested controller -> button.setPressed(true)
			wait [((motor.state == (DIRECTION:STOPPED) || motor.state == (DIRECTION:NULL)) && (elevator.doorOpen == false))]
			//wait until [elevator.doorOpen == false]
			
			alternative [button.level < levelSensor.level]
			{
				// TODO: PROBLEM HERE
				// when the last state of motor is DOWN or NULL, 
				// move down
				alternative [motor.lastState == DIRECTION:DOWN || motor.lastState == DIRECTION:NULL || elevator.activeButtonsInDirection]
				{
					requested controller -> motor.moveDown()
					motor -> motor.setState(DIRECTION:DOWN)
				}

			}or [button.level > levelSensor.level]
			{
				// TODO: PROBLEM HERE
				// when the last state of motor is UP or NULL, 
				// move down
				alternative [motor.lastState == DIRECTION:UP || motor.lastState == DIRECTION:NULL || elevator.activeButtonsInDirection]
				{
					requested controller -> motor.moveUp()
					motor -> motor.setState(DIRECTION:UP)
				}
			}or [button.level == levelSensor.level]
			{
				requested controller -> motor.stop()
				motor -> motor.setState(DIRECTION:STOPPED)
			}
		}
		
		// specify when the motor should be stopped
		// according to the level of CallButton		
		guarantee scenario changingLevelforCallButton
		bindings [ 
			elevator = controller.elevator
			button = elevator.callButton
			motor = elevator.motor
		]
		{	
 			levelSensor -> levelSensor.setLevel(*)
 			alternative [button.level == levelSensor.level]
 			{
 				alternative [button.pressed == true]
 				{
 					requested controller -> motor.stop()
 				}
 			}	
		}
		
		// specify when the motor should be stopped
		// according to the level of LevelButton
		guarantee scenario changingLevelforLevelButton
		bindings [ 
			elevator = controller.elevator
			button = elevator.levelButton
			motor = elevator.motor
		]
		{	
 			levelSensor -> levelSensor.setLevel(*)
 			alternative [button.level == levelSensor.level]
 			{
 				alternative [button.pressed == true]
 				{
 					requested controller -> motor.stop()
 				}
 			}	
		}

		// Environment: the motor is moving down
		assumption scenario motorMovingDown
		bindings [
			elevator = controller.elevator
			levelSensor = elevator.levelSensor
		]
		{
			controller -> motor.moveDown()
			//message motor -> motor.setMoving(true)
			//message motor -> motor.setLastState(motor.state)
			committed motor -> motor.setLastState(motor.state)
			committed motor -> motor.setState(DIRECTION:DOWN)
			// when motor is moving, 
			// move DOWN
			while 
			[ 
				motor.state == DIRECTION:DOWN
			]
			{
				levelSensor -> levelSensor.setLevel(levelSensor.level - 1)
			}
		}
		
		// Environment: the motor is moving up
		assumption scenario motorMovingUp
		bindings [
			elevator = controller.elevator
			levelSensor = elevator.levelSensor
		]
		{
			controller -> motor.moveUp()
			committed motor -> motor.setLastState(motor.state)
			committed motor -> motor.setState(DIRECTION:UP)
			// when the motor is moving, 
			// move UP
			while 
			[ 
				motor.state == DIRECTION:UP
			]
			{
				levelSensor -> levelSensor.setLevel(levelSensor.level + 1)
			}
		}
		
		// Environment: the motor is stopping
		// it must not be followed with 
		// message levelSensor -> levelSensor.setLevel(*)
		assumption scenario motorStopping
		bindings [
			elevator = controller.elevator
		]
		{
			controller -> motor.stop()
			//committed motor -> motor.setMoving(false)
			committed motor -> motor.setLastState(motor.state)
			committed motor -> motor.setState(DIRECTION:STOPPED)
		}constraints[
			forbidden levelSensor -> levelSensor.setLevel(*)
		]
		
		// The motor is stopped, and then 
		// the door must be opened 
		// and finally closed
		guarantee scenario motorStopped
		bindings [
			elevator = controller.elevator
			timer = controller.timer
		]
		{
			motor -> motor.setState(DIRECTION:STOPPED)
			strict requested controller -> elevator.openDoor()
			wait [elevator.doorOpen == true]
			// expire time
 			strict requested controller -> timer.start()
			timer -> controller.timerExpired()
			strict requested controller -> elevator.closeDoor()
		}
		
		// Environment: before the new state is set, store Laststate at first 
		assumption scenario storeLastState
		{
			motor -> motor.setLastState(motor.state)
			motor -> motor.setState(*)
		}
		
		// Environment: Timer 
		assumption scenario timerExpires
		{
			controller -> timer.start()
			timer -> controller.timerExpired()
		}
		
		// Environment: the door is opening
		assumption scenario doorOpening
		{
			controller -> elevator.openDoor()
			elevator -> elevator.setDoorOpen(true)
		}
		
		// Environment: the door is closing
		assumption scenario doorClosing
		{
			controller -> elevator.closeDoor()
			elevator -> elevator.setDoorOpen(false)
			
		}
		
		
		assumption scenario continueMoving
		bindings [
			button = elevator.callButton
			motor = elevator.motor
		]
		{
			controller -> elevator.closeDoor()
			//message requested controller -> elevator.setActiveButtonsInDirection(false)
			// the scenario will be interrupted when the button is not pressed
			interrupt [ !button.pressed ]
			// the scenario goes on when the door is closed
			wait [ !elevator.doorOpen]
			// committed
			// bevor etw passiert, muss das auftreten
			strict committed button -> controller.requestElevator(button.level)
			// TODO: parameter unifiable statt message unifiable? 
		}
		
		guarantee scenario buttonsInDirection
		bindings [
			button = elevator.callButton
			motor = elevator.motor
		]
		{
			controller -> elevator.closeDoor()
			requested controller -> elevator.setActiveButtonsInDirection(false)
			//message requested controller -> elevator.setActiveButtonsInDirection(false)
			// the scenario will be interrupted when the button is not pressed
			interrupt [ !button.pressed ]
			// the scenario goes on when the door is closed
			interrupt [!((motor.lastState == DIRECTION:DOWN && button.level < levelSensor.level) || (motor.lastState == DIRECTION:UP && button.level > levelSensor.level))]
			requested controller -> elevator.setActiveButtonsInDirection(true)
		}
		
//		assumption scenario changingMovementState
//		with dynamic bindings [
//			bind callButton to elevator.callButton
//			bind levelSensor to elevator.levelSensor
//		]
//		{
//			message button -> controller.requestElevator(button.level)
//			//interrupt if buttons are the same or not pressed
//			interrupt if [button.level == callButton.level | callButton.pressed == false]
//			 
//				//| (motor.lastState == DIRECTION:DOWN & callButton.level < levelSensor.level) | (motor.lastState == DIRECTION:UP & callButton.level > levelSensor.level)
//			
//			interrupt if [ &
//			alternative if[]
//			{
//				interrupt if[	
//			}
//		}
		
//		assumption scenario changingDirection
//		with dynamic bindings [
//			message controller -> elevator.closeDoor()
//			// the scenario will be interrupted when the button is not pressed
//			interrupt if [ !button.pressed ]
//		]
	}

}



//		specification scenario MyScenario {
//			message callButton -> controller.request()
//			message strict requested controller -> callButton.response()
//		}
		
//		specification scenario callElevator
//		with dynamic bindings [	bind elevator to controller.elevator
//								bind motor to elevator.motor
//								bind levelSensor to elevator.levelSensor
//		] 
//		//	with dynamic bindings [bind motor to elevator.motor]
//		{
//			message callButton -> controller.requestElevator(callButton.level)//0
//			alternative if[ levelSensor.level < callButton.level ]{ //(1:)0
//				message strict requested controller -> motor.moveUp()//
//				while[levelSensor.level != callButton.level]{//1
//					message strict requested levelSensor -> levelSensor.setLevel(levelSensor.level + 1)	//0
//				}
//				message strict requested controller -> motor.stop()
//				message strict requested controller -> elevator.openDoor()
//					
//			} or if [levelSensor.level > callButton.level ]{ //(1:)0
//				message strict requested controller -> motor.moveDown()
//				while[levelSensor.level != callButton.level]{
//					message strict requested levelSensor -> levelSensor.setLevel(levelSensor.level - 1)	
//				}	
//				message strict requested controller -> motor.stop()
//				message strict requested controller -> elevator.openDoor()
//			} or if[ levelSensor.level == callButton.level ]{ //(1:)0
//				message strict requested controller -> elevator.openDoor()
//			} 
//			
//		}

//		specification scenario pressButton
//		with dynamic bindings [ bind button to elevator.levelButton ]
//		{
//			message button -> controller.requestElevator(button.level)
//			message strict requested button -> button.setPressed(true)
//		}
		
//		specification scenario inactivateLevelButton
//		with dynamic bindings [ bind button to elevator.levelButton]
//		{
//			message controller -> elevator.openDoor()
//			alternative if [levelSensor.level == button.level]
//			{
//				message strict requested button -> button.setPressed(false)
//			}
//		}

//		specification scenario changingLevel
//		with dynamic bindings [ bind motor to elevator.motor]
//		{	
// 			message levelSensor -> levelSensor.setLevel(1)
// 			alternative if [button.level == levelSensor.level]
// 			{
// 				alternative if [button.pressed == true]
// 				{
// 					message requested controller -> motor.stop()
// 				}
// 			}	
//		}