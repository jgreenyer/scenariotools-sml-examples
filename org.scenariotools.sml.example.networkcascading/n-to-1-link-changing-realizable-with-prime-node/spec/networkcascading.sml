import "../model/networkcascading.ecore"
specification networkcascading{ 

	domain networkcascading
	
	controllable {
		Node
	}
	
	channels {
		Node.transformNextworkTopology over Environment.primeNode
	}
	
	collaboration Networkcascading {
		
		dynamic role Environment env
		dynamic role Node primeNode
		dynamic role Node nextNode
		dynamic role Container c
		dynamic multi role Node node
		
		guarantee scenario NetworkTransformation bindings [
			c = primeNode.container
			node = c.node
		]{
			env->primeNode.transformNextworkTopology()
			strict urgent primeNode->node.setNextNode(c.node.any())
		}

		guarantee scenario SetPreviousNode {
			primeNode->node.setNextNode(bind nextNode)
			strict committed node->nextNode.setPreviousNode(node)
		}

		guarantee scenario CheckNodeHasNonNullNextPointer {
			env->node.checkNodePointer()

//			// good example for an (easily for Incremental GR(1) solvable) realizable specification
//			violation [false]

			// good example for an (easily for Incremental GR(1) solvable) unrealizable specification
//			violation [true]

			// good example to create a realizable specification where the incremental GR(1)-Alg. needs many iterations and switches of ENV<->SYS
			violation [node.previousNode == null]
		}
		
		
	}
	
}