import "../model/networkcascading.ecore"
specification networkcascading{ 

	domain networkcascading
	
	controllable {
		Node
	}
	
	collaboration Networkcascading {
		
		dynamic role Environment e
		dynamic role Node n1
		dynamic role Container c
		dynamic multi role Node n2
		
		guarantee scenario Cascade1 bindings [
			c = n1.container
			n2 = c.node
		]{
			e->n1.e()
			strict urgent n1->n2.m()
//			strict urgent n2->e.r()
		}

	}
	
}