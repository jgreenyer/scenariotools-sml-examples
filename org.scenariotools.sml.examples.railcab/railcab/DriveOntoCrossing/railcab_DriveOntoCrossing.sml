import "railcab_DriveOntoCrossing.ecore"

specification DriveOntoCrossing {
	
	domain DriveOntoCrossing
	
	controllable {
		RailCab
		TrackSectionControl
		CrossingControl
	}
		
	
	
	collaboration DriveOntoCrossing {
		
		static role Environment env
		static role RailCab rc
		static role TrackSectionControl tsc1
		static role TrackSectionControl tsc2
		dynamic role TrackSectionControl next
		static role CrossingControl crc
		dynamic role Barriers b
		
		guarantee scenario RequestEnterAtEndOfTrackSection 
			bindings [next = tsc1.next]// bind next to rc.getCurrent.getNext
		{
			env -> rc.endOfTS()
			strict urgent rc -> next.requestEnter()
			var EBoolean allowed
			strict urgent next -> rc.enterAllowed(bind allowed)//(allowed: Boolean)
			env -> rc.lastBrake() 
		}
		
		guarantee scenario EnterAllowedDefault {
			rc -> next.requestEnter()
			next -> rc.enterAllowed(true)//(allowed: Boolean)
		}
		
		//OrderedRailCabNotifications EnvironmentAssumptionStateMachine
		
		assumption scenario BreakLeadsToStop {
			rc -> env.brake()
			strict eventually env -> rc.stop()
		}constraints[
			interrupt rc -> env.acc()
		]
		
		assumption scenario AccLeadsToGo {
			rc -> env.acc()
			strict eventually env -> rc.go()
		}constraints[
			interrupt rc -> env.brake()
		]
		
		assumption scenario BreakeStopsInTime {
			rc -> env.brake()
			env -> rc.noReturn()
			strict eventually env -> rc.stop()
			env -> rc.enterNext()
		}constraints[
			interrupt rc -> env.acc()
		]
		
		assumption scenario NoStopWithoutBreake {
			env -> rc.endOfTS()
			env -> rc.stop()
			wait strict [false]
//			condition < false >
//			rc -> env.brake()
		} constraints [interrupt rc->env.brake()] 
		
		//StopBeforeObstacle StateMachine
		guarantee scenario StopBeforeObstacle {
			env -> rc.obstacle()
			//loop{
			//	env -> rc.obstacle()
			//}
			strict eventually rc -> env.brake()
			rc -> env.acc()
		}
		
		guarantee scenario CloseBarriers 
			bindings [b = crc.barriers]
		{
			rc -> crc.requestEnter()
			strict urgent crc -> b.close()
			alternative{
				strict b -> crc.barriersClosed()
				urgent crc -> rc.enterAllowed(true)//(allowed: Boolean)
			}or{
				strict b -> crc.barriersBlocked()
				strict urgent crc -> rc.enterAllowed(false)//(allowed: Boolean)
			}
		}	
		// TODO check this
		assumption scenario  TimelyBarriersStatus 
			bindings [//bind crc to rc.getCurrent.next,	
									 b = crc.barriers]
		{ 	
			env -> rc.endOfTS()
			rc -> crc.requestEnter()
			crc -> b.close()
			alternative {
				strict eventually b -> crc.barriersClosed()
			}or{
				strict eventually b -> crc.barriersBlocked()
			}
			env -> rc.lastBrake()
		}
	}
}