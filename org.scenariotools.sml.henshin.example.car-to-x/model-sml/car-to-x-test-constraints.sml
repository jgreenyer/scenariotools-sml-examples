import "car-to-x.ecore"

specification CarToX {

	domain cartox
	
	controllable {
		Car
		StreetSectionControl
		ObstacleBlockingOneLaneControl
	}

	non-spontaneous events {
		Car.setApproachingObstacle
		Car.entersNarrowPassage
		Car.hasLeftNarrowPassage
	}

	collaboration CarApproachingObstacleAssumptions {

		dynamic role Environment env
		dynamic role Car car
		dynamic role LaneArea currentArea
		dynamic role LaneArea nextArea
		dynamic role LaneArea nextToNextArea
		dynamic role Obstacle obstacle

		/*
		 * When a car approaches an obstacle on the blocked lane, an according event will occur
		 */

		assumption scenario ApproachingObstacleOnBlockedLaneAssumption_OppositeDirection bindings [
			currentArea = car.inArea
			nextArea = currentArea.next
			nextToNextArea = nextArea.nextTo
			obstacle = nextToNextArea.obstacle
		] {
			env -> car.carMovesToNextArea()
			{
				env -> car.carMovesToNextArea()
			} constraints [
				forbidden env -> car.carMovesToNextAreaOnOvertakingLane()
				interrupt env -> car.carMovesToNextArea()
				ignore env -> car.carMovesToNextAreaOnOvertakingLane()
				consider env -> car.carMovesToNextAreaOnOvertakingLane()
				forbidden env -> car.carMovesToNextAreaOnOvertakingLane()
				interrupt env -> car.carMovesToNextArea()
				ignore env -> car.carMovesToNextAreaOnOvertakingLane()
				consider env -> car.carMovesToNextAreaOnOvertakingLane()
			]
			interrupt [ obstacle == null ]
			strict eventually env -> car.setApproachingObstacle(obstacle)
		} constraints [
			forbidden env -> car.carMovesToNextAreaOnOvertakingLane()
			interrupt env -> car.carMovesToNextArea()
			ignore env -> car.carMovesToNextAreaOnOvertakingLane()
			consider env -> car.carMovesToNextAreaOnOvertakingLane()
			forbidden env -> car.carMovesToNextAreaOnOvertakingLane()
			interrupt env -> car.carMovesToNextArea()
			ignore env -> car.carMovesToNextAreaOnOvertakingLane()
			consider env -> car.carMovesToNextAreaOnOvertakingLane()
		]

	}

}