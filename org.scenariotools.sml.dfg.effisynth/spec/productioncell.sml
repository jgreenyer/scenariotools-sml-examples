import "../model/productioncell.xcore"
import "productioncell_opt-goals.collaboration"
import "productioncell_costs.collaboration"

specification ProductioncellSpecification {

	domain productioncell

	controllable {
		Controller
	}
	
	non-spontaneous events {
		Controller.pickedUpItem
		Controller.releasedItem
		Controller.workDone
		Controller.accelerate
		Controller.movingAtConstantVelocity
		Controller.decelerate
		Controller.arrivedAt
	}

	collaboration FeedBeltBehavior {

		static role Controller controller
		static role ConveyorBelt feedBelt
		static role ConveyorBelt workBelt
		static role PickAndPlaceRobot feedRobot
		static role WorkerRobot workerRobot

		assumption scenario ArmPicksUpItemBeforeNewBlankArrives {
			feedBelt -> controller.blankArrived(feedBelt)
			wait strict [!feedBelt.hasItem] 				
		}

		guarantee scenario FeedRobotPicksUpNewBlank {
			feedBelt -> controller.blankArrived(feedBelt)
			wait eventually [feedRobot.location == feedBelt && !feedRobot.carriesItem]
			urgent controller -> feedRobot.pickUp()
		}
		
		guarantee scenario FeedRobotDeliversItem {
			feedRobot -> controller.pickedUpItem(feedRobot)
			urgent controller -> feedRobot.moveTo(workBelt)
			feedRobot -> controller.arrivedAt(feedRobot, workBelt)
			wait eventually [!workBelt.hasItem && workerRobot.location == workerRobot.idleLocation]
			urgent controller -> feedRobot.releaseItem()
			feedRobot -> controller.releasedItem(feedRobot)
			urgent controller -> feedRobot.moveTo(feedBelt)			
		}
	}
	
	collaboration WorkBeltBehavior {
		
		static role Controller controller
		static role PickAndPlaceRobot feedRobot
		static role WorkerRobot workerRobot
		static role PickAndPlaceRobot depositRobot

		guarantee scenario RobotStartsWorking {
			feedRobot -> controller.releasedItem(feedRobot)
			violation [workerRobot.location != workerRobot.idleLocation]
			urgent controller -> workerRobot.performWork()
		}
		
		guarantee scenario WorkerRobotMovesBackToIdleLocation {
			workerRobot -> controller.workDone()
			eventually controller -> workerRobot.moveTo(workerRobot.idleLocation)
		}
		
		guarantee scenario FinishedItemGetsPickedUp {
			workerRobot -> controller.workDone()
			wait eventually [depositRobot.location == workerRobot.belt && !depositRobot.carriesItem]
			urgent controller -> depositRobot.pickUp()
		}
	}
	
	collaboration DepositBeltBehavior {
		
		static role Controller controller
		static role PickAndPlaceRobot depositRobot
		static role Location workBelt

		guarantee scenario DepositArmDeliversPressedItem {
			depositRobot -> controller.pickedUpItem(depositRobot)
			eventually controller -> depositRobot.moveTo(depositRobot.belt)
			depositRobot -> controller.arrivedAt(depositRobot, depositRobot.belt)
			urgent controller -> depositRobot.releaseItem()
			depositRobot -> controller.releasedItem(depositRobot)
			eventually controller -> depositRobot.moveTo(workBelt)
		}		
	}

	collaboration RobotArmMovement {

		dynamic role Controller controller
		dynamic role RobotArm robot
		dynamic role Location targetLocation
		
		assumption scenario ArmMovesToLocation {
			controller -> robot.moveTo(bind targetLocation)
			committed robot -> controller.accelerate()
			eventually robot -> controller.movingAtConstantVelocity()
			eventually robot -> controller.decelerate()
			eventually robot -> controller.arrivedAt(robot, targetLocation)
		}
	}
	
	collaboration PickAndPlaceRobotBehavior {
		
		dynamic role Controller controller
		dynamic role PickAndPlaceRobot robot
		dynamic role Location location
		
		assumption scenario ArmPicksUpItem
		bindings [
			location = robot.location
		] {
			controller -> robot.pickUp()
			eventually robot -> controller.pickedUpItem(robot)
		}
		
		assumption scenario ArmReleasesItem
		bindings [
			location = robot.location
		] {
			controller -> robot.releaseItem()
			eventually robot -> controller.releasedItem(robot)
		}
	}
	
	collaboration WorkRobotBehavior {
		dynamic role Controller controller
		dynamic role WorkerRobot robot
		
		assumption scenario RobotPerformsWork { // very high level of abstraction (intended)
			controller -> robot.performWork()
			eventually robot -> controller.workDone()
		}
	}
	
	include collaboration OptimizationGoals
	include collaboration Costs
}
