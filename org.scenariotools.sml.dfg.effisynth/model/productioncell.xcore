package productioncell

abstract class NamedElement {
	String[1] name
}

class ProductionCellContainer extends NamedElement {
	contains Controller[1] controller
	contains ConveyorBelt[3] conveyorBelts
}

class Controller extends NamedElement {
	op void blankArrived(Location sender) {
		sender.hasItem = true
	}

	op void pickedUpItem(PickAndPlaceRobot sender) {
		sender.carriesItem = true
		sender.location.hasItem = false
	}
	
	op void releasedItem(PickAndPlaceRobot sender) {
		sender.carriesItem = false
		sender.location.hasItem = true
	}
	
	op void workDone()
	op void accelerate()
	op void movingAtConstantVelocity()
	op void decelerate()
	op void arrivedAt(RobotArm sender, Location location) {
		sender.location = location
	}
}

class RobotArm extends NamedElement {
	refers ConveyorBelt belt opposite arm
	refers Location[1] location
	
	op void moveTo(Location targetLocation)	
}

class PickAndPlaceRobot extends RobotArm {
	boolean[1] carriesItem = "false"
	
	op void pickUp()
	op void releaseItem()
}

class WorkerRobot extends RobotArm {
	contains Location idleLocation
	
	op void performWork() {
		location = belt
	}
}

class Location extends NamedElement {
	boolean[1] hasItem = "false"
}

class ConveyorBelt extends Location {
	contains RobotArm arm opposite belt
}

